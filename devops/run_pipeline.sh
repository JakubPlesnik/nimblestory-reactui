#!/bin/sh

echo "run_pipeline.sh: Begin..."
echo $HOSTNAME

#Parameters received
pipeline_method=$1
pipeline_target=$2

#Local Variables
en=""
en_host=""
en_port=""
en_user=""
en_homepath=""
en_projfolder=""
en_dockertype=""
en_dockername=""

#Functions
CheckDeployEnvironment(){
    echo "run_pipeline.sh: CheckDeployEnvironment() Executing"
    case $pipeline_target in
        sde-devel)
            en="sde-devel"
            en_host="pimcore-demo.sde.cz"
            en_port="2020"
            en_user="user"
            en_homepath="/home/user/"
            en_projfolder="frontend"
            en_dockertype="prod"
            en_dockername="docker_node_1"
            en_apiurl="http://pimcore-demo.sde.cz/"
            ;;
        aws-test)
            en="aws-test"
            en_host="3.140.222.25"
            en_port="22"
            en_user="ns-deploy"
            en_homepath="/home/ns-deploy/"
            en_projfolder="nslive-frontend"
            en_dockertype="prod"
            en_dockername="frontend-react"
            en_apiurl="https://test-content.nimblestory.com/"
            ;;
        aws-stage)
            en="aws-stage"
            en_host="18.253.86.126"
            en_port="22"
            en_user="ns-deploy"
            en_homepath="/home/ns-deploy/"
            en_projfolder="nslive-frontend"
            en_dockertype="prod"
            en_dockername="frontend-react"
            en_apiurl="https://stage-content.nimblestory.com/"
            ;;
        aws-prod)
            en="aws-prod"
            en_host="18.253.205.188"
            en_port="22"
            en_user="ns-deploy"
            en_homepath="/home/ns-deploy/"
            en_projfolder="nslive-frontend"
            en_dockertype="prod"
            en_dockername="frontend-react"
            en_apiurl="https://content.nimblestory.com/"
            ;;
        *)
            echo "No known install environment provided. Only local tests available!"
            ;;
    esac

    echo "---------------------------------------------------------------------------------------------------------------------------------------------------"
    echo "Type is ${pipeline_method}"
    echo "Target is ${pipeline_target}."
    echo "          Using en: ${en}, en_host: ${en_host}, en_port: ${en_port}, en_user: ${en_user}, en_homepath: ${en_homepath}"
    echo "                en_projfolder: ${en_projfolder}, en_dockertype: ${en_dockertype}, en_dockername: ${en_dockername}, en_apiurl: ${en_apiurl}"
    echo "---------------------------------------------------------------------------------------------------------------------------------------------------"
}


SendPayloadToRemote(){
    #Turn echo on for debugging
    set -x
    echo "run_pipeline.sh: SendPayloadToRemote() Executing"
    echo "run_pipeline.sh: Pipeline Server IP Address"
    curl ifconfig.me
    echo "\n"
    sleep 5
    echo "run_pipeline.sh: Tar the files"
    cd ..
    tar -cvzf payload-frontend.tar.gz build --warning=no-file-changed
    echo "run_pipeline.sh: Transfer payload to host with scp"
    scp -v -P $en_port -i ~/.ssh/id_rsa -rp payload-frontend.tar.gz ${en_user}@${en_host}:${en_homepath}
    echo "run_pipeline.sh: Connect to host with ssh and execute deploy.sh locally"
    ssh -v -p $en_port -i ~/.ssh/id_rsa $en_user@$en_host "rm -rf build-frontend ; mkdir -p build-frontend ; tar -zxvf payload-frontend.tar.gz --directory ./build-frontend ; sudo cp -a build-frontend/build/. ${en_projfolder} ; cd ${en_projfolder}/docker ; ./deploy.sh ${en_dockername} $en_apiurl"
}

# MAIN FLOW IS HERE...
echo "run_pipeline.sh: Main Flow Executing"
CheckDeployEnvironment
case $pipeline_method in
    deploy)
        echo "run_pipeline.sh: Method =deploy Executing"
        SendPayloadToRemote
    ;;
    debug)
        #Turn echo on for debugging
        set -x
        echo "run_pipeline.sh: Method =debug Executing, for testing only"
        SendPayloadToRemote
        echo "debug will cat id_rsa, config, known_hosts and output current ip of pipeline server"
        echo cat id_rsa and config and known_hosts
        ls -al ~/.ssh
        cat ~/.ssh/id_rsa
        cat ~/.ssh/config
        cat ~/.ssh/known_hosts
        curl ifconfig.me

    ;;
    *)
        echo "run_pipeline.sh: Method =$pipeline_method not found, doing nothing"
    ;;
esac

echo "run_pipeline.sh: ...End"