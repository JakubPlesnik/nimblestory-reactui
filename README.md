# Nimblestory React UI #

NimbleStory is a software platform that is centered around delivering Visual Insights. This nimble, flexible and expandable system enables quick integration through the use of modules. Modules provide powerful building blocks that can be customized to create a singular visual solution

## Technology stack
* Node.js
* React / Next.js

## Requirements
* Node.js  
* npm  



## Setup
1. Install Node.js and npm https://www.npmjs.com/get-npm
2. Start with cloning the repository
3. Run following commands for desired platform
4. Check 

### Setup on local machine 
```
cd src
npm install
npm run dev
```
open localhost:9001

### Setup with Docker
```
cd docker
docker-compose up -d
docker exec docker_node_1 npm install
docker exec docker_node_1 npm run dev
```