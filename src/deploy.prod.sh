#!/bin/bash

# in source folder clone the repo
# git clone https://github.com/maga-interactive/nimblestory-react-frontend.git .
# then run this script


appDeployPath=/opt/bitnami/apps/nimblefrontend
timestamp=$(date +"%y%m%d-%s")
appenvironment=$1

if [ $# == 0 ]; then
    echo "Example usage:"
    echo "bash ./deploy.sh staging"
    echo "bash ./deploy.sh production"
    exit 1
elif [ "$1" == "staging" ]; then
    uid=nfstaging
    port=9001
elif [ "$1" == "production" ]; then
    uid=nfproduction
    port=9002
else
    echo "Argument should be: staging or production"
    exit
fi

resultpath=${appDeployPath}/${timestamp}_${appenvironment}
echo "Update latest files and copy app ($uid:$port) to $resultpath"
git pull
cp -R . $resultpath
cd $resultpath
echo "I'm now in $PWD"

appbuildcount=$(git rev-list --count master)
if [ "$appbuildcount" -lt 100 ];then
    appbuildcount="0$appbuildcount"
fi
appdeploydate=$(date +"%Y%m%d")
appdeployid="$appdeploydate$appbuildcount"
echo "App deployment id: $appdeployid"

yarn install 
yarn build 
forever stop $uid
env NODE_ENV=production NIMBLEFRONTENDPORT=$port APPBUILDID=$appdeployid forever start --uid "$uid" --append app.js
echo "App is running on port $port"