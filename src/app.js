const express = require('express');
// const logger = require('morgan');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const next = require('./next');

const app = express();
// Put in place textbook middlewares for express.
if (process.env.NODE_ENV !== 'production') {
    // app.use(logger('dev'));
}
const severPort = process.env.NIMBLEFRONTENDPORT || 9001;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/', express.static(path.join(__dirname, 'public')));

const kNSApps = {
    Portfolio: 'portfolio',
    Geoexplorer: 'geoexplorer',
    Conceptexplorer: 'conceptexplorer',
    Manage: 'manage',
    Home: 'home',
    Collection: 'collection',
};

const start = async port => {
    // Couple Next.js with our express server.
    // app and handle from "next" will now be available as req.app and req.handle.
    await next(app);

    // Normal routing, if you need it.
    // Use your SSR logic here.
    // Even if you don't do explicit routing the pages inside app/pages
    // will still get rendered as per their normal route.

    app.get('/login', (req, res) =>
        req.app.render(req, res, '/login', {
            application: 'login',
        })
    );

    app.get('/resetpassword', (req, res) =>
        req.app.render(req, res, '/resetpassword', {
            application: 'resetpassword',
            journey: req.query.token,
        })
    );
    app.get('/changepassword', (req, res) =>
        req.app.render(req, res, '/changepassword', {
            application: 'changepassword',
        })
    );

    /*
    Concept Explorer
    */
    app.get(
        '/conceptexplorer/:organization/:project/journey/:journey',
        (req, res) =>
            req.app.render(req, res, '/conceptsolution', {
                token: req.query.token,
                application: kNSApps.Conceptexplorer,
                organization: req.params.organization,
                project: req.params.project,
                journey: req.params.journey,
            })
    );

    app.get(
        '/conceptexplorer-embed/:organization/:project/journey/:journey',
        (req, res) =>
            req.app.render(req, res, '/conceptsolution-embed', {
                token: req.query.token,
                application: kNSApps.Conceptexplorer,
                organization: req.params.organization,
                project: req.params.project,
                journey: req.params.journey,
                embed: 1,
            })
    );

    app.get(
        '/conceptexplorer/:organization/:project/visual/:visual',
        (req, res) =>
            req.app.render(req, res, '/conceptsolution', {
                token: req.query.token,
                application: kNSApps.Conceptexplorer,
                organization: req.params.organization,
                project: req.params.project,
                visual: req.params.visual,
            })
    );

    app.get(
        '/conceptexplorer-embed/:organization/:project/visual/:visual',
        (req, res) =>
            req.app.render(req, res, '/conceptsolution-embed', {
                token: req.query.token,
                application: kNSApps.Conceptexplorer,
                organization: req.params.organization,
                project: req.params.project,
                visual: req.params.visual,
                embed: 1,
            })
    );

    app.get('/conceptexplorer-embed/:organization/:project', (req, res) =>
        req.app.render(req, res, '/conceptsolution-embed', {
            token: req.query.token,
            application: kNSApps.Conceptexplorer,
            organization: req.params.organization,
            project: req.params.project,
            embed: 1,
        })
    );

    app.get('/conceptexplorer/:organization/:project', (req, res) =>
        req.app.render(req, res, '/conceptsolution', {
            token: req.query.token,
            application: kNSApps.Conceptexplorer,
            organization: req.params.organization,
            project: req.params.project,
        })
    );

    app.get('/conceptexplorer/:organization', (req, res) =>
        req.app.render(req, res, '/conceptsolutions', {
            token: req.query.token,
            application: kNSApps.Conceptexplorer,
            organization: req.params.organization,
        })
    );
    /*
        Geographic Explorer
      */

    app.get('/geoexplorer', (req, res) =>
        req.app.render(req, res, '/geoexplorersolutions', {
            token: req.query.token,
            application: kNSApps.Geoexplorer,
        })
    );

    app.get('/geoexplorer/:organization', (req, res) =>
        req.app.render(req, res, '/geoexplorersolutions', {
            token: req.query.token,
            application: kNSApps.Geoexplorer,
            organization: req.params.organization,
        })
    );

    app.get('/geoexplorer/:organization/:project', (req, res) =>
        req.app.render(req, res, '/geoexplorersolution', {
            token: req.query.token,
            application: kNSApps.Geoexplorer,
            organization: req.params.organization,
            project: req.params.project,
        })
    );

    //Collections
    app.get('/portfolio/:organization/collections', (req, res) =>
        req.app.render(req, res, '/collections', {
            token: req.query.token,
            application: kNSApps.Collection,
            organization: req.params.organization,
            projectfilter: null,
        })
    );

    /*
    Collection detail in collection
    */
    app.get('/portfolio/:organization/collection/:collection', (req, res) =>
        req.app.render(req, res, '/collection', {
            token: req.query.token,
            application: kNSApps.Collection,
            organization: req.params.organization,
            collection: req.params.collection,
        })
    );

    /*
    Projects in Organization
    */

    app.get('/portfolio/:organization/all', (req, res) =>
        req.app.render(req, res, '/projects', {
            token: req.query.token,
            application: kNSApps.Portfolio,
            organization: req.params.organization,
            projectfilter: null,
        })
    );

    app.get('/portfolio/:organization/active', (req, res) =>
        req.app.render(req, res, '/projects', {
            token: req.query.token,
            organization: req.params.organization,
            application: kNSApps.Portfolio,
            projectfilter: 'active',
        })
    );

    app.get('/portfolio/:organization/inactive', (req, res) =>
        req.app.render(req, res, '/projects', {
            token: req.query.token,
            organization: req.params.organization,
            application: kNSApps.Portfolio,
            projectfilter: 'inactive',
        })
    );

    app.get('/portfolio/:organization', (req, res) =>
        req.app.render(req, res, '/home', {
            token: req.query.token,
            application: kNSApps.Home,
            organization: req.params.organization,
            projectfilter: null,
        })
    );

    app.get('/portfolio', (req, res) =>
        req.app.render(req, res, '/home', {
            token: req.query.token,
            application: kNSApps.Home,
            organization: null,
            project: null,
        })
    );

    /*
    Assets in all projects
    */
    app.get('/portfolio/:organization/assets', (req, res) =>
        req.app.render(req, res, '/project', {
            token: req.query.token,
            application: kNSApps.Portfolio,
            organization: req.params.organization,
            project: 'assets',
            projectfilter: null,
        })
    );

    app.get('/portfolio/:organization/assets/active', (req, res) =>
        req.app.render(req, res, '/project', {
            token: req.query.token,
            application: kNSApps.Portfolio,
            organization: req.params.organization,
            project: 'assets',
            projectfilter: 'active',
        })
    );

    app.get('/portfolio/:organization/assets/inactive', (req, res) =>
        req.app.render(req, res, '/project', {
            token: req.query.token,
            application: kNSApps.Portfolio,
            organization: req.params.organization,
            project: 'assets',
            projectfilter: 'inactive',
        })
    );

    //Manage
    app.get('/portfolio/:organization/manage', (req, res) =>
        req.app.render(req, res, '/manager', {
            token: req.query.token,
            application: kNSApps.Manage,
            organization: req.params.organization,
            project: 'assets',
            projectfilter: null,
        })
    );

    app.get('/geoexplorer/:organization/manage', (req, res) =>
        req.app.render(req, res, '/manager', {
            token: req.query.token,
            application: kNSApps.Geoexplorer,
            organization: req.params.organization,
            project: 'assets',
            projectfilter: null,
        })
    );

    /*
    Assets in project
    */
    app.get('/portfolio/:organization/:project', (req, res) =>
        req.app.render(req, res, '/project', {
            token: req.query.token,
            organization: req.params.organization,
            project: req.params.project,
        })
    );

    /*
    Selected asset
    */
    app.get('/portfolio/:organization/:project/:asset', (req, res) =>
        req.app.render(req, res, '/asset', {
            token: req.query.token,
            organization: req.params.organization,
            project: req.params.project,
            asset: req.params.asset,
        })
    );

    app.get('/', (req, res) => req.app.render(req, res, '/organization', {}));

    app.get('/organization', (req, res) =>
        req.app.render(req, res, '/organization', {
            token: req.query.token,
            // application: kNSApps.Home,
            // organization: null,
            // project: null,
        })
    );

    app.get('/healthcheck', (req, res) =>
        req.app.render(req, res, '/healthcheck', {
            application: 'healthcheck',
        })
    );

    app.listen(port);
};

// Start the express server.
start(severPort);
