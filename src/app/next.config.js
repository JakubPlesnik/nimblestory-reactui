const path = require('path');
const glob = require('glob');

const nextConfig = {
    publicRuntimeConfig: {
        APPBUILDIDX: process.env.APPBUILDID,
        ENV: process.env.NODE_ENV,
        API: process.env.API,
    },

    useFileSystemPublicRoutes: false,
    webpack: config => {
        config.module.rules.push(
            {
                test: /\.(css|scss)/,
                loader: 'emit-file-loader',
                options: {
                    name: 'dist/[path][name].[ext]',
                },
            },
            {
                test: /\.css$/,
                use: ['babel-loader', 'raw-loader', 'postcss-loader'],
            },
            {
                test: /\.s(a|c)ss$/,
                use: [
                    'babel-loader',
                    'raw-loader',
                    'postcss-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: ['styles', 'node_modules']
                                .map(d => path.join(__dirname, d))
                                .map(g => glob.sync(g))
                                .reduce((a, c) => a.concat(c), []),
                        },
                    },
                ],
            }
        );

        const originalEntry = config.entry;

        config.entry = async () => {
            const entries = await originalEntry();

            if (
                entries['main.js'] &&
                !entries['main.js'].includes('./client/polyfills.js')
            ) {
                entries['main.js'].unshift('./client/polyfills.js');
            }

            return entries;
        };

        return config;
    },
    exportPathMap() {
        return {
            '/': { page: '/' },
        };
    },
    distDir: 'build',
};

if (process.env.ANALYZE === 'true') {
    const withBundleAnalyzer = require('@next/bundle-analyzer')({
        enabled: true,
    });
    module.exports = withBundleAnalyzer(nextConfig);
} else if (process.env.SOURCEMAPS === 'true') {
    const withSourceMaps = require('@zeit/next-source-maps')();
    module.exports = withSourceMaps(nextConfig);
} else {
    module.exports = nextConfig;
}
