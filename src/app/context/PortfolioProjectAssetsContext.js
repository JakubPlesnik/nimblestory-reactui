import { createContext } from 'react';

const PortfolioProjectAssets = createContext();

export default PortfolioProjectAssets;
