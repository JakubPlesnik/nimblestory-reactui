import { createContext } from 'react';

const GeoExplorerContext = createContext();

export default GeoExplorerContext;
