import { createContext } from 'react';

const ConceptExplorerContext = createContext();

export default ConceptExplorerContext;
