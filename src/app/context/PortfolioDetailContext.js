import { createContext } from 'react';

const PortfolioDetailContext = createContext();

export default PortfolioDetailContext;