const pctAsset = 'pecontentdefault';
const pctLink = 'pecontentweblink';
const pctWall = 'pecontentdigitalwall';
const pctVideo = 'pecontentvidlink';
const pctModuleFile = 'pecontentwebbundle';
const project = 'project';
const geoexplorer = 'geographicexplorer';
const conceptexplorer = 'conceptexplorer';
const folder = 'folder';
const pin = 'pushpin';
const collection = 'collection';

export const ICONS = {
    pctAsset,
    pctLink,
    pctWall,
    pctVideo,
    pctModuleFile,
    project,
    geoexplorer,
    conceptexplorer,
    folder,
    pin,
    collection,
};
