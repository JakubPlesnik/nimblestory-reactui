const Logout = 'logout';
const Messages = 'messages';
const Changeorg = 'changeorg';
const Reporterror = 'reporterror';
const ListProjects = 'listprojects';
const ListCollections = 'listcollections';
const AccountInfo = 'accountinfo';

export const NavigationSystemActions = {
    Logout,
    Messages,
    Changeorg,
    Reporterror,
    ListProjects,
    ListCollections,
    AccountInfo,
};

export default NavigationSystemActions;
