const Route = 'route';
const Content = 'content';
const SystemAction = 'system_action';

const NavigationTargetTypes = {
    Route,
    Content,
    SystemAction,
};

export default NavigationTargetTypes;
