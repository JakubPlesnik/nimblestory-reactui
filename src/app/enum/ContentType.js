const PCTAsset = 'pctAsset';
const PCTLink = 'pctLink';
const PCTWall = 'pctWall';
const PCTVideo = 'pctVideo';
const PCTModuleFile = 'pctModuleFile';
const PortfolioContent = 'PortfolioContent';
const CE = 'CE';
const GE = 'GE';
const Project = 'projects';
const Folder = 'folder';
const Collection = 'collection';
const NONE = '';

export const ContentTypes = {
    PCTAsset,
    PCTLink,
    PCTWall,
    PCTVideo,
    PCTModuleFile,
    PortfolioContent,
    CE,
    GE,
    Project,
    Folder,
    Collection,
    NONE,
};

export default ContentTypes;
