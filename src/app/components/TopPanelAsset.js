import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import Link from 'next/link';
import AppLinkData from '../services/applink';
import { getFileExtension } from '../services/textService';
import { Breadcrumb } from 'antd';
import { fetchAsset } from '../services/api/apiService';
import DownloadButton from './portfolio/DownloadButton';
import { Row, Col } from 'antd';
import { Button } from 'antd';

const TopPanel = ({ pathinfo, project, asset, setIsSliderVisible }) => {
    const [visible, setVisible] = useState(false);

    const [buttonText, setButtonText] = useState('View Details');

    const showDrawer = () => {
        setIsSliderVisible(visible);
        setVisible(!visible);
    };

    const backToHomeLink = AppLinkData({
        homeLink: pathinfo,
    });
    const backToProjLink = AppLinkData({
        organization: pathinfo.organization,
        project: pathinfo.project,
    });
    const backToPortfolioLink = AppLinkData({
        portfolioAllLink: pathinfo,
    });

    const [imageUrl, setImageUrl] = useState('');
    const [fileName, setFileName] = useState('');

    useEffect(() => {
        if (imageUrl == '') return;
        const linkSource = `${imageUrl}`;
        const downloadLink = document.createElement('a');
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        setImageUrl('');
    }, [imageUrl]);

    useEffect(() => {
        if (!visible) setButtonText('View Details');
        else setButtonText('Hide Details');
    }, [visible]);

    const downloadFile = (id, fileName, extension) => {
        if (extension) {
            setFileName(fileName + extension);
        } else {
            setFileName(fileName);
        }

        fetchAsset(id, setImageUrl, null);
    };

    const downloadTypes = [{ title: `Preview File`, pathkey: 'FileLgPrev' }];

    const downloadPaths = downloadTypes
        .map(type =>
            asset[type.pathkey]
                ? {
                      title: type.title,
                      path: asset[type.pathkey],
                      extension: '.' + getFileExtension(asset.AssetFileName),
                      downloadName: `${asset.slug} - ${type.title}`,
                  }
                : null
        )
        .filter(n => n);

    asset.addtlfiles &&
        asset.addtlfiles.forEach(item => {
            downloadPaths.push({
                title: item.label,
                path: item.file,
                extension: item.extension,
                downloadName: `${asset.slug} - ${item.label}`,
            });
        });

    const hasDownload = downloadPaths.length > 0 ? true : false;

    return (
        <div className="topPanel">
            <Row>
                <Col span={12}>
                    <div className="title" style={{ cursor: 'inherit' }}>
                        <h1 data-cy="asset-title">{asset.title} </h1>
                    </div>

                    <div className="secondaryTitleWrapper">
                        <div className="secondaryTitle">
                            <Breadcrumb>
                                <Breadcrumb.Item>
                                    {' '}
                                    <a href={`${backToHomeLink.as}`}>Home</a>
                                </Breadcrumb.Item>
                                <Breadcrumb.Item>
                                    <a href={`${backToPortfolioLink.as}`}>
                                        Projects
                                    </a>
                                </Breadcrumb.Item>
                                <Breadcrumb.Item>
                                    <Link {...backToProjLink}>
                                        <a>{project.title}</a>
                                    </Link>
                                </Breadcrumb.Item>
                                <Breadcrumb.Item>
                                    {' '}
                                    <a>{asset.title}</a>
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </div>
                    </div>
                </Col>
                <Col span={12}>
                    {hasDownload && (
                        <div className="btnTopRight">
                            <DownloadButton
                                downloadPaths={downloadPaths}
                                downloadFile={downloadFile}
                            ></DownloadButton>
                        </div>
                    )}

                    <Button
                        type="primary"
                        ghost
                        className={'detailButton'}
                        size="small"
                        onClick={showDrawer}
                        style={{
                            position: 'absolute',
                            top: '97px',
                            right: '72px',
                        }}
                        data-cy="asset-detail-button"
                    >
                        {buttonText}
                    </Button>
                </Col>
            </Row>

            <style jsx>
                {`
                    .title {
                        padding-top: 24px;
                        padding-left: 30px;
                        padding-bottom: 24px;
                    }

                    .topPanel {
                        position: relative;
                    }

                    .btnTopRight {
                        position: absolute;
                        top: 36px;
                        right: 72px;
                    }

                    .projThumbnail {
                        width: 208px;
                        background-size: cover;
                        background-position: center;
                        background-repeat: no-repeat;
                        margin-bottom: 24px;
                    }
                    .topPanelSubtitle {
                        color: #0d3b5f;
                        font-weight: bold;
                        text-transform: uppercase;
                        margin-top: 4px;
                        margin-bottom: 4px;
                        letter-spacing: 2px;
                    }

                    .toolbar-row {
                        display: flex;
                        flex-direction: row;
                        align-items: flex-end;
                        justify-content: space-between;
                    }
                    .portfolioHeader {
                        padding-top: 24px;
                        padding-left: 24px;
                        padding-bottom: 24px;
                    }
                    .secondaryTitle {
                        padding-top: 13px;
                        padding-left: 18px;
                        text-align: left;
                        padding-bottom: 16px;
                    }
                    .primaryTitle {
                        font-weight: 500;
                        font-size: 24px;
                        line-height: 28px;
                        padding: 9px 0px;
                    }

                    .projectInfo {
                        margin-top: 50px;
                    }
                `}
            </style>
        </div>
    );
};
TopPanel.propTypes = {
    pathinfo: PropTypes.shape({
        organization: PropTypes.string,
        project: PropTypes.string,
    }).isRequired,
    asset: PropTypes.shape({
        'File1(SmPrev)': PropTypes.number,
        'File2(LgPrev)': PropTypes.number,
        'File4(Source)': PropTypes.number,
    }).isRequired,
    project: PropTypes.shape({
        ProjYear: PropTypes.number,
        description: PropTypes.string,
        title: PropTypes.string,
    }).isRequired,
};
export default TopPanel;
