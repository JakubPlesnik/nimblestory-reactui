import { Fragment, useContext } from 'react';
import PortfolioContext from '../context/PortfolioContext';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';
import GeoExplorerContext from '../context/GeoExplorerContext';
import HomepageHeader from './HomepageHeader';
import ConceptExplorerContext from '../context/ConceptExplorerContext';

import ProjectPortfolio from '../components/cards/AppCardProject';
import AssetPortfolio from '../components/cards/AppCardAsset';
import ProjectGeoExplorer from './GeoExplorerSolutionCard';
import ProjectConceptExplorer from '../components/cards/AppCardConteptExplorer';
import AppCardHomepageviewFolder from '../components/cards/AppCardHomepageviewFolder';
import { assetSortOptions } from '../services/filteringAssets';
import { Space } from 'antd';

const pctAsset = 'pctAsset';
const pctLink = 'pctLink';
const pctWall = 'pctWall';
const pctVideo = 'pctVideo';
const pctModuleFile = 'pctModuleFile';
const contentfolder = 'contentFolder';
const projects = 'projects';
const geoexplorer = 'geoexplorer';
const conceptexplorer = 'CE';

const SECTIONS = {
    pctAsset,
    pctLink,
    pctWall,
    pctVideo,
    pctModuleFile,
    contentfolder,
    projects,
    geoexplorer,
    conceptexplorer,
};

const HomepageView = () => {
    const { organization, pinned } = useContext(LoginContext);
    const { projects: PortfolioProjects } = useContext(PortfolioContext);
    const { solutions: GeoexplorerProject } = useContext(GeoExplorerContext);
    const { solutions: ConceptexplorerProject } = useContext(
        ConceptExplorerContext
    );

    const { pathinfo } = useContext(UserContext);

    return (
        <div className="pagearea">
            <HomepageHeader
                titleText={
                    organization && organization.OrgHomeTitle
                        ? organization.OrgHomeTitle
                        : 'Organization Home'
                }
            />
            <div className="divider15" />

            {organization && (
                <Fragment>
                    <div className="assetsContainer">
                        <Space
                            size={[16, 35]}
                            wrap
                            style={{ marginBottom: '0px' }}
                        >
                            {/* Pinned */}
                            {pinned &&
                                pinned.length > 0 &&
                                pinned.map(item => {
                                    switch (item.section) {
                                        case SECTIONS.projects:
                                            return (
                                                <ProjectPortfolio
                                                    key={`${item.id}-pp`}
                                                    organization={
                                                        pathinfo.organization
                                                    }
                                                    project={item}
                                                />
                                            );
                                        case SECTIONS.conceptexplorer:
                                            return (
                                                <ProjectConceptExplorer
                                                    key={`${item.id}-ce`}
                                                    organization={
                                                        pathinfo.organization
                                                    }
                                                    project={item}
                                                />
                                            );
                                        case SECTIONS.geoexplorer:
                                            return (
                                                <ProjectGeoExplorer
                                                    key={`${item.id}-ge`}
                                                    organization={
                                                        pathinfo.organization
                                                    }
                                                    project={item}
                                                />
                                            );
                                        case SECTIONS.contentfolder:
                                            return (
                                                <AppCardHomepageviewFolder
                                                    key={`${item.id}-folder`}
                                                    folder={item}
                                                    organization={
                                                        pathinfo.organization
                                                    }
                                                />
                                            );
                                        case SECTIONS.pctAsset:
                                        case SECTIONS.pctLink:
                                        case SECTIONS.pctVideo:
                                        case SECTIONS.pctWall:
                                        case SECTIONS.pctModuleFile:
                                            return (
                                                <AssetPortfolio
                                                    key={`${item.id}-peasset`}
                                                    organization={
                                                        pathinfo.organization
                                                    }
                                                    asset={item}
                                                />
                                            );
                                    }
                                })}

                            {/* No Pinned */}
                            {pinned &&
                                pinned.length === 0 &&
                                pinned.findIndex(
                                    i => i.section == SECTIONS.projects
                                ) == -1 &&
                                PortfolioProjects.sort(
                                    assetSortOptions[2].sorter
                                ).map(item => (
                                    <ProjectPortfolio
                                        key={`${item.id}-projects`}
                                        organization={pathinfo.organization}
                                        project={item}
                                    />
                                ))}
                            {pinned &&
                                pinned.length === 0 &&
                                pinned.findIndex(
                                    i => i.section == SECTIONS.conceptexplorer
                                ) == -1 &&
                                ConceptexplorerProject.sort(
                                    assetSortOptions[2].sorter
                                ).map(item => (
                                    <ProjectConceptExplorer
                                        key={`${item.id}-ce`}
                                        organization={pathinfo.organization}
                                        project={item}
                                    />
                                ))}
                            {pinned &&
                                pinned.length === 0 &&
                                pinned.findIndex(
                                    i => i.section == SECTIONS.geoexplorer
                                ) == -1 &&
                                GeoexplorerProject.sort(
                                    assetSortOptions[2].sorter
                                ).map(item => (
                                    <ProjectGeoExplorer
                                        key={`${item.id}-ge`}
                                        organization={pathinfo.organization}
                                        project={item}
                                    />
                                ))}
                        </Space>
                    </div>
                </Fragment>
            )}

            <style jsx>
                {`
                    .assetsContainer {
                        margin-left: 16px;
                        display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                    }
                    .pageTitle {
                        margin-left: 31px;
                    }
                    .divider15 {
                        height: 14px;
                        clear: both;
                    }
                `}
            </style>
        </div>
    );
};
export default HomepageView;
