import { useState, useEffect } from 'react';
import useLocalStorage from '../../hooks/UseLocalStorage.js';
import { Drawer, Collapse, Badge } from 'antd';

const MessageSidebar = ({
    isOpen,
    setShowBadge,
    toggleHover,
    messagesList,
    setOpen,
    showBadge,
}) => {
    const { Panel } = Collapse;
    const [messages, setMessages] = useState(false);
    const [lastSeenMessage, setLastSeenMessage] = useLocalStorage(
        'message',
        null
    );

    useEffect(() => {
        setMessages(messagesList.messages);
    }, [messagesList]);

    const setAsRead = id => {
        let tmpArr = getArray();

        if (!tmpArr.includes(id)) {
            tmpArr.push(id);
        }
        if (messages.length == tmpArr.length) {
            if (showBadge != 0) {
                setShowBadge(0);
                toggleHover();
            }
        }
        setLastSeenMessage(JSON.stringify(tmpArr));
    };

    const getArray = () => {
        let tmpArr = JSON.parse(lastSeenMessage);

        if (!Array.isArray(tmpArr)) {
            tmpArr = [];
            if (lastSeenMessage) tmpArr.push(lastSeenMessage);
        }

        return tmpArr;
    };

    function onPanelChanged(key) {
        if (key) {
            setAsRead(key);
        }
    }

    function isMessageReaded(id) {
        let tmpArr = getArray();
        return tmpArr.includes(String(id));
    }

    function generateBadge(id) {
        if (!isMessageReaded(id)) {
            return <Badge count={1} dot></Badge>;
        }
    }

    return (
        <Drawer
            title="Your Notifications Center"
            placement="right"
            closable={true}
            onClose={() => setOpen(false)}
            visible={isOpen}
            style={{ padding: '0px' }}
            key="right"
            width="512"
        >
            <Collapse onChange={onPanelChanged} key={1} accordion>
                {messages &&
                    messages.map(item => (
                        <Panel
                            header={
                                <span
                                    style={
                                        !isMessageReaded(item.id)
                                            ? { fontWeight: '900' }
                                            : {}
                                    }
                                    dangerouslySetInnerHTML={{
                                        __html: item.title + ' - ' + item.date,
                                    }}
                                />
                            }
                            extra={generateBadge(item.id)}
                            key={item.id}
                        >
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: item.content,
                                }}
                            />
                        </Panel>
                    ))}
            </Collapse>
        </Drawer>
    );
};

export default MessageSidebar;
