import { useContext, useState, useEffect } from 'react';
import UserContext from '../context/UserContext';
import PortfolioDetailContext from '../context/PortfolioDetailContext';
import TopPanel from './TopPanelAsset';
import AssetVideo from './AssetVideo';
import AssetModule from './AssetModule';
import AssetLargeImage from './AssetLargeImage';
import AssetLargePDF from './AssetLargePDF';
import { Layout } from 'antd';
import Thumbnail from './image/Thumbnail';
import { getFileExtension } from '../services/textService';

const PortfolioAssetView = () => {
    const { pathinfo } = useContext(UserContext);
    const { project, asset } = useContext(PortfolioDetailContext);
    const { Content, Sider } = Layout;
    const [isSliderCollapsed, setVisible] = useState(true);
    const [displayBody, setDisplayBody] = useState('none');
    let assetId = false;
    let haveVideo = false;
    let haveModule = false;
    let havePDFViwable = false;
    let haveImgViwable = false;

    useEffect(() => {
        if (isSliderCollapsed) setDisplayBody('none');
        else setDisplayBody('');
    }, [isSliderCollapsed]);

    if (asset) {
        if (asset.entryType == 'pctAsset') {
            if (asset.AssetFileType == 'video') {
                haveVideo = true;
                assetId = asset.AssetId;
            } else if (asset.AssetFileType == 'link') {
                haveModule = true;
                assetId = asset.Link;
            } else if (
                asset.AssetFileType == 'document' &&
                asset['AssetFileName'] &&
                /\.pdf/i.test(asset['AssetFileName'])
            ) {
                havePDFViwable = true;
                assetId = asset['AssetId'];
            } else if (asset.AssetFileType == 'image') {
                haveImgViwable = true;
                assetId = asset['AssetId'];
            }
        } else if (asset.entryType == 'pctVideo') {
            haveModule = true;
            assetId = asset.Link;
        } else if (asset.entryType == 'pctLink') {
            haveModule = true;
            assetId = asset.Link;
        } else if (asset.entryType == 'pctWall') {
            //TODO
            haveModule = true;
            assetId = asset.Link;
        } else if (asset.entryType == 'pctModuleFile') {
            haveModule = true;
            assetId = process.env.NEXT_PUBLIC_BACKEND_URL + asset.Link;
        }
    }

    const createDate = date => {
        let newDate = new Date(date)
            .toLocaleDateString('en-GB', {
                month: 'short',
                day: 'numeric',

                year: 'numeric',
            })
            .split(' ')
            .join(', ');

        newDate = newDate.replace(',', '');
        return newDate;
    };

    return (
        <Layout>
            <Content>
                <div className="fullheightcontainer">
                    {project && asset && (
                        <TopPanel
                            pathinfo={pathinfo}
                            project={project}
                            setIsSliderVisible={setVisible}
                            asset={asset}
                        />
                    )}

                    {asset && (
                        <div className="pagearea-asset">
                            <div
                                className="assetContainer"
                                data-cy="asset-display-container"
                            >
                                {haveVideo && <AssetVideo assetId={assetId} />}
                                {haveImgViwable && (
                                    <AssetLargeImage assetId={assetId} />
                                )}
                                {havePDFViwable && (
                                    <AssetLargePDF assetId={assetId} />
                                )}
                                {haveModule && (
                                    <AssetModule
                                        filepath={assetId}
                                        asset={asset}
                                    />
                                )}
                            </div>
                        </div>
                    )}
                </div>
            </Content>
            {project && asset && (
                <Sider
                    theme="light"
                    width={256}
                    collapsedWidth={0}
                    collapsed={isSliderCollapsed}
                >
                    <div className="ant-drawer-header">
                        <div className="ant-drawer-title">
                            Content Properties
                        </div>
                    </div>
                    <div className="sliderBody">
                        <div className="projThumbnail">
                            <Thumbnail
                                thumbnailId={asset.AssetId}
                                width={208}
                                height={144}
                            />
                        </div>
                        <div data-cy="content-properties-title">
                            <strong>Title: </strong>
                            {asset.title}
                        </div>

                        <div data-cy="content-properties-status">
                            <strong>Status: </strong>
                            {asset.AssetStatus.map(i => i).join(', ')}{' '}
                        </div>
                        <div data-cy="content-properties-type">
                            <strong>Type: </strong>
                            {asset.AssetType}
                        </div>

                        {asset.entryType == 'pctAsset' && (
                            <div data-cy="content-properties-file-type">
                                <strong>Preview File Type: </strong>
                                {getFileExtension(
                                    asset.AssetFileName
                                ).toUpperCase()}
                            </div>
                        )}

                        {asset.addtlfiles.length > 0 && (
                            <div>
                                <strong>Related Files: </strong>
                                {asset.addtlfiles.length}
                            </div>
                        )}

                        {/* <div>
                            {' '}
                            <strong>Last updated: </strong>{' '}
                            {createDate(asset.AssetUpdated)}
                        </div> */}
                        <div data-cy="content-properties-description">
                            {' '}
                            <strong>Description: </strong> {asset.description}
                        </div>
                        <div className="projectInfo">
                            <h5 className="bold"> Project info</h5>
                            <div>
                                <strong>Title: </strong> {project.title}{' '}
                            </div>
                            <div>
                                {' '}
                                <strong>Description: </strong>
                                {project.description}{' '}
                            </div>
                            <div>
                                {' '}
                                <strong>Last updated: </strong>
                                {createDate(project.dateUpdated)}
                            </div>
                        </div>
                    </div>
                </Sider>
            )}

            <style jsx>
                {`
                    .fullheightcontainer {
                        min-height: 100%;
                        height: 100%;
                        display: flex;
                        flex-direction: column;
                    }
                    .pagearea-asset {
                        flex: 1 1 auto;
                    }
                    .assetContainer {
                        position: relative;
                        height: 100%;
                    }
                    .spinnerspace {
                        margin-top: 200px;
                    }
                    .divider56 {
                        height: 6px;
                        clear: both;
                    }
                    .sliderBody {
                        padding: 24px;
                        display: ${displayBody};
                    }

                    .projectInfo {
                        margin-top: 50px;
                    }
                    .projThumbnail {
                        width: 208px;
                        background-size: cover;
                        background-position: center;
                        background-repeat: no-repeat;
                        margin-bottom: 24px;
                    }
                `}
            </style>
        </Layout>
    );
};

export default PortfolioAssetView;
