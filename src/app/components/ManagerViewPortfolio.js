import * as React from 'react';
import { Fragment, useState, useContext, useEffect } from 'react';
import { useRouter } from 'next/router';
import cn from 'classnames';
import Button from 'react-bootstrap/Button';
import LoaderContext from '../context/LoaderContext';
import ManagerContext from '../context/ManagerContext';
import AppDropdownActionMenu from './AppDropdownActionMenu';
import FilterEmptyState from './FilterEmptyState';
import { authorizeToken } from '../services/cmsapi-manage';
import { AppDropdownBox } from './AppDropdownBox';
import ManageTableGeoexplorer from './ManageTableGeoexplorer';
import ManageTableProjects from './ManageTableProjects';
import ManageTableAssets from './ManageTableAssets';
import { assetSortOptions } from '../services/filteringAssets';

const ManagerViewPortfolio = () => {
    const { loadersState } = useContext(LoaderContext);

    const router = useRouter();

    const [selectedTab, setSelectedTab] = useState(0);

    useEffect(() => {
        // required cookie for admin cms changes
        authorizeToken();
    }, []);

    const {
        projects,
        assets,
        geosolutions,
        managerAddNewAsset,
        managerAddNewProject,
        managerCancelNewAsset,
        managerCancelNewProject,
        updateAssetData,
        updateProjectData,
        setFilterProjectSlug,
        selectedProjectIndex,
        importAssets,
        assetfilters,
        setAssetFilters,
        selectedAssetSortOption,
        setAssetSortOption,
        totalProjects,
        totalAssets,
    } = useContext(ManagerContext);

    const substractHidden = router.query.projectfilter != null ? 1 : 0;

    return (
        <div className="pagearea">
            <div className="navApplications">
                <button
                    className={cn(
                        'btnTab',
                        { btnTabSelected: selectedTab === 0 },
                        { btnTabSelected: selectedTab === 1 }
                    )}
                    onClick={() => setSelectedTab(0)}
                >
                    Portfolio
                </button>

                <button
                    className={cn(
                        { hidden: importAssets === false },
                        { btnTabSelected: selectedTab === 2 },
                        'btnTab'
                    )}
                    onClick={() => setSelectedTab(2)}
                >
                    Import Preview
                </button>

                <button
                    className={cn(
                        { hidden: !geosolutions.length },
                        { btnTabSelected: selectedTab === 3 },
                        'btnTab'
                    )}
                    onClick={() => setSelectedTab(3)}
                >
                    GeoExplorer
                </button>
            </div>
            <div className="divider3" />
            {selectedTab === 0 && (
                <div
                    className={cn('headerCounter', {
                        invisible: !projects.length,
                    })}
                >
                    {totalProjects} Portfolio Explorer projects
                </div>
            )}
            {selectedTab === 1 && (
                <div
                    className={cn('headerCounter', {
                        invisible: !assets.length,
                    })}
                >
                    {totalAssets} assets in project
                </div>
            )}
            {selectedTab === 3 && (
                <div
                    className={cn('headerCounter', {
                        invisible: loadersState.length,
                    })}
                >
                    {geosolutions.length} Geographic Explorer Solutions
                </div>
            )}

            <div className="float-left filterHolder">
                {selectedTab === 1 && (
                    <Fragment>
                        <span className="label">&nbsp;</span>
                        <Button
                            block
                            variant="secondary"
                            size="lg"
                            onClick={() => setSelectedTab(0)}
                        >
                            ← Projects
                        </Button>
                    </Fragment>
                )}
            </div>

            <div className="float-left filterHolder">
                {selectedTab === 0 && (
                    <Fragment>
                        <span className="label">&nbsp;</span>
                        <Button
                            block
                            variant="secondary"
                            size="lg"
                            onClick={() => {
                                managerAddNewProject();
                            }}
                        >
                            Add Project
                        </Button>
                    </Fragment>
                )}
            </div>

            <div className="float-left filterHolder">
                {selectedTab === 1 && (
                    <React.Fragment>
                        <AppDropdownActionMenu
                            label="PROJECT FILTER"
                            title={
                                selectedProjectIndex > -1
                                    ? projects[selectedProjectIndex].title
                                    : 'BruceTest'
                            }
                            menuheader="Select project:"
                            options={projects.map(({ title }) => title)}
                            selectedIndex={selectedProjectIndex}
                            onSelect={index => {
                                managerCancelNewAsset();
                                setFilterProjectSlug(projects[index].slug);
                            }}
                        />
                    </React.Fragment>
                )}
            </div>

            <div className="float-left filterHolder">
                {selectedTab === 1 && (
                    <AppDropdownActionMenu
                        label="&nbsp;"
                        title="Add Content"
                        menuheader={
                            selectedProjectIndex > -1
                                ? 'Select type:'
                                : 'Select project first'
                        }
                        options={
                            selectedProjectIndex > -1
                                ? [
                                      'Regular Content',
                                      'Video Content',
                                      'Link Content',
                                      'Bundle Content',
                                  ]
                                : []
                        }
                        onSelect={index => {
                            if (selectedProjectIndex > -1) {
                                const entryTypes = [
                                    'magaAsset',
                                    'magaAssetVideo',
                                    'magaAssetLink',
                                    'magaAssetModuleFile',
                                ];
                                managerAddNewAsset(
                                    projects[selectedProjectIndex],
                                    entryTypes[index]
                                );
                            }
                        }}
                    />
                )}
            </div>

            {/* <div className="float-left filterHolder">
        {selectedTab === 1 && (
          <React.Fragment>
            <span className="label">&nbsp;</span>
            <ManagerGooglePicker
              developerKey={user.auth.googleApiKey}
              clientId={user.auth.googleApiClientId}
              viewId="SPREADSHEETS"
              onAuthFailed={error => {
                console.log("Auth error")
                console.dir(error)
              }}
              onPicked={async ({ docs, oauthToken }) => {
                const fileId = docs[0].id
                const fileUrl = `https://www.googleapis.com/drive/v3/files/${fileId}/export`
                const params = {
                  mimeType: "text/csv",
                  fields: "*",
                }
                const data = await downloadTextFile(fileUrl, params, oauthToken)
                const assetsForImport = data.map((rawItem, index) =>
                  assetFromRaw({
                    rawItem,
                    itemkey: index,
                    projects,
                    assetTypes,
                    assetStatus,
                  })
                )
                setImportAssets(assetsForImport)
                setSelectedTab(2)
              }}
              onChange={data => {}}> */}

            {/* <button className="btn btn-secondary btn-lg">Import Assets</button> */}
            {/*
            </ManagerGooglePicker>
          </React.Fragment>
        )}
      </div> */}

            {selectedTab === 1 && (
                <div className="float-right sortHolder">
                    <AppDropdownBox
                        label="SORT"
                        menuOptions={assetSortOptions.map(({ title }) => title)}
                        selectedOptionKey={selectedAssetSortOption}
                        onOptionSelected={setAssetSortOption}
                    />
                </div>
            )}

            <div className="divider15" />

            <div className="assetsContainer">
                {selectedTab === 0 && (
                    <ManageTableProjects
                        projects={projects}
                        actNavigateToAssetsFiltered={projectSlug => {
                            setFilterProjectSlug(projectSlug);
                            setSelectedTab(1);
                        }}
                        managerCancelNewProject={managerCancelNewProject}
                        updateProjectData={updateProjectData}
                    />
                )}
                {selectedTab === 1 && projects != null && (
                    <ManageTableAssets
                        projects={projects}
                        assets={assets}
                        updateAssetData={data =>
                            updateAssetData(data, selectedTab)
                        }
                        managerCancelNewAsset={managerCancelNewAsset}
                    />
                )}
                {selectedTab === 2 && (
                    <ManageTableAssets
                        projects={projects}
                        assets={importAssets}
                        updateAssetData={data =>
                            updateAssetData(data, selectedTab)
                        }
                        managerCancelNewAsset={managerCancelNewAsset}
                    />
                )}
            </div>

            {substractHidden === 0 &&
                assets.length === 0 &&
                assetfilters.length > 0 && (
                    <FilterEmptyState
                        onClick={() => {
                            setAssetFilters([]);
                        }}
                    />
                )}

            {substractHidden === 1 &&
                assets.length === 0 &&
                assetfilters.length > 0 && <FilterEmptyState />}

            <div className="divider56" />

            {selectedTab === 3 && geosolutions.length > 0 && (
                <div className="assetsContainer">
                    <ManageTableGeoexplorer projects={geosolutions} />
                </div>
            )}

            <div className="divider56" />

            <style jsx>
                {`
        .navApplications {
          display: flex;
          background-color: #efefef;
          padding-top: 6px;
          padding-left: 26px;
        }
            .sectionTitle {
                font-size: 18px;
                font-weight: bold;
            }
            .fullWidth {
                width 100%;
            }
            .divider3 {
                height: 3px;
                clear: both;
            }
            .divider15 {
                height: 14px;
                clear: both;
            }
            .assetsContainer {
                margin-left: 29px;
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
            }
            .sortHolder {
                margin-right: 28px;
                min-width: 134px;
            }
            .headerCounter {
              margin-left: 28px;
              padding-top: 16px;
              font-size: 16px;
            }
            .filterHolder {
                margin-left: 28px;
                min-width: 134px;
                max-width: 388px;
                position: relative;
            }
            .btnTab {
                margin-bottom: -1px;
                margin-right: 4px;
                display: block;
                padding: .5rem 1rem;
                color: #495057;
                border: none;
                outline: none;
            }
            .btnTabSelected {
                font-weight: bold;
                background-color: #fff;
                border: 1px solid transparent;
                border-top-left-radius: .25rem;
                border-top-right-radius: .25rem;
                border-color: #dee2e6 #dee2e6 #fff;
            }
            .hidden {
              display: none;
            }
        `}
            </style>
        </div>
    );
};

export default ManagerViewPortfolio;
