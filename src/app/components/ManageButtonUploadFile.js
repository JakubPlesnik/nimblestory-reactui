import * as React from 'react';
import { useState, useContext } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import LoginContext from '../context/LoginContext';
import ManagerGooglePicker from './ManagerGooglePicker';
import ManagerFilePicker from './ManagerFilePicker';
import { saveForm } from '../services/cmsapi-manage';

const ManageButtonUploadFile = ({ entryId, field, onSuccess, caption }) => {
    const { user } = useContext(LoginContext);
    const [loading, setLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useState(false);
    const [selectedFile, setSelectedFile] = useState(null);

    const [showModal, setShowModal] = useState(false);
    const handleClose = () => {
        setLoading(false);
        setAlertMessage(false);
        setSelectedFile(null);
        setShowModal(false);
    };
    const handleShow = () => setShowModal(true);

    const submit = async () => {
        setAlertMessage('Loading...');
        setLoading(true);

        const formData = new FormData();
        formData.append('entryId', entryId);
        formData.append(`fields[${field}]`, selectedFile);

        const result = await saveForm(formData);

        if (result && !result.error) {
            setAlertMessage('Saved');
            onSuccess(result.item);

            window.setTimeout(() => {
                handleClose();
            }, 1000);

            window.setTimeout(() => {
                setLoading(false);
                setAlertMessage(false);
            }, 2000);
        } else if (result.error) {
            setAlertMessage(result.error);
        }
    };

    const onFilePicked = results => {
        setSelectedFile(results[0]);
        setAlertMessage('Confirm upload?');
        handleShow();
    };

    const onGooglePicked = async ({ docs, oauthToken }) => {
        setSelectedFile(false);
        setLoading(true);

        const { name, id, mimeType } = docs[0];
        const fileUrl = `https://www.googleapis.com/drive/v3/files/${id}?alt=media`;
        setAlertMessage(`Loading ${name}...`);
        handleShow();

        const response = await fetch(fileUrl, {
            method: 'GET',
            headers: {
                Accept: '*/*',
                mode: 'cors',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${oauthToken}`,
            },
        });

        const data = await response.blob();
        const metadata = {
            type: mimeType,
        };
        setSelectedFile(new File([data], name, metadata));
        setAlertMessage('Confirm upload?');
        setLoading(false);
    };

    const googlePickerWrapper = ({ children }) => (
        <ManagerGooglePicker
            key={`gp-${entryId}-${field}`}
            developerKey={user.auth.googleApiKey}
            clientId={user.auth.googleApiClientId}
            viewId="DOCS"
            onAuthFailed={error => {
                // eslint-disable-next-line no-console
                console.log('Auth error');
                // eslint-disable-next-line no-console
                console.dir(error);
            }}
            onPicked={onGooglePicked}
            onChange={() => {}}
        >
            <Button variant="link" size="lg">
                {children}
            </Button>
        </ManagerGooglePicker>
    );

    const filePickerWrapper = ({ children }) => (
        <ManagerFilePicker
            key={`fp-${entryId}-${field}`}
            onPicked={onFilePicked}
        >
            <Button variant="link" size="lg">
                {children}
            </Button>
        </ManagerFilePicker>
    );

    return (
        <React.Fragment>
            <Dropdown>
                <Dropdown.Toggle variant="secondary" size="sm">
                    Upload
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {caption &&
                        caption
                            .split('\n')
                            .map((caption, i) => (
                                <Dropdown.Header key={i}>
                                    {caption}
                                </Dropdown.Header>
                            ))}
                    <Dropdown.Item as={googlePickerWrapper}>
                        GDrive
                    </Dropdown.Item>
                    <Dropdown.Item as={filePickerWrapper}>
                        Local disk
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Upload File</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>
                        {selectedFile && (
                            <span>
                                Selected file:
                                <br />
                                <strong>
                                    {selectedFile.name} (
                                    {(selectedFile.size / 1024).toFixed(2)}
                                    KB)
                                </strong>
                            </span>
                        )}
                        <br />
                        <br />
                        {alertMessage && (
                            <strong className="alert">{alertMessage}</strong>
                        )}
                    </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button
                        variant="primary"
                        onClick={submit}
                        disabled={loading}
                    >
                        Save
                    </Button>
                </Modal.Footer>
            </Modal>
            <style jsx>
                {`
                    .alert {
                        overflow-wrap: break-word;
                    }
                `}
            </style>
        </React.Fragment>
    );
};
export default ManageButtonUploadFile;
