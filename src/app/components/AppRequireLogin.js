import { useRouter } from 'next/router';
import { useState, useContext, useEffect } from 'react';
import LoginContext from '../context/LoginContext';
import UserContext from '../context/UserContext';

import { fetchOrgs, fetchPinned } from '../services/api/apiService';
import { logout } from '../services/auth/authService';

import { fetchEmbedConceptExplorer } from '../services/api/apiService';

export const kAppReqOrganization = 'kAppReqOrganization';
export const kAppReqTheme = 'kAppReqTheme';
export const kAppReqAdmin = 'kAppReqAdmin';

const loginToken = router => {
    let token;
    if (router.query && router.query.token) {
        token = router.query.token;

        try {
            if (window.localStorage) {
                localStorage.setItem('token', token);
            }
        } catch (err) {
            document.localStorage = { token };
        }
    } else if (localStorage.getItem('token')) {
        token = localStorage.getItem('token');
    } else {
        token = false;
    }
    return token;
};

const redirectToLogin = router => {
    localStorage.removeItem('token');

    window.location = `/login?referrer=${router.asPath}`;
};

const AppRequireLogin = ({ require, setData, children }) => {
    const router = useRouter();
    const {
        pathinfo,
        setThemeColors,
        appState,
        setAppState,
        pageReady,
    } = useContext(UserContext);

    const {
        isLoggedIn: PrevIsLoggedIn,
        user: PrevUser,
        organizations: PrevOrganizations,
        organization: PrevOrganization,
        defaultOrganization: PrevDefaultOrg,
    } = appState.loginstate;

    const { pinned: PrevPins } = appState.orgcache;

    const [isLoggedIn, setIsLoggedIn] = useState(PrevIsLoggedIn || false);
    const [user, setUser] = useState(PrevUser || false);
    const [organizations, setOrganizations] = useState(
        PrevOrganizations || false
    );

    const [defaultOrganization, setDefaultOrganization] = useState(
        PrevDefaultOrg || false
    );
    const [organization, setOrganization] = useState(PrevOrganization || false);
    const [pinned, setPinned] = useState(PrevPins || []);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const [isOrgPermisssion, setOrgPermisssion] = useState(true);

    const resetDataOnLogout = () => {
        let emptyField = false;

        setAppState({
            loginstate: {
                isLoggedIn,
                user,
                emptyField,
                organization,
                // eslint-disable-next-line no-dupe-keys
                emptyField,
            },
        });
    };

    /*
    Login
    */
    function checkUser() {
        if (!user) {
            const checkToken = loginToken(router);
            if (!checkToken) {
                redirectToLogin(router);
            } else {
                // hide token from URL bar
                if (router.query && router.query.token) {
                    // history.pushState({}, null, `/portfolio/${router.query.organization}/`)
                    const queryPath = router.asPath.split('?token')[0];
                    history.pushState({}, null, `${queryPath}`);
                    // console.log('location pathname', window.location.pathname);
                    // console.log('router pathname', router.pathname);
                    // if (!window.location.pathname.match(/\S+\/\S+\/\S+/)) {
                    //   router.replace(`/portfolio/${router.query.organization}/`)
                    // }
                }
            }
        }

        if (!organizations) {
            fetchOrgs()
                .then(result => {
                    const { organizations, user } = result;
                    setUser(user);
                    if (organizations && organizations.length > 0) {
                        setOrganizations(organizations);
                    } else {
                        logout(resetDataOnLogout);
                    }
                })
                .catch(() => {
                    logout(resetDataOnLogout);
                });
        }
    }

    /*
    Embed/Theme
  */
    function checkTheme() {
        const checkToken = loginToken(router);
        if (!checkToken) {
            // console.log("Missing token")
        } else {
            if (pathinfo.organization && pathinfo.project) {
                const token = new URL(window.location.href).searchParams.get(
                    'token'
                );
                fetchEmbedConceptExplorer(
                    pathinfo.organization,
                    pathinfo.project,
                    token
                ).then(result => {
                    const { data, meta } = result;
                    setThemeColors(meta);
                    if (data) {
                        setData(data);
                    }
                });
            }
        }
    }

    useEffect(() => {
        if (pageReady) {
            if (require.includes(kAppReqOrganization)) checkUser();
            if (require.includes(kAppReqTheme)) checkTheme();
            if (!organization && PrevOrganization) {
                // restore loaded org
                if (PrevPins) {
                    setPinned(PrevPins);
                }
                setOrganization(PrevOrganization);
            }
        } else if (organization && pinned) {
            // save state
            setAppState({ orgcache: { ...appState.orgcache, pinned } });
        }
    }, [pageReady]);

    /*
    Set logged in user
    */
    useEffect(() => {
        if (user) {
            setIsLoggedIn(true);
        } else if (isLoggedIn) {
            setIsLoggedIn(false);
        }
    }, [user]);

    /*
    Set organizations
    */

    useEffect(() => {
        if (organizations && organizations.length) {
            if (!pathinfo.organization) {
                // router.push(`/geoexplorersolutions`, `/geoexplorer/developer`, {shallow: true})
                window.locaton = '/organization';
            }
            /* let prev_org = localStorage.getItem("prev_org")
      if( pathinfo.organization &&  pathinfo.organization !== prev_org
          && pathinfo.application != 'home') {
        setOrgPermisssion(false);

        // var result = confirm("Your userid does not have permission to access this content.");
        // if (result) {
        //   // window.locaton = "/logout"
        //   gotoLogin();
        // }
      } */

            //set default org
            organizations.map(function(org) {
                if (org.default) {
                    if (!defaultOrganization) {
                        setDefaultOrganization(org.slug);
                    }
                }
            });

            if (!organization || pathinfo.organization !== organization.slug) {
                // handle changing org
                setPinned([]);
                const selectedOrg = organizations.find(
                    org => org.slug == pathinfo.organization
                );

                if (!selectedOrg) {
                    if (pathinfo.organization !== undefined) {
                        setOrgPermisssion(false);
                    }
                }

                setOrganization(selectedOrg);

                if (selectedOrg && selectedOrg.themeSettings) {
                    setThemeColors(selectedOrg.themeSettings);
                }
            }
        }
    }, [pageReady, organizations, pathinfo.organization]);

    useEffect(() => {
        if (pageReady && organization) {
            const { slug } = organization;
            if (slug == pathinfo.organization) {
                setAppState({
                    loginstate: {
                        isLoggedIn,
                        user,
                        organizations,
                        organization,
                        defaultOrganization,
                    },
                });
            }

            if (!PrevPins) {
                // load org pins
                fetchPinned(organization.slug).then(items => {
                    if (items) {
                        if (items.pinned.length) {
                            setPinned(items.pinned);
                        }
                        setAppState({
                            orgcache: {
                                ...appState.orgcache,
                                pinned: items.pinned,
                            },
                        });
                    }
                });
            }

            localStorage.setItem('prev_org', slug);
        }
    }, [organization, defaultOrganization]);

    return (
        <LoginContext.Provider
            value={{
                isLoggedIn,
                isModalOpen,
                setIsModalOpen,
                user,
                organization,
                organizations,
                pinned,
                setPinned,
                isOrgPermisssion,
                defaultOrganization,
                setDefaultOrganization,
                resetDataOnLogout,
            }}
        >
            {children}
        </LoginContext.Provider>
    );
};

export default AppRequireLogin;
