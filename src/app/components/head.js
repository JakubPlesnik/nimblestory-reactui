import React from 'react';
import NextHead from 'next/head';

// loading this in <script> gives error
const addUserSnap = () => {
    const script = document.createElement('script');
    script.async = 1;
    script.src =
        'https://api.usersnap.com/load/8633510b-ce18-455c-8d70-0b42e6decbaa.js?onload=onUsersnapLoad';
    document.getElementsByTagName('head')[0].appendChild(script);
};

const Head = ({ description, mainstyle, children }) => {
    const devEnvironment = false;

    if (!devEnvironment && typeof window !== 'undefined') {
        const ga =
            window.ga ||
            function() {
                (ga.q = ga.q || []).push(arguments);
            };
        ga.l = +new Date();
        ga('create', 'UA-30314275-14', 'auto');
        ga('require', 'urlChangeTracker');
        ga('send', 'pageview');

        window.onUsersnapLoad = api => {
            api.init();
            window.Usersnap = api;
        };

        window.addEventListener('load', addUserSnap, false);
    }

    return (
        <NextHead>
            <meta charSet="UTF-8" />
            <title>NimbleStory</title>
            <meta name="description" content={description || ''} />
            <meta
                name="viewport"
                content="width=device-width, initial-scale=1"
            />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
            <link
                rel="icon"
                type="image/png"
                sizes="32x32"
                href="/favicon.ico"
            />
            <link rel="stylesheet" href="/appfiles/fontIcons/styles.css"></link>
            <link
                rel="icon"
                type="image/png"
                sizes="16x16"
                href="/favicon.ico"
            />
            {!devEnvironment && (
                <script
                    async
                    defer
                    src="https://www.google-analytics.com/analytics.js"
                />
            )}
            {!devEnvironment && (
                <script async defer src="/appfiles/vendor/autotrack.js" />
            )}

            <style dangerouslySetInnerHTML={mainstyle} />

            {children}
        </NextHead>
    );
};

export default Head;
