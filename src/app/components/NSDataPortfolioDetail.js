import { useState, useContext, useEffect } from 'react';
import UserContext from '../context/UserContext';
import PortfolioDetailContext from '../context/PortfolioDetailContext';

import {
    fetchContentDetail,
    fetchprojectBySlug,
} from '../services/api/apiService';

const NSDataPortfolioDetail = ({ children }) => {
    const [project, setProject] = useState(null);
    const [asset, setAsset] = useState(null);

    const { pathinfo, currentProject, setCurrentProjectGlobal } = useContext(
        UserContext
    );

    async function getProjects() {
        const projectsData = await fetchprojectBySlug(pathinfo.project);
        setCurrentProjectGlobal(projectsData.result);
        setProject(projectsData.result);
    }

    useEffect(() => {
        if (!currentProject) return;
        setProject(currentProject);
    }, [currentProject]);

    useEffect(() => {
        if (!pathinfo.organization) return;
        if (!pathinfo.project) return;
        if (!currentProject) {
            getProjects();
        }
    }, [pathinfo.project]);

    useEffect(() => {
        if (pathinfo.asset) {
            fetchContentDetail(pathinfo.project, pathinfo.asset).then(items => {
                if (items.item) {
                    setAsset(items.item);
                }
            });
        }
    }, [pathinfo.asset]);

    return (
        <PortfolioDetailContext.Provider
            value={{
                project,
                asset,
            }}
        >
            {children}
        </PortfolioDetailContext.Provider>
    );
};

export default NSDataPortfolioDetail;
