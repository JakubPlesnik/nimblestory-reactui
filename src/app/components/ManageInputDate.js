import DatePicker from 'react-datepicker';

const ManageInputDate = ({ data, field, onSave }) => {
    const handleDateChange = date => {
        const unixDate = date.getTime() / 1000;
        onSave({ [field]: unixDate });
    };

    const selectedDate = data[field] ? new Date(data[field] * 1000) : null;

    return <DatePicker selected={selectedDate} onChange={handleDateChange} />;
};

export default ManageInputDate;
