import { useContext } from 'react';
import UserContext from '../context/UserContext';

const ConceptExplorerNavToggle = ({ children, onClick }) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    return (
        <>
            <a
                href=""
                className="ddcontrolbutton"
                onClick={e => {
                    e.preventDefault();
                    onClick(e);
                }}
            >
                {children}
            </a>
            <div
                className="ddcarret"
                onClick={e => {
                    e.preventDefault();
                    onClick(e);
                }}
            />
            <style jsx>
                {`
                    .ddcarret {
                        border-radius: 0 0 20px 20px;
                        background-color: ${color1Darkest};
                        width: 40px;
                        height: 40px;
                        display: inline-block;
                        float: right;
                        color: white;
                        text-align: center;
                        transform: rotate(-90deg);
                        font-size: 20px;
                        line-height: 58px;
                        background-image: url(/appfiles/icons/icon-arrow.svg);
                        background-repeat: no-repeat;
                        background-position: center;
                        background-position-x: 45%;
                    }

                    .ddcontrolbutton {
                        display: inline-block;
                        background-color: #f3f5f8;
                        width: 483px;
                        height: 40px;
                        padding-left: 14px;
                        font-size: 16px;
                        line-height: 50px;
                        text-decoration: none;
                        color: #656565;
                    }
                `}
            </style>
        </>
    );
};

export default ConceptExplorerNavToggle;
