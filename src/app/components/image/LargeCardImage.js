import Thumbnail from './Thumbnail';
import { ICONS } from '../../enum/projectIcons';

const LargeCardImage = ({
    thumnails,
    projectThumbnail = null,
    labelColor,
    contentType,
}) => {
    let thumb1,
        thumb2,
        thumb3 = null;

    if (projectThumbnail) {
        thumb1 = projectThumbnail;
    } else {
        if (thumnails.length >= 2) {
            thumb1 = thumnails[0];
            thumb2 = thumnails[1];
            thumb3 = thumnails[2];
        }
    }

    return (
        <div className="flex-container">
            <div
                className="flex-left"
                style={!thumb3 ? { width: 512 + 'px' } : {}}
            >
                <Thumbnail
                    thumbnailId={thumb1}
                    placeholderIcon={
                        contentType == 'project'
                            ? ICONS.project
                            : ICONS.collection
                    }
                    contentType={contentType}
                    labColor={labelColor}
                    width={thumb3 ? 298 : 512}
                    height={208}
                />
            </div>
            <div
                className="flex-right"
                style={!thumb3 ? { display: 'none' } : {}}
            >
                <div className="image1">
                    <Thumbnail
                        thumbnailId={thumb2}
                        placeholderIcon={''}
                        width={214}
                        height={99}
                    />{' '}
                </div>
                <div className="image2">
                    <Thumbnail
                        thumbnailId={thumb3}
                        placeholderIcon={''}
                        width={214}
                        height={109}
                    />{' '}
                </div>
            </div>
            <style jsx>
                {`
                    img {
                        object-fit: cover;
                    }

                    .flex-container {
                        display: flex;
                        flex-direction: row;
                        width: 512px;
                        height: 208px;
                    }

                    .flex-left {
                        width: 296px;
                    }

                    .flex-right {
                        width: 215px;
                    }
                `}
            </style>
        </div>
    );
};

export default LargeCardImage;
