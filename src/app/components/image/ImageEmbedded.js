import React from 'react';
import { useState, useEffect } from 'react';
import { fetchEmbeddedAsset } from '../../services/api/apiService';

const ImageEmbedded = ({ type, slug, assetId }) => {
    const [imageUrl, setImageUrl] = useState('');

    useEffect(() => {
        if (Number.isInteger(assetId)) {
            const token = localStorage.getItem('token');
            fetchEmbeddedAsset(type, slug, assetId, token, setImageUrl, null);
            return;
        }
    }, []);

    return (
        <div>
            <img src={imageUrl} />
            <style jsx>
                {`
                    img {
                        width: 100%;
                    }
                `}
            </style>
        </div>
    );
};

export default ImageEmbedded;
