import React from 'react';
import { useState, useEffect } from 'react';
import { fetchAsset } from '../../services/api/apiService';

const Image = ({ assetId }) => {
    const [imageUrl, setImageUrl] = useState('');

    useEffect(() => {
        if (Number.isInteger(assetId)) {
            fetchAsset(assetId, setImageUrl, null);
            return;
        }
    }, []);

    return (
        <div>
            <img src={imageUrl} />
            <style jsx>
                {`
                    img {
                        width: 100%;
                    }
                `}
            </style>
        </div>
    );
};

export default Image;
