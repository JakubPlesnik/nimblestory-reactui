import React from 'react';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../../context/UserContext';
import { fetchThumbnail } from '../../services/api/apiService';
import Icon from '../Icon';

const Thumbnail = ({
    thumbnailId,
    placeholderIcon,
    contentType = '',
    width = 246,
    height = 159,
}) => {
    width += 2;

    const [imageUrl, setImageUrl] = useState('');
    const [className] = useState('projectThumbnail');

    const [labelColor, setLabelColor] = useState('');
    const [label, setLabel] = useState('');
    useEffect(() => {
        if (Number.isInteger(thumbnailId)) {
            fetchThumbnail(thumbnailId, setImageUrl, null);
            return;
        }
    }, []);

    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    useEffect(() => {
        if (!color1Darkest) return;
        setLabelColor(color1Darkest);
    }, [color1Darkest]);

    useEffect(() => {
        if (!contentType) return;
        setLabel(contentType);
    }, [contentType]);

    return (
        <div className="imageWrapper">
            {contentType != '' && (
                <div className="typeContainer">
                    <span>{label}</span>
                </div>
            )}
            {imageUrl && (
                <div
                    className={className}
                    style={{ backgroundImage: `url(${imageUrl})` }}
                >
                    {' '}
                </div>
            )}
            {!imageUrl && (
                <div className={className}>
                    <Icon
                        name={placeholderIcon}
                        size="140"
                        type="outline"
                        color="#000"
                        margin={width > 250 ? 30 : 0}
                    />
                </div>
            )}

            <style jsx>
                {`
                    .projectThumbnail {
                        height: ${height}px;
                        width: ${width}px;
                        background-color: #f9f9f9;
                        background-size: cover;
                        background-position: center center;
                        background-repeat: no-repeat;
                        padding-top: 15px;
                        border-radius: 2px 2px 0 0;

                        transition: all 1s ease;
                        -moz-transition: all 1s ease;
                        -ms-transition: all 1s ease;
                        -webkit-transition: all 1s ease;
                        -o-transition: all 1s ease;
                    }

                    .typeContainer {
                        width: 175px;
                        height: 24px;
                        flex-grow: 0;
                        z-index: 20;
                        position: absolute;
                        bottom: 0;
                        background: linear-gradient(
                            225deg,
                            transparent 20px,
                            ${labelColor} 20px
                        );
                    }

                    .typeContainer span {
                        font-size: 12px;
                        font-weight: normal;
                        font-stretch: normal;
                        display: block;
                        font-style: normal;
                        padding-left: 8px;
                        margin-top: 4px;
                        color: #ffffff;
                    }

                    .imageWrapper {
                        position: relative;
                        overflow: hidden;
                    }
                `}
            </style>
        </div>
    );
};

export default Thumbnail;
