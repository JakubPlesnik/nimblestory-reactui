import { useState, useContext } from 'react';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import UserContext from '../context/UserContext';
import GeoExplorerFilters from './GeoExplorerFilters';
import LoginContext from '../context/LoginContext';
import AppLinkData from '../services/applink';

const ManagerPinPageButton = dynamic(() =>
    import('../components/ManagerPinPageButton')
);

const GeoExplorerTopPanel = ({ pathinfo, project, asset, setPanelState }) => {
    const backToHomeLink = AppLinkData({
        homeLink: pathinfo,
    });
    const backToGeoExplorerLink = AppLinkData({
        GeoExplorerLink: pathinfo,
    });

    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;
    const { user } = useContext(LoginContext);

    const [selectedTab, setSelectedTab] = useState(0);
    const [panelCollapsed, setPanelCollapsed] = useState(false);

    const classPanelNav0 =
        selectedTab === 0 ? 'btnInfoTab selectedTab' : 'btnInfoTab';
    const classPanelNav1 =
        selectedTab === 1 ? 'btnInfoTab selectedTab' : 'btnInfoTab';

    const classTabContent2 =
        !panelCollapsed && selectedTab === 2 ? 'tabContent' : 'hidden';
    const classTabContent1 =
        !panelCollapsed && selectedTab === 1 ? 'tabContent' : 'hidden';
    const classTabContent0 =
        !panelCollapsed && selectedTab === 0 ? 'tabContent' : 'hidden';

    const classBntCollapse =
        panelCollapsed === false ? 'btnHolder' : 'btnHolder icnCollapsed';

    const displayManagerMenu =
        user &&
        (user.groups.includes('manager') || user.groups.includes('admin'));

    const handleClickPanelTab = tabId => {
        setSelectedTab(tabId);
        if (panelCollapsed) {
            setPanelCollapsed(false);
        }
        setPanelState({ collapsed: false, tab: tabId });
    };

    const onPanelToggle = () => {
        const newValue = !panelCollapsed;
        setPanelCollapsed(newValue);
        setPanelState({ collapsed: newValue, tab: selectedTab });
    };

    const assetThumbPath = asset['File1(SmPrev)'];

    const thumbBgImgStyleAsset = assetThumbPath
        ? {
              backgroundImage: `url(${assetThumbPath})`,
          }
        : {
              backgroundImage: "url('/appfiles/icons/icon-asset.png')",
              backgroundSize: '50%',
          };

    return (
        <div className="topPanel">
            <div className="toolbar-row">
                <div
                    className="appHeader"
                    style={{
                        color: color1Darkest,
                    }}
                >
                    <div className="secondaryTitle">
                        <Link {...backToHomeLink}>
                            <a
                                style={{
                                    color: color1Darkest,
                                }}
                            >
                                Home
                            </a>
                        </Link>

                        <span> / </span>

                        <Link {...backToGeoExplorerLink}>
                            <a
                                style={{
                                    color: color1Darkest,
                                }}
                            >
                                Geographic Explorer
                            </a>
                        </Link>
                    </div>
                    <div className="primaryTitle">{project.title}</div>
                </div>

                <div className="panelNav">
                    <button
                        type="button"
                        className={classPanelNav0}
                        onClick={() => handleClickPanelTab(0)}
                    >
                        Filters
                    </button>
                    <button
                        type="button"
                        className={classPanelNav1}
                        onClick={() => handleClickPanelTab(1)}
                    >
                        About
                    </button>
                </div>
            </div>
            <div className="tabsContainer">
                <div className={classTabContent0}>
                    <GeoExplorerFilters />
                </div>
                <div className={classTabContent1}>
                    <div
                        className="projThumbnail"
                        style={thumbBgImgStyleAsset}
                    />
                    <div className="projInfo">
                        <p>{project.description}</p>
                        {displayManagerMenu && (
                            <ManagerPinPageButton newpin={project} />
                        )}
                    </div>
                </div>
                <div className={classTabContent2}>
                    <div className="projInfo">Empty</div>
                </div>
                <button
                    type="button"
                    className={classBntCollapse}
                    onClick={onPanelToggle}
                />
            </div>

            <style jsx>
                {`
                    .btnTopRight {
                        position: absolute;
                        top: 20px;
                        right: 106px;
                    }
                    .projInfo > p {
                        font-size: 16px;
                        line-height: 20px;
                        color: #656565;
                        padding-right: 288px;
                    }
                    p.panelSecondaryText {
                        font-size: 12px;
                        line-height: 33px;
                        color: #656565;
                    }

                    .panelInfoItem {
                        font-weight: bold;
                    }
                    .pageTitle {
                        float: left;
                        padding-left: 45px;
                        font-size: 24px;
                        height: 50px;
                        width: 100%;
                        padding-right: 305px;
                        line-height: 50px;
                        overflow: hidden;
                    }
                    .titleP {
                        line-height: 24px;
                        display: inline-block;
                    }
                    .topPanel {
                        padding-top: 3px;
                        position: relative;
                    }
                    .tabsContainer {
                        background-color: #f3f5f8;
                        clear: both;
                    }
                    .tabContent {
                        clear: both;
                        position: relative;
                        display: flex;
                        padding-top: 7px;
                        overflow: hidden;
                    }
                    .panelNav {
                        width: 350px;
                        margin-right: 20px;
                    }
                    button.btnInfoTab {
                        width: 150px;
                        height: 50px;
                        display: block;
                        float: left;
                        margin-left: 5px;
                        border: none;
                        outline: none;
                        cursor: pointer;
                        text-align: center;
                        line-height: 50px;
                        font-size: 14px;
                        background-color: #ffffff;
                        color: #5d5d5d;
                        border-radius: 8px 8px 0px 0px;
                    }
                    button.selectedTab {
                        background-color: #f3f5f8;
                        font-weight: bold;
                    }
                    .projThumbnail {
                        min-width: 197px;
                        height: 150px;
                        margin-left: 55px;
                        margin-right: 46px;
                        background-color: #e8e8e8;
                        background-size: cover;
                        background-position: center;
                        background-repeat: no-repeat;
                        box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
                    }
                    .topPanelSubtitle {
                        color: #0d3b5f;
                        font-weight: bold;
                        text-transform: uppercase;
                        margin-top: 4px;
                        margin-bottom: 4px;
                        letter-spacing: 2px;
                    }

                    button.btnHolder {
                        width: 32px;
                        height: 20px;
                        border: none;
                        outline: none;
                        background-color: transparent;
                        margin-left: 24px;
                        background-size: 20px 11px;
                        background-position: 6px 3px;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/icons/sidebar-collapse-up.png);
                    }
                    .icnCollapsed {
                        transform: rotate(180deg);
                    }
                    .toolbar-row {
                        display: flex;
                        flex-direction: row;
                        align-items: flex-end;
                        justify-content: space-between;
                    }
                    .appHeader {
                        padding-top: 16px;
                        padding-left: 36px;
                    }
                    .secondaryTitle {
                        font-weight: 500;
                        font-size: 12px;
                    }
                    .primaryTitle {
                        font-weight: 500;
                        font-size: 24px;
                        line-height: 28px;
                        padding: 9px 0px;
                    }
                `}
            </style>
        </div>
    );
};

export default GeoExplorerTopPanel;
