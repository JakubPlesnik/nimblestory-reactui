import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import cn from 'classnames';
import * as React from 'react';
import { useState, useEffect } from 'react';

const FilterOption = ({ option, activefilters, onSelect }) => {
    const [isActive, setActive] = useState(false);

    const handleClick = e => {
        e.preventDefault();
        onSelect(option, !isActive);
    };

    useEffect(() => {
        const statusActive =
            activefilters.findIndex(
                filterOption => filterOption.title === option.title
            ) !== -1;
        if (isActive !== statusActive) {
            setActive(statusActive);
        }
    }, [activefilters]);

    return (
        <React.Fragment>
            <Col xs="3">
                <a href="#" onClick={handleClick} className="optionlink">
                    <div
                        className={cn(
                            'square',
                            { 'theme-color1dark--background': isActive },
                            { 'white-bg': !isActive }
                        )}
                    />
                    <span className="filterOptionTitle">{option.title}</span>
                </a>
            </Col>
            <style jsx>
                {`
                    .optionlink {
                        overflow: hidden;
                        text-overflow: clip;
                    }
                    .square {
                        width: 30px;
                        height: 30px;
                        border: 4px solid white;
                        display: inline-block;
                        vertical-align: middle;
                        margin-right: 20px;
                    }
                    .white-bg {
                        background-color: white;
                    }

                    a {
                        display: block;
                        white-space: nowrap;
                    }
                    a:link {
                        text-decoration: none;
                    }
                    .filterOptionTitle {
                        display: inline-block;
                    }
                `}
            </style>
        </React.Fragment>
    );
};

const FilterLane = ({
    filter,
    showBar,
    panefilters,
    setPaneFilters,
    selectClick,
}) => {
    const isMultipleChoice = filter.type === 'multiplechoice';

    const handleSelect = (option, isOn) => {
        const newFiltersState = panefilters.slice();

        if (isMultipleChoice && isOn) {
            // Add option to multiple-choice filter
            newFiltersState.push(option);
        } else if (isMultipleChoice && !isOn) {
            // Remove option from multiple-choice filter
            const found = newFiltersState.findIndex(
                f => f.title === option.title
            );
            if (found > -1) newFiltersState.splice(found, 1);
        } else if (!isMultipleChoice) {
            // Single choice-filter, can remove all filter options first
            filter.options.forEach(item => {
                const found = newFiltersState.findIndex(
                    f => f.title === item.title
                );
                if (found > -1) {
                    newFiltersState.splice(found, 1);
                }
            });

            if (isOn) {
                // Single choice-filter, add filter
                newFiltersState.push(option);
            }
        }
        setPaneFilters(newFiltersState);
        selectClick(newFiltersState);
    };

    const size = 4; // filter options columns
    let optionslanes;
    if (filter.options.length > size) {
        const lanes = Math.ceil(filter.options.length / size);
        optionslanes = new Array(lanes);

        filter.options.forEach((element, index) => {
            const setnum = index > 0 ? Math.ceil((index + 1) / size) - 1 : 0;
            if (!optionslanes[setnum]) optionslanes[setnum] = [];
            optionslanes[setnum].push(element);
        });
    } else {
        optionslanes = [filter.options];
    }

    const lgL = 1;
    const lgR = 11;

    return (
        <React.Fragment>
            <Row>
                <Col className="filterTitle theme-color1dark--color">
                    {filter.title}
                </Col>
            </Row>
            <div className="filterOptionsRow">
                <Col xs="0" lg="0" xl={lgL} />
                <Col xs="12" lg="12" xl={lgR} className="optionsArea">
                    {optionslanes.map((optionslane, key) => (
                        <React.Fragment key={key}>
                            <Row>
                                {optionslane.map((option, key) => (
                                    <FilterOption
                                        key={key}
                                        option={option}
                                        activefilters={panefilters}
                                        onSelect={handleSelect}
                                    />
                                ))}
                            </Row>
                            {key < optionslanes.length && (
                                <div className="optionsLineBreak" />
                            )}
                        </React.Fragment>
                    ))}
                </Col>
            </div>
            {showBar && (
                <div className="divider theme-color1dark--background" />
            )}
            <style jsx global>
                {`
                    .filterOptionsRow {
                        display: flex;
                        flex-wrap: wrap;
                    }

                    .optionsArea {
                        padding-bottom: 15px;
                    }
                    .filterTitle {
                        margin-top: 18px;
                        margin-bottom: 12px;
                        font-size: 12px;
                        font-weight: bold;
                        letter-spacing: 2px;
                        color: #0d3b5f;
                        text-transform: uppercase;
                    }
                    .divider {
                        background-color: #0d3b5f;
                        height: 1px;
                    }
                    .optionsLineBreak {
                        margin-bottom: 15px;
                        clear: both;
                    }
                `}
            </style>
        </React.Fragment>
    );
};

const AppFilterPane = ({
    filters,
    panefilters,
    setPaneFilters,
    selectClick,
}) => {
    const filtersToDisplay = filters.filter(filter => filter.type != 'hidden');
    // console.log("filtersToDisplay: ", filtersToDisplay)
    return (
        <div className="filterArea">
            <div>
                {filtersToDisplay.map((filter, key) => (
                    <FilterLane
                        key={key}
                        panefilters={panefilters}
                        setPaneFilters={setPaneFilters}
                        filter={filter}
                        showBar={key + 1 != filtersToDisplay.length}
                        selectClick={newFiltersState =>
                            selectClick(newFiltersState)
                        }
                    />
                ))}
            </div>
            <style jsx global>
                {`
                    .filterArea {
                        background-color: #dddddd;
                        margin: 0 25px;
                        padding: 0 18px;
                        clear: both;
                    }
                `}
            </style>
        </div>
    );
};

export default AppFilterPane;
