import React from 'react';
import Link from 'next/link';

const AppFolderBreadcrumb = ({
    pathInfo,
    items,
    type = '',
    setAsCurentFolder,
}) => {
    return (
        <div className="card-text">
            {type == 'project' && (
                <span>
                    {' '}
                    <Link
                        href={`/home?application=home&organization=${pathInfo.organization}`}
                        as={`/portfolio/${pathInfo.organization}`}
                    >
                        <span className="breadcrumb-item" key={`ns`}>
                            {' '}
                            / Home
                        </span>
                    </Link>
                    <Link
                        href={`/projects?application=portfolio&organization=${pathInfo.organization}`}
                        as={`/portfolio/${pathInfo.organization}/all`}
                    >
                        <span className="breadcrumb-item" key={`proje`}>
                            {' '}
                            / Projects
                        </span>
                    </Link>
                </span>
            )}
            {items.map((item, i) => {
                return (
                    <span
                        className={
                            Number(i) == Number(items.length - 1)
                                ? 'breadcrumb-item dark'
                                : 'breadcrumb-item'
                        }
                        key={`${item[0]}-folder`}
                        onClick={() => setAsCurentFolder(item[1])}
                        data-cy="breadcrumb-item"
                    >
                        {' '}
                        / {item[0]}
                    </span>
                );
            })}
            <style jsx>{`
                .dark {
                    color: #262626 !important;
                }

                a {
                    text-decoration: none;
                    color: #8c8c8c;
                }

                .breadcrumb-item {
                    cursor: pointer;
                }
            `}</style>
        </div>
    );
};
export default AppFolderBreadcrumb;
