import { useState, useContext, useEffect, useReducer } from 'react';
import VidPlayIcon from '@atlaskit/icon/glyph/vid-play';
import VidPauseIcon from '@atlaskit/icon/glyph/vid-pause';
import VidFullScreenOnIcon from '@atlaskit/icon/glyph/vid-full-screen-on';
import Button from '@atlaskit/button';
import Link from 'next/link';
import { useRouter } from 'next/router';
import AppLinkData from '../services/applink';
import LoginContext from '../context/LoginContext';
import UserContext from '../context/UserContext';

const tickIntervalSec = 6;

function onTickChange(state, action) {
    switch (action.type) {
        case 'start':
            return { tick: 1 };
        case 'next':
            return { tick: state.tick + 1 };
        case 'stop':
            return { tick: 0 };
        default:
    }
}

const ConceptExplorerTopPanel = ({
    pathinfo,
    project,
    components,
    layoutState,
    setLayoutState,
    waypoint,
    waypoints,
    setWaypoint,
}) => {
    const backToHomeLink = AppLinkData({
        homeLink: pathinfo,
    });
    const backToConceptLink = AppLinkData({
        ConceptExplorerLink: pathinfo,
    });

    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;
    const { user } = useContext(LoginContext);

    const defaultViewData = {
        title: '',
        description: '',
        isManager: false,
    };

    const [viewData, setViewData] = useState(defaultViewData);

    const [tick, setTick] = useReducer(onTickChange, { tick: 0 });

    const router = useRouter();

    function localStopPlay() {
        clearTimeout(window.mytimer);
        window.mytimer = null;
    }
    function localSetTick() {
        if (window.mytimer) setTick({ type: 'next' });
    }
    function updateTick() {
        if (tick.tick) {
            if (window.mytimer) {
                clearTimeout(window.mytimer);
                window.mytimer = null;
            }
            window.mytimer = window.setTimeout(() => {
                tick.tick && localSetTick();
            }, tickIntervalSec * 1000);
        }
    }

    useEffect(() => {
        if (tick && tick.tick) {
            let waypointId = 0;

            if (waypoint) {
                const found = waypoints.findIndex(w => w.id === waypoint.id);
                if (found != -1) {
                    const lastIdx = waypoints.length - 1;
                    if (found === lastIdx) {
                        waypointId = 0;
                    } else {
                        waypointId = found + 1;
                    }
                }
            }

            setWaypoint(waypoints[waypointId]);
            updateTick();
        } else {
            localStopPlay();
        }
    }, [tick]);

    useEffect(() => {
        if (project) {
            const { title, description } = project;

            const isManager =
                user &&
                (user.groups.includes('manager') ||
                    user.groups.includes('admin'));

            setViewData({ title, description, isManager });
        }
    }, [project]);

    useEffect(() => {
        if (layoutState.isFullScreen && tick.tick) {
            localStopPlay();
            setTick({ type: 'stop' });
        }
    }, [layoutState]);

    return (
        <div className="topPanel">
            <div className="toolbar-row">
                <div
                    className="appHeader"
                    style={{
                        color: color1Darkest,
                    }}
                >
                    {!router.query.embed && (
                        <div className="secondaryTitle">
                            <Link {...backToHomeLink}>
                                <a
                                    style={{
                                        color: color1Darkest,
                                    }}
                                >
                                    Home
                                </a>
                            </Link>

                            <span> / </span>

                            <Link {...backToConceptLink}>
                                <a
                                    style={{
                                        color: color1Darkest,
                                    }}
                                >
                                    Concept Explorer
                                </a>
                            </Link>
                        </div>
                    )}

                    <div className="primaryTitle" data-cy="concept-panel-title">
                        {viewData.title}{' '}
                    </div>
                </div>

                <div className="dropList">
                    <div className="dropListNav">
                        {components.ComponentDropdownNav}
                    </div>

                    {layoutState.tab === 1 && (
                        <div className="journeyControlsArea">
                            <div className="whiteBg">
                                {components.ComponentExplorerNavWaypointsWindow}
                            </div>
                            <Button
                                onClick={() => setTick({ type: 'stop' })}
                                css={[
                                    { width: '40px', height: '40px' },
                                    { backgroundColor: color1Darkest },
                                    { justifyContent: 'space-evenly' },
                                    { borderRadius: '0px' },
                                ]}
                                iconBefore={
                                    tick.tick ? (
                                        <VidPauseIcon primaryColor="white" />
                                    ) : (
                                        <VidPauseIcon primaryColor="grey" />
                                    )
                                }
                            />
                            <Button
                                onClick={() => setTick({ type: 'start' })}
                                css={[
                                    { width: '40px', height: '40px' },
                                    { backgroundColor: color1Darkest },
                                    { justifyContent: 'space-evenly' },
                                    { borderRadius: '0px' },
                                ]}
                                iconBefore={
                                    !tick.tick ? (
                                        <VidPlayIcon primaryColor="white" />
                                    ) : (
                                        <VidPlayIcon primaryColor="grey" />
                                    )
                                }
                            />
                            <Button
                                onClick={() =>
                                    setLayoutState({ isFullScreen: true })
                                }
                                css={[
                                    { width: '40px', height: '40px' },
                                    { backgroundColor: color1Darkest },
                                    { justifyContent: 'space-evenly' },
                                    { borderRadius: '0px' },
                                ]}
                                iconBefore={
                                    <VidFullScreenOnIcon primaryColor="white" />
                                }
                            />
                        </div>
                    )}
                </div>
            </div>

            <style jsx>
                {`
                    .whiteBg {
                        background-color: white;
                    }
                    .journeyControlsArea {
                        position: absolute;
                        z-index: 1000;
                        margin-top: 35px;
                        display: flex;
                    }
                    .journeyControlTitle {
                        font-weight: 900;
                        font-size: 18px;
                        line-height: 100%;
                        letter-spacing: 0.1px;
                        color: #0d3b5f;
                        margin-bottom: 4px;
                        text-transform: uppercase;
                    }
                    .playControlArea {
                        margin-left: 18px;
                        margin-right: 8px;
                    }
                    .topPanel {
                        height: 75px;
                        position: relative;
                    }
                    .toolbar-row {
                        height: 75px;
                        display: flex;
                        align-items: unset !important;
                        flex-direction: row;
                        align-items: flex-end;
                        justify-content: space-between;
                    }
                    .appHeader {
                        padding-top: 18px;
                        padding-left: 36px;
                    }
                    .secondaryTitle {
                        font-weight: 500;
                        font-size: 12px;
                    }
                    .primaryTitle {
                        font-weight: 500;
                        font-size: 24px;
                        line-height: 28px;
                        min-height: 28px;
                        padding: 6px 0px 6px 0;
                        box-sizing: content-box;
                    }
                    .dropList {
                        margin-right: 35px;
                    }
                `}
            </style>
        </div>
    );
};

export default ConceptExplorerTopPanel;
