import { useState, useContext, useEffect } from 'react';
import dynamic from 'next/dynamic';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import LoginContext from '../context/LoginContext';

const ManagerPinPageButton = dynamic(() =>
    import('../components/ManagerPinPageButton')
);

const ConceptExplorerNavAbout = (modalOpen, setModalOpen, project) => {
    const { user } = useContext(LoginContext);

    const defaultViewData = {
        title: '',
        description: '',
        isManager: false,
    };

    const [viewData, setViewData] = useState(defaultViewData);

    useEffect(() => {
        if (project) {
            const { title, description } = project;

            const isManager =
                user &&
                (user.groups.includes('manager') ||
                    user.groups.includes('admin'));
            setViewData({ title, description, isManager });
        }
    }, [project]);

    const [isOpen, setIsOpen] = useState(modalOpen);
    const close = () => {
        setIsOpen(false);
        setModalOpen(false);
    };

    return (
        <ModalTransition>
            {isOpen && (
                <Modal
                    actions={[{ text: 'Close', onClick: close }]}
                    onClose={close}
                    heading={viewData.title}
                >
                    <p>{viewData.description}</p>
                    {viewData.isManager && (
                        <ManagerPinPageButton newpin={project} />
                    )}
                </Modal>
            )}
        </ModalTransition>
    );
};

export default ConceptExplorerNavAbout;
