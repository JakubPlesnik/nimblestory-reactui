import { useContext } from 'react';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';

const HomepageHeader = ({ titleText }) => {
    const { organization } = useContext(LoginContext);
    const { themeColors } = useContext(UserContext);
    const { color1Darkest, colorHomeHeaderText } = themeColors;

    const { OrgHeroImage } = organization || {};
    return (
        <div
            className="homeHeader"
            style={{
                color: colorHomeHeaderText || color1Darkest,
                backgroundImage: OrgHeroImage
                    ? `url(${process.env.NEXT_PUBLIC_BACKEND_URL +
                          OrgHeroImage})`
                    : '',
                backgroundColor: color1Darkest,
            }}
        >
            <div>
                <div className="secondaryTitle">
                    <img
                        src="/appfiles/img/ns-logo-small-white@2x.png"
                        width="81"
                        height="16"
                    />{' '}
                </div>
                <div className="primaryTitle" data-cy="homepage-title">
                    <h1 style={{ color: themeColors.colorHeaderText }}>
                        {titleText}
                    </h1>
                </div>
            </div>

            <style jsx>
                {`
                    .homeHeader {
                        height: 176px;
                        padding: 14px 20px;
                        background-color: #f0f0f0;
                        display: flex;
                        justify-content: space-between;
                        flex-direction: row;
                        background-repeat: no-repeat;
                        background-size: cover;
                        background-position: right;
                        color: white;
                    }
                    .secondaryTitle {
                        font-weight: 500;
                        font-size: 12px;
                        margin-left: 16px;
                        margin-bottom: 17px;
                    }
                    .primaryTitle {
                        margin-left: 16px;
                        color: white;
                    }
                    .orgLogo {
                        width: 220px;
                        height: 200px;
                        background-size: 100%;
                        background-position: contain;
                        background-repeat: no-repeat;
                        margin: 0px 10px;
                    }
                    .orgLogoWrapper {
                        width: 220px;
                        margin-right: 72px;
                        padding-bottom: 10px;
                    }
                `}
            </style>
        </div>
    );
};
export default HomepageHeader;
