import { useRouter } from 'next/router';
import { useState, useContext, useEffect } from 'react';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';
import LoaderContext from '../context/LoaderContext';
import PortfolioContext from '../context/PortfolioContext';
import PaginationContext from '../context/PaginationContext';

import {
    fetchAssetTypes,
    fetchAssetStatus,
    fetchProjects,
    fetchCollections,
} from '../services/api/apiService';
import {
    projectSortOptions,
    filteredItems,
    projectFilterOptionActive,
    projectFilterOptionInactive,
    fetchProjectFilterOptions,
} from '../services/filteringProjects';

const NSDataPortfolio = ({ children }) => {
    const { pageReady, pathinfo, appState, setAppState } = useContext(
        UserContext
    );
    const { updateLoaders } = useContext(LoaderContext);
    const { isLoggedIn, organization } = useContext(LoginContext);

    const router = useRouter();

    const {
        assetTypes: PrevAssetTypesDynamic,
        assetStatus: PrevAssetStatusDynamic,
    } = appState.orgcache;

    const [projectfilters, setProjectFilters] = useState([]);
    const [projectFilterOptions, setProjectFilterOptions] = useState([]);
    const [totalProjects, setTotalProjects] = useState(null);
    const [totalPages, setTotalPages] = useState(null);
    const [filteredProjects, setFilteredProjects] = useState([]);
    const [projects, setProjects] = useState([]);
    const [collections, setCollections] = useState([]);
    const [projectsMeta, setProjectsMeta] = useState(null);
    const [assetTypesDynamic, setAssetTypes] = useState(
        PrevAssetTypesDynamic || null
    );
    const [assetStatusDynamic, setAssetStatus] = useState(
        PrevAssetStatusDynamic || null
    );

    const [searchFilterKeyword, setSearchFilterKeyword] = useState(false);
    const [selectedProjectSortOption, setProjectSortOption] = useState(0);

    const [orderBy, setOrderBy] = useState('df');
    const [filterBy, setFilterBy] = useState([]);
    const [loadMore, setLoadMore] = useState(false);
    const [hasMore, setHasMore] = useState(true);

    const { setOnReachedEnd } = useContext(PaginationContext) || {
        setOnReachedEnd: () => {},
    };

    useEffect(() => {
        updateLoaders({ title: 'portfoliodata', progress: 1 });
    }, []);

    async function loadData(page, sort, filter) {
        if (page === 1 && (!assetTypesDynamic || !assetStatusDynamic)) {
            await loadTypes();
        }

        const projectsData = await fetchProjects(
            pathinfo.organization,
            page,
            sort,
            filter
        );

        const collectionsData = await fetchCollections(
            pathinfo.organization,
            page,
            sort,
            filter
        );

        if (collectionsData) {
            setCollections(collectionsData.collections);
        }

        if (!projectsData) return false;

        const { results, meta } = projectsData;

        if (page === 1) {
            setProjects(results);
            updateLoaders({ title: 'portfoliodata', progress: 100 });
        } else {
            setProjects(prevState => [...prevState, ...results]);
        }

        const totalpages = meta.pagination.total_pages;
        setProjectsMeta(meta);
        setTotalPages(totalpages);
        setTotalProjects(meta.pagination.total);

        // set pagination
        if (page < totalpages) {
            setHasMore(true);
            setOnReachedEnd({
                trigger: () => {
                    setOnReachedEnd(false);
                    loadData(page + 1, sort, filter);
                },
            });
        } else {
            setHasMore(false);
        }
    }

    const loadTypes = async () => {
        const [assetTypeRes, assetStatusRes] = await Promise.all([
            fetchAssetTypes(),
            fetchAssetStatus(),
        ]);

        const { assetTypes } = assetTypeRes;
        const { assetStatus } = assetStatusRes;
        if (assetTypes && assetStatus) {
            setAssetTypes(assetTypes);
            setProjectFilterOptions(fetchProjectFilterOptions(assetTypes));
            setAssetStatus(assetStatus);

            setAppState({
                orgcache: { ...appState.orgcache, assetTypes, assetStatus },
            });
        }
    };

    useEffect(() => {
        if (
            pageReady &&
            isLoggedIn &&
            organization &&
            pathinfo.organization === organization.slug
        ) {
            setFilteredProjects([]);
            loadData(1, orderBy, []);
        }
    }, [pageReady, organization]);

    useEffect(() => {
        if (projects) {
            setFilteredProjects(projectsFilteredSorted(projects));
        }
    }, [searchFilterKeyword, projects, projectfilters]);

    const projectsFilteredSorted = localProjects => {
        const filteredProjects =
            localProjects &&
            projectfilters.length &&
            projectFilterOptions.length
                ? filteredItems(
                      localProjects,
                      projectfilters,
                      projectFilterOptions
                  )
                : localProjects;

        const filteredProjectsBySearch =
            !searchFilterKeyword || searchFilterKeyword.length < 2
                ? filteredProjects
                : filteredProjects.filter(item => {
                      let filterKeyword = searchFilterKeyword.replaceAll(
                          '*',
                          ''
                      );
                      filterKeyword = filterKeyword.replaceAll('(', '');
                      filterKeyword = filterKeyword.replaceAll(')', '');
                      filterKeyword = filterKeyword.replaceAll('+', '');
                      const re = new RegExp(`\\b${filterKeyword}`, 'i');
                      return re.test(item.title) || re.test(item.description);
                  });

        const sortedProjects = localProjects
            ? filteredProjectsBySearch.sort(
                  projectSortOptions[selectedProjectSortOption].sorter
              )
            : null;

        return sortedProjects;
    };

    useEffect(() => {
        const { orderBy: OrderByLocal } = projectSortOptions[
            selectedProjectSortOption
        ];
        setOrderBy(OrderByLocal);

        if (projectsMeta && Object.entries(projectsMeta).length > 0) {
            if (
                projectsMeta.pagination.current_page ===
                projectsMeta.pagination.total_pages
            ) {
                // local sort
                setFilteredProjects(projectsFilteredSorted(projects));
            } else {
                // reload first page with orderBy and filterBy
                loadData(1, OrderByLocal, filterBy);
            }
        }
    }, [selectedProjectSortOption]);

    // select filterOptions event handler
    const selectFilter = async newFiltersState => {
        try {
            const { current_page } = projectsMeta.pagination;
            // console.log("Selected__________!", current_page, total_pages, totalPage, newFiltersState)

            // return if last page
            if (current_page === totalPages) return;

            // get filter options array from selected options const
            const filterArray = newFiltersState.map(filter => filter.title);

            // return if unchecking
            if (
                filterArray.length < filterBy.length &&
                filterArray.length !== 0
            )
                return;

            setFilterBy(filterArray);

            // reload first page with orderBy and filterBy
            const projectsData = await fetchProjects(
                pathinfo.organization,
                null,
                orderBy,
                filterArray
            );
            setProjects(projectsData.results);
            setProjectsMeta(projectsData.meta);
        } catch (err) {
            throw new Error(err);
        }
    };

    useEffect(() => {
        const { projectfilter } = router.query;

        if (projectfilter == 'active') {
            setProjectFilters([projectFilterOptionActive]);
        } else if (projectfilter == 'inactive') {
            setProjectFilters([projectFilterOptionInactive]);
        } else if (projectfilters.length) {
            setProjectFilters([]);
        }
    }, [router.query.projectfilter]);

    return (
        <PortfolioContext.Provider
            value={{
                totalPages,
                totalProjects,
                setTotalPages,
                projects: filteredProjects,
                projectsMeta,
                projectfilters,
                setProjectFilters,
                assetTypesDynamic,
                assetStatusDynamic,
                searchFilterKeyword,
                setSearchFilterKeyword,
                selectedProjectSortOption,
                setProjectSortOption,
                selectFilter,
                hasMore,
                loadMore,
                setLoadMore,
                collections,
            }}
        >
            {children}
        </PortfolioContext.Provider>
    );
};
export default NSDataPortfolio;
