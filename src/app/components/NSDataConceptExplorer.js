import { useState, useEffect, useContext } from 'react';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';
import LoaderContext from '../context/LoaderContext';
import ConceptExplorerContext from '../context/ConceptExplorerContext';
import { fetchConceptExplorer } from '../services/api/apiService';

const NSDataConceptExplorer = ({ data, children }) => {
    const { pathinfo } = useContext(UserContext);
    const { isLoggedIn, organization } = useContext(LoginContext);

    const [solutions, setSolutions] = useState([]);
    const [solution, setSolution] = useState(null);

    const { updateLoaders } = useContext(LoaderContext);

    useEffect(() => {
        updateLoaders({ title: 'cesolutionsdata', progress: 1 });
    }, []);

    useEffect(() => {
        if (data) {
            setSolution(data);
            updateLoaders({ title: 'cesolutionsdata', progress: 100 });
        }
    }, [data]);

    useEffect(() => {
        if (!data) {
            if (solutions && solutions.length) {
                setSolutions([]);
            }
            if (
                isLoggedIn &&
                organization &&
                pathinfo.application == 'conceptexplorer' &&
                organization.slug === pathinfo.organization &&
                pathinfo.organization &&
                pathinfo.project
            ) {
                fetchConceptExplorer(
                    pathinfo.organization,
                    pathinfo.project
                ).then(newsolution => {
                    if (newsolution.data) {
                        setSolution(newsolution.data);
                    }
                    updateLoaders({ title: 'cesolutionsdata', progress: 100 });
                });
            }

            if (pathinfo.application == 'home') {
                updateLoaders({ title: 'cesolutionsdata', progress: 100 });
            }
        }
    }, [isLoggedIn, pathinfo.organization, pathinfo.application, organization]);

    return (
        <ConceptExplorerContext.Provider
            value={{
                solutions,
                solution,
            }}
        >
            {children}
        </ConceptExplorerContext.Provider>
    );
};
export default NSDataConceptExplorer;
