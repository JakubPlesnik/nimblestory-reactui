import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'antd';
import { message, Form } from 'antd';
import ContentAddTypes from './ContentAddTypes';
import ModalButton from './modalButton';

import { Row, Col } from 'antd';

import UPL from './formUpload';

const ModalUpload = ({
    isOpen,
    setOpen,
    refreshContent,
    projectSlug,
    orgSlug,
    contentStatuses,
    contentTypes,
}) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [selectedType, setSelectedType] = useState('');
    const [selectedTypeId, setSelectedTypeId] = useState(0);
    const [errorMessage, seterrorMessage] = useState('');

    const handleOk = () => {
        setLoading(true);
        seterrorMessage('');
        form.validateFields()
            .then(values => {
                onSubmit(values);
            })
            .catch(() => {
                setLoading(false);
            });
    };

    const handleCancel = () => {
        setIsModalVisible(false);
        setOpen(false);
        setLoading(false);
    };

    const [form] = Form.useForm();
    useEffect(() => {
        setIsModalVisible(isOpen);
    }, [isOpen]);

    const changeSelectedType = newType => {
        setSelectedType(newType);
        setSelectedTypeId(
            newType != ''
                ? ContentAddTypes.findIndex(x => x.type === newType)
                : 0
        );
    };

    const createNewContent = values => {
        const assetsBool =
            ContentAddTypes[selectedTypeId].formFields.reguestValue ===
            'assetsId';

        ContentAddTypes[selectedTypeId].apiCall(
            values.title,
            values.description ? values.description : '',
            values.status,
            projectSlug,
            values.typeSelector
                ? values.typeSelector
                : ContentAddTypes[selectedTypeId].apiType,
            assetsBool
                ? [values.assetsId]
                : values[
                      ContentAddTypes[selectedTypeId].formFields.reguestValue
                  ],
            onCreateSuccess,
            onCreateError
        );
        return;
    };

    const onCreateSuccess = () => {
        showSuccessMessage();
        setLoading(false);
        setIsModalVisible(false);
        form.resetFields();
        refreshContent();
        setOpen(false);
    };

    const showSuccessMessage = () => {
        message.success('Upload success');
    };

    const onCreateError = error => {
        if (!error) {
            seterrorMessage('Server is not responding');
        } else if (error.status === 401) {
            if (error.data.message) seterrorMessage(error.data.message);
            else seterrorMessage(error.data.error.message);
        } else seterrorMessage('Something went wrong.');
        setLoading(false);
    };

    const onSubmit = values => {
        createNewContent(values);
    };

    return (
        <Modal
            visible={isModalVisible}
            title={
                selectedType == ''
                    ? 'Content Type Selection'
                    : 'Add Content - ' +
                      ContentAddTypes[selectedTypeId].displayName
            }
            onCancel={() => handleCancel()}
            footer={
                selectedType != ''
                    ? [
                          <Button
                              key="resetSelection"
                              type="link"
                              style={{ float: 'left' }}
                              onClick={() => changeSelectedType('')}
                          >
                              Back
                          </Button>,
                          <Button key="back" onClick={() => handleCancel()}>
                              Cancel
                          </Button>,
                          <Button
                              key="submit"
                              type="primary"
                              loading={loading}
                              onClick={() => handleOk()}
                              data-cy="add-content-submit"
                          >
                              Submit
                          </Button>,
                      ]
                    : null
            }
        >
            {selectedType == '' ? (
                <Row gutter={[8, 16]} style={{ padding: '0 40px 0 40px' }}>
                    {ContentAddTypes.map((type, index) => {
                        return (
                            <Col key={index} span={8}>
                                <ModalButton
                                    contentType={type}
                                    changeSelectedType={changeSelectedType}
                                ></ModalButton>
                            </Col>
                        );
                    })}
                </Row>
            ) : (
                <UPL
                    form={form}
                    project={projectSlug}
                    organization={orgSlug}
                    errorMessage={errorMessage}
                    contentStatuses={contentStatuses}
                    contentTypes={contentTypes}
                    formFields={
                        selectedType == ''
                            ? ''
                            : ContentAddTypes[selectedTypeId].formFields
                    }
                ></UPL>
            )}
            <style jsx>{``}</style>
        </Modal>
    );
};

export default ModalUpload;
