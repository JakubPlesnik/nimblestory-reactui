import { useState } from 'react';
import { Button } from 'antd';

const ModalButton = ({ contentType, changeSelectedType }) => {
    const [fillColor, setFillColor] = useState('#8c8c8c');
    const [textColor, setTextColor] = useState('#000');

    return (
        <div
            className="btnModalUpload"
            onMouseEnter={() => {
                setFillColor('#1890FF');
                setTextColor('#1890FF');
            }}
            onMouseLeave={() => {
                setFillColor('#8c8c8c');
                setTextColor('#000');
            }}
        >
            <Button
                variant="outline-dark"
                size="large"
                onClick={() => changeSelectedType(contentType.type)}
                style={{
                    padding: '20px',
                    height: '120px',
                    width: '120px',
                    border: `1px solid ${fillColor}`,
                    borderRadius: '2px',
                }}
            >
                <contentType.addIcon
                    style={{ fontSize: '40px', color: `${fillColor}` }}
                ></contentType.addIcon>
                <p
                    style={{
                        color: `${textColor}`,
                        fontSize: '14px',
                        fontWeight: 500,
                        marginTop: '15px',
                    }}
                    data-cy="content-type-link"
                >
                    {contentType.displayName}
                </p>
            </Button>
        </div>
    );
};

export default ModalButton;
