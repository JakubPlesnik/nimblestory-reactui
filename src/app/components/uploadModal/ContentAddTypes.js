import ContentTypes from '../../enum/ContentType';
import {
    createNewContentRequestAsset,
    createNewContentRequestVideo,
    createNewContentRequestLink,
    createNewContentRequestModuleFile,
} from '../../services/api/apiService';
import {
    FileAddOutlined,
    VideoCameraAddOutlined,
    LinkOutlined,
    ClusterOutlined,
} from '@ant-design/icons';

const PCTAsset = {
    type: 'PCTAsset',
    name: ContentTypes.PCTAsset,
    displayName: 'Files',
    apiType: 'Document',
    addIcon: FileAddOutlined,
    formFields: {
        reqDragger: true,
        reguestValue: 'assetsId',
        info: 'Attach files and documents to upload to your Project.',
        typeSelector: true,
    },
    apiCall: createNewContentRequestAsset,
};
const PCTVideo = {
    type: 'PCTVideo',
    name: ContentTypes.PCTVideo,
    displayName: 'Video',
    apiType: 'Video',
    addIcon: VideoCameraAddOutlined,
    formFields: {
        reqDragger: false,
        link: true,
        reguestValue: 'video',
        info: 'Link a video to your Project.',
    },
    apiCall: createNewContentRequestVideo,
};
const PCTLink = {
    type: 'PCTLink',
    name: ContentTypes.PCTLink,
    displayName: 'Web Link',
    apiType: 'ContentLink',
    addIcon: LinkOutlined,
    formFields: {
        reqDragger: false,
        link: true,
        reguestValue: 'link',
        info: 'Link another website or app to your Project.',
    },
    apiCall: createNewContentRequestLink,
};
const PCTModuleFile = {
    type: 'PCTModuleFile',
    name: ContentTypes.PCTModuleFile,
    displayName: 'Web Bundle',
    apiType: 'Interactive',
    addIcon: ClusterOutlined,
    formFields: {
        reqDragger: true,
        reguestValue: 'moduleFileId',
        info: 'Attach a web bundle to embed in your Project.',
    },
    apiCall: createNewContentRequestModuleFile,
};

const ContentAddTypes = [PCTAsset, PCTVideo, PCTLink, PCTModuleFile];

export default ContentAddTypes;
