import React from 'react';
import { getToken } from '../../services/api/actions';
import { InboxOutlined } from '@ant-design/icons';

import { Form, Input, Select, Upload, message, Alert } from 'antd';

const { TextArea } = Input;
const { Option } = Select;
const { Dragger } = Upload;

const UploadForm = ({
    form,
    errorMessage,
    project,
    organization,
    contentStatuses,
    contentTypes,
    formFields,
}) => {
    const apiUrl = process.env.NEXT_PUBLIC_API_URL;

    const props = {
        name: 'file',
        data: { project: project, organization: organization },
        multiple: false,
        maxCount: 1,
        action: apiUrl + '/asset/create',
        headers: {
            Authorization: `Bearer ${getToken()}`,
        },
        onChange(info) {
            const { status } = info.file;
            if (status === 'done') {
                message.success(
                    `${info.file.name} file uploaded successfully.`
                );
                const tempValue = {};
                tempValue[formFields.reguestValue] =
                    info.file.response.result[0].id;
                form.setFieldsValue({
                    ...tempValue,
                });
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    const onStatusChanged = value => {
        form.setFieldsValue({
            status: value,
        });
    };

    const onTypeSelectorChanged = value => {
        form.setFieldsValue({
            typeSelector: value,
        });
    };

    const ShowAlert = () => (
        <Alert message={errorMessage} type="error" showIcon />
    );

    return (
        <div>
            {formFields.info && (
                <div className="formInfo">{formFields.info}</div>
            )}
            <Form layout={'vertical'} form={form}>
                <Form.Item
                    name="title"
                    label="Title"
                    hasFeedback
                    rules={[
                        { required: true, message: 'Please provide a title' },
                    ]}
                >
                    <Input maxLength={60} data-cy="add-content-title-input" />
                </Form.Item>
                <Form.Item name="description" label="Description">
                    <TextArea maxLength={200} rows={2} />
                </Form.Item>
                <Form.Item
                    rules={[{ required: true }]}
                    name="status"
                    label="Status"
                >
                    <Select
                        placeholder="Select project status"
                        onChange={onStatusChanged}
                        data-cy="add-content-status-select"
                    >
                        {contentStatuses.map((option, key) => (
                            <Option
                                key={key}
                                value={option.label}
                                data-cy="add-content-select-label"
                            >
                                {option.label}
                            </Option>
                        ))}
                    </Select>
                </Form.Item>

                {formFields.link && (
                    <Form.Item
                        name={formFields.reguestValue}
                        label="Link"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Please provide a link',
                            },
                        ]}
                    >
                        <Input maxLength={200} data-cy="add-content-link" />
                    </Form.Item>
                )}

                {formFields.typeSelector && (
                    <Form.Item
                        rules={[{ required: true }]}
                        name="typeSelector"
                        label="Type Selector"
                    >
                        <Select
                            placeholder="Select type of asset"
                            onChange={onTypeSelectorChanged}
                            data-cy="add-content-selector-select"
                        >
                            {contentTypes.map((option, key) => (
                                <Option
                                    key={key}
                                    value={option.label}
                                    data-cy="add-content-selector-label"
                                >
                                    {option.label}
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                )}

                {formFields.reqDragger && (
                    <Form.Item
                        rules={[
                            {
                                required: true,
                                message: 'Please provide a file',
                            },
                        ]}
                        name={formFields.reguestValue}
                    >
                        <Dragger {...props} data-cy="add-content-file">
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">
                                Click or drag file to this area to upload
                            </p>
                        </Dragger>{' '}
                        {errorMessage !== '' ? <ShowAlert /> : null}
                    </Form.Item>
                )}
            </Form>
            <style jsx>
                {`
                    .formInfo {
                        margin-bottom: 24px;
                        margin-right: 24px;
                    }
                `}
            </style>
        </div>
    );
};

export default UploadForm;
