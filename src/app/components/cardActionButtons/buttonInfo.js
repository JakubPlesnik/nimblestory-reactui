import { InfoCircleOutlined } from '@ant-design/icons';

const buttonInfo = ({ disabled = false }) => {
    return (
        <InfoCircleOutlined
            key="edit"
            style={
                disabled
                    ? { cursor: 'default', color: 'gray', visibility: 'hidden' }
                    : {}
            }
        />
    );
};
export default buttonInfo;
