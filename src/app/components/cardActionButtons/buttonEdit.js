import { EditOutlined } from '@ant-design/icons';

const buttonEdit = ({ disabled = false }) => {
    return (
        <EditOutlined
            key="edit"
            style={
                disabled
                    ? { cursor: 'default', color: 'gray', display: 'none' }
                    : {}
            }
        />
    );
};
export default buttonEdit;
