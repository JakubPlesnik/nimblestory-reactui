import { PushpinFilled, PushpinOutlined } from '@ant-design/icons';

const buttonPin = ({ disabled = false, isPinned = false }) => {
    if (isPinned) {
        return (
            <PushpinFilled
                rotate={315}
                key="pinned"
                style={{ color: '#002545' }}
            />
        );
    } else {
        return (
            <PushpinOutlined
                rotate={315}
                key="unpinned"
                style={
                    disabled
                        ? { cursor: 'default', color: 'gray', display: 'none' }
                        : {}
                }
            />
        );
    }
};
export default buttonPin;
