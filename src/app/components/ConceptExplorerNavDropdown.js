import { useState, useContext, useEffect, useRef } from 'react';
import cn from 'classnames';
import dynamic from 'next/dynamic';
import Modal, {
    ModalBody,
    ModalHeader,
    ModalTransition,
} from '@atlaskit/modal-dialog';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';
import ConceptExplorerNavToggle from './ConceptExplorerNavToggle';
import ConceptExplorerNavFooter from './ConceptExplorerNavFooter';

const ManagerPinPageButton = dynamic(() =>
    import('../components/ManagerPinPageButton')
);

const SGM_1ST_VISUAL_NO_WAYPOINTS = 0;
const SGM_1ST_VISUAL_ALL_WAYPOINTS = 1;
const SGM_1ST_JOURNEY = 2;

const ConceptExplorerNavDropdown = ({
    solution,
    dropList,
    dropListSelectedItem,
    setVisual,
    setJourney,
    setWaypoint,
    setLayoutState,
    setDropListSelectedItem,
}) => {
    const wrapperRef = useRef(null);
    const [showMenu, setShowMenu] = useState(false);
    const [showText, setShowText] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const [viewData, setViewData] = useState(defaultViewData);
    const { user } = useContext(LoginContext);
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    const close = () => setIsOpen(false);
    const open = () => setIsOpen(true);

    const defaultViewData = {
        title: '',
        description: '',
        isManager: false,
    };

    const useOutsideAlerter = ref => {
        useEffect(() => {
            const handleClickOutside = event => {
                if (ref.current && !ref.current.contains(event.target)) {
                    setShowMenu(false);
                }
            };

            document.addEventListener('mousedown', handleClickOutside);
            return () => {
                document.removeEventListener('mousedown', handleClickOutside);
            };
        }, [ref, showMenu]);
    };

    useOutsideAlerter(wrapperRef);

    const isActive = item => {
        return dropList.indexOf(item) === parseInt(dropListSelectedItem);
    };

    const openVisualNoWaypoints = index => {
        setDropListSelectedItem(index);
        setLayoutState({ tab: 0 });
        setVisual(dropList[index]);
        setWaypoint(false);
        setShowText(`Visual - ${dropList[index].title} (No Waypoints)`);
    };

    const openVisualAllWaypoints = index => {
        setDropListSelectedItem(index);
        setLayoutState({ tab: 0 });
        setVisual(dropList[index]);
        setWaypoint(false);
        setShowText(`Visual - ${dropList[index].title} (All Waypoints)`);
    };

    const openJourney = index => {
        setDropListSelectedItem(index);
        setLayoutState({ tab: 1 });
        setJourney(dropList[index]);
        setShowText(`Journey - ${dropList[index].title}`);
    };

    const openAbout = () => {
        setLayoutState({ tab: 2 });
        open();
    };

    useEffect(() => {
        if (window.performance) {
            if (performance.navigation.type == 0) {
                openVisualNoWaypoints(0);
            } else {
                const countVisuals = solution.visuals.length;
                const { selfguidedmode } = solution;

                if (
                    selfguidedmode === SGM_1ST_JOURNEY &&
                    countVisuals < dropList.length
                ) {
                    openJourney(countVisuals);
                } else if (selfguidedmode === SGM_1ST_VISUAL_ALL_WAYPOINTS) {
                    openVisualAllWaypoints(1);
                } else if (selfguidedmode === SGM_1ST_VISUAL_NO_WAYPOINTS) {
                    openVisualNoWaypoints(0);
                } else {
                    openAbout();
                }
            }
        }

        if (solution) {
            const { title, description } = solution;
            const isManager =
                user &&
                (user.groups.includes('manager') ||
                    user.groups.includes('admin'));

            setViewData({ title, description, isManager });
        }
    }, [solution]);

    let controlLabel = showText;
    controlLabel = showMenu ? 'Select one...' : controlLabel;

    return (
        <div ref={wrapperRef} className="jdropdownContainer">
            <p className="concept-explorer-controls">
                Concept Explorer Controls
            </p>
            <ConceptExplorerNavToggle onClick={() => setShowMenu(!showMenu)}>
                {controlLabel}
            </ConceptExplorerNavToggle>

            <ul className={cn('jmenu', { hidden: !showMenu })}>
                {!!dropList &&
                    dropList.map((list, key) => {
                        const countVisuals = solution.visuals.length;

                        if (key < countVisuals) {
                            if (key % 2 === 0) {
                                return (
                                    <li
                                        className={cn('jListItem', {
                                            active: isActive(list),
                                        })}
                                        key={key}
                                        onClick={() => {
                                            openVisualNoWaypoints(key);
                                            setShowMenu(false);
                                        }}
                                    >
                                        <span>
                                            Visual -{list.title}
                                            (No WayPoints)
                                        </span>
                                    </li>
                                );
                            }
                            return (
                                <li
                                    className={cn('jListItem', {
                                        active: isActive(list),
                                    })}
                                    key={key}
                                    onClick={() => {
                                        openVisualAllWaypoints(key);
                                        setShowMenu(false);
                                    }}
                                >
                                    <span>
                                        Visual -{list.title}
                                        (All WayPoints)
                                    </span>
                                </li>
                            );
                        }
                        return (
                            <li
                                className={cn('jListItem', {
                                    active: isActive(list),
                                })}
                                key={key}
                                onClick={() => {
                                    openJourney(key);
                                    setShowMenu(false);
                                }}
                            >
                                <span>Journey -{list.title}</span>
                            </li>
                        );
                    })}

                <li
                    className={cn('jListItem')}
                    onClick={() => {
                        openAbout();
                        setShowMenu(false);
                    }}
                >
                    <span>About -{solution.title}</span>
                </li>
            </ul>

            <ModalTransition>
                {isOpen && (
                    <Modal
                        onClose={close}
                        components={{
                            Footer: ConceptExplorerNavFooter,
                        }}
                        width="500px"
                        height="450px"
                    >
                        <ModalHeader
                            style={{
                                padding: 50,
                                fontSize: 25,
                            }}
                        >
                            {viewData.title}
                        </ModalHeader>
                        <ModalBody
                            style={{
                                paddingLeft: 50,
                                paddingRight: 50,
                            }}
                        >
                            {viewData.description}
                            {viewData.isManager && (
                                <ManagerPinPageButton newpin={solution} />
                            )}
                        </ModalBody>
                    </Modal>
                )}
            </ModalTransition>

            <style jsx>
                {`
                    .jdropdownContainer {
                        position: relative;
                        height: 40px;
                    }
                    .concept-explorer-controls {
                        font-size: 12px;
                        text-transform: uppercase;
                        font-weight: bold;
                        font-style: ${color1Darkest};
                        margin-top: 0px;
                        padding-top: 10px;
                        margin-bottom: 0px;
                    }
                    ul.jmenu {
                        background-color: #f3f5f8;
                        width: 483px;
                        position: absolute;
                        list-style: none;
                        margin: 0;
                        padding: 14px 0px;
                        z-index: 999999;
                        position: fixed;
                    }
                    .jListItem {
                        line-height: 40px;
                        cursor: pointer;
                        border-bottom: 1px solid #cecece;
                    }
                    .jListItem > span {
                        padding-left: 14px;
                    }
                    .jListItem.active {
                        background-color: ${color1Darkest};
                        color: white;
                    }
                    .jListItem:hover {
                        background-color: #cecece;
                    }
                `}
            </style>
        </div>
    );
};
export default ConceptExplorerNavDropdown;
