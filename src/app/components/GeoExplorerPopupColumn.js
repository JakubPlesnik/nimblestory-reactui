import cn from 'classnames';

const GeoExplorerPopupColumn = ({ color, label, value, index, length }) => (
    <div
        className={cn(
            'column',
            { first: index === 0 },
            { last: index === length - 1 }
        )}
    >
        <div className={`indicator ${color}`} />
        <div className="label">{label}</div>
        <div className="value">{value}</div>
        <style jsx>
            {`
                .green {
                    background: #1cbe02;
                }
                .orange {
                    background: #fb8c00;
                }
                .blue {
                    background: #1f506e;
                }
                .red {
                    background: #dc0808;
                }
                .yellow {
                    background: #f9a502;
                }
                .neutral {
                    background: #00897b;
                }
                .column {
                    width: 80px;
                    text-align: center;
                    border-left: 1px solid #c4c4c4;
                    border-right: 1px solid #c4c4c4;
                    position: relative;
                }
                .column.first {
                    border-left: none;
                }
                .column.last {
                    border-right: none;
                }

                .indicator {
                    height: 5px;
                }
                .label {
                    margin: 16px 10px;
                    color: #000000;
                    font-weight: bold;
                    font-size: 11px;
                    line-height: 11px;
                }
                .value {
                    position: absolute;
                    width: 100%;
                    top: 60px;

                    letter-spacing: -0.1px;
                    text-transform: capitalize;
                    font-weight: bold;
                    font-size: 14px;
                    line-height: 14px;
                    color: #293e57;
                }
            `}
        </style>
    </div>
);

export default GeoExplorerPopupColumn;
