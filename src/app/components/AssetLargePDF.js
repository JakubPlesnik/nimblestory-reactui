import PropTypes from 'prop-types';
import { useState, useEffect, useContext } from 'react';
import { fetchAsset } from '../services/api/apiService';
import LoaderContext from '../context/LoaderContext';
const AssetLargePDF = ({ assetId }) => {
    const [pdfUrl, setPdfUrl] = useState('');
    const { updateLoaders } = useContext(LoaderContext);

    useEffect(() => {
        if (Number.isInteger(assetId)) {
            updateLoaders({ title: 'largePDF', progress: 1 });
            fetchAsset(assetId, setPdfUrl, null);
            return;
        }
    }, []);

    useEffect(() => {
        if (!pdfUrl) return;
        updateLoaders({ title: 'largePDF', progress: 100 });
    }, [pdfUrl]);

    return (
        <div className="assetLarge" style={{ height: 'inherit' }}>
            {pdfUrl && (
                <iframe
                    title="Preview"
                    className="assetiFrame"
                    src={pdfUrl}
                    width="100%"
                    height="100%"
                    data-cy="asset-display-pdf"
                />
            )}
            <style jsx>
                {`
                    .assetiFrame {
                        width: 100%;
                        height: 100%;
                        border: 0;
                    }
                `}
            </style>
        </div>
    );
};

AssetLargePDF.propTypes = {
    assetId: PropTypes.number.isRequired,
};
export default AssetLargePDF;
