import * as React from 'react';
import { useState, useContext, useReducer } from 'react';
import Link from 'next/link';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import UserContext from '../context/UserContext';
import PortfolioContext from '../context/PortfolioContext';
import AppLinkData from '../services/applink';
import { saveAsset } from '../services/cmsapi-manage';
import ManageInputArea from './ManageInputArea';
import ManageInputMultipleOption from './ManageInputMultipleOption';
import ManageInputDate from './ManageInputDate';
import ManageButtonUploadFile from './ManageButtonUploadFile';
import { getFieldKeyByValue } from '../services/appfields';

import AppDropdownActionMenu from './AppDropdownActionMenu';

const isDataValid = data => {
    if (!data.ProjSlug) {
        return false;
    }

    if (!data.title || !data.title.trim().length) {
        return false;
    }
    return true;
};

const ManageTableAssets = props => {
    const { pathinfo } = useContext(UserContext);
    const { organization } = pathinfo;

    return (
        <Table bordered size="sm">
            <thead>
                <tr>
                    <th />
                    {/* <th className="minwidth-medium">Project Name</th> */}
                    <th className="minwidth-medium">Content Name</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Updated</th>
                    <th>Version</th>
                    <th>Thumbnail</th>
                    <th>Viewable</th>
                    <th>View</th>
                </tr>
            </thead>
            <tbody>
                {props.assets &&
                    props.assets.map(asset => (
                        <ManageTableAssetsRow
                            {...props}
                            key={asset.id}
                            asset={asset}
                            organization={organization}
                            editView={
                                asset.id.toString().startsWith('new') ? 1 : 0
                            }
                        />
                    ))}
            </tbody>
            <style jsx>
                {`
                    .minwidth-small {
                        min-width: 88px;
                    }
                    .minwidth-medium {
                        min-width: 210px;
                    }
                    .minwidth-long {
                        min-width: 310px;
                    }
                `}
            </style>
        </Table>
    );
};

const ManageTableAssetsRow = props => {
    const [isEditView, setEditView] = useState(props.editView);
    return isEditView === 0 ? (
        <ManageTableAssetsRowView {...props} setEditView={setEditView} />
    ) : (
        <ManageTableAssetsRowEdit {...props} setEditView={setEditView} />
    );
};

const ManageTableAssetsRowView = ({
    organization,
    projects,
    asset,
    setEditView,
    updateAssetData,
}) => {
    const {
        assetTypesDynamic: assetTypes,
        assetStatusDynamic: assetStatus,
    } = useContext(PortfolioContext);

    const btnInlineEdit = (
        <Button variant="outline-info" size="sm" onClick={() => setEditView(1)}>
            Edit
        </Button>
    );

    let assetViewLink;
    let project;
    if (asset.ProjSlug) {
        project = projects.find(({ slug }) => slug === asset.ProjSlug);
    }
    if (project) {
        const linkData = AppLinkData({
            organization,
            project: project.slug,
            asset: asset.slug,
        });
        assetViewLink = (
            <Link href={linkData.href} as={linkData.as}>
                <a className="btn btn-secondary" target="_blank">
                    Live
                </a>
            </Link>
        );
    } else {
        assetViewLink = <div />;
        project = { title: 'N/A' };
    }
    // const assetCmsLink = (
    //   <a className="btn btn-secondary" target="_blank" href={asset.cpEditUrl}>
    //     CMS
    //   </a>
    // )

    const captionForField = field => {
        const captions = {
            filePreview: 'High-res JPG or PNG.\nUsed to generate thumbnails.',
            fileViewable: 'Optimized PDF\nor high-res JPG',
            fileSource: 'The original file for download',
        };
        return captions[field] || '';
    };

    return (
        <tr>
            <td>{btnInlineEdit}</td>
            {/* <td>{project.title || ""}</td> */}
            <td>{asset.title}</td>
            <td>
                {asset.AssetType.map(item => {
                    const found = assetTypes.find(i => i.value == item);
                    return found ? found.label : '';
                }).join(', ')}
            </td>
            <td>
                {asset.AssetStatus.map(item => {
                    const found = assetStatus.find(i => i.value == item);
                    return found ? found.label : '';
                }).join(', ')}
            </td>
            <td>
                {asset.AssetUpdatedUnix
                    ? new Date(
                          asset.AssetUpdatedUnix * 1000
                      ).toLocaleDateString()
                    : ''}
            </td>
            <td>{asset.AssetVersion}</td>

            {['File2(LgPrev)', 'File3(Viewable)'].map((prop, index) => {
                if (
                    prop === 'File3(Viewable)' &&
                    asset.entryType === 'magaAssetVideo'
                ) {
                    return (
                        <td key={index} className="centered">
                            {asset.Video && (
                                <a
                                    className="btn btn-outline-info"
                                    target="_blank"
                                    rel="noreferrer"
                                    href={asset.Video}
                                >
                                    Open Video
                                </a>
                            )}
                            {!asset.Video && <span>-</span>}
                        </td>
                    );
                }
                if (
                    prop === 'File3(Viewable)' &&
                    (asset.entryType === 'magaAssetLink' ||
                        asset.entryType === 'magaAssetModuleFile')
                ) {
                    return (
                        <td key={index} className="centered">
                            {asset.Link && (
                                <a
                                    className="btn btn-outline-info"
                                    target="_blank"
                                    rel="noreferrer"
                                    href={asset.Link}
                                >
                                    Open Module
                                </a>
                            )}
                        </td>
                    );
                }
                return (
                    <td
                        key={`ar-${asset.id}-${index}`}
                        className="cellForButtons"
                    >
                        <ManageButtonUploadFile
                            key={`uf-${asset.id}-${index}`}
                            entryId={asset.id}
                            field={getFieldKeyByValue(prop)}
                            caption={captionForField(getFieldKeyByValue(prop))}
                            onSuccess={updatedFile => {
                                const newdata = Object.assign(
                                    {},
                                    asset,
                                    updatedFile
                                );
                                updateAssetData(newdata);
                            }}
                        />
                        <br />
                        {asset[prop] && (
                            <a
                                className="btn btn-outline-info"
                                href={asset[prop]}
                                target="_blank"
                                rel="noreferrer"
                            >
                                Open
                            </a>
                        )}
                    </td>
                );
            })}

            {/* <td>
        {asset.addtlfiles.map((prop, index) => {
          return (
            <React.Fragment key={`additional-${index}`}>
              <a href={prop.file}>{prop.label}</a>
              <br />
            </React.Fragment>
          )
        })}
      </td> */}

            <td className="cellForButtons">
                {assetViewLink}
                {/* <br />
        <br />
        {assetCmsLink} */}
            </td>
            <style jsx>
                {`
                    .cellForButtons {
                        line-height: 4px;
                        text-align: center;
                    }
                `}
            </style>
        </tr>
    );
};

const ManageTableAssetsRowEdit = ({
    projects,
    asset,
    setEditView,
    managerCancelNewAsset,
    updateAssetData,
}) => {
    const {
        assetTypesDynamic: assetTypes,
        assetStatusDynamic: assetStatus,
    } = useContext(PortfolioContext);

    const { pathinfo } = useContext(UserContext);

    const saveField = (state, updateKeyValue) => {
        const fieldsChanged = state.fieldsChanged || [];

        Object.keys(updateKeyValue).forEach(fieldName => {
            if (!fieldsChanged.includes(fieldName)) {
                fieldsChanged.push(fieldName);
            }
        });

        fieldsChanged.push(...Object.keys(updateKeyValue));
        return Object.assign({}, state, updateKeyValue, { fieldsChanged });
    };
    const onSaveRow = async () => {
        if (!isSaving) {
            if (
                !dataToSave.id.toString().startsWith('new') &&
                !dataToSave.fieldsChanged
            ) {
                return false;
            }

            if (!isDataValid(dataToSave)) {
                return false;
            }

            setIsSaving(true);

            let dataToPush;
            if (dataToSave.id.toString().startsWith('new')) {
                dataToPush = {
                    ...dataToSave,
                    org: pathinfo.organization,
                };
            } else {
                dataToPush = {
                    id: dataToSave.id,
                    ProjSlug: dataToSave.ProjSlug,
                    entryType: dataToSave.entryType,
                    org: pathinfo.organization,
                };
                dataToSave.fieldsChanged.forEach(fieldName => {
                    dataToPush[fieldName] = dataToSave[fieldName];
                });
            }

            const saveApiResult = await saveAsset(dataToPush);
            if (!saveApiResult || !saveApiResult.success) {
                setIsSaving(false);
                return;
            }
            const syncedDataToSave = Object.assign(
                {},
                dataToSave,
                saveApiResult.item
            );
            updateAssetData(syncedDataToSave);
            setIsSaving(false);
            setEditView(0);
        }
    };
    const onCancel = () => {
        if (asset.id === 'new') {
            managerCancelNewAsset();
        } else {
            setEditView(0);
        }
    };

    const [dataToSave, saveFieldKeyValue] = useReducer(saveField, asset);
    const [isSaving, setIsSaving] = useState(false);

    const btnInlineSave = (
        <Button variant="primary" size="sm" onClick={() => onSaveRow()}>
            {isSaving ? 'Saving...' : 'Save'}
        </Button>
    );
    const btnInlineCancelEdit = (
        <Button variant="link" size="sm" onClick={() => onCancel()}>
            Cancel
        </Button>
    );

    // const defaultStatusOptions = ['Approved', 'Draft', 'Sketch', 'Working Document', 'Final Deliverable']
    // const assetStatusOptions = [...new Set([...defaultStatusOptions,  ...dataToSave['AssetStatus']])].map(item => ({label: item, value: item}) ) //remove duplicates, make key-value

    // const defaultTypesOptions = assetTypes//.map(value => ({label: assetTypes[value], value}))

    let projectDisplay;
    const project = dataToSave.ProjSlug
        ? projects.find(({ slug }) => slug === dataToSave.ProjSlug)
        : null;

    if (dataToSave.id.startsWith('new')) {
        const selectedProjectIndex = dataToSave.ProjSlug
            ? projects.findIndex(({ slug }) => slug === dataToSave.ProjSlug)
            : null;
        projectDisplay = (
            <AppDropdownActionMenu
                title={project ? project.title : 'Select Project'}
                menuheader="Projects:"
                options={projects.map(({ title }) => title)}
                selectedIndex={selectedProjectIndex}
                onSelect={index => {
                    saveFieldKeyValue({ ProjSlug: projects[index].slug });
                }}
            />
        );
    } else {
        projectDisplay = project.title;
    }
    const isImport = asset.id.startsWith('new') && asset.id !== 'new';
    return (
        <React.Fragment>
            {!isImport && (
                <tr className="editMode editHeader">
                    <td />
                    <td>Project:</td>
                    <td>Content Name:</td>
                    <td>Select type tag(s):</td>
                    <td>Select status tag(s):</td>
                    <td>Select date (optional):</td>
                    <td>Version (optional):</td>
                    <td colSpan="3">File upload is available after saving.</td>

                    <td />
                </tr>
            )}
            <tr className="editMode">
                <td className="centered">
                    {btnInlineSave}
                    <br />
                    {!isImport && btnInlineCancelEdit}
                </td>
                <td>{projectDisplay}</td>
                <td>
                    <ManageInputArea
                        data={dataToSave}
                        field="title"
                        onSave={saveFieldKeyValue}
                    />
                </td>
                <td>
                    <ManageInputMultipleOption
                        data={dataToSave}
                        field="AssetType"
                        onSave={saveFieldKeyValue}
                        options={assetTypes}
                        tagid={dataToSave.id}
                    />
                    {dataToSave.AssetTypeIncorrect && (
                        <div className="importwarning">
                            Unrecognized value in{' '}
                            <span>{dataToSave.AssetTypeIncorrect}</span>
                        </div>
                    )}
                </td>
                <td>
                    <ManageInputMultipleOption
                        data={dataToSave}
                        field="AssetStatus"
                        onSave={saveFieldKeyValue}
                        options={assetStatus}
                        tagid={dataToSave.id}
                    />
                    {dataToSave.AssetStatusIncorrect && (
                        <div className="importwarning">
                            Unrecognized value in{' '}
                            <span>{dataToSave.AssetStatusIncorrect}</span>
                        </div>
                    )}
                </td>
                <td>
                    <ManageInputDate
                        data={dataToSave}
                        field="AssetUpdatedUnix"
                        onSave={saveFieldKeyValue}
                    />
                </td>
                <td>
                    <ManageInputArea
                        data={dataToSave}
                        field="AssetVersion"
                        onSave={saveFieldKeyValue}
                    />
                </td>

                {['File2(LgPrev)', 'File3(Viewable)', 'File4(Source)'].map(
                    (prop, index) => {
                        if (
                            prop === 'File3(Viewable)' &&
                            asset.entryType === 'magaAssetVideo'
                        ) {
                            return (
                                <td key={index}>
                                    <span>Video Link</span>
                                    <ManageInputArea
                                        data={dataToSave}
                                        field="Video"
                                        onSave={saveFieldKeyValue}
                                    />
                                </td>
                            );
                        }
                        if (
                            prop === 'File3(Viewable)' &&
                            (asset.entryType === 'magaAssetLink' ||
                                asset.entryType === 'magaAssetModuleFile')
                        ) {
                            return (
                                <td key={index}>
                                    {asset.entryType === 'magaAssetLink' && (
                                        <>
                                            <span>Module Link</span>
                                            <ManageInputArea
                                                data={dataToSave}
                                                field="Link"
                                                onSave={saveFieldKeyValue}
                                            />
                                        </>
                                    )}
                                    <span>Module Width</span>
                                    <ManageInputArea
                                        data={dataToSave}
                                        field="width"
                                        onSave={saveFieldKeyValue}
                                    />
                                    <span>Module Height</span>
                                    <ManageInputArea
                                        data={dataToSave}
                                        field="height"
                                        onSave={saveFieldKeyValue}
                                    />
                                </td>
                            );
                        }
                        return <td key={index} />;
                    }
                )}

                <td />
            </tr>
            <style jsx>
                {`
                    .importwarning {
                        font-size: 11px;
                        margin: 4px;
                        color: #c30000;
                    }
                    tr.editMode {
                        background-color: #fffbd5;
                    }
                    tr.editHeader {
                        font-size: 12px;
                        font-weight: bold;
                    }
                `}
            </style>
        </React.Fragment>
    );
};

export default ManageTableAssets;
