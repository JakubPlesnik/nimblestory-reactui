import { useState, useContext, useEffect } from 'react';
import Link from 'next/link';
import UserContext from '../context/UserContext';
import LoaderContext from '../context/LoaderContext';
import ConceptExplorerContext from '../context/ConceptExplorerContext';
import Project from '../components/cards/AppCardConteptExplorer';
import AppLinkData from '../services/applink';

const ConceptExplorerSolutionsView = () => {
    const { pathinfo, themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    const { solutions } = useContext(ConceptExplorerContext);
    const { updateLoaders } = useContext(LoaderContext);

    const [pageReady, setPageReady] = useState(false);

    const backToHomeLink = AppLinkData({
        homeLink: pathinfo,
    });
    const backToConceptLink = AppLinkData({
        ConceptExplorerLink: pathinfo,
    });

    useEffect(() => {
        updateLoaders({ title: 'cesolutions', progress: 1 });
    }, []);

    useEffect(() => {
        if (solutions) {
            setPageReady(true);
            updateLoaders({ title: 'cesolutions', progress: 100 });
        }
    }, [solutions]);

    const countProjects = solutions.length > 0 ? `(${solutions.length})` : '';

    return (
        <div className="pagearea">
            <div
                className="appHeader"
                style={{
                    color: color1Darkest,
                }}
            >
                <div className="secondaryTitle">
                    <Link {...backToHomeLink}>
                        <a
                            style={{
                                color: color1Darkest,
                            }}
                        >
                            Home
                        </a>
                    </Link>

                    <span> / </span>

                    <Link {...backToConceptLink}>
                        <a
                            style={{
                                color: color1Darkest,
                            }}
                        >
                            Concept Explorer
                        </a>
                    </Link>
                </div>

                <div className="primaryTitle">
                    Solutions
                    {countProjects}
                </div>
            </div>

            <div className="divider15" />

            <div className="assetsContainer">
                {pageReady &&
                    solutions.map(item => (
                        <Project
                            key={`${item.id}-projects`}
                            organization={pathinfo.organization}
                            project={item}
                        />
                    ))}
            </div>
            <style jsx>
                {`
                    .assetsContainer {
                        margin-left: 29px;
                        display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                    }
                    .pageTitle {
                        margin-left: 31px;
                    }
                    .divider15 {
                        height: 14px;
                        clear: both;
                    }

                    .appHeader {
                        padding: 16px 36px;
                    }
                    .secondaryTitle {
                        font-weight: 500;
                        font-size: 12px;
                    }
                    .primaryTitle {
                        font-weight: 500;
                        font-size: 24px;
                        line-height: 28px;
                        padding: 9px 0px;
                    }
                `}
            </style>
        </div>
    );
};
export default ConceptExplorerSolutionsView;
