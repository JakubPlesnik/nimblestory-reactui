import { useContext } from 'react';
import cn from 'classnames';
import UserContext from '../context/UserContext';

const CustomToggle = ({ children, onSelectPrev, onSelectNext }) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    return (
        <div className="ddcontrolbutton">
            <a
                className="ddcarret ddcarret-left"
                onClick={e => {
                    e.preventDefault();
                    onSelectPrev();
                }}
            />
            <div>{children}</div>
            <a
                className="ddcarret ddcarret-right"
                onClick={e => {
                    e.preventDefault();
                    onSelectNext();
                }}
            />

            <style jsx>
                {`
                    .ddcarret {
                        display: block;
                        background-color: ${color1Darkest};
                        width: 38px;
                        height: 38px;
                        display: inline-block;
                        color: white;
                        text-align: center;
                        font-size: 20px;
                        line-height: 58px;

                        cursor: pointer;
                        user-select: none;

                        background-repeat: no-repeat;
                        background-position: center;
                        background-position-x: 45%;
                    }
                    .ddcarret-left {
                        background-image: url(/appfiles/icons/icon-arrow.svg);
                    }
                    .ddcarret-right {
                        background-image: url(/appfiles/icons/icon-arrow.svg);
                        transform: rotate(180deg);
                    }

                    .ddcontrolbutton {
                        display: flex;
                        justify-content: space-between;
                        position: relative;
                        text-align: center;

                        width: 362px;
                        height: 38px;
                        padding: 0;
                        box-sizing: content-box;
                        font-size: 16px;
                        line-height: 42px;
                        text-decoration: none;
                        color: #656565;
                        border: 1px solid ${color1Darkest};
                    }
                `}
            </style>
        </div>
    );
};

const ConceptExplorerNavWaypointsWindow = ({
    waypoints,
    waypoint,
    setWaypoint,
}) => {
    let controlLabel = 'Start journey...';
    if (waypoint) {
        const found = waypoints.findIndex(w => w.id === waypoint.id);
        const waypointNumber = found + 1;
        controlLabel = `[${waypointNumber}/${waypoints.length}] ${waypoint.label}`;
    }

    const onSelectPrev = () => {
        let waypointId = 0;
        if (waypoint) {
            const found = waypoints.findIndex(w => w.id === waypoint.id);
            if (found != -1) {
                if (found === 0) {
                    const lastIdx = waypoints.length - 1;
                    waypointId = lastIdx;
                } else {
                    waypointId = found - 1;
                }
            }
        }

        setWaypoint(waypoints[waypointId]);
    };

    const onSelectNext = () => {
        let waypointId = 0;

        if (waypoint) {
            const found = waypoints.findIndex(w => w.id === waypoint.id);
            if (found != -1) {
                const lastIdx = waypoints.length - 1;
                if (found === lastIdx) {
                    waypointId = 0;
                } else {
                    waypointId = found + 1;
                }
            }
        }

        setWaypoint(waypoints[waypointId]);
    };

    window.callPrevWaypoint = onSelectPrev;
    window.callNextWaypoint = onSelectNext;

    return (
        <div className="jdropdownContainer">
            <CustomToggle
                onSelectPrev={onSelectPrev}
                onSelectNext={onSelectNext}
            >
                <span
                    onClick={() => {
                        setWaypoint(waypoint || waypoints[0]);
                    }}
                    className={cn('waypointtitle', {
                        wactive: waypoint != false,
                    })}
                >
                    {controlLabel}
                </span>
            </CustomToggle>

            <style jsx>
                {`
                    .wactive {
                        color: #000000;
                    }
                    .waypointtitle {
                        font-size: 12px;
                        cursor: pointer;
                    }
                    .jdropdownContainer {
                        position: relative;
                        background-color: #f4f5f7;
                        display: contents;
                    }
                `}
            </style>
        </div>
    );
};

export default ConceptExplorerNavWaypointsWindow;
