import { useState } from 'react';
import Modal, { ModalFooter, ModalTransition } from '@atlaskit/modal-dialog';
import Button from '@atlaskit/button';
import cn from 'classnames';
import { Checkbox } from '@atlaskit/checkbox';
import { saveDefault } from '../services/api/apiService';

const OrgsModalFooter = ({
    onSave,
    selectedId,
    makeDefault,
    setMakeDefault,
}) => (
    <ModalFooter showKeyline={false}>
        <div className={cn({ invisible: selectedId === -1 })}>
            <Checkbox
                isChecked={makeDefault}
                onChange={() => {
                    setMakeDefault(!makeDefault);
                }}
                label="Set as Default"
                value="makeDefault"
                name="controlled-checkbox"
            />
        </div>
        <Button
            appearance="primary"
            onClick={onSave}
            isDisabled={selectedId === -1}
        >
            Continue
        </Button>
    </ModalFooter>
);

const ModalOrganizations = ({
    isOpen,
    setOpen,
    organizations,
    onChangeOrg,
    isOutSideNotClosable,
}) => {
    const [selectedId, setSelectedId] = useState(-1);
    const [makeDefault, setMakeDefault] = useState(false);

    const onSave = () => {
        if (makeDefault) {
            const { slug } = organizations[selectedId];
            saveDefault(slug);
        }
        setOpen(false);
        onChangeOrg(selectedId);
        setSelectedId(-1);
    };

    const FooterWrapper = () => (
        <OrgsModalFooter
            onSave={onSave}
            makeDefault={makeDefault}
            setMakeDefault={setMakeDefault}
            selectedId={selectedId}
        />
    );

    return (
        <ModalTransition>
            {isOpen && (
                <Modal
                    // actions={actions}
                    onClose={() => {
                        if (!isOutSideNotClosable) {
                            setOpen(false);
                        }
                    }}
                    heading="Organizations"
                    scrollBehavior="inside"
                    components={{ Footer: FooterWrapper }}
                >
                    <div>
                        {organizations
                            .sort((a, b) => a.title.localeCompare(b.title))
                            .map((item, idx) => (
                                <div
                                    key={idx}
                                    className={cn('orgItem', {
                                        orgSelectOne: idx === selectedId,
                                    })}
                                    onClick={() =>
                                        setSelectedId(
                                            selectedId === idx ? -1 : idx
                                        )
                                    }
                                >
                                    {item.title}
                                </div>
                            ))}

                        <style jsx>
                            {`
                                .orgItem {
                                    display: block;
                                    text-decoration: none;
                                    background-color: white;
                                    line-height: 40px;
                                    padding: 0px 18px;
                                    color: #656565;
                                    font-weight: bold;
                                    border: 1px solid white;
                                    cursor: pointer;
                                }
                                .orgSelectOne {
                                    background-color: #f1eded;
                                    border: 1px solid #002445;
                                    border-radius: 6px;
                                }
                            `}
                        </style>
                    </div>
                </Modal>
            )}
        </ModalTransition>
    );
};
export default ModalOrganizations;
