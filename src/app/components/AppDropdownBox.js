import Dropdown from 'react-bootstrap/Dropdown';
import * as React from 'react';
import { Select } from 'antd';

export const DropdownFilterButton = ({
    label,
    btnText,
    onPaneToggle,
    active,
}) => (
    <React.Fragment>
        <span className="label theme-color1dark--color">{label}</span>
        <Dropdown.Toggle
            block
            variant="secondary"
            size="lg"
            onClick={onPaneToggle}
            active={active}
        >
            {btnText}
        </Dropdown.Toggle>
        <style jsx>
            {`
                .label {
                    font-weight: bold;
                    font-size: 12px;
                    letter-spacing: 1px;
                    color: #002445;
                }
            `}
        </style>
    </React.Fragment>
);

export const AppDropdownBox = ({
    label,
    menuOptions,
    onOptionSelected,
    width = 224,
}) => {
    const { Option } = Select;
    return (
        <React.Fragment>
            <span className="label">
                <h5
                    className="semibold"
                    data-cy={`content-card-title-${label}`}
                >
                    {label}
                </h5>
            </span>
            <Select
                defaultValue="Default"
                size={'large'}
                style={{ width: width }}
                onChange={onOptionSelected}
            >
                {menuOptions.map((item, key) => (
                    <Option key={key} value={key}>
                        {item}
                    </Option>
                ))}
            </Select>
            <style jsx>
                {`
                    .label {
                        display: block;
                        color: #262626;
                        margin-bottom: 8px;
                    }
                `}
            </style>
        </React.Fragment>
    );
};
