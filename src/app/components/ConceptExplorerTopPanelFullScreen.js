import { useState, useContext, useEffect, useReducer } from 'react';
import VidPlayIcon from '@atlaskit/icon/glyph/vid-play';
import VidPauseIcon from '@atlaskit/icon/glyph/vid-pause';
import VidFullScreenOffIcon from '@atlaskit/icon/glyph/vid-full-screen-off';
import Button from '@atlaskit/button';

import LoginContext from '../context/LoginContext';
import UserContext from '../context/UserContext';

const tickIntervalSec = 6;

function onTickChange(state, action) {
    switch (action.type) {
        case 'start':
            return { tick: 1 };
        case 'next':
            return { tick: state.tick + 1 };
        case 'stop':
            return { tick: 0 };
        default:
    }
}

const ConceptExplorerTopPanelFullScreen = ({
    project,
    components,
    layoutState,
    setLayoutState,
    waypoint,
    waypoints,
    setWaypoint,
}) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest, colorHomeHeaderText } = themeColors;

    const { organization } = useContext(LoginContext);
    const { OrgLogo } = organization || {};

    const [viewData, setViewData] = useState({ title: '' });

    const [tick, setTick] = useReducer(onTickChange, { tick: 0 });

    const defaultControlBtnCss = [
        { width: '50px', height: '50px' },
        { backgroundColor: color1Darkest },
        { justifyContent: 'space-evenly' },
        { verticalAlign: 'top' },
    ];

    function localStopPlay() {
        clearTimeout(window.mytimer);
        window.mytimer = null;
    }
    function localSetTick() {
        if (window.mytimer) setTick({ type: 'next' });
    }
    function updateTick() {
        if (tick.tick) {
            if (window.mytimer) {
                clearTimeout(window.mytimer);
                window.mytimer = null;
            }
            window.mytimer = window.setTimeout(() => {
                tick.tick && localSetTick();
            }, tickIntervalSec * 1000);
        }
    }

    useEffect(() => {
        if (!layoutState.isFullScreen && tick.tick) {
            localStopPlay();
            setTick({ type: 'stop' });
        }
    }, [layoutState]);

    useEffect(() => {
        if (tick && tick.tick) {
            let waypointId = 0;

            if (waypoint) {
                const found = waypoints.findIndex(w => w.id === waypoint.id);
                if (found != -1) {
                    const lastIdx = waypoints.length - 1;
                    if (found === lastIdx) {
                        waypointId = 0;
                    } else {
                        waypointId = found + 1;
                    }
                }
            }

            setWaypoint(waypoints[waypointId]);
            updateTick();
        } else {
            localStopPlay();
        }
    }, [tick]);

    useEffect(() => {
        if (project) {
            const { title, description } = project;
            setViewData({ title, description });
        }
    }, [project]);

    return (
        <div
            className="topPanel"
            style={{
                backgroundColor: color1Darkest,
            }}
        >
            {OrgLogo && (
                <div
                    className="orgLogo"
                    style={{
                        backgroundImage: `url(${OrgLogo})`,
                    }}
                />
            )}

            <div
                className="appHeader"
                style={{
                    color: colorHomeHeaderText || color1Darkest,
                }}
            >
                <div className="primaryTitle">{viewData.title} </div>
            </div>

            <div className="journeyControlsArea">
                {components.ComponentExplorerNavWaypoints}
            </div>
            <div className="playControlArea">
                <Button
                    onClick={() =>
                        setTick(
                            tick.tick ? { type: 'stop' } : { type: 'start' }
                        )
                    }
                    css={defaultControlBtnCss}
                    iconBefore={
                        !tick.tick ? (
                            <VidPlayIcon primaryColor="white" />
                        ) : (
                            <VidPauseIcon primaryColor="white" />
                        )
                    }
                />
            </div>
            <div className="playControlArea">
                <Button
                    onClick={() => setLayoutState({ isFullScreen: false })}
                    css={defaultControlBtnCss}
                    iconBefore={<VidFullScreenOffIcon primaryColor="white" />}
                />
            </div>

            <style jsx>
                {`
                    .appHeader {
                        flex-grow: 1;
                    }
                    .playControlArea {
                        margin-left: 18px;
                        margin-right: 8px;
                    }
                    .topPanel {
                        display: flex;
                        justify-content: space-between;
                        padding-left: 45px;
                        font-size: 24px;
                        line-height: 50px;
                        background-color: #ebecf0;
                    }
                    .journeyControlsArea {
                        background-color: ${color1Darkest};
                    }
                    .orgLogo {
                        width: 110px;
                        height: 40px;
                        background-size: contain;
                        margin-left: -25px;
                        margin-right: 15px;
                        margin-top: 5px;
                        background-repeat: no-repeat;
                    }
                `}
            </style>
        </div>
    );
};
export default ConceptExplorerTopPanelFullScreen;
