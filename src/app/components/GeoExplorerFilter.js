import { useRef, useContext } from 'react';
import GeoExplorerContext from '../context/GeoExplorerContext';

const GeoExplorerFilter = ({ filter, onFilterShow }) => {
    const { geosolutionsfilters } = useContext(GeoExplorerContext);

    let selectedText = 'All';

    const { options, label } = filter;
    const selectedVal = geosolutionsfilters[filter.source];
    if (!selectedVal) {
        selectedText = 'All';
    } else if (Array.isArray(selectedVal)) {
        if (selectedVal.length === 1) {
            selectedText = selectedVal[0];
        } else if (selectedVal.length >= 2) {
            selectedText = `${selectedVal.length} items`;
        }
    } else {
        const selectedId = selectedVal
            ? options.findIndex(item => item.value === selectedVal)
            : -1;
        selectedText = selectedId === -1 ? 'All' : options[selectedId].label;
    }

    const target = useRef(null);

    return (
        <div className="filterWrapper" ref={target}>
            <div className="label">{label}</div>
            <div className="bar" onClick={() => onFilterShow(target)}>
                {selectedText}
                <span className="caret" />
            </div>

            <style jsx>
                {`
                    .caret {
                        display: block;
                        position: absolute;
                        width: 16px;
                        height: 16px;
                        background-position: center;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/icons/filter-dropdown.svg);
                        right: 8px;
                        top: 32px;
                    }
                    .bar {
                        background: #ffffff;
                        border: 1px solid rgba(163, 173, 186, 0.3);
                        border-radius: 3px;
                        font-weight: 500;
                        font-size: 14px;
                        line-height: 40px;
                        height: 40px;
                        padding-left: 12px;
                        cursor: pointer;

                        overflow-wrap: inherit;
                        white-space: nowrap;
                        overflow: hidden;
                        text-overflow: ellipsis;
                        padding-right: 20px;
                    }
                    .label {
                        white-space: nowrap;
                        font-family: 'Roboto', sans-serif;
                        font-style: normal;
                        font-weight: 500;
                        font-size: 11px;
                        line-height: 12px;
                        letter-spacing: -0.1px;
                        text-transform: uppercase;
                        margin-bottom: 7px;
                    }
                    .filterWrapper {
                        width: 110px;
                        height: 64px;
                        margin-right: 26px;
                        position: relative;
                        margin-top: 8px;
                    }
                `}
            </style>
        </div>
    );
};
GeoExplorerFilter.defaultProps = {
    selectedId: null,
};
export default GeoExplorerFilter;
