import { Fragment, useState, useRef, useContext, useEffect } from 'react';
import Overlay from 'react-bootstrap/Overlay';
import GeoExplorerContext from '../context/GeoExplorerContext';
import GeoExplorerFilter from './GeoExplorerFilter';
import GeoExplorerFilterControlsRadio from './GeoExplorerFilterControlsRadio';
import GeoExplorerFilterControlsMultiple from './GeoExplorerFilterControlsMultiple';

import {
    kMapViews,
    kMapFocusMetricKey,
    kMapPerspectiveKey,
    kMapPerspectiveWarehouse,
    kLastMovement,
    kPutawayTime,
    kPropProgramUtilizationRate,
    kPropValuationExistence,
    kPropValuationCompletion,
    kPropDateOfLastInventory,
    kPropReadyForIssueCondition,
    kPropTurnoverRate,
    kPropAccuracyRange,
    kPropValuation,
} from '../services/geoexplorerconsts';

const GeoExplorerFilterControls = props => {
    if (props.type === 'radio') {
        return <GeoExplorerFilterControlsRadio {...props} />;
    }
    return <GeoExplorerFilterControlsMultiple {...props} />;
};

const GeoExplorerFilters = () => {
    const [isOpen, setIsOpen] = useState(true);
    const [openedFilterRef, setOpenedFilterRef] = useState(useRef(null));
    // const [filterValues, setFilterValues] = useState(defaultFilters)
    const [filterDisplayed, setFilterDisplayed] = useState(null);
    const [mapViews, setMapViews] = useState(kMapViews);

    const filtersAreaRef = useRef(null);
    const { geosolutionsfilters, geosolutionalldatafilters } = useContext(
        GeoExplorerContext
    );

    useEffect(() => {
        const cleanMapViews = kMapViews.filter(
            item => item.source != kMapFocusMetricKey
        );

        const focusMetricOptionsExecutive = [
            { label: kPropAccuracyRange, value: kPropValuationExistence },
            { label: kPropValuation, value: kPropValuationCompletion },
            {
                label: kPropDateOfLastInventory,
                value: kPropDateOfLastInventory,
            },
            {
                label: kPropReadyForIssueCondition,
                value: kPropReadyForIssueCondition,
            },
            { label: kPropTurnoverRate, value: kPropTurnoverRate },
        ];
        const focusMetricOptionsWarehouse = [
            { label: kLastMovement, value: kLastMovement },
            { label: kPutawayTime, value: kPutawayTime },
            {
                label: kPropProgramUtilizationRate,
                value: kPropProgramUtilizationRate,
            },
        ];
        const focusMetricOptions =
            geosolutionsfilters[kMapPerspectiveKey] === kMapPerspectiveWarehouse
                ? focusMetricOptionsWarehouse
                : focusMetricOptionsExecutive;

        const focusMetricFilter = {
            label: 'Focus Metric',
            source: kMapFocusMetricKey,
            type: 'radio',
            options: focusMetricOptions, // focusMetricOptions.map(item => ({label:item,value:item}))
        };

        setMapViews([...cleanMapViews, focusMetricFilter]);
    }, [geosolutionsfilters]);

    return (
        <Fragment>
            <div className="filters">
                {[...mapViews, ...geosolutionalldatafilters].map(
                    (filter, i) => (
                        <GeoExplorerFilter
                            key={`view_${i}`}
                            filter={filter}
                            onFilterShow={ref => {
                                if (ref === openedFilterRef) {
                                    setOpenedFilterRef(null);
                                } else {
                                    window.setTimeout(() => {
                                        setOpenedFilterRef(ref);
                                        setFilterDisplayed(filter);
                                        setIsOpen(true);
                                    }, 100);
                                }
                            }}
                        />
                    )
                )}
            </div>

            {openedFilterRef != null && (
                <Overlay
                    containerPadding={40}
                    container={filtersAreaRef.current}
                    target={openedFilterRef}
                    show={isOpen}
                    rootClose
                    rootCloseEvent="click"
                    placement="bottom-start"
                    onHide={() => {
                        setIsOpen(false);
                        setOpenedFilterRef(null);
                    }}
                >
                    <div className="contentContainer">
                        <div className="topBar">
                            {filterDisplayed && filterDisplayed.label}
                            <button
                                type="button"
                                className="close"
                                aria-label="Close"
                                onClick={() => {
                                    setIsOpen(false);
                                    setOpenedFilterRef(null);
                                }}
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="contentArea">
                            {filterDisplayed && (
                                <GeoExplorerFilterControls
                                    type={filterDisplayed.type}
                                    options={filterDisplayed.options}
                                    filterId={filterDisplayed.source}
                                />
                            )}
                        </div>
                    </div>
                </Overlay>
            )}
            <style jsx>
                {`
                    button.close {
                        outline: none;
                    }
                    .contentContainer {
                        background: #ffffff;
                        box-shadow: 0px 1px 5px rgba(126, 147, 157, 0.2),
                            0px 3px 4px rgba(0, 59, 87, 0.12),
                            0px 2px 4px rgba(0, 59, 87, 0.14);
                        border-radius: 8px;
                        backgroundcolor: white;
                        padding: 15px 18px;
                        padding-bottom: 24px;
                        color: #3d4d60;
                    }
                    .topBar {
                        font-size: 15px;
                        color: #848484;
                        height: 35px;
                        line-height: 27px;
                        text-transform: capitalize;
                    }
                    .contentArea {
                        margin-right: 40px;
                        min-width: 150px;
                    }
                    .filters {
                        padding-left: 16px;
                        padding-bottom: 12px;
                        display: flex;
                        flex-wrap: wrap;
                    }
                `}
            </style>
        </Fragment>
    );
};

export default GeoExplorerFilters;
