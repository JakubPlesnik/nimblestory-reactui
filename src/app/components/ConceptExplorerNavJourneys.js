import { useState, useContext } from 'react';
import cn from 'classnames';
import UserContext from '../context/UserContext';

const CustomToggle = ({ children, onClick }) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    return (
        <a
            href=""
            className="ddcontrolbutton"
            onClick={e => {
                e.preventDefault();
                onClick(e);
            }}
        >
            {children}
            <div className="ddcarret" />

            <style jsx>
                {`
                    .ddcarret {
                        background-color: ${color1Darkest};
                        width: 50px;
                        height: 50px;
                        display: inline-block;
                        float: right;
                        color: white;
                        text-align: center;
                        transform: rotate(-90deg);
                        font-size: 20px;
                        line-height: 58px;
                        background-image: url(/appfiles/icons/icon-arrow.svg);
                        background-repeat: no-repeat;
                        background-position: center;
                        background-position-x: 45%;
                    }

                    .ddcontrolbutton {
                        display: inline-block;
                        background-color: white;
                        width: 483px;
                        height: 50px;
                        padding-left: 14px;
                        font-size: 16px;
                        line-height: 50px;
                        text-decoration: none;
                        color: #656565;
                    }
                `}
            </style>
        </a>
    );
};

const ConceptExplorerNavJourneys = ({ journey, journeys, setJourney }) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    const [showMenu, setShowMenu] = useState(false);

    let controlLabel = journey ? journey.title : '';
    controlLabel = showMenu ? 'Select one...' : controlLabel;
    return (
        <div className="jdropdownContainer">
            <CustomToggle onClick={() => setShowMenu(!showMenu)}>
                {controlLabel}
            </CustomToggle>

            <ul className={cn('jmenu', { hidden: !showMenu })}>
                {journeys.map((j, key) => (
                    <li
                        className={cn('jListItem', {
                            active: j.id === journey.id,
                        })}
                        key={key}
                        onClick={() => {
                            setJourney(j);
                            setShowMenu(false);
                        }}
                    >
                        <span>{j.title}</span>
                    </li>
                ))}
            </ul>

            <style jsx>
                {`
                    .jdropdownContainer {
                        position: relative;
                    }
                    ul.jmenu {
                        background-color: white;
                        width: 483px;
                        position: absolute;
                        list-style: none;
                        margin: 0;
                        padding: 14px 0px;
                        z-index: 999999;
                        position: fixed;
                    }
                    .jListItem {
                        line-height: 40px;
                        cursor: pointer;
                        border-bottom: 1px solid #cecece;
                    }
                    .jListItem > span {
                        padding-left: 14px;
                    }
                    .jListItem.active {
                        background-color: ${color1Darkest};
                        color: white;
                    }
                    .jListItem:hover {
                        background-color: #cecece;
                    }
                `}
            </style>
        </div>
    );
};
export default ConceptExplorerNavJourneys;
