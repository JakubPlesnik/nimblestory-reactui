import * as React from 'react';

const ManageInputArea = ({ data, field, onSave }) => {
    const onFieldChaged = () => {
        const safeVal = event.target.value.replace(/\n/g, '');
        onSave({ [field]: safeVal });
    };
    return (
        <React.Fragment>
            <textarea
                value={data[field] ? data[field] : ''}
                onChange={onFieldChaged}
                className="minwidth"
            />
            <style jsx>
                {`
                    .minwidth {
                        min-width: 99%;
                        height: 75px;
                        font-size: 13px;
                    }
                `}
            </style>
        </React.Fragment>
    );
};
export default ManageInputArea;
