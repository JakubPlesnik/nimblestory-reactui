import * as React from 'react';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { fetchAsset } from '../services/api/apiService';

const AssetVideo = ({ assetId }) => {
    const [imageUrl, setImageUrl] = useState('');

    useEffect(() => {
        if (Number.isInteger(assetId)) {
            fetchAsset(assetId, setImageUrl, null);
            return;
        }
    }, []);

    return (
        <React.Fragment>
            <iframe
                src={imageUrl}
                className="assetiFrame"
                title="Video"
                width="640"
                height="360"
                frameBorder="0"
                allow="fullscreen"
                allowFullScreen
            />
            <style jsx>
                {`
                    .assetiFrame {
                        width: 100%;
                        height: 100vh;
                    }
                `}
            </style>
        </React.Fragment>
    );
};

AssetVideo.propTypes = {
    assetId: PropTypes.string.isRequired,
};
export default AssetVideo;
