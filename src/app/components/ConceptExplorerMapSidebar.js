import { useContext } from 'react';
import ChevronRightCircleIcon from '@atlaskit/icon/glyph/chevron-right-circle';
import { N0, B400 } from '@atlaskit/theme/colors';
import UserContext from '../context/UserContext';
import Image from './image/Image';
import ImageEmbedded from './image/ImageEmbedded';

const ConceptExplorerMapSidebar = ({ waypoint, isEmbedded, slug }) => {
    if (!waypoint) {
        return <div />;
    }

    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;
    const { card, label } = waypoint;

    const printContent = cardContent => {
        if (cardContent.type == 'text') {
            return (
                <div
                    className="cardContent"
                    dangerouslySetInnerHTML={{ __html: cardContent.text }}
                />
            );
        }

        if (cardContent.type == 'image') {
            return (
                <div>
                    <div className="cardImage">
                        {!isEmbedded && <Image assetId={cardContent.assetId} />}
                        {isEmbedded && (
                            <ImageEmbedded
                                type="ce"
                                slug={slug}
                                assetId={cardContent.assetId}
                            />
                        )}
                    </div>
                    <style jsx>
                        {`
                            .cardImage {
                                padding: 10px 0px;
                                width: 100%;
                            }
                            .cardImage img {
                                width: 100%;
                            }
                        `}
                    </style>
                </div>
            );
        }
    };

    return (
        <div className="sidebarwrapper">
            <div className="waypointtitle">{label}</div>
            <div className="cardContentWrapper">
                {card.cardContent.map(content => printContent(content))}
            </div>

            <span className="resizeIcon">
                <ChevronRightCircleIcon
                    style={{
                        borderWidth: 1,
                        borderColor: B400,
                    }}
                    primaryColor={N0}
                    secondaryColor={B400}
                />
            </span>

            <style jsx>
                {`
                    .cardImage {
                        padding: 10px 0px;
                        width: 100%;
                    }
                    .sidebarwrapper {
                        background-color: white;
                        padding: 25px;
                        position: relative;
                        border-width: 2px;
                        border-style: solid;
                        border-color: #181d2f #f3f5f8 #f3f5f8 #f3f5f8;
                        height: 100%;
                        z-index: 100;
                        display: flex;
                        flex-direction: column;
                    }
                    .sidebarwrapper::-webkit-scrollbar-track {
                        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
                        border-radius: 10px;
                        background-color: white;
                    }
                    .resizeIcon {
                        position: absolute;
                        z-index: 99999;
                        left: -14px;
                        bottom: 20px;
                    }
                    .sidebarwrapper::-webkit-scrollbar {
                        width: 12px;
                        background-color: white;
                    }

                    .sidebarwrapper::-webkit-scrollbar-thumb {
                        border-radius: 10px;
                        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
                        background-color: white;
                    }

                    .waypointtitle {
                        color: ${color1Darkest};
                        font-size: 22px;
                        margin-top: 30px;
                        margin-bottom: 35px;
                    }
                    .cardContent {
                        font-size: 16px;
                    }
                    .cardContentWrapper {
                        overflow-y: scroll;
                        height: 100%;
                    }
                `}
            </style>
        </div>
    );
};
export default ConceptExplorerMapSidebar;
