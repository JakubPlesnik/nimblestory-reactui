import { useState, useContext, useEffect } from 'react';
import ManagerContext from '../context/ManagerContext';
import GeoExplorerContext from '../context/GeoExplorerContext';
import PortfolioContext from '../context/PortfolioContext';
import PortfolioProjectAssetsContext from '../context/PortfolioProjectAssetsContext';
import { filteredItems, assetSortOptions } from '../services/filteringAssets';

const NSManagerData = ({ children }) => {
    const [projects, setProjects] = useState([]);
    const [assets, setAssets] = useState([]);
    const [assetsFiltered, setAssetsFiltered] = useState([]);

    const [geosolutions, setGeosolutions] = useState([]);

    const [importAssets, setImportAssets] = useState(false);
    const [assetfilters, setAssetFilters] = useState([]);

    // eslint-disable-next-line no-unused-vars
    const [assetFilterOptions, setAssetFilterOptions] = useState([]);
    const [selectedAssetSortOption, setAssetSortOption] = useState(0);

    const [filterProjectSlug, setFilterProjectSlug] = useState(false);
    const [selectedProjectIndex, setSelectedProjectIndex] = useState(-1);

    const { projects: PortfolioProjects, totalProjects } = useContext(
        PortfolioContext
    );
    const { assets: PortfolioAssets, setProject, totalAssets } = useContext(
        PortfolioProjectAssetsContext
    );
    const { solutions: GeoexplorerSolutions } = useContext(GeoExplorerContext);

    useEffect(() => {
        if (PortfolioProjects && !projects.length) {
            setProjects(PortfolioProjects);
        }
    }, [PortfolioProjects]);

    useEffect(() => {
        if (PortfolioAssets) {
            setAssets(PortfolioAssets);
        }
    }, [PortfolioAssets]);

    useEffect(() => {
        if (GeoexplorerSolutions) {
            setGeosolutions(GeoexplorerSolutions);
        }
    }, [GeoexplorerSolutions]);

    useEffect(() => {
        const index = filterProjectSlug
            ? projects.findIndex(item => item.slug === filterProjectSlug)
            : -1;
        setSelectedProjectIndex(index);
        setAssets([]);
        setProject(projects[index]);
        // load assets fornew slug
    }, [filterProjectSlug]);

    useEffect(() => {
        if (assets.length) {
            const filteredAssets =
                assets.length > 0 && assetfilters.length
                    ? filteredItems(assets, assetfilters)
                    : assets;
            const filteredAssetsByProject =
                assets.length > 0 && filterProjectSlug
                    ? filteredAssets.filter(
                          item => item.ProjSlug == filterProjectSlug
                      )
                    : filteredAssets;
            const sortedAssets =
                assets.length > 0
                    ? filteredAssetsByProject.sort(
                          assetSortOptions[selectedAssetSortOption].sorter
                      )
                    : null;
            setAssetsFiltered(sortedAssets);
        } else if (assetsFiltered.length) {
            setAssetsFiltered([]);
        }
    }, [assets, assetfilters, selectedAssetSortOption]);

    const updateProjectData = newProjectData => {
        const newProjects = projects.map(item => {
            if (
                newProjectData.hash &&
                newProjectData.hash === 'new' &&
                item.hash === 'new'
            ) {
                // save new item, replace template item
                delete newProjectData.hash;
                return newProjectData;
            }
            if (item.slug === newProjectData.slug) {
                // replace existing item
                return newProjectData;
            }
            // keep
            return item;
        });
        setProjects(newProjects);
    };

    const updateAssetData = (newAssetData, selectedTab) => {
        const existingassets = selectedTab === 2 ? importAssets : assets;

        const newAssets = existingassets.map(item => {
            if (newAssetData.hash && item.hash === newAssetData.hash) {
                // save new item, replace template item
                delete newAssetData.hash;
                return newAssetData;
            }
            if (item.slug === newAssetData.slug) {
                return newAssetData;
            }
            return item;
        });

        if (selectedTab === 2) {
            setImportAssets(newAssets);
        } else {
            setAssets(newAssets);
        }
    };

    const managerAddNewProject = () => {
        if (projects.findIndex(({ id }) => id === 'new') === -1) {
            // only one new item at a time
            const newProjectBlank = {
                id: 'new',
                hash: 'new',
                title: '',
                ProjStatus: 'active',
                description: '',
                ProjType: [],
                ProjYear: '',
                ProjThumb: null,
                ProjArchive: null,
            };
            const newProjects = [newProjectBlank, ...projects];
            setProjects(newProjects);
        }
    };

    const managerAddNewAsset = (project, entryType) => {
        if (assets.findIndex(({ id }) => id === 'new') === -1) {
            const newAssetBlank = {
                id: 'new',
                hash: 'new',
                entryType,
                dateEntryCreatedUnix: new Date().getTime() / 1000,
                title: '',
                ProjSlug: project.slug,
                AssetStatus: [],
                AssetType: [],
                AssetVersion: null,
            };
            const newAssets = [...assets, newAssetBlank];
            setAssets(newAssets);
        }
    };

    const managerCancelNewProject = newProjectData => {
        const newProjects = projects.filter(item => item != newProjectData);
        setProjects(newProjects);
    };

    const managerCancelNewAsset = () => {
        const newAssets = assets.filter(({ id }) => id != 'new');
        setAssets(newAssets);
    };

    return (
        <ManagerContext.Provider
            value={{
                assets: assetsFiltered,
                projects,
                geosolutions,
                managerAddNewAsset,
                managerAddNewProject,
                managerCancelNewAsset,
                managerCancelNewProject,
                updateAssetData,
                updateProjectData,
                setFilterProjectSlug,
                selectedProjectIndex,
                importAssets,
                setImportAssets,
                assetfilters,
                setAssetFilters,
                assetFilterOptions,
                selectedAssetSortOption,
                setAssetSortOption,
                totalProjects,
                totalAssets,
            }}
        >
            {children}
        </ManagerContext.Provider>
    );
};
export default NSManagerData;
