import IframedUrl from '../components/PCTlinkContent/IframedUrl';
import EmbedCode from '../components/PCTlinkContent/EmbedCode';
import IframedVimeo from '../components/PCTlinkContent/IframeVimeo';
import LandingPage from '../components/PCTlinkContent/LandingPage';
import ProxiedUrl from '../components/PCTlinkContent/ProxiedUrl';
const AssetModule = ({ filepath, asset }) => {
    const renderOpenMethodCompoent = () => {
        if (asset.entryType == 'pctLink') {
            if (!asset.openType || asset.openType == 'IframedUrl') {
                return <IframedUrl filepath={filepath}></IframedUrl>;
            } else if (asset.openType == 'IframedEmbed') {
                return <EmbedCode embedCode={filepath}></EmbedCode>;
            } else if (asset.openType == 'IframedVimeo') {
                return <IframedVimeo vimeoId={filepath}></IframedVimeo>;
            } else if (asset.openType == 'LandingPage') {
                return (
                    <LandingPage
                        urlLink={filepath}
                        thumbnailId={asset.thumbnailId}
                        description={asset.description}
                    ></LandingPage>
                );
            } else if (asset.openType == 'ProxiedUrl') {
                return <ProxiedUrl proxyUrl={filepath}></ProxiedUrl>;
            }
        }

        return <IframedUrl filepath={filepath}></IframedUrl>;
    };

    return (
        <div className="assetWrapper">
            {renderOpenMethodCompoent()}

            <style jsx>
                {`
                    .assetWrapper {
                        position: absolute;
                        top: 0;
                        right: 0;
                        left: 0;
                        bottom: 0;
                    }
                `}
            </style>
        </div>
    );
};
export default AssetModule;
