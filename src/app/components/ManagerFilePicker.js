const ManagerFilePicker = ({ children, onPicked, multiple = false }) => {
    let fileInput = null;

    const handleFileSelect = async e => {
        if (e.target.files.length) {
            onPicked(e.target.files);
        }
    };

    const onStartPicker = () => {
        if (!fileInput) {
            fileInput = document.createElement('input');
            fileInput.type = 'file';
            fileInput.name = 'images';
            fileInput.multiple = multiple;
            fileInput.addEventListener('change', handleFileSelect, false);
        }
        fileInput.click();
    };

    return (
        <div onClick={onStartPicker}>
            {children || <button>Open file picker</button>}
        </div>
    );
};
export default ManagerFilePicker;
