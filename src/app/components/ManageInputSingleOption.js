import Form from 'react-bootstrap/Form';

const ManageInputSingleOption = ({ data, options, field, onSave }) => (
    <Form>
        {options.map(item => (
            <Form.Check
                type="radio"
                label={item.label}
                key={`managefieldoption_${item.value}`}
                id={`managefieldoption_${item.value}`}
                checked={data[field] === item.value}
                onChange={() => onSave({ [field]: item.value })}
            />
        ))}
    </Form>
);
export default ManageInputSingleOption;
