import { useEffect } from 'react';
import PropTypes from 'prop-types';
import loadScript from '../services/loadscript';

const GOOGLE_SDK_URL = 'https://apis.google.com/js/api.js';

const isGoogleReady = () => !!window.gapi;
const isGoogleAuthReady = () => !!window.gapi.auth;
const isGooglePickerReady = () => !!window.google.picker;
let picker = null;
let scriptLoadingStarted = false;

const ManagerGooglePicker = ({
    children,
    clientId,
    width,
    height,
    scope,
    authImmediate,
    disabled,
    onAuthenticate,
    onAuthFailed,
    viewId,
    mimeTypes,
    query,
    origin,
    navHidden,
    multiselect,
    developerKey,
    onPicked,
    onChange,
}) => {
    const onApiLoad = () => {
        if (window.gapi) {
            window.gapi.load('auth');
            window.gapi.load('picker');
        }
    };

    const doAuth = callback => {
        window.gapi.auth.authorize(
            {
                client_id: clientId,
                scope,
                immediate: authImmediate,
            },
            callback
        );
    };

    const onChoose = () => {
        if (
            !isGoogleReady() ||
            !isGoogleAuthReady() ||
            !isGooglePickerReady() ||
            disabled
        ) {
            return null;
        }

        const token = window.gapi.auth.getToken();
        const oauthToken = token && token.access_token;

        if (oauthToken) {
            createPicker(oauthToken);
        } else {
            doAuth(response => {
                if (response.access_token) {
                    createPicker(response.access_token);
                } else {
                    onAuthFailed(response);
                }
            });
        }
    };

    const createPicker = oauthToken => {
        onAuthenticate(oauthToken);

        const google = window.google;
        const pickerCallback = data => {
            const action = data[google.picker.Response.ACTION];
            if (action == google.picker.Action.PICKED) {
                onPicked({ ...data, oauthToken });
            } else {
                onChange(data);
            }
        };

        const googleViewId = google.picker.ViewId[viewId];
        const view = new window.google.picker.DocsView(googleViewId);

        view.setMode(google.picker.DocsViewMode.GRID);

        if (mimeTypes) {
            view.setMimeTypes(mimeTypes.join(','));
        }
        if (query) {
            view.setQuery(query);
        }

        if (!view) {
            throw new Error("Can't find view by viewId");
        }

        picker = new window.google.picker.PickerBuilder()
            .addView(view)
            .setOAuthToken(oauthToken)
            .setDeveloperKey(developerKey)
            .setCallback(pickerCallback)
            .setSize(width, height);

        view.setMode(google.picker.DocsViewMode.LIST);
        view.setIncludeFolders(true);

        if (origin) {
            picker.setOrigin(origin);
        }

        if (navHidden) {
            picker.enableFeature(window.google.picker.Feature.NAV_HIDDEN);
        }

        if (multiselect) {
            picker.enableFeature(
                window.google.picker.Feature.MULTISELECT_ENABLED
            );
        }

        picker.build().setVisible(true);
    };

    useEffect(() => {
        if (isGoogleReady()) {
            // google api is already exists
            // init immediately
            onApiLoad();
        } else if (!scriptLoadingStarted) {
            // load google api and the init
            scriptLoadingStarted = true;
            loadScript(GOOGLE_SDK_URL, onApiLoad);
        } else {
            // is loading
        }
    }, []);

    return (
        <div onClick={onChoose}>
            {children || <button>Open google picker</button>}
        </div>
    );
};

ManagerGooglePicker.defaultProps = {
    onChange: () => {},
    onAuthenticate: () => {},
    onAuthFailed: () => {},
    width: 900,
    height: 500,
    scope: ['https://www.googleapis.com/auth/drive.readonly'],
    viewId: 'DOCS',
    authImmediate: false,
    multiselect: false,
    navHidden: false,
    disabled: false,
};

ManagerGooglePicker.propTypes = {
    children: PropTypes.node,
    clientId: PropTypes.string.isRequired,
    developerKey: PropTypes.string,
    scope: PropTypes.array,
    viewId: PropTypes.string,
    authImmediate: PropTypes.bool,
    origin: PropTypes.string,
    onChange: PropTypes.func,
    onAuthenticate: PropTypes.func,
    onAuthFailed: PropTypes.func,
    createPicker: PropTypes.func,
    multiselect: PropTypes.bool,
    navHidden: PropTypes.bool,
    disabled: PropTypes.bool,
};

export default ManagerGooglePicker;
