import Link from 'next/link';
import { useContext } from 'react';
import AppLinkData from '../../services/applink';
import LoginContext from '../../context/LoginContext';
import UserContext from '../../context/UserContext';
import Thumbnail from '../image/Thumbnail';
import { ICONS } from '../../enum/projectIcons';
import { Card, Tooltip } from 'antd';

import InfoButton from '../cardActionButtons/buttonInfo';
import EditButton from '../cardActionButtons/buttonEdit';
import PinButton from '../cardActionButtons/buttonPin';
import { isPinned } from '../../services/contentCardService';

function text_truncate(str, length) {
    if (!str) return '';

    const ending = '...';

    if (str.length > length) {
        return str.substring(0, length - ending.length).trim() + ending;
    }
    return str;
}

const ConceptExplorerSolutionCard = ({ organization, project }) => {
    const { pinned } = useContext(LoginContext);
    const projectLinkData = AppLinkData({
        organization,
        project: project.slug,
        application: 'conceptexplorer',
    });
    const projTitle = project.title;
    const { themeColors } = useContext(UserContext);

    const { color1Darkest } = themeColors;

    return (
        <Card
            className="custom-card"
            style={{ width: 248, height: 352 }}
            cover={
                <Link href={projectLinkData.href} as={projectLinkData.as}>
                    <div className="iconHolder">
                        {' '}
                        <Thumbnail
                            thumbnailId={project.thumbnailId}
                            placeholderIcon={ICONS.conceptexplorer}
                            contentType={project.AssetType}
                            labColor={color1Darkest}
                        />
                    </div>
                </Link>
            }
            actions={[
                <PinButton
                    disabled={true}
                    isPinned={isPinned(pinned, project.id)}
                    key="pin"
                />,
                <EditButton key="edit" disabled={true} />,
                <InfoButton key="info" disabled={true} />,
            ]}
        >
            <div className="ant-card-cover"></div>
            <div className="ant-card-meta-detail">
                <div className="ant-card-meta-title ">
                    <Link href={projectLinkData.href} as={projectLinkData.as}>
                        <Tooltip title={projTitle}>
                            <div className="title">
                                <h5
                                    className="semibold"
                                    data-cy="content-explorer-title"
                                >
                                    {text_truncate(projTitle, 100)}
                                </h5>
                            </div>
                        </Tooltip>
                    </Link>
                </div>
                <div className="ant-card-meta-description card-text text-limit">
                    {project.description}
                </div>
            </div>

            <style jsx>
                {`
                    .iconHolder {
                        cursor: pointer;
                    }
                    .text-limit {
                        max-height: 66px;
                    }

                    img {
                        object-fit: cover;
                    }
                `}
            </style>
        </Card>
    );
};
export default ConceptExplorerSolutionCard;
