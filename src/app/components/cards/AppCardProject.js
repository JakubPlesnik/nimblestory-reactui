import Link from 'next/link';
import { useContext } from 'react';
import AppLinkData from '../../services/applink';
import LoginContext from '../../context/LoginContext';
import UserContext from '../../context/UserContext';
import { Card, Tooltip } from 'antd';
import InfoButton from '../cardActionButtons/buttonInfo';
import EditButton from '../cardActionButtons/buttonEdit';
import PinButton from '../cardActionButtons/buttonPin';
import CardCoverImage from '../image/LargeCardImage';
import { isPinned } from '../../services/contentCardService';

function text_truncate(str, length) {
    if (!str) return '';

    const ending = '...';

    if (str.length > length) {
        return str.substring(0, length - ending.length).trim() + ending;
    }
    return str;
}

const AppCardProject = ({ organization, project }) => {
    const { pinned } = useContext(LoginContext);
    const projectLinkData = AppLinkData({
        organization,
        project: project.slug,
    });

    let projectThumbnail = project.ProjThumb
        ? project.ProjThumb
        : project.thumbnailId;

    const { themeColors } = useContext(UserContext);

    const { color1Darkest } = themeColors;

    const projTitle =
        project.ProjYear != null && project.ProjYear.length
            ? `${project.ProjYear} - ${project.title}`
            : project.title;

    return (
        <Card
            className="custom-card"
            style={{ width: 512, height: 352 }}
            cover={
                <Link href={projectLinkData.href} as={projectLinkData.as}>
                    <div className="iconHolder">
                        <CardCoverImage
                            contentType={'Project'}
                            thumnails={project.cardThumbnails}
                            projectThumbnail={projectThumbnail}
                            labelColor={color1Darkest}
                        ></CardCoverImage>
                    </div>
                </Link>
            }
            actions={[
                <PinButton
                    isPinned={isPinned(pinned, project.id)}
                    disabled={true}
                    key="pin"
                />,
                <EditButton key="edit" disabled={true} />,
                <InfoButton key="info" disabled={true} />,
            ]}
        >
            <div className="ant-card-cover"></div>
            <div className="ant-card-meta-detail">
                <div className="ant-card-meta-title ">
                    <Link href={projectLinkData.href} as={projectLinkData.as}>
                        <Tooltip title={projTitle}>
                            <div className="title">
                                <h5
                                    className="semibold"
                                    data-cy="folder-card-title"
                                >
                                    {text_truncate(projTitle, 100)}
                                </h5>
                            </div>
                        </Tooltip>
                    </Link>
                </div>
                <div className="ant-card-meta-description card-text text-limit">
                    {project.description}
                </div>
            </div>

            <style jsx>
                {`

                .hide {
                    display:none;
                }
                  .iconHolder{
                    cursor: pointer; 
                }
                    .text-limit {
                        max-height: 66px;
                        white-space: nowrap;
                        overflow: hidden;
                        text-overflow: ellipsis;
}
                    }

                    img {
                        object-fit: cover;
                    }

                    .flex-container {
                        display: flex;
                        flex-direction: row;
                         width: 512px;
                          height: 208px;
                      }
                      
                      .flex-left {
                        width: 296px;
                      
                      }
                      
                      .flex-right {
                        width: 215px;
                      
                      }
                      
                      .image1 {
                        background-color:red;
                        height:99px
                        background-position-x: center;
                        background-position-y: center;
                      }
                      
                      .image2 {
                        background-color:blue;
                         height:109px
                         background-position-x: center;
                        background-position-y: center;
                      }
                `}
            </style>
        </Card>
    );
};
export default AppCardProject;
