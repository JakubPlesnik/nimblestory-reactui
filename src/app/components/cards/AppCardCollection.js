import Link from 'next/link';
import { useContext } from 'react';
import AppLinkData from '../../services/applink';
import LoginContext from '../../context/LoginContext';
import UserContext from '../../context/UserContext';
import { Card, Tooltip } from 'antd';
import CardCoverImage from '../image/LargeCardImage';
import InfoButton from '../cardActionButtons/buttonInfo';
import EditButton from '../cardActionButtons/buttonEdit';
import PinButton from '../cardActionButtons/buttonPin';
import { isPinned } from '../../services/contentCardService';

function text_truncate(str, length) {
    if (!str) return '';

    const ending = '...';

    if (str.length > length) {
        return str.substring(0, length - ending.length).trim() + ending;
    }
    return str;
}

const AppCardCollection = ({ organization, collection }) => {
    const { pinned } = useContext(LoginContext);
    const projectLinkData = AppLinkData({
        organization,
        collection: collection.slug,
        application: 'collection',
    });

    const { themeColors } = useContext(UserContext);

    const { color1Darkest } = themeColors;

    return (
        <Card
            className="custom-card"
            style={{ width: 512, height: 352 }}
            cover={
                <Link href={projectLinkData.href} as={projectLinkData.as}>
                    <div className="iconHolder">
                        <CardCoverImage
                            contentType={'Collection'}
                            thumnails={collection.thumbnails}
                            labelColor={color1Darkest}
                        ></CardCoverImage>

                        <div></div>
                    </div>
                </Link>
            }
            actions={[
                <PinButton
                    disabled={true}
                    isPinned={isPinned(pinned, collection.id)}
                    key="pin"
                />,
                <EditButton key="edit" disabled={true} />,
                <InfoButton key="info" disabled={true} />,
            ]}
        >
            <div className="ant-card-cover"></div>
            <div className="ant-card-meta-detail">
                <div className="ant-card-meta-title ">
                    <Link href={projectLinkData.href} as={projectLinkData.as}>
                        <Tooltip title={collection.name}>
                            <div className="title">
                                <h5
                                    className="semibold"
                                    data-cy={`content-card-title-${text_truncate(
                                        collection.name,
                                        100
                                    )}`}
                                >
                                    {text_truncate(collection.name, 100)}
                                </h5>
                            </div>
                        </Tooltip>
                    </Link>
                </div>
                <div className="ant-card-meta-description card-text text-limit">
                    {collection.description}
                </div>
            </div>

            <style jsx>
                {`
                    .iconHolder {
                        cursor: pointer;
                    }
                    .text-limit {
                        max-height: 66px;
                        white-space: nowrap;
                        overflow: hidden;
                        text-overflow: ellipsis;
                    }
                `}
            </style>
        </Card>
    );
};
export default AppCardCollection;
