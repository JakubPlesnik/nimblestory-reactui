import Thumbnail from '../image/Thumbnail';
import Link from 'next/link';
import { ICONS } from '../../enum/projectIcons';

import { useContext } from 'react';
import UserContext from '../../context/UserContext';
import { Card, Tooltip } from 'antd';

import AppLinkData from '../../services/applink';
import LoginContext from '../../context/LoginContext';
import PortfolioContext from '../../context/PortfolioContext';

import InfoButton from '../cardActionButtons/buttonInfo';
import EditButton from '../cardActionButtons/buttonEdit';
import PinButton from '../cardActionButtons/buttonPin';
import { isPinned } from '../../services/contentCardService';

const AppCardHomepageviewFolder = ({ organization, folder }) => {
    const { themeColors } = useContext(UserContext);
    const { setCurrentProjectGlobal } = useContext(UserContext);
    const { pinned } = useContext(LoginContext);
    const { projects } = useContext(PortfolioContext);
    const folderName = folder.title;
    const { color1Darkest } = themeColors;
    const linkData = AppLinkData({
        organization,
        project: folder.ProjSlug,
        folderId: folder.id,
    });

    const selectCurentProject = () => {
        const selectedProject = projects.find(
            item => item.slug == folder.ProjSlug
        );
        if (selectedProject) {
            setCurrentProjectGlobal(selectedProject);
        }
    };

    return (
        <Card
            className="custom-card"
            onClick={() => selectCurentProject()}
            style={{ width: 248, height: 352 }}
            cover={
                <Link href={linkData.href} as={linkData.as}>
                    <div className="iconHolder">
                        {' '}
                        <Thumbnail
                            thumbnailId={folder.thumbnailId}
                            placeholderIcon={ICONS.folder}
                            contentType={'Folder'}
                            labColor={color1Darkest}
                        />
                    </div>
                </Link>
            }
            actions={[
                <PinButton
                    disabled={true}
                    isPinned={isPinned(pinned, folder.id)}
                    key="pin"
                />,
                <EditButton key="edit" disabled={true} />,
                <InfoButton key="info" disabled={true} />,
            ]}
        >
            <div className="ant-card-cover"></div>
            <div className="ant-card-meta-detail">
                <div className="ant-card-meta-title ">
                    <Link href={linkData.href} as={linkData.as}>
                        <Tooltip title={folderName}>
                            <div className="title">
                                <h5 className="semibold">{folderName}</h5>
                            </div>
                        </Tooltip>
                    </Link>
                </div>
                {folder.numberOfChildrens && (
                    <div className="ant-card-meta-description card-text text-limit">
                        <strong>Items: </strong>
                        {<span>{folder.numberOfChildrens}</span>}
                    </div>
                )}
            </div>

            <style jsx>
                {`
                    .iconHolder {
                        cursor: pointer;
                    }
                    .text-limit {
                        max-height: 66px;
                    }

                    img {
                        object-fit: cover;
                    }
                `}
            </style>
        </Card>
    );
};
export default AppCardHomepageviewFolder;
