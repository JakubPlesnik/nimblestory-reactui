import Thumbnail from '../image/Thumbnail';
import { ICONS } from '../../enum/projectIcons';

import { useContext } from 'react';
import UserContext from '../../context/UserContext';
import { Card, Tooltip } from 'antd';

import InfoButton from '../cardActionButtons/buttonInfo';
import EditButton from '../cardActionButtons/buttonEdit';
import PinButton from '../cardActionButtons/buttonPin';

const AppCardFolder = ({ folder, setAsCurentFolder }) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;
    const thumbnailAssetId = folder['thumbnailAssetId'];

    return (
        <Card
            className="custom-card"
            onClick={() => setAsCurentFolder(folder.id)}
            style={{ width: 248, height: 352 }}
            cover={
                <div className="iconHolder">
                    {' '}
                    <Thumbnail
                        thumbnailId={thumbnailAssetId}
                        placeholderIcon={ICONS.folder}
                        contentType={'Folder'}
                        labColor={color1Darkest}
                    />
                </div>
            }
            actions={[
                <PinButton disabled={true} isPinned={false} key="pin" />,
                <EditButton key="edit" disabled={true} />,
                <InfoButton key="info" disabled={true} />,
            ]}
        >
            <div className="ant-card-cover"></div>
            <div className="ant-card-meta-detail">
                <div className="ant-card-meta-title ">
                    <Tooltip title={folder.title}>
                        <div
                            className="title"
                            onClick={() => setAsCurentFolder(folder.id)}
                        >
                            <h5
                                className="semibold"
                                data-cy="folder-card-title"
                            >
                                {folder.title}
                            </h5>
                        </div>
                    </Tooltip>
                </div>
                <div className="ant-card-meta-description card-text text-limit">
                    <strong>Items: </strong>
                    {<span>{folder.numberOfChildrens}</span>}
                </div>
            </div>

            <style jsx>
                {`
                    .iconHolder {
                        cursor: pointer;
                    }
                    .text-limit {
                        max-height: 66px;
                    }

                    img {
                        object-fit: cover;
                    }
                `}
            </style>
        </Card>
    );
};
export default AppCardFolder;
