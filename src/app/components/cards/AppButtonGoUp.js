import { useState } from 'react';
import { Card, Tooltip } from 'antd';
import { LeftCircleOutlined } from '@ant-design/icons';

const AppButtonGoUp = ({ goUpFunction, parentFolderName }) => {
    const [fillColor, setFillColor] = useState('#595959');

    return (
        <Card
            className="custom-card"
            onClick={goUpFunction}
            style={{ width: 248, height: 352 }}
            onMouseEnter={() => {
                setFillColor('#1890ff');
            }}
            onMouseLeave={() => {
                setFillColor('#595959');
            }}
            actions={[]}
        >
            <div className="addCardInfo">
                <div className="upIcon">
                    <LeftCircleOutlined
                        style={{ fontSize: '140px', color: `${fillColor}` }}
                    />
                </div>
            </div>
            <div className="ant-card-meta-detail">
                <div className="ant-card-meta-title ">
                    <Tooltip title={parentFolderName}>
                        <div className="title" onClick={goUpFunction}>
                            <h5 className="semibold">
                                Back to <br /> {parentFolderName}
                            </h5>
                        </div>
                    </Tooltip>
                </div>
            </div>

            <style jsx>
                {`
                    .iconHolder {
                        cursor: pointer;
                    }
                    .upIcon {
                        cursor: pointer;
                        width: 100%;
                        height: 140px;
                        text-align: center;
                        margin-top: 46px;
                    }
                    .text-limit {
                        max-height: 66px;
                    }
                    .title {
                        cursor: pointer;
                        color: #002545;
                        text-align: center;
                        margin-top: 20px;
                    }

                    img {
                        object-fit: cover;
                    }
                `}
            </style>
        </Card>
    );
};
export default AppButtonGoUp;
