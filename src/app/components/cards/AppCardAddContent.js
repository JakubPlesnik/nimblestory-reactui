import { useState } from 'react';
import { PlusCircleOutlined } from '@ant-design/icons';
import { Card } from 'antd';

const AppCardAddContent = ({ setOpen }) => {
    const [fillColor, setFillColor] = useState('#595959');

    return (
        <Card
            onMouseEnter={() => {
                setFillColor('#1890ff');
            }}
            onMouseLeave={() => {
                setFillColor('#595959');
            }}
            className="custom-card"
            style={{ width: 248, height: 352 }}
        >
            <div className="addCardInfo">
                <div
                    className="addThumbnail"
                    onClick={() => setOpen(true)}
                    data-cy="add-content-thumbnail"
                >
                    <PlusCircleOutlined
                        style={{ fontSize: '140px', color: `${fillColor}` }}
                    />
                </div>
            </div>

            <div className="ant-card-meta-detail">
                <div className="ant-card-meta-title">
                    <div className="title">
                        <h5 className="semibold" data-cy="add-content-title">
                            {'Add content'}
                        </h5>
                    </div>
                </div>
            </div>

            <style jsx>
                {`
                    .iconHolder {
                        cursor: pointer;
                    }

                    .addThumbnail {
                        cursor: pointer;
                        width: 100%;
                        height: 140px;
                        text-align: center;
                        margin-top: 46px;
                    }
                    .title {
                        cursor: pointer;
                        color: #002545;
                        text-align: center;
                        margin-top: 20px;
                    }
                `}
            </style>
        </Card>
    );
};
export default AppCardAddContent;
