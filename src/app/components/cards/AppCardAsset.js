import Link from 'next/link';
import { useContext } from 'react';
import AppLinkData from '../../services/applink';
import LoginContext from '../../context/LoginContext';
import UserContext from '../../context/UserContext';
import Thumbnail from '../image/Thumbnail';
import PortfolioContext from '../../context/PortfolioContext';
import { ICONS } from '../../enum/projectIcons';
import { Card, Tooltip } from 'antd';

import InfoButton from '../cardActionButtons/buttonInfo';
import EditButton from '../cardActionButtons/buttonEdit';
import PinButton from '../cardActionButtons/buttonPin';

import { isPinned } from '../../services/contentCardService';

function text_truncate(str, length) {
    if (!str) return '';

    const ending = '...';

    if (str.length > length) {
        return str.substring(0, length - ending.length).trim() + ending;
    }
    return str;
}

const Asset = ({ organization, asset }) => {
    const { pinned } = useContext(LoginContext);
    const linkData = AppLinkData({
        organization,
        project: asset.ProjSlug,
        asset: asset.slug,
    });

    const { themeColors } = useContext(UserContext);

    const { color1Darkest } = themeColors;

    const thumbnailAssetId = asset['thumbnailId'];
    const { setCurrentProjectGlobal } = useContext(UserContext);
    const { projects } = useContext(PortfolioContext);

    const selectCurentProject = () => {
        const selectedProject = projects.find(
            item => item.slug == asset.ProjSlug
        );
        if (selectedProject) {
            setCurrentProjectGlobal(selectedProject);
        }
    };

    const getIcon = () => {
        if (!asset.entryType) {
            return '';
        }

        if (asset.entryType === 'pctAsset') {
            return ICONS.pctAsset;
        }

        if (asset.entryType === 'pctLink') {
            return ICONS.pctLink;
        }

        if (asset.entryType === 'pctVideo') {
            return ICONS.pctVideo;
        }

        if (asset.entryType === 'pctWall') {
            return ICONS.pctWall;
        }

        if (asset.entryType === 'pctModuleFile') {
            return ICONS.pctModuleFile;
        }
    };
    return (
        <Card
            className="custom-card"
            onClick={() => selectCurentProject()}
            style={{ width: 248, height: 352 }}
            cover={
                <Link href={linkData.href} as={linkData.as}>
                    <div className="iconHolder">
                        {' '}
                        <Thumbnail
                            thumbnailId={thumbnailAssetId}
                            placeholderIcon={getIcon()}
                            contentType={asset.AssetType}
                            labColor={color1Darkest}
                        />
                    </div>
                </Link>
            }
            actions={[
                <PinButton
                    disabled={true}
                    isPinned={isPinned(pinned, asset.id)}
                    key="pin"
                />,
                <EditButton key="edit" disabled={true} />,
                <InfoButton key="info" disabled={true} />,
            ]}
        >
            <div className="ant-card-cover"></div>
            <div className="ant-card-meta-detail">
                <div className="ant-card-meta-title ">
                    <Link href={linkData.href} as={linkData.as}>
                        <Tooltip title={asset.title}>
                            <div className="title">
                                <h5
                                    className="semibold"
                                    data-cy={`content-card-title-${text_truncate(
                                        asset.title,
                                        100
                                    )}`}
                                >
                                    {text_truncate(asset.title, 100)}
                                </h5>
                            </div>
                        </Tooltip>
                    </Link>
                </div>
                <div className="ant-card-meta-description card-text text-limit">
                    {asset.description}
                </div>
            </div>

            <style jsx>
                {`
                    .iconHolder {
                        cursor: pointer;
                    }
                    .text-limit {
                        max-height: 66px;
                    }

                    img {
                        object-fit: cover;
                    }
                `}
            </style>
        </Card>
    );
};
export default Asset;
