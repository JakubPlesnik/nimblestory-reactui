import { useState, useEffect } from 'react';
import PaginationContext from '../context/PaginationContext';

const AppPagination = ({ children }) => {
    const [onReachedEnd, setOnReachedEnd] = useState(null);

    useEffect(() => {
        if (onReachedEnd) {
            window.addEventListener('scroll', onScroll, false);
        } else {
            window.removeEventListener('scroll', onScroll, false);
        }

        // cleanup on unmount
        return () => window.removeEventListener('scroll', onScroll, false);
    }, [onReachedEnd]);

    const onScroll = () => {
        const { innerHeight } = window;
        const { scrollHeight } = document.documentElement;
        const scrollTop =
            (document.documentElement && document.documentElement.scrollTop) ||
            document.body.scrollTop;
        if (innerHeight + scrollTop < scrollHeight - 300) return;
        if (onReachedEnd && onReachedEnd.trigger) onReachedEnd.trigger(true);
    };

    return (
        <PaginationContext.Provider
            value={{
                onReachedEnd,
                setOnReachedEnd,
            }}
        >
            {children}
        </PaginationContext.Provider>
    );
};
export default AppPagination;
