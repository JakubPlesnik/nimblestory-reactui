import { Fragment, useContext } from 'react';
import Router from 'next/router';
import FolderFilledIcon from '@atlaskit/icon/glyph/folder-filled';
import WorldIcon from '@atlaskit/icon/glyph/world';
import ShortcutIcon from '@atlaskit/icon/glyph/shortcut';
import RoomMenuIcon from '@atlaskit/icon/glyph/room-menu';
import {
    GroupHeading,
    HeaderSection,
    Item as ItemComponent,
    MenuSection,
    Separator,
} from '@atlaskit/navigation-next';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';
import SidebarHeaderTitle from './SidebarHeaderTitle';

const Item = ({ testKey, ...props }) => {
    const item = <ItemComponent {...props} />;
    return testKey ? <div data-webdriver-test-key={testKey}>{item}</div> : item;
};

const ContainerNavigation = () => {
    const { pathinfo } = useContext(UserContext);
    const { organization } = useContext(LoginContext);
    const { geoexplorerAccess, conceptexplorerAccess } = organization;

    return (
        <div data-webdriver-test-key="container-navigation">
            <HeaderSection>
                {({ css }) => (
                    <div
                        data-webdriver-test-key="container-header"
                        style={{
                            ...css,
                            padding: '20px 0px',
                        }}
                    >
                        <SidebarHeaderTitle />
                    </div>
                )}
            </HeaderSection>

            <MenuSection>
                {({ className }) => (
                    <div className={className}>
                        <Item
                            before={FolderFilledIcon}
                            text="Portfolio"
                            isSelected={pathinfo.application == 'portfolio'}
                            testKey="container-item-portfolio"
                            onClick={() => {
                                Router.push(
                                    `/projects?application=portfolio&organization=${pathinfo.organization}`,
                                    `/portfolio/${pathinfo.organization}/all`
                                );
                            }}
                        />

                        {geoexplorerAccess && (
                            <Item
                                before={WorldIcon}
                                text="Geographic Explorer"
                                isSelected={
                                    pathinfo.application == 'geoexplorer'
                                }
                                testKey="container-item-geoexplorer"
                                onClick={() => {
                                    Router.push(
                                        `/geoexplorersolutions?application=geoexplorer&organization=${pathinfo.organization}`,
                                        `/geoexplorer/${pathinfo.organization}`
                                    );
                                }}
                            />
                        )}

                        {conceptexplorerAccess && (
                            <Item
                                before={RoomMenuIcon}
                                text="Concept Explorer"
                                isSelected={
                                    pathinfo.application == 'conceptexplorer'
                                }
                                testKey="container-item-conceptexplorer"
                                onClick={() => {
                                    Router.push(
                                        `/conceptsolutions?application=conceptexplorer&organization=${pathinfo.organization}`,
                                        `/conceptexplorer/${pathinfo.organization}`
                                    );
                                }}
                            />
                        )}

                        <Separator />
                        {(pathinfo.application == 'portfolio' ||
                            pathinfo.application == 'home') && (
                            <Fragment>
                                <GroupHeading>Portfolio Shortcuts</GroupHeading>
                                <Item
                                    before={ShortcutIcon}
                                    text="All Projects"
                                    onClick={() => {
                                        Router.push(
                                            `/projects?application=portfolio&organization=${pathinfo.organization}`,
                                            `/portfolio/${pathinfo.organization}/all`
                                        );
                                    }}
                                />
                                <Item
                                    before={ShortcutIcon}
                                    text="All Content"
                                    onClick={() => {
                                        Router.push(
                                            `/project?application=portfolio&organization=${pathinfo.organization}&project=assets`,
                                            `/portfolio/${pathinfo.organization}/assets`
                                        );
                                    }}
                                />
                                {/* <Item
                  before={ShortcutIcon}
                  text="Active Projects"
                  onClick={() => {
                    Router.push(
                      `/projects?application=portfolio&organization=${pathinfo.organization}&projectfilter=active`,
                      `/portfolio/${pathinfo.organization}/active`
                    )
                  }}
                />
                <Item
                  before={ShortcutIcon}
                  text="Inactive Projects"
                  onClick={() => {
                    Router.push(
                      `/projects?application=portfolio&organization=${pathinfo.organization}&projectfilter=inactive`,
                      `/portfolio/${pathinfo.organization}/inactive`
                    )
                  }}
                /> */}

                                {/* Assets Shortcut element */}
                                {/* <GroupHeading>Assets Shortcuts</GroupHeading> */}

                                {/* <Item
                  before={ShortcutIcon}
                  text="Active Assets"
                  onClick={() => {
                    Router.push(
                      `/project?application=portfolio&organization=${pathinfo.organization}&project=assets&projectfilter=active`,
                      `/portfolio/${pathinfo.organization}/assets/active`
                    )
                  }}
                />
                <Item
                  before={ShortcutIcon}
                  text="Inactive Assets"
                  onClick={() => {
                    Router.push(
                      `/project?application=portfolio&organization=${pathinfo.organization}&project=assets&projectfilter=inactive`,
                      `/portfolio/${pathinfo.organization}/assets/inactive`
                    )
                  }}
                /> */}
                            </Fragment>
                        )}
                    </div>
                )}
            </MenuSection>
        </div>
    );
};
export default ContainerNavigation;
