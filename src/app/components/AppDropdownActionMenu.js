import { useEffect } from 'react';
import * as React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

const AppDropdownActionMenu = ({
    label,
    title,
    menuheader,
    options,
    selectedIndex,
    onSelect,
}) => {
    const lists = options;
    useEffect(() => {
        lists.pop();
    }, [options]);
    return (
        <React.Fragment>
            {label && (
                <span className="label theme-color1dark--color">{label}</span>
            )}
            <Dropdown>
                <Dropdown.Toggle block variant="secondary" size="lg">
                    {title}
                </Dropdown.Toggle>

                <Dropdown.Menu id={title}>
                    <Dropdown.Header>{menuheader}</Dropdown.Header>
                    {lists.map((list, index) => (
                        <Dropdown.Item
                            key={index}
                            onClick={() => onSelect(index)}
                            active={index === selectedIndex}
                        >
                            {list}
                        </Dropdown.Item>
                    ))}
                </Dropdown.Menu>
            </Dropdown>
            <style jsx>
                {`
                    .label {
                        font-weight: bold;
                        font-size: 12px;
                        letter-spacing: 1px;
                        color: #002445;
                    }
                `}
            </style>
        </React.Fragment>
    );
};

export default AppDropdownActionMenu;
