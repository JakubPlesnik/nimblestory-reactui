import { useState, useContext, useEffect } from 'react';
import cn from 'classnames';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';

export const TopPanelProjectAssets = ({ assetsnum }) => {
    const countAssets = assetsnum ? `(${assetsnum})` : '';
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    return (
        <div className="topPanel">
            <div
                className="portfolioHeader"
                style={{
                    color: color1Darkest,
                }}
            >
                <div className="secondaryTitle">Projects</div>
                <div className="primaryTitle">
                    Content
                    {countAssets}
                </div>
            </div>

            <style jsx>
                {`
                    .portfolioHeader {
                        padding-top: 16px;
                        padding-left: 36px;
                    }
                    .secondaryTitle {
                        font-weight: 500;
                        font-size: 12px;
                    }
                    .primaryTitle {
                        font-weight: 500;
                        font-size: 24px;
                        line-height: 28px;
                        padding: 9px 0px;
                    }
                    .topPanel {
                        padding-top: 3px;
                        position: relative;
                        maring-bottom: 20px;
                    }
                `}
            </style>
        </div>
    );
};

export const TopPanelProject = ({ project }) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;
    const { organization } = useContext(LoginContext);
    const [ready, setReady] = useState(false);

    // const displayManagerMenu =
    //     user &&
    //     (user.groups.includes('manager') || user.groups.includes('admin'));

    useEffect(() => {
        if (project && project.title) {
            setReady(true);
        }
    }, [project]);

    return (
        <div className="topPanel">
            <div className="toolbar-row">
                <div
                    className="portfolioHeader"
                    style={{
                        color: color1Darkest,
                    }}
                >
                    <div
                        className={cn('titleContainer', { invisible: !ready })}
                    >
                        <div className="title">
                            <h1>{organization.title} </h1>
                        </div>
                        <div className="subTitle">
                            <h3>{project.title}</h3>
                        </div>
                    </div>
                </div>
            </div>

            <style jsx>
                {`
                    .collapseButton {
                        margin-left: 20px;
                    }
                    .columns {
                        display: flex;
                    }
                    .columns > div {
                        flex: 0 0 50%;
                        padding-right: 15px;
                    }
                    .pageTitle {
                        float: left;
                        padding-left: 45px;
                        font-size: 24px;
                        height: 50px;
                        width: 100%;
                        padding-right: 305px;
                        line-height: 50px;
                        overflow: hidden;
                    }
                    .titleP {
                        line-height: 24px;
                        display: inline-block;
                    }
                    .topPanel {
                        position: relative;
                    }
                    .tabsContainer {
                        background-color: #f3f5f8;
                        clear: both;
                    }
                    .tabContent {
                        clear: both;
                        display: flex;
                        padding-top: 7px;
                    }
                    .panelNav {
                        width: 310px;
                        margin-right: 20px;
                    }

                    .topPanelSubtitle {
                        color: #0d3b5f;
                        font-weight: bold;
                        text-transform: uppercase;
                        margin-top: 4px;
                        margin-bottom: 4px;
                        letter-spacing: 2px;
                    }
                    .projInfo > p {
                        font-size: 16px;
                        line-height: 20px;
                        color: #656565;
                        padding-right: 288px;
                    }

                    .projInfo {
                        width: 100%;
                    }

                    .toolbar-row {
                        display: flex;
                        flex-direction: row;
                        align-items: flex-end;
                        justify-content: space-between;
                        width: 100%;
                    }
                    .portfolioHeader {
                        width: 100%;
                    }
                    .secondaryTitle {
                        font-weight: 500;
                        font-size: 12px;
                        background-color: #fafafa;
                    }
                    .titleContainer {
                        padding: 21px 0px 0px 28px;
                        background-color: #fafafa;
                    }

                    .subTitle {
                        padding-bottom: 24px;
                    }

                    .hidden {
                        display: none;
                    }
                `}
            </style>
        </div>
    );
};
