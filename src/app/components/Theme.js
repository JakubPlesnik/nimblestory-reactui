import React, { useReducer, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import Router from 'next/router';
import Head from './head';
import LoaderContext from '../context/LoaderContext';
import indexStyle from '../styles/index.scss';
import ThemePageLoadingIndicator from './ThemePageLoadingIndicator';
import LoginContext from '../context/LoginContext';
import ManagerModal from './ManagerModal';

const loadersInitialState = [{ title: 'page', progress: 1 }];

function loadersReducer(state, action) {
    switch (action.progress) {
        case 100:
            return filterState(state, action);
        default:
            return [...filterState(state, action), action];
    }
}
function filterState(state, action) {
    return state.filter(item => item.title !== action.title);
}

function addToHeadReduced(state, action) {
    return [...state, action];
}

const Theme = ({ insertedStyles, children }) => {
    const [loadersState, updateLoaders] = useReducer(
        loadersReducer,
        loadersInitialState
    );
    const [headState, addToHead] = useReducer(addToHeadReduced, []);
    const { isOrgPermisssion, isModalOpen } = useContext(LoginContext);

    useEffect(() => {
        updateLoaders({ title: 'page', progress: 100 });
    }, []);

    return (
        <React.Fragment>
            <Head
                description="An Interactive Visualization Platform"
                mainstyle={{ __html: indexStyle }}
            >
                {insertedStyles &&
                    insertedStyles.map((item, i) => (
                        <style
                            key={`is${i}`}
                            dangerouslySetInnerHTML={{ __html: item }}
                        />
                    ))}
              
                {headState.map((item, i) => {
                    if (item.type == 'stylesheet') {
                        return (
                            <link
                                key={`hs${i}`}
                                href={item.href}
                                rel="stylesheet"
                            />
                        );
                    }
                    if (item.type == 'css') {
                        return (
                            <style
                                key={`hs${i}`}
                                dangerouslySetInnerHTML={{ __html: item.data }}
                            />
                        );
                    }
                })}
            </Head>

            {isModalOpen && <ManagerModal />}
            <LoaderContext.Provider
                value={{ loadersState, updateLoaders, addToHead }}
            >
                <ModalTransition>
                    {!isOrgPermisssion && (
                        <Modal
                            actions={[
                                {
                                    text: 'Close',
                                    onClick: () => {
                                        Router.push('/loggedin');
                                    },
                                },
                            ]}
                            heading=" "
                        >
                            <p>
                                Your userid does not have permission to access
                                this content.
                            </p>
                        </Modal>
                    )}
                </ModalTransition>

                {/* <ThemePageLoadingIndicator /> */}
                {isOrgPermisssion && <ThemePageLoadingIndicator />}

                {isOrgPermisssion && <div id="appwrapper">{children}</div>}
            </LoaderContext.Provider>
        </React.Fragment>
    );
};

Theme.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.element),
        PropTypes.element,
    ]).isRequired,
};

export default Theme;
