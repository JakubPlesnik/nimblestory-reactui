import React, { Fragment, useState, useContext, useEffect } from 'react';
import Modal, { ModalFooter, ModalTransition } from '@atlaskit/modal-dialog';
import Button from '@atlaskit/button';
import cn from 'classnames';
import LoginContext from '../context/LoginContext';
import UserContext from '../context/UserContext';
import { savePins, authorizeToken } from '../services/cmsapi-manage';
import ManagerDraggableList from './ManagerDraggableList';
import Icon from './Icon';
import {
    ICONS
  } from '../enum/projectIcons';

const lblPin = 'Pin this page';
const lblUnPin = 'Page pinned';

const CardItem = ({ item, isActive, isHovering, isDragging, onRemoveItem }) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    return (
        <div
            className={cn(
                'cardwrapper',
                { dragging: !!isDragging || isActive },
                { hovering: isHovering }
            )}
        >
            {item.section === 'magaAssets' && (
                <Icon
                name={ICONS.pctAsset}
                size="24"
                type="outline"
                color={color1Darkest}
            />
            )}
            {item.section === 'conceptexplorer' && (
                <Icon
                name={ICONS.conceptexplorer}
                size="24"
                type="outline"
                color={color1Darkest}
            />
            )}
            {item.section === 'geoexplorer' && (
              <Icon
              name={ICONS.geoexplorer}
              size="24"
              type="outline"
              color={color1Darkest}
          />
            )}
            {item.section === 'projects' && (
               <Icon
               name={ICONS.project}
               size="24"
               type="outline"
               color={color1Darkest}
           />
            )}
            {item.title}
            <Button onClick={() => onRemoveItem(item)} appearance="subtle">
                Remove
            </Button>
            <style jsx>
                {`
        .dragging {
            cursor:  grabbing;
            text-decoration: none;
            background-color: #cecece;
        }
        .hovering {
            background-color: #cecece;
            text-decoration: none;
        }
        .cardwrapper {
            display: flex;
            justify-content: space-between;
            align-items: baseline;

            border-radius: 3px;
            display: flex;
            position: relative;
            padding: 10px 10px;
            border: 1px solid #cecece;
            margin-bottom: 6px;
        `}
            </style>
        </div>
    );
};

const ManagerPinModalFooter = ({ onSave }) => (
    <ModalFooter className={cn('modal-footer')} showKeyline={false}>
        <div>Drag/Drop to set Home Page display order</div>
        <Button
            style={{ backgroundColor: '#002445', borderRadius: '0px' }}
            variant="primary"
            onClick={onSave}
        >
            <p style={{ color: '#fff', paddingTop: '15px' }}>Save</p>
        </Button>
    </ModalFooter>
);

const ManagerPinPageButton = ({ newpin }) => {
    const { organization, pinned, setPinned } = useContext(LoginContext);
    const [localPins, setLocalPins] = useState([]);
    const [isOpen, setOpen] = useState(false);
    const [btnText, setBtnText] = useState('');

    const ManagerPinModalFooterWrapper = () => (
        <ManagerPinModalFooter
            onSave={() => {
                authorizeToken().then(success => {
                    if (success) {
                        setPinned(localPins);
                        savePins({
                            organization: organization.slug,
                            pins: localPins.map(i => i.id),
                        });
                    }
                });
                setOpen(false);
            }}
        />
    );

    useEffect(() => {
        if (isOpen) {
            const found = pinned.findIndex(i => i.id == newpin.id);
            const syncedPinned =
                found === -1 ? [newpin, ...pinned] : [...pinned];
            setLocalPins(syncedPinned);
        }
    }, [isOpen]);

    useEffect(() => {
        const found = pinned.findIndex(i => i.id == newpin.id);
        if (found === -1) {
            setBtnText(lblPin);
        } else {
            setBtnText(lblUnPin);
        }
    }, [pinned]);

    return (
        <Fragment>
            <div>
                <Button className="btnPin" onClick={() => setOpen(true)}>
                    {btnText}
                </Button>
                <style jsx>
                    {`
                        div {
                            margin-top: 10px;
                        }
                    `}
                </style>
            </div>
            <ModalTransition>
                {isOpen && (
                    <Modal
                        onClose={() => setOpen(false)}
                        heading="Edit pinned items list"
                        height="100%"
                        components={{ Footer: ManagerPinModalFooterWrapper }}
                    >
                        {localPins.length > 0 && (
                            <ManagerDraggableList
                                pinned={localPins}
                                onOrderChange={updated => {
                                    setLocalPins([...updated]);
                                }}
                                components={{ Card: CardItem }}
                            />
                        )}
                    </Modal>
                )}
            </ModalTransition>
        </Fragment>
    );
};
export default ManagerPinPageButton;
