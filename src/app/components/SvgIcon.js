const getPath = (name, props) => {
    switch (name) {
        case 'icon-pin':
            return (
                <path
                    {...props}
                    d="M16,12V4H17V2H7V4H8V12L6,14V16H11.2V22H12.8V16H18V14L16,12Z"
                />
            );
        default:
            return <path />;
    }
};

const getViewBox = name => {
    switch (name) {
        case 'icon-pin':
            return '0 0 24 24';
        default:
            return '';
    }
};

const SVG = ({
    name = '',
    style = {},
    fill = '#000',
    width = '100%',
    className = '',
    height = '100%',
}) => (
    <svg
        width={width}
        style={style}
        height={height}
        viewBox={getViewBox(name)}
        className={className}
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
    >
        {getPath(name, { fill })}
    </svg>
);

export default SVG;
