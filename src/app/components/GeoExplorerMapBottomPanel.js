import { useState, useEffect } from 'react';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';

import {
    kPropLocationName,
    kPropAddress,
    kPropPOC,
    kPropPhoneNumber,
    kPropEmail,
    kPropCategory,
    kInvTitle,
    kPartNumber,
    kTypeOfMaterial,
    kOwningBSO,
    kOwningOrg,
    kResponsible,
    kManagedby,
    kPrimeContractor,
    kStatus,
    kCondition,
} from '../services/geoexplorerconsts';

const GeoExplorerPlanInfoTable = ({ data }) => {
    const rows = [
        [kPropLocationName, kPropAddress],
        [kPropPOC, kPropPhoneNumber],
        [kPropEmail],
    ];
    return (
        <table>
            <tbody>
                {rows.map((row, i) => (
                    <tr colSpan={row.length} key={i}>
                        {row.map((cell, i) => (
                            <td key={i}>
                                <div className="label">{cell}</div>
                                <div className="value">{data[cell]}</div>
                            </td>
                        ))}
                    </tr>
                ))}
            </tbody>
            <style jsx>
                {`
                    table {
                        width: 100%;
                        max-width: 600px;
                        color: #212529;
                    }
                    td {
                        vertical-align: top;
                    }

                    .label {
                        color: #5f5f5f;
                        text-transform: uppercase;
                        font-weight: bold;
                        font-size: 10px;
                        line-height: 24px;
                    }
                    .value {
                        font-weight: 500;
                        font-size: 14px;
                        line-height: 24px;
                        color: #293e57;
                        white-space: pre-wrap;
                    }
                `}
            </style>
        </table>
    );
};

const GeoExplorerPanelTableRow = ({ data, columns }) => (
    <tr>
        {columns.map((column, i) => (
            <td key={i}>{data[column]}</td>
        ))}
        <style jsx>
            {`
                td {
                    padding: 0.3rem;
                    vertical-align: top;
                    border-top: 1px solid #dee2e6;
                }
            `}
        </style>
    </tr>
);
const GeoExplorerPanelTable = ({ inventory }) => {
    const headerSources = [
        kPropCategory,
        kInvTitle,
        kPartNumber,
        kTypeOfMaterial,
        kOwningBSO,
        kOwningOrg,
        kResponsible,
        kManagedby,
        kPrimeContractor,
        kStatus,
        kCondition,
    ];
    const headerLabels = [
        'Cat.',
        'Title/Desc',
        'PartNumber',
        'Type',
        'BSO',
        'OwnOrg',
        'RespOrg',
        'MngBy',
        'Prime',
        'Status',
        'Cond',
    ];
    return (
        <table>
            <thead>
                <tr>
                    {headerLabels.map((label, i) => (
                        <th key={i}>{label}</th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {inventory.map((data, i) => (
                    <GeoExplorerPanelTableRow
                        key={i}
                        data={data}
                        columns={headerSources}
                    />
                ))}
            </tbody>
            <style jsx>
                {`
                    table {
                        width: 100%;
                        color: #212529;
                        border-collapse: collapse;
                        position: relative;
                        font-size: 12px;
                        background-color: #dddddd;
                    }
                    table thead th {
                        vertical-align: bottom;
                        border-bottom: 2px solid #dee2e6;
                        text-align: left;

                        padding: 0.3rem;
                        vertical-align: top;
                    }
                `}
            </style>
        </table>
    );
};

const GeoExplorerMapBottomPanel = ({ plant, setPanelState }) => {
    if (!plant) {
        return <></>;
    }
    const kTabResults = 'kTabResults';
    const kTabPlant = 'kTabPlant';
    const kTabNone = 'kTabNone';

    const kPanelSmall = 'kPanelSmall';
    const kPanelMedium = 'kPanelMedium';
    const kPanelLarge = 'kPanelLarge';

    const [selectedTab, setSelectedTabHook] = useState(kTabNone);
    const [panelSize, setPanelSizeHook] = useState(kPanelSmall);
    const classesPanelSize = {
        kPanelSmall: 'small',
        kPanelMedium: 'compact',
        kPanelLarge: 'expanded',
    };
    const classPanelSize = classesPanelSize[panelSize];

    const setPanelSize = newSize => {
        if (!plant) {
            return;
        }

        if (newSize == kPanelSmall) {
            setSelectedTabHook(kTabNone);
        } else if (selectedTab == kTabNone) {
            setSelectedTabHook(kTabResults);
        }

        setPanelSizeHook(newSize, setPanelState({ bottom: newSize }));
    };
    const setSelectedTab = newTab => {
        if (!plant) {
            return;
        }

        setSelectedTabHook(newTab);
        if (panelSize == kPanelSmall) {
            setPanelSizeHook(kPanelMedium);
        }
    };

    useEffect(() => {
        if (panelSize == kPanelSmall) {
            setPanelSize(kPanelMedium);
        }
    }, [plant]);
    return (
        <div className={`geoMapBottomPanel ${classPanelSize}`}>
            <div className="panelTopNav">
                <ul className="menu">
                    <li>
                        <button
                            className={
                                selectedTab == kTabResults ? 'selected' : ''
                            }
                            onClick={() => setSelectedTab(kTabResults)}
                        >
                            Results
                        </button>
                    </li>
                    <li>
                        <button
                            className={
                                selectedTab == kTabPlant ? 'selected' : ''
                            }
                            onClick={() => setSelectedTab(kTabPlant)}
                        >
                            Plant
                        </button>
                    </li>
                </ul>

                <div className="actionButtonsContainer">
                    <OverlayTrigger
                        key="btnexport"
                        placement="top"
                        overlay={
                            <Tooltip id="tooltipexport">
                                Data export is currently disabled
                            </Tooltip>
                        }
                    >
                        <button
                            className={`${
                                selectedTab == kTabResults &&
                                panelSize != kPanelSmall
                                    ? 'btnExport'
                                    : 'btnExport invisible'
                            }`}
                        >
                            Export...
                        </button>
                    </OverlayTrigger>

                    <OverlayTrigger
                        key="resize1"
                        placement="left"
                        overlay={
                            <Tooltip id="tooltipresize1">
                                Minimize panel
                            </Tooltip>
                        }
                    >
                        <button
                            className={`btnPanelSize iconPanelSmall ${
                                panelSize == kPanelSmall ? 'sizeSelected' : ''
                            }`}
                            onClick={() => setPanelSize(kPanelSmall)}
                        />
                    </OverlayTrigger>

                    <OverlayTrigger
                        key="resize2"
                        placement="left"
                        overlay={
                            <Tooltip id="tooltipresize2">Compact panel</Tooltip>
                        }
                    >
                        <button
                            className={`btnPanelSize iconPanelMedium ${
                                panelSize == kPanelMedium ? 'sizeSelected' : ''
                            }`}
                            onClick={() => setPanelSize(kPanelMedium)}
                        />
                    </OverlayTrigger>

                    <OverlayTrigger
                        key="resize3"
                        placement="left"
                        overlay={
                            <Tooltip id="tooltipresize3">Expand panel</Tooltip>
                        }
                    >
                        <button
                            className={`btnPanelSize iconPanelLarge ${
                                panelSize == kPanelLarge ? 'sizeSelected' : ''
                            }`}
                            onClick={() => setPanelSize(kPanelLarge)}
                        />
                    </OverlayTrigger>
                </div>
            </div>
            <div
                className={`${
                    selectedTab == kTabPlant ? 'panelPlant' : 'hidden'
                }`}
            >
                {plant && <GeoExplorerPlanInfoTable data={plant} />}
            </div>
            <div
                className={`${
                    selectedTab == kTabResults ? 'panelTable' : 'hidden'
                }`}
            >
                {plant && <GeoExplorerPanelTable inventory={plant.inventory} />}
            </div>
            <style jsx>
                {`
                    .actionButtonsContainer {
                        width: 200px;
                        margin-right: 0;
                        margin-top: 9px;
                    }
                    .btnExport {
                        background-color: #b1b1b1;
                        border: none;
                        height: 30px;
                        line-height: 30px;
                        font-weight: 400;
                        font-size: 11px;
                        width: 71px;
                        color: #333333;
                        outline: none;
                        margin-right: 23px;
                    }
                    .panelPlant {
                        padding-left: 30px;
                    }
                    .geoMapBottomPanel {
                        width: 100%;
                        background-color: #dddddd;
                    }
                    .geoMapBottomPanel.compact {
                        height: 240px;
                    }
                    .geoMapBottomPanel.small {
                        height: 48px;
                    }
                    .geoMapBottomPanel.expanded {
                        position: absolute;
                        top: 0;
                        z-index: 2;
                        bottom: 0;
                        width: calc(100% - 85px);
                    }

                    .panelTopNav {
                        width: 100%;
                        height: 48px;
                        display: flex;
                        background-color: #cecece;
                        padding-left: 200px;
                    }

                    ul.menu {
                        margin: auto;
                        padding: 0;
                        width: 240px;
                        outline: none;
                    }
                    ul.menu > li {
                        list-style: none;
                        display: inline-block;
                    }
                    ul.menu > li > button {
                        border: none;
                        outline: none;
                        line-height: 48px;
                        height: 48px;
                        font-size: 14px;
                        width: 120px;
                        background-color: #cecece;
                        color: #5d5d5d;
                    }

                    button.selected {
                        background-color: #dddddd !important;
                    }
                    button.sizeSelected {
                        font-weight: bold;
                        background-color: #2f2e2e !important;
                    }

                    button.btnPanelSize {
                        border: none;
                        outline: none;
                        background-color: #797979;
                        margin-right: 3px;
                        border-radius: 2px;
                    }
                    button.iconPanelSmall {
                        width: 28px;
                        height: 6px;
                    }
                    button.iconPanelMedium {
                        width: 28px;
                        height: 10px;
                    }
                    button.iconPanelLarge {
                        width: 28px;
                        height: 20px;
                    }
                `}
            </style>
        </div>
    );
};
export default GeoExplorerMapBottomPanel;
