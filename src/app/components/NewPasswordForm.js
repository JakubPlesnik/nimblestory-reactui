import { useState } from 'react';
import { saveNewPassword } from '../services/api/apiService';

const Form = ({ token }) => {
    const [newPassword, setNewPassword] = useState('');
    const [retypedPassword, setRetypedPassword] = useState('');
    const [message, setMessage] = useState('');
    const [redirectButtonVisible, setRedirectButtonVisible] = useState(false);

    const createNewPassword = e => {
        e.preventDefault();
        if (newPassword === retypedPassword && newPassword !== '') {
            saveNewPassword(
                token,
                newPassword,
                onSetPasswordSuccess,
                onSetPasswordError
            );
        } else if (newPassword === '' && newPassword === '')
            setMessage('Please enter your username and password');
        else if (newPassword !== retypedPassword)
            setMessage('The password confirmation does not match');
    };

    const onSetPasswordSuccess = () => {
        setMessage('New password has been set');
        setRedirectButtonVisible(true);
    };

    const onSetPasswordError = error => {
        if (!error) {
            setMessage('Server is not responding');
        } else if (error.status === 400) {
            setMessage(error.data.error.message);
        } else setMessage('Something went wrong.');
    };

    return (
        <form className="sign-in">
            <div className="form-group">
                <label htmlFor="email">New Password</label>
                <input
                    type="password"
                    name="newpass"
                    placeholder="New Password"
                    className="form-control"
                    onChange={e => setNewPassword(e.target.value)}
                />
            </div>

            <div className="form-group">
                <label htmlFor="email">Confirm your new password</label>
                <input
                    type="password"
                    name="retypedpass"
                    placeholder="confirm password"
                    className="form-control"
                    onChange={e => setRetypedPassword(e.target.value)}
                />
            </div>

            {message != '' && (
                <div style={{ marginBottom: '10px' }} className="message">
                    {message}
                </div>
            )}

            {!redirectButtonVisible && (
                <button
                    className="btn btn-primary"
                    onClick={e => createNewPassword(e)}
                >
                    Change password
                </button>
            )}

            {redirectButtonVisible && (
                <a className="btn btn-primary" href="/login">
                    Back to the login
                </a>
            )}
        </form>
    );
};

export default Form;
