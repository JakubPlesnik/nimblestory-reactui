import * as React from 'react';
import { useState, useContext, useReducer } from 'react';
import cn from 'classnames';
import Link from 'next/link';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import AppLinkData from '../services/applink';
import UserContext from '../context/UserContext';
import { saveProject } from '../services/cmsapi-manage';
import ManageInputArea from './ManageInputArea';
import ManageInputMultipleOption from './ManageInputMultipleOption';
import ManageInputSingleOption from './ManageInputSingleOption';
import ManageButtonUploadFile from './ManageButtonUploadFile';
import { projectSortOptions } from '../services/filteringProjects';

const ManageTableProjects = props => {
    const { pathinfo } = useContext(UserContext);
    const { organization } = pathinfo;
    const selectedSortOption = projectSortOptions.findIndex(
        item => item.title == 'A-Z'
    );

    return (
        <Table bordered size="sm">
            <thead>
                <tr>
                    <th />
                    <th className="minwidth-medium">Project Name</th>
                    <th className="minwidth-small">Status</th>
                    <th>Content</th>
                    <th className="minwidth-long">Description</th>
                    <th>Type</th>
                    <th>Year</th>
                    <th>Thumb</th>
                    {/* <th>Archive</th> */}
                    <th>View</th>
                </tr>
            </thead>
            <tbody>
                {props.projects &&
                    props.projects
                        .sort(projectSortOptions[selectedSortOption].sorter)
                        .map(project => (
                            <ManageTableProjectsRow
                                {...props}
                                key={project.id}
                                organization={organization}
                                editView={project.id === 'new' ? 1 : 0}
                                project={project}
                                // actNavigateToAssetsFiltered={actNavigateToAssetsFiltered}
                                // managerCancelNewProject={managerCancelNewProject}
                            />
                        ))}
            </tbody>
            <style jsx>
                {`
                    .minwidth-small {
                        min-width: 88px;
                    }
                    .minwidth-medium {
                        min-width: 210px;
                    }
                    .minwidth-long {
                        min-width: 310px;
                    }
                `}
            </style>
        </Table>
    );
};
const ManageTableProjectsRow = props => {
    const [isEditView, setEditView] = useState(props.editView);
    return isEditView === 0 ? (
        <ManageTableProjectsRowView {...props} setEditView={setEditView} />
    ) : (
        <ManageTableProjectsRowEdit {...props} setEditView={setEditView} />
    );
};
const ManageTableProjectsRowView = ({
    organization,
    project,
    setEditView,
    actNavigateToAssetsFiltered,
    updateProjectData,
}) => {
    const projectLinkData = AppLinkData({
        organization,
        project: project.slug,
    });

    const projectViewLink = (
        <Link href={projectLinkData.href} as={projectLinkData.as}>
            <a className="btn btn-secondary" target="_blank">
                Live
            </a>
        </Link>
    );

    // const projectEditLink = (
    //   <a className="btn btn-secondary" target="_blank" href={project.cpEditUrl}>
    //     CMS
    //   </a>
    // )
    const btnProjectInlineEdit = (
        <Button variant="outline-info" size="sm" onClick={() => setEditView(1)}>
            Edit
        </Button>
    );
    const btnAssetsInProject = (
        <button
            className={cn('btn', { empty: !project.assetsCount })}
            size="sm"
            onClick={() => actNavigateToAssetsFiltered(project.slug)}
        >
            {project.assetsCount || 0}
            <style jsx>
                {`
                    .empty {
                        background-color: #cecece;
                    }
                    button {
                        min-width: 40px;
                        padding: 0.25rem 0.5rem;
                        font-size: 0.875rem;
                        line-height: 1.5;
                        border-radius: 0;
                        color: #fff;
                        background-color: #17a2b8;
                    }
                `}
            </style>
        </button>
    );

    return (
        <React.Fragment>
            <tr>
                <td className="centered">{btnProjectInlineEdit}</td>
                <td className="projTitle">
                    {project.title}
                    <br />
                </td>
                <td>
                    {project.ProjStatus != 'inactive' ? 'Active' : 'Inactive'}
                </td>
                <td>{btnAssetsInProject}</td>
                <td>{project.description}</td>
                <td>{project.ProjType.join(', ')}</td>
                <td>{project.ProjYear}</td>

                <td className="cellForButtons">
                    <ManageButtonUploadFile
                        key={`uf-thumb-${project.id}}`}
                        entryId={project.id}
                        field="projectThumbnail"
                        caption={
                            'Select a high-resolution\nimage (JPG or PNG):'
                        }
                        onSuccess={updatedFile => {
                            const newdata = Object.assign(
                                {},
                                project,
                                updatedFile
                            );
                            updateProjectData(newdata);
                        }}
                    />
                    <br />
                    {project.ProjThumb && (
                        <a
                            className="btn btn-outline-info"
                            href={project.ProjThumb}
                            target="_blank"
                            rel="noreferrer"
                        >
                            Open
                        </a>
                    )}
                </td>
                {/* <td>{project["ProjArchive"]} </td> */}

                <td>
                    <ButtonGroup aria-label="Manage actionse">
                        {projectViewLink}
                        {/* {projectEditLink} */}
                    </ButtonGroup>
                </td>
            </tr>
            <style jsx>
                {`
                    .cellForButtons {
                        line-height: 4px;
                        text-align: center;
                    }
                `}
            </style>
        </React.Fragment>
    );
};

const ManageTableProjectsRowEdit = ({
    project,
    setEditView,
    managerCancelNewProject,
    updateProjectData,
}) => {
    const { pathinfo } = useContext(UserContext);
    const [isSaving, setIsSaving] = useState(false);

    const saveField = (state, updateKeyValue) => {
        const fieldsChanged = state.fieldsChanged || [];

        Object.keys(updateKeyValue).forEach(fieldName => {
            if (!fieldsChanged.includes(fieldName)) {
                fieldsChanged.push(fieldName);
            }
        });

        fieldsChanged.push(...Object.keys(updateKeyValue));
        return Object.assign({}, state, updateKeyValue, { fieldsChanged });
    };

    const [dataToSave, saveFieldKeyValue] = useReducer(saveField, project);

    const onSaveRow = async () => {
        if (!isSaving) {
            if (!dataToSave.fieldsChanged) {
                return false;
            }
            setIsSaving(true);
            const dataToPush = {
                id: dataToSave.id,
                org: pathinfo.organization,
            };
            dataToSave.fieldsChanged.forEach(fieldName => {
                dataToPush[fieldName] = dataToSave[fieldName];
            });
            const saveApiResult = await saveProject(dataToPush);
            if (!saveApiResult || !saveApiResult.success) {
                setIsSaving(false);
                return;
            }

            const syncedDataToSave = Object.assign(
                {},
                dataToSave,
                saveApiResult.item
            );
            updateProjectData(syncedDataToSave);
            setIsSaving(false);
            setEditView(0);
        }
    };
    const onCancel = () => {
        if (project.id === 'new') {
            managerCancelNewProject(project);
        } else {
            setEditView(0);
        }
    };

    const btnProjectInlineSave = (
        <Button variant="primary" size="sm" onClick={() => onSaveRow()}>
            {isSaving ? 'Saving...' : 'Save'}
        </Button>
    );
    const btnProjectInlineCancelEdit = (
        <Button variant="link" size="sm" onClick={() => onCancel()}>
            Cancel
        </Button>
    );

    const defaultTypeOptions = [
        'Strategy',
        'Communications',
        'Branding',
        'UX',
        'Interactive',
    ];
    const projectTypeOptions = [
        ...new Set([...defaultTypeOptions, ...dataToSave.ProjType]),
    ].map(item => ({ label: item, value: item })); // combine without duplicates

    return (
        <React.Fragment>
            <tr className="editMode editHeader">
                <td />
                <td className="minwidth-medium">Project Name:</td>
                <td className="minwidth-small">Status:</td>
                <td />
                <td className="minwidth-long">Short description paragraph:</td>
                <td>Type:</td>
                <td>When this project was completed (YYYY):</td>
                <td />
                <td>Link to download ZIP file with all the files:</td>
                <td />
            </tr>
            <tr className="editMode">
                <td className="centered">
                    {btnProjectInlineSave}
                    {btnProjectInlineCancelEdit}
                </td>
                <td>
                    <ManageInputArea
                        data={dataToSave}
                        field="title"
                        onSave={saveFieldKeyValue}
                    />
                </td>
                <td>
                    <ManageInputSingleOption
                        data={dataToSave}
                        field="ProjStatus"
                        onSave={saveFieldKeyValue}
                        options={[
                            { label: 'Active', value: 'active' },
                            { label: 'Inactive', value: 'inactive' },
                        ]}
                    />
                </td>
                <td />
                <td>
                    <ManageInputArea
                        data={dataToSave}
                        field="ProjDesc"
                        onSave={saveFieldKeyValue}
                    />
                </td>
                <td>
                    <ManageInputMultipleOption
                        data={dataToSave}
                        field="ProjType"
                        onSave={saveFieldKeyValue}
                        options={projectTypeOptions}
                        allowAddTag
                    />
                </td>
                <td>
                    <ManageInputArea
                        data={dataToSave}
                        field="ProjYear"
                        onSave={saveFieldKeyValue}
                    />
                </td>

                <td />
                {/* <td>
          <ManageInputArea data={dataToSave} field="ProjArchive" onSave={saveFieldKeyValue} />
        </td> */}

                <td />
            </tr>
            <style jsx>
                {`
                    tr.editMode {
                        background-color: #fffbd5;
                    }
                    tr.editHeader {
                        font-size: 12px;
                        font-weight: bold;
                    }
                `}
            </style>
        </React.Fragment>
    );
};

export default ManageTableProjects;
