import * as React from 'react';
import { useContext } from 'react';
import GeoExplorerContext from '../context/GeoExplorerContext';
import GeoExplorerPopupWarehouse from './GeoExplorerPopupWarehouse';
import GeoExplorerPopupExecutive from './GeoExplorerPopupExecutive';
import {
    kMapPerspectiveKey,
    kMapPerspectiveWarehouse,
} from '../services/geoexplorerconsts';

const GeoExplorerPopupRouter = React.forwardRef(function popupRouter(
    props,
    ref
) {
    const { geosolutionsfilters } = useContext(GeoExplorerContext);
    let popupComponent;
    if (geosolutionsfilters[kMapPerspectiveKey] === kMapPerspectiveWarehouse) {
        popupComponent = (
            <GeoExplorerPopupWarehouse
                key="GeoExplorerPopupWarehouse"
                {...props}
            />
        );
    } else {
        popupComponent = (
            <GeoExplorerPopupExecutive
                key="GeoExplorerPopupExecutive"
                {...props}
            />
        );
    }

    return <div ref={ref}>{popupComponent}</div>;
});
export default GeoExplorerPopupRouter;
