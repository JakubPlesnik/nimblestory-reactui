import React, { useEffect, useContext } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import cn from 'classnames';

import NProgress from 'nprogress';
import Router from 'next/router';
import LoaderContext from '../context/LoaderContext';

Router.onRouteChangeStart = () => NProgress.start();
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

const ThemePageLoadingIndicator = () => {
    const { loadersState } = useContext(LoaderContext);

    useEffect(() => {
        if (loadersState.length === 0) {
            NProgress.done();
        } else {
            NProgress.start();
        }
    }, [loadersState]);

    return (
        <div
            id="appLoadingIndicator1"
            key="appLoadingIndicator1"
            className={cn({
                'spinnerspace d-flex justify-content-center':
                    loadersState.length >= 1,
                hidden: loadersState.length == 0,
            })}
        >
            <Spinner animation="border" role="status" variant="primary">
                <span className="sr-only">Loading...</span>
            </Spinner>

            <style jsx>
                {`
                    .hidden {
                        display: none;
                    }
                    .spinnerspace {
                        position: absolute;
                        width: 100%;
                        margin-top: 200px;
                        z-index: 100000;
                    }
                `}
            </style>
        </div>
    );
};
export default ThemePageLoadingIndicator;
