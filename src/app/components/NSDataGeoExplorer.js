import axios from 'axios';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';
import LoaderContext from '../context/LoaderContext';
import GeoExplorerContext from '../context/GeoExplorerContext';

import { fetchFilteredGeoSolutions } from '../services/cmsapi';
import { kPropPlantId } from '../services/geoexplorerconsts';

async function loadData(orgslug) {
    const solutionsData = await fetchFilteredGeoSolutions(
        orgslug,
        null,
        'df',
        []
    );
    const solutions = solutionsData.results || false;
    return solutions;
}

const NSDataGeoExplorer = ({ children }) => {
    const { pathinfo } = useContext(UserContext);
    const { isLoggedIn, organization } = useContext(LoginContext);

    const [solutions, setSolutions] = useState([]);
    const [solution, setSolution] = useState(null);
    const [geosolutionsfilters, setGeosolutionsfilters] = useState([]);
    const [geosolutionalldatafilters, setGeosolutionalldatafilters] = useState(
        []
    );

    const [dataPlantLocations, setDataPlantLocations] = useState(null);
    const [dataPlantTotals, setDataPlantTotals] = useState({});
    const [dataPlantInventory, setDataPlantInventory] = useState({});
    const [
        dataPlantLocationsFiltered,
        setDataPlantLocationsFiltered,
    ] = useState(null);
    const [
        dataPlantInventoryFiltered,
        setDataPlantInventoryFiltered,
    ] = useState({});

    const { updateLoaders } = useContext(LoaderContext);

    useEffect(() => {
        updateLoaders({ title: 'solutionsdata', progress: 1 });
    }, []);

    useEffect(() => {
        if (solutions.length) {
            setSolutions([]);
        }

        if (!organization) {
            return;
        }

        const { geoexplorerAccess } = organization;

        if (!geoexplorerAccess) {
            updateLoaders({ title: 'solutionsdata', progress: 100 });
        } else if (
            isLoggedIn &&
            organization &&
            organization.slug === pathinfo.organization
        ) {
            loadData(pathinfo.organization).then(solutions => {
                if (solutions) {
                    setSolutions(solutions);
                }
                updateLoaders({ title: 'solutionsdata', progress: 100 });
            });
        }
    }, [isLoggedIn, pathinfo.organization, organization]);

    useEffect(() => {
        if (solutions.length && pathinfo.project) {
            const project = solutions.find(
                item => item.slug === pathinfo.project
            );
            setSolution(project);
            if (project) {
                fetchLocationData(project);
            }
        }
    }, [solutions, pathinfo.project]);

    const fetchLocationData = async asset => {
        const datasource = asset.locationFiles[0];

        const [
            ServerDataPlantLocations,
            ServerDataPlantTotals,
            ServerDataPlantInventory,
            ServerGeosolutionalldatafilters,
        ] = await Promise.all(
            [
                datasource.geojsonUrl,
                datasource.jsonPlantInfo,
                datasource.jsonInventory,
                datasource.jsonFilters,
            ].map(url => axios.get(url).then(resp => resp.data))
        );

        for (let i = 0; i < ServerDataPlantLocations.features.length; i++) {
            const feature = ServerDataPlantLocations.features[i];
            const plantId = feature.properties[kPropPlantId];
            const plantTotals = ServerDataPlantTotals[`plant_${plantId}`];
            if (plantTotals) {
                ServerDataPlantLocations.features[i].properties = {
                    ...feature.properties,
                    ...plantTotals,
                };
            }
        }
        setGeosolutionalldatafilters(ServerGeosolutionalldatafilters);

        setDataPlantInventory(ServerDataPlantInventory);
        setDataPlantTotals(ServerDataPlantTotals);
        setDataPlantLocations(ServerDataPlantLocations);
        setDataPlantLocationsFiltered(ServerDataPlantLocations);
    };

    useEffect(() => {
        const availablesFilters = geosolutionalldatafilters.map(e => e.source);
        const activeFilters = Object.keys(geosolutionsfilters).filter(
            e =>
                availablesFilters.includes(e) &&
                geosolutionsfilters[e].length > 0
        );

        if (activeFilters.length) {
            const filteredInventory = {};

            const filteredPlantLocations = dataPlantLocations.features.filter(
                location => {
                    const locationProps = location.properties;
                    const inventory =
                        dataPlantInventory[
                            `plant_${locationProps[kPropPlantId]}`
                        ] || [];

                    const filteredInventoryForPlant = inventory.filter(
                        asset => {
                            let pass = true;
                            activeFilters.forEach(filterId => {
                                const sourceFiled = asset[filterId];
                                const filterAccepts =
                                    geosolutionsfilters[filterId];

                                const found = [sourceFiled].some(r =>
                                    filterAccepts.includes(r)
                                );
                                if (!found) {
                                    pass = false;
                                }
                            });
                            return pass;
                        }
                    );

                    if (filteredInventoryForPlant.length) {
                        filteredInventory[
                            `plant_${locationProps[kPropPlantId]}`
                        ] = filteredInventoryForPlant;
                        return true;
                    }
                    return false;
                }
            );

            setDataPlantInventoryFiltered(filteredInventory);
            setDataPlantLocationsFiltered({
                type: 'FeatureCollection',
                features: filteredPlantLocations,
            });
        } else {
            setDataPlantInventoryFiltered(dataPlantInventory);
            setDataPlantLocationsFiltered(dataPlantLocations);
        }
    }, [geosolutionsfilters]);

    return (
        <GeoExplorerContext.Provider
            value={{
                solutions,
                solution,
                geosolutionsfilters,
                geosolutionalldatafilters,
                setGeosolutionsfilters,
                dataPlantLocations,
                dataPlantTotals,
                dataPlantInventory,
                dataPlantLocationsFiltered,
                dataPlantInventoryFiltered,
            }}
        >
            {children}
        </GeoExplorerContext.Provider>
    );
};

export default NSDataGeoExplorer;
