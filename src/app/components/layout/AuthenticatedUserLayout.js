import { useContext } from 'react';
import AppRequireLogin, { kAppReqOrganization } from '../AppRequireLogin';
import Theme from '../Theme';
import AppNavigationWrapper from '../navigation/AppNavigationWrapper';
import { Content } from 'antd/lib/layout/layout';
import PropTypes from 'prop-types';
import UserContext from '../../context/UserContext';

const AuthenticatedUserLayout = ({ children }) => {
    const { navigationCollapsed } = useContext(UserContext);

    return (
        <AppRequireLogin require={[kAppReqOrganization]}>
            <Theme>
                <div className="main-container">
                    <div className="menu-sider">
                        <AppNavigationWrapper />
                    </div>
                    <Content
                        className="content"
                        style={{
                            display: 'flex',
                            flex: 1,
                            paddingLeft: !navigationCollapsed
                                ? '265px'
                                : '20px',
                            marginLeft: '64px',
                            marginTop: '0px',
                            willChange: 'padding-left',
                        }}
                    >
                        {children}
                    </Content>
                </div>
                <style jsx>
                    {`
                        .main-container {
                            display: flex;
                            flex-orientation: row;
                            flex: 1;
                        }
                        .menu-sider {
                            display: flex;
                            bottom: 0px;
                            left: 0px;
                            position: fixed;
                            top: 0px;
                            z-index: 200;
                        }
                        .content {
                            transition: padding-left 300ms
                                cubic-bezier(0.2, 0, 0, 1) 0s;
                        }
                    `}
                </style>
            </Theme>
        </AppRequireLogin>
    );
};

AuthenticatedUserLayout.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.element),
        PropTypes.element,
    ]).isRequired,
};

export default AuthenticatedUserLayout;
