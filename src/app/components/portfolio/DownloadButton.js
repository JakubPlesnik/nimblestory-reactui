import React from 'react';

import { Select, Button } from 'antd';

const { Option } = Select;

const DownloadButton = ({ downloadPaths, downloadFile }) => {
    const downloadButtonType = downloadPaths.length == 1 ? 'single' : 'multi';
    function handleChange(value) {
        const found = downloadPaths.find(i => i.path == value);
        downloadFile(found.path, found.downloadName, found.extension);
    }

    return (
        <div className="btnDownload">
            {downloadButtonType == 'multi' ? (
                <Select
                    defaultValue="-1"
                    size="large"
                    style={{ width: 245 }}
                    value="Download Asset"
                    onChange={handleChange}
                >
                    {downloadPaths.map(item => (
                        <Option
                            key={item.path}
                            href="#"
                            value={item.path}
                            rel="noreferrer noopener"
                        >
                            {item.title}
                        </Option>
                    ))}
                </Select>
            ) : (
                <Button
                    variant="primary"
                    size="large"
                    onClick={() =>
                        downloadFile(
                            downloadPaths[0].path,
                            downloadPaths[0].downloadName,
                            downloadPaths[0].extension
                        )
                    }
                    data-cy="asset-download-button"
                >
                    Download Content
                </Button>
            )}
        </div>
    );
};

export default DownloadButton;
