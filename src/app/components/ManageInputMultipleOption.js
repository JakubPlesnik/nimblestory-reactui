import Form from 'react-bootstrap/Form';

const ManageInputMultipleOption = ({
    data,
    options,
    field,
    onSave,
    allowAddTag,
    tagid,
}) => {
    const handleCheck = newItem => {
        const newData = [...data[field], newItem];
        onSave({ [field]: newData });
    };

    const handleUncheck = removeItem => {
        const newData = data[field].filter(item => item !== removeItem);
        onSave({ [field]: newData });
    };

    const handleKeyPress = e => {
        if (event.key === 'Enter') {
            e.preventDefault();
            const newItem = e.target.value.trim();
            // check that new tag is not empty and that it's not already there
            if (
                newItem.length &&
                data[field].findIndex(
                    item => item.toLowerCase() === newItem.toLowerCase()
                ) === -1
            ) {
                const newItemCapitalized =
                    newItem.charAt(0).toUpperCase() + newItem.slice(1);
                handleCheck(newItemCapitalized);
                e.target.value = '';
            }
        }
    };

    return (
        <Form>
            {options.map(item => {
                const selected =
                    data[field].findIndex(i => i == item.value) !== -1;
                const event = selected
                    ? () => handleUncheck(item.value)
                    : () => handleCheck(item.value);

                return (
                    <Form.Check
                        type="checkbox"
                        label={item.label}
                        key={`managefieldoption_${tagid}_${item.value}`}
                        id={`managefieldoption_${tagid}_${item.value}`}
                        checked={selected}
                        onChange={event}
                    />
                );
            })}

            {allowAddTag === true && (
                <Form.Control
                    size="sm"
                    type="text"
                    placeholder="Add new tag..."
                    onKeyPress={handleKeyPress}
                />
            )}
        </Form>
    );
};
export default ManageInputMultipleOption;
