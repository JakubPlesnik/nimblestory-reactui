import Link from 'next/link';

import { useState, useContext } from 'react';
import cn from 'classnames';
import WorldIcon from '@atlaskit/icon/glyph/world';
import AppLinkData from '../services/applink';
import LoginContext from '../context/LoginContext';
import UserContext from '../context/UserContext';
import SvgIcon from './SvgIcon';

function text_truncate(str, length) {
    if (!str) return '';

    const ending = '...';

    if (str.length > length) {
        return str.substring(0, length - ending.length).trim() + ending;
    }
    return str;
}

const GeoExplorerSolutionCard = ({ organization, project }) => {
    const { pinned } = useContext(LoginContext);
    const projectLinkData = AppLinkData({
        organization,
        project: project.slug,
        application: 'geoexplorer',
    });
    const [hovering, setHovering] = useState(false);
    const showIcon = !!project['File1(SmPrev)'];
    const projTitle = project.title;

    const { themeColors } = useContext(UserContext);
    const { color1Darkest, color3Light } = themeColors;

    return (
        <div
            className={cn('projectCard', { hovering })}
            onMouseEnter={() => setHovering(true)}
            onMouseLeave={() => setHovering(false)}
        >
            <Link href={projectLinkData.href} as={projectLinkData.as}>
                <a>
                    {showIcon && (
                        <div
                            className="projectThumbnail"
                            style={{
                                backgroundImage: `url(${project['File1(SmPrev)']})`,
                            }}
                        />
                    )}

                    {!showIcon && <div className="projectThumbnailIcon" />}
                </a>
            </Link>
            <div className="projInfo">
                <Link href={projectLinkData.href} as={projectLinkData.as}>
                    <a>{text_truncate(projTitle, 61)}</a>
                </Link>
                <p>{text_truncate(project.description, 340)} </p>
                <div className="iconarea">
                    <div className="lefticon">
                        {pinned.findIndex(i => i.id == project.id) != -1 && (
                            <SvgIcon
                                name="icon-pin"
                                width={24}
                                height={24}
                                fill={color3Light}
                            />
                        )}
                    </div>
                    {/* <div className="centertext">
            <p> recently updated </p>
          </div> */}
                    <div className="righticon">
                        <WorldIcon primaryColor={color3Light} />
                    </div>
                </div>
            </div>
            <style jsx>
                {`
                    .projectCard {
                        width: 253px;
                        height: 364px;
                        border-radius: 6px;
                        border-width: thin;
                        border-color: ${color1Darkest};
                        border-style: solid;
                        background-color: #f8f8f8;
                        overflow: hidden;
                        margin-left: 10px;
                        margin-bottom: 34px;
                        position: relative;
                    }
                    .projectThumbnail {
                        width: 100%;
                        height: 140px;
                        background-color: #e8e8e8;
                        background-size: 100%;
                        background-position: top center;
                        background-repeat: no-repeat;
                    }
                    .projectThumbnailIcon {
                        width: 100%;
                        height: 140px;
                        background-color: #e8e8e8;
                        background-size: cover;
                        background-position: center;
                        background-repeat: no-repeat;
                        background-image: url('https://staging.nimblestory.com/appfiles/icons/icon-project.png');
                        background-size: 45%;
                    }
                    .projInfo {
                        background-color: #f4f5f7;
                    }
                    .projInfo > a:link,
                    .projInfo > a:active,
                    .projInfo > a:visited {
                        font-style: normal;
                        color: #002545;
                        padding: 10px 16px;
                        font-weight: bold;
                        font-size: 14px;
                        line-height: 150%;
                        text-decoration: none;
                        height: 60px;
                        display: -webkit-box;
                        -webkit-line-clamp: 2;
                        -webkit-box-orient: vertical;
                        overflow: hidden;
                    }
                    .projInfo > p {
                        padding-left: 10px;
                        padding-right: 10px;
                        padding-bottom: 10px;
                        height: 115px;
                        font-size: 12px;
                        line-height: 120%;
                        font-style: normal;
                        color: #002545;
                        opacity: 0.7;
                        overflow: hidden;
                    }
                    .iconarea {
                        height: 35px;
                        padding: 5px;
                        position: absolute;
                        left: 0;
                        right: 0;
                        background-color: ${color1Darkest};
                    }
                    .iconarea > .lefticon {
                        float: left;
                        width: 25px;
                        height: 25px;
                        border-radius: 5px;
                        text-align: center;
                        display: inline-block;
                    }
                    .iconarea > .righticon {
                        float: right;
                        width: 25px;
                        height: 25px;
                        border-radius: 5px;
                        text-align: center;
                        display: inline-block;
                    }
                    .iconarea > .centertext {
                        width: 150px;
                        height: 25px;
                        margin-left: 20px;
                        text-align: center;
                        display: inline-block;
                    }
                    .centertext > p {
                        font-style: normal;
                        font-weight: bold;
                        font-size: 12px;
                        line-height: 125%;
                        text-align: center;
                        color: #002545;
                        opacity: 0.5;
                    }
                    .hovering.projectCard {
                        box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
                        transition: box-shadow 0.3s ease;
                    }
                    .hovering .projectThumbnail {
                        background-size: 110%;
                        transition: background-size 0.3s ease;
                    }
                    .hovering .projectThumbnailIcon {
                        background-size: 50% !important;
                        transition: background-size 0.3s ease;
                    }
                `}
            </style>
        </div>
    );
};
export default GeoExplorerSolutionCard;
