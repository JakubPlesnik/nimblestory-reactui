import { useState } from 'react';
import { sendRecoveryLink } from '../services/api/apiService';

const Form = () => {
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [redirectButtonVisible, setRedirectButtonVisible] = useState(false);

    const sendRecoveryEmail = e => {
        e.preventDefault();

        if (email != '') {
            sendRecoveryLink(email, onEmailSuccess, onEmailError);
            return;
        }

        setMessage('Please enter your email');
    };

    const onEmailSuccess = () => {
        setMessage('Email with link has been send. Check your email address.');
        setRedirectButtonVisible(true);
    };

    const onEmailError = error => {
        if (!error) {
            setMessage('Server is not responding');
        } else if (error.status === 400) {
            setMessage(error.data.error.message);
        } else setMessage('Something went wrong.');
    };

    return (
        <form className="sign-in">
            <div className="form-group">
                <label htmlFor="email">Enter your email address</label>
                <input
                    type="email"
                    required
                    name="email"
                    placeholder="email address"
                    className="form-control"
                    onChange={e => setEmail(e.target.value)}
                />
            </div>

            {message != '' && (
                <div style={{ marginBottom: '10px' }} className="message">
                    {message}
                </div>
            )}

            {!redirectButtonVisible && (
                <button
                    type="submit"
                    className="btn btn-primary"
                    onClick={e => sendRecoveryEmail(e)}
                >
                    Submit
                </button>
            )}

            {redirectButtonVisible && (
                <a className="btn btn-primary" href="/login">
                    Back to the login
                </a>
            )}
        </form>
    );
};

export default Form;
