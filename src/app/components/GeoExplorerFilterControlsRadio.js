import { useContext } from 'react';

import Form from 'react-bootstrap/Form';
import cn from 'classnames';
import GeoExplorerContext from '../context/GeoExplorerContext';

import {
    kMapPerspectiveKey,
    kMapPerspectiveWarehouse,
    kPropValuationExistence,
    kPropProgramUtilizationRate,
    kMapFocusMetricKey,
} from '../services/geoexplorerconsts';

const GeoExplorerFilterControlsRadio = ({ filterId, options }) => {
    const { setGeosolutionsfilters, geosolutionsfilters } = useContext(
        GeoExplorerContext
    );

    return (
        <div className="item-controls-container">
            <Form>
                {options.map((option, key) => {
                    const isSelected =
                        geosolutionsfilters[filterId] &&
                        geosolutionsfilters[filterId] === option.value;
                    const elementId = `customRadio_${filterId}_${key}`;
                    return (
                        <Form.Check
                            type="radio"
                            key={elementId}
                            id={elementId}
                            name={elementId}
                            label={option.label}
                            checked={isSelected}
                            className={cn('custom-control-input', {
                                'btn-maptype-selected': isSelected,
                            })}
                            onChange={() => {
                                if (filterId === kMapPerspectiveKey) {
                                    const focusMetric =
                                        option.value ===
                                        kMapPerspectiveWarehouse
                                            ? kPropProgramUtilizationRate
                                            : kPropValuationExistence;
                                    setGeosolutionsfilters({
                                        ...geosolutionsfilters,
                                        [filterId]: option.value,
                                        [kMapFocusMetricKey]: focusMetric,
                                    });
                                } else {
                                    setGeosolutionsfilters({
                                        ...geosolutionsfilters,
                                        [filterId]: option.value,
                                    });
                                }
                            }}
                        />
                    );
                })}
            </Form>
            <style jsx>
                {`
                    div :global(.custom-control-input > label) {
                        cursor: pointer;
                    }
                    div :global(.custom-control-input > input) {
                        cursor: pointer;
                    }
                    div :global(.custom-control-input) {
                        line-height: 28px;
                    }
                    div :global(.btn-maptype-selected) {
                        font-weight: bold;
                    }
                `}
            </style>
        </div>
    );
};
export default GeoExplorerFilterControlsRadio;
