import { useRouter } from 'next/router';
import { useState, useContext, useEffect } from 'react';
import PortfolioContext from '../context/PortfolioContext';
import UserContext from '../context/UserContext';
import LoaderContext from '../context/LoaderContext';
import PortfolioProjectAssetsContext from '../context/PortfolioProjectAssetsContext';
import PaginationContext from '../context/PaginationContext';
import AppLinkData from '../services/applink';

import {
    fetchContent,
    fetchCollectionDetails,
} from '../services/api/apiService';

import {
    filteredItems,
    assetSortOptions,
    assetFilterOptionInactive,
    assetFilterOptionActive,
    fetchAssetFilterOptions,
} from '../services/filteringAssets';

const NSDataPortfolioProjectAssets = ({ children }) => {
    const router = useRouter();
    const [project, setProject] = useState(null);

    const [projectSubfolders, setProjectSubfolders] = useState([]);
    const [projectFilteredSubfolders, setProjectFilteredSubfolders] = useState(
        []
    );

    const [
        filteredSortedAssestsWithSubfolders,
        setFilteredSortedAssestsWithSubfolders,
    ] = useState([]);

    const [projectCurentSubfolder, setProjectCurentSubfolder] = useState(0);
    const [projectRootFolderId, setProjectRootFolderId] = useState(0);

    const [collection, setCollection] = useState(null);
    const [assets, setAssets] = useState([]);
    const [asset, setAsset] = useState(null);
    const [assetsFilteredSorted, setAssetsFilteredSorted] = useState([]);
    const [assetsMeta, setAssetsMeta] = useState({});
    const [assetfilters, setAssetFilters] = useState([]);
    const [searchFilterKeyword, setSearchFilterKeyword] = useState(false);
    const [selectedAssetSortOption, setAssetSortOption] = useState(0);

    const [assetFilterOptions, setAssetFilterOptions] = useState([]);
    const [orderBy, setOrderBy] = useState('df');
    const [filterBy, setFilterBy] = useState([]);
    const [totalAssets, setTotalAssets] = useState(null);

    const [pageReady, setPageReady] = useState(false);
    const [loadMore, setLoadMore] = useState(false);
    const [hasMore, setHasMore] = useState(false);

    const { updateLoaders } = useContext(LoaderContext);
    const {
        projects,
        assetTypesDynamic,
        assetStatusDynamic,
        collections,
    } = useContext(PortfolioContext);
    const { pathinfo } = useContext(UserContext);
    const { setOnReachedEnd } = useContext(PaginationContext) || {
        setOnReachedEnd: () => {},
    };

    useEffect(() => {
        if (projects.length && pathinfo.project) {
            const selectedProject = projects.find(
                item => item.slug == pathinfo.project
            );
            if (selectedProject) {
                setProject(selectedProject);
            }
        }
    }, [projects, pathinfo.project]);

    useEffect(() => {
        if (collections.length && pathinfo.collection) {
            const selectedCollection = collections.find(
                item => item.slug == pathinfo.collection
            );
            if (selectedCollection) {
                setCollection(selectedCollection);
            }
        }
    }, [collections, pathinfo.collection]);

    useEffect(() => {
        if (assets.length && pathinfo.asset) {
            const selectedAsset = assets.find(
                item => item.slug == pathinfo.asset
            );
            if (selectedAsset) {
                setAsset(selectedAsset);
            }
        }
    }, [assets, pathinfo.asset]);

    /*
    Load assets
    */

    async function loadData(page, order, filter) {
        updateLoaders({ title: 'assets', progress: 1 });

        if (pathinfo.collection) {
            const collectionData = await fetchCollectionDetails(
                pathinfo.organization,
                pathinfo.collection,
                page,
                order,
                filter
            );

            if (collectionData && collectionData.collection.contentItems) {
                setAssets(collectionData.collection.contentItems);
                setPageReady(true);
            }
        } else {
            const slug =
                pathinfo.project === 'assets' ? pathinfo.project : project.slug;
            const assetsData = await fetchContent(
                pathinfo.organization,
                slug,
                page,
                order,
                filter
            );

            if (assetsData && assetsData.results) {
                // update meta data
                const totalpages = assetsData.meta.pagination.total_pages;
                setAssetsMeta(assetsData.meta);
                setTotalAssets(assetsData.meta.pagination.total);

                // update assets
                if (page === 1) {
                    setAssets(assetsData.results);
                    setPageReady(true);
                } else {
                    setAssets(prevState => [
                        ...prevState,
                        ...assetsData.results,
                    ]);
                }

                // set pagination
                if (page < totalpages) {
                    setHasMore(true);
                    setOnReachedEnd({
                        trigger: async () => {
                            setOnReachedEnd(false);
                            await loadData(page + 1, order, filter);
                        },
                    });
                } else {
                    setHasMore(false);
                }
            }

            if (assetsData && assetsData.folders && project.id) {
                setProjectSubfolders(assetsData.folders);

                setProjectRootFolderId(project.id);
                setFolderOnLoad(project.id);
            }
        }

        updateLoaders({ title: 'assets', progress: 100 });
    }

    const setFolderOnLoad = projectId => {
        var params = new URLSearchParams(window.location.search);

        if (!params.has('folderId')) {
            setProjectCurentSubfolder(projectId);
        }
    };

    useEffect(() => {
        const filteredSubfolders = projectSubfolders.filter(item => {
            return item.parentId == projectCurentSubfolder;
        });
        setProjectFilteredSubfolders(filteredSubfolders);
        if (projectCurentSubfolder) {
            const linkData = AppLinkData({
                organization: pathinfo.organization,
                project: pathinfo.project,
                folderId: projectCurentSubfolder,
            });

            window.history.pushState(
                {},
                null,
                window.location.origin + linkData.as
            );
        }
    }, [projectCurentSubfolder, projectRootFolderId]);

    useEffect(() => {
        if (project) {
            loadData(1, 'df', []);
        }
    }, [project]);

    useEffect(() => {
        if (pathinfo.project === 'assets' || pathinfo.collection) {
            loadData(1, 'df', []);
        }
    }, [pathinfo.project, pathinfo.collection]);

    useEffect(() => {
        if (assetTypesDynamic && assetStatusDynamic) {
            setAssetFilterOptions(
                fetchAssetFilterOptions(assetTypesDynamic, assetStatusDynamic)
            );
        }
    }, [assetTypesDynamic, assetStatusDynamic]);

    useEffect(() => {
        const { projectfilter } = router.query;

        if (projectfilter == 'active') {
            setAssetFilters([assetFilterOptionActive]);
        } else if (projectfilter == 'inactive') {
            setAssetFilters([assetFilterOptionInactive]);
        } else if (assetfilters.length) {
            setAssetFilters([]);
        }
    }, [router.query.projectfilter]);

    const sortAndFilterPortfolios = () => {
        let sortedAssets = [...assets, ...projectSubfolders];

        if (assets && assetFilterOptions.length > 0) {
            const filteredAssets = assetfilters.length
                ? filteredItems(sortedAssets, assetfilters, assetFilterOptions)
                : sortedAssets;

            const filteredAssetsBySearch =
                !searchFilterKeyword || searchFilterKeyword.length < 2
                    ? filteredAssets
                    : filteredAssets.filter(item => {
                          let filterKeyword = searchFilterKeyword.replaceAll(
                              '*',
                              ''
                          );
                          filterKeyword = filterKeyword.replaceAll('(', '');
                          filterKeyword = filterKeyword.replaceAll(')', '');
                          filterKeyword = filterKeyword.replaceAll('+', '');
                          const re = new RegExp(`\\b${filterKeyword}`, 'i');
                          return re.test(item.title);
                      });

            sortedAssets = filteredAssetsBySearch;
        }

        if (assets && projectCurentSubfolder) {
            sortedAssets = sortedAssets.filter(item => {
                return item.parentId === projectCurentSubfolder;
            });
        }

        setAssetsFilteredSorted([...sortedAssets]);
        setFilteredSortedAssestsWithSubfolders(
            [...sortedAssets].sort(
                assetSortOptions[selectedAssetSortOption].sorter
            )
        );
    };

    useEffect(() => {
        sortAndFilterPortfolios();
    }, [projectCurentSubfolder]);

    useEffect(() => {
        sortAndFilterPortfolios();
    }, [assets, assetfilters, assetFilterOptions, searchFilterKeyword]);

    useEffect(() => {
        const { orderBy: newOrderBy } = assetSortOptions[
            selectedAssetSortOption
        ];
        setOrderBy(newOrderBy);

        if (assetsMeta && Object.entries(assetsMeta).length > 0) {
            if (
                assetsMeta.pagination.current_page ===
                assetsMeta.pagination.total_pages
            ) {
                sortAndFilterPortfolios();
            } else {
                loadData(1, newOrderBy, filterBy);
            }
        }
    }, [selectedAssetSortOption]);

    // select filterOptions event handler
    const selectFilter = async newFiltersState => {
        try {
            const { current_page, total_pages } = assetsMeta.pagination;
            // console.log("Selected__________!", current_page, total_pages, totalPage, newFiltersState)

            // return if last page
            if (current_page === total_pages) return;

            // get filter options array from selected options const
            const filterArray = newFiltersState.map(filter => filter.title);

            // return if unchecking
            if (
                filterArray.length < filterBy.length &&
                filterArray.length !== 0
            )
                return;

            setFilterBy(filterArray);

            const assetsData = await fetchContent(
                pathinfo.organization,
                project.slug,
                null,
                orderBy,
                filterArray
            );
            // console.log('assetsData by filtering from backend:', assetsData)
            setAssets(assetsData.results);
            setAssetsMeta(assetsData.meta);
        } catch (err) {
            throw new Error(err);
        }
    };

    return (
        <PortfolioProjectAssetsContext.Provider
            value={{
                project,
                collection,
                setProject,
                assets: assetsFilteredSorted,
                assetsMeta,
                assetfilters,
                pageReady,
                searchFilterKeyword,
                setSearchFilterKeyword,
                setAssetSortOption,
                selectedAssetSortOption,
                assetFilterOptions,
                setAssetFilters,
                selectFilter,
                loadMore,
                setLoadMore,
                hasMore,
                totalAssets,
                asset,
                projectSubfolders,
                projectFilteredSubfolders,
                filteredSortedAssestsWithSubfolders,
                projectRootFolderId,
                projectCurentSubfolder,
                setProjectCurentSubfolder,
                sortAndFilterPortfolios,
                loadData,
            }}
        >
            {children}
        </PortfolioProjectAssetsContext.Provider>
    );
};

export default NSDataPortfolioProjectAssets;
