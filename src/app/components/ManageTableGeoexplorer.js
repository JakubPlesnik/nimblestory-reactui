import * as React from 'react';
import { useContext } from 'react';
import Table from 'react-bootstrap/Table';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import UserContext from '../context/UserContext';
import { projectSortOptions } from '../services/filteringProjects';
import ManageButtonUploadFile from './ManageButtonUploadFile';

const ManageTableGeoexplorer = props => {
    const { pathinfo } = useContext(UserContext);
    const { organization } = pathinfo;
    const selectedSortOption = projectSortOptions.findIndex(
        item => item.title == 'A-Z'
    );

    return (
        <Table bordered size="sm">
            <thead>
                <tr>
                    <th />
                    <th className="minwidth-medium">Project Name</th>
                    <th className="minwidth-long">Description</th>

                    <th>Thumb</th>
                    <th>Locations</th>
                    <th>Stats/Totals</th>
                    <th>Inventory</th>
                    <th>Filters</th>
                    <th>View</th>
                </tr>
            </thead>
            <tbody>
                {props.projects &&
                    props.projects
                        .sort(projectSortOptions[selectedSortOption].sorter)
                        .map(project => (
                            <ManageTableRow
                                {...props}
                                key={project.id}
                                organization={organization}
                                editView={0}
                                project={project}
                            />
                        ))}
            </tbody>
            <style jsx>
                {`
                    .minwidth-small {
                        min-width: 88px;
                    }
                    .minwidth-medium {
                        min-width: 210px;
                    }
                    .minwidth-long {
                        min-width: 310px;
                    }
                `}
            </style>
        </Table>
    );
};
export default ManageTableGeoexplorer;

const ManageTableRow = props => {
    return <ManageTableRowView {...props} />;
    // for editing geoexplorer in the future
    // return isEditView === 0 ?
    //    <ManageTableRowView {...props} setEditView={setEditView} /> :
    //        <ManageTableRowEdit {...props} setEditView={setEditView} />
};

const ManageTableRowView = ({ organization, project, updateProjectData }) => {
    const projectViewLink = (
        <a
            href={`/geoexplorer/${organization}/${project.slug}`}
            className="btn btn-secondary"
            target="_blank"
            rel="noreferrer"
        >
            Live
        </a>
    );

    const projectEditLink = (
        <a
            className="btn btn-secondary"
            target="_blank"
            rel="noreferrer"
            href={project.cpEditUrl}
        >
            CMS
        </a>
    );
    // const btnProjectInlineEdit = <Button variant="outline-info" size="sm" onClick={()=>setEditView(1)}>Edit</Button>

    return (
        <React.Fragment>
            <tr>
                <td className="centered" />
                <td className="projTitle">
                    {project.title}
                    <br />
                </td>

                <td>{project.description}</td>

                <td className="cellForButtons">
                    <ManageButtonUploadFile
                        key={`uf-thumb-${project.id}}`}
                        entryId={project.id}
                        field="projectThumbnail"
                        caption={
                            'Select a high-resolution\nimage (JPG or PNG):'
                        }
                        onSuccess={updatedFile => {
                            const newdata = Object.assign(
                                {},
                                project,
                                updatedFile
                            );
                            updateProjectData(newdata);
                        }}
                    />
                    <br />
                    {project.ProjThumb && (
                        <a
                            className="btn btn-outline-info"
                            href={project.ProjThumb}
                            target="_blank"
                            rel="noreferrer"
                        >
                            Open
                        </a>
                    )}
                </td>
                <td>
                    {project.locationFiles && (
                        <a
                            className="btn btn-outline-info"
                            href={project.locationFiles[0].geojsonUrl}
                            arget="_blank"
                        >
                            JSON Locations
                        </a>
                    )}
                </td>

                <td>
                    {project.locationFiles && (
                        <a
                            className="btn btn-outline-info"
                            href={project.locationFiles[0].jsonPlantInfo}
                            arget="_blank"
                        >
                            JSON Totals
                        </a>
                    )}
                </td>

                <td>
                    {project.locationFiles && (
                        <a
                            className="btn btn-outline-info"
                            href={project.locationFiles[0].jsonInventory}
                            arget="_blank"
                        >
                            JSON Inventory
                        </a>
                    )}
                </td>

                <td>
                    {project.locationFiles && (
                        <a
                            className="btn btn-outline-info"
                            href={project.locationFiles[0].jsonFilters}
                            arget="_blank"
                        >
                            JSON Filters
                        </a>
                    )}
                </td>

                <td>
                    <ButtonGroup aria-label="Manage actionse">
                        {projectViewLink}
                        {projectEditLink}
                    </ButtonGroup>
                </td>
            </tr>
            <style jsx>
                {`
                    .cellForButtons {
                        line-height: 4px;
                        text-align: center;
                    }
                `}
            </style>
        </React.Fragment>
    );
};
