import Spinner from 'react-bootstrap/Spinner';
import { useContext, useEffect } from 'react';
import UserContext from '../context/UserContext';
import PortfolioContext from '../context/PortfolioContext';
import LoaderContext from '../context/LoaderContext';
import FilterEmptyState from './FilterEmptyState';
import Project from '../components/cards/AppCardProject';
import HomepageHeader from './HomepageHeader';
import { Space } from 'antd';

const PortfolioProjectsView = () => {
    //const [isFilterPaneOn, setFilterPaneOn] = useState(false);
    const { pathinfo } = useContext(UserContext);
    const { loadersState } = useContext(LoaderContext);
    const {
        projects,
        projectfilters,
        setProjectFilters,
        hasMore,
        loadMore,
        setLoadMore,
    } = useContext(PortfolioContext);

    useEffect(() => {
        window.addEventListener('scroll', onScroll, false);
        return () => window.removeEventListener('scroll', onScroll, false);
    }, []);

    // Scroll Event Handler
    const onScroll = () => {
        const { innerHeight } = window;
        const { scrollHeight } = document.documentElement;

        const scrollTop =
            (document.documentElement && document.documentElement.scrollTop) ||
            document.body.scrollTop;
        if (innerHeight + scrollTop < scrollHeight - 300 || loadMore) return;
        if (hasMore) setLoadMore(true);
    };

    return (
        <div className="pagearea">
            <HomepageHeader titleText={'Projects Home'} />
            <div className="divider15" />

            <div className="assetsContainer">
                <Space size={[16, 35]} wrap style={{ marginBottom: '0px' }}>
                    {projects.map(item => (
                        <Project
                            key={`${item.id}-projects`}
                            organization={pathinfo.organization}
                            project={item}
                        />
                    ))}
                </Space>
            </div>
            {// set loading spinner in the bottom when switching new page by scrolling
            loadMore && hasMore && (
                <div className="fullWidth">
                    <div className="divider15" />
                    <div className="d-flex justify-content-center">
                        <Spinner
                            animation="border"
                            role="status"
                            variant="primary"
                        >
                            <span className="sr-only">Loading...</span>
                        </Spinner>
                    </div>
                    <div className="divider15" />
                </div>
            )}

            {loadersState.length == 0 &&
                projectfilters.length !== 0 &&
                !projects.length && (
                    <FilterEmptyState
                        onClick={() => {
                            setProjectFilters([]);
                        }}
                    />
                )}

            <style jsx>
                {`
                    .controls {
                        display: flex;
                        flex-direction: row;
                        flex-wrap: nowrap;
                        align-content: center;
                        width: 100%;
                        padding-left: 17px;
                        padding-right: 28px;
                    }
                    .controlsOne {
                        min-width: 134px;
                        max-width: 350px;
                        margin-right: 10px;
                    }
                    .controlsTwo {
                        flex-grow: 1;
                        margin-right: 10px;
                    }
                    .controlsThree {
                        min-width: 134px;
                    }
                    .titleSeparator {
                        margin-bottom: 22px;
                    }
                    .assetsContainer {
                        margin-left: 16px;
                        display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                    }
                    .portfolioHeader {
                        padding: 16px 36px;
                    }
                    .secondaryTitle {
                        font-weight: 500;
                        font-size: 12px;
                    }
                    .primaryTitle {
                        font-weight: 500;
                        font-size: 24px;
                        line-height: 28px;
                        padding: 9px 0px;
                    }

                    .pageTitle {
                        margin-left: 31px;
                    }
                    .divider15 {
                        height: 14px;
                        clear: both;
                    }

                    .portfolioLink {
                        cursor: pointer;
                    }
                `}
            </style>
        </div>
    );
};
export default PortfolioProjectsView;
