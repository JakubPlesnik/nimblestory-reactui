const Icon = ({
    name = '',
    type = '',
    size = '24',
    color = 'white',
    margin = 0,
}) => {
    const getClassName = (name, type) => {
        return 'icon icon-ns-' + name + '-' + type;
    };

    return (
        <div
            style={{
                color: color,
                fontSize: size + 'px',
                marginTop: margin + 'px',
            }}
            className={getClassName(name, type)}
        ></div>
    );
};

export default Icon;
