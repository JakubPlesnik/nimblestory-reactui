const EmbedCode = ({ embedCode }) => {
    embedCode = embedCode.replace(/\bwidth="(\d+)"/g, 'width = "100%"');
    embedCode = embedCode.replace(/\bheight="(\d+)"/g, 'height ="100%"');

    return (
        <div
            style={{ height: '100%', width: '100%' }}
            dangerouslySetInnerHTML={{
                __html: embedCode,
            }}
            data-cy="asset-display-module"
        ></div>
    );
};

export default EmbedCode;
