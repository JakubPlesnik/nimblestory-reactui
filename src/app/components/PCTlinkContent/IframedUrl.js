const IframedUrl = ({ filepath }) => {
    const frameid = 'moduleframe';

    return (
        <iframe
            src={filepath}
            id={frameid}
            className="assetiFrame"
            title="Module"
            width="100%"
            height="100%"
            frameBorder="0"
            allow="fullscreen"
            allowFullScreen
            data-cy="asset-display-module"
        />
    );
};

export default IframedUrl;
