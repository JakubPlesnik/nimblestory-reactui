const ProxiedUrl = ({ proxyUrl }) => {
    proxyUrl = proxyUrl.replace(
        'https://shelf.nimblestory.com',
        `${process.env.NEXT_PUBLIC_BACKEND_URL}proxy-shelf`
    );

    const frameid = 'moduleframe';

    return (
        <iframe
            src={proxyUrl}
            id={frameid}
            className="assetiFrame"
            title="Module"
            width="100%"
            height="100%"
            frameBorder="0"
            allow="fullscreen"
            allowFullScreen
            data-cy="asset-display-module"
        />
    );
};

export default ProxiedUrl;
