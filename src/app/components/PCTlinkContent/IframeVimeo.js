const IframedVimeo = ({ vimeoId }) => {
    return (
        <div style={{ height: '100%', width: '100%' }}>
            <iframe
                src={`https://player.vimeo.com/video/${vimeoId}`}
                frameBorder="0"
                width="100%"
                height="100%"
                allow="autoplay; fullscreen; picture-in-picture"
                data-cy="asset-display-module"
            ></iframe>
        </div>
    );
};

export default IframedVimeo;
