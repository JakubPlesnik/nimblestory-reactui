import { Button, Space } from 'antd';
import Thumbnail from '../image/Thumbnail';
const LandingPage = ({ urlLink, thumbnailId, description }) => {
    return (
        <div className="centeredDiv" data-cy="asset-display-module">
            <Space direction="vertical" align="center">
                <Thumbnail
                    thumbnailId={thumbnailId}
                    placeholderIcon={''}
                    width={239}
                    height={159}
                />
                <p className={'textContainer'} style={{ width: '248px' }}>
                    {description}
                </p>
                <Button
                    type="primary"
                    className={'detailButton'}
                    size="large"
                    onClick={() => window.open(urlLink, '_blank')}
                >
                    Open link
                </Button>
                <p>{urlLink}</p>
            </Space>
            <style jsx>{`
                .centeredDiv {
                    text-align: center;
                    position: relative;
                    top: 50%;
                    transform: translate(0, -50%);
                }

                .textContainer {
                    text-overflow: ellipsis;
                    overflow: hidden;
                    max-height: 4.8em;
                    line-height: 1.8em;
                }
            `}</style>
        </div>
    );
};

export default LandingPage;
