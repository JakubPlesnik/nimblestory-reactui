import Card from 'react-bootstrap/Card';

const LoginInfoBox = () => {
    return (
        <Card>
            <Card.Header>
                <div className="nslogosmall" />
                An Interactive Visualization Platform
            </Card.Header>
            <Card.Body>
                <div className="buildinfo">
                    <div className="padLeft">
                        <p>
                            Build: {process.env.NEXT_PUBLIC_APP_VERSION}
                            <br />
                            By:{' '}
                            <a
                                href="https://www.throughline.com/"
                                target="_blank"
                                rel="noreferrer"
                            >
                                Throughline
                            </a>
                        </p>
                    </div>
                    <div className="orgSecondary" />
                </div>
                <a
                    className="btnStdColorDark padLeft btn btn-primary d-block"
                    href="mailto:admin@nimblestory.com?subject=Login Page Inquiry"
                >
                    Get Help or Provide Feedback
                </a>
            </Card.Body>

            <style jsx>
                {`
                    .nslogosmall {
                        display: block;
                        width: 157px;
                        height: 30px;
                        background-size: 157px 30px;
                        background-position: center;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/img/ns-logo-small@2x.png);
                        padding-bottom: 12px;
                    }
                    .buildinfo {
                        display: flex;
                    }
                    .orgSecondary {
                        display: block;
                        margin-left: 30px;
                        width: 120px;
                        height: 45px;
                        background-size: 120px;
                        background-position: center;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/img/Throughline_Wordmark.png);
                    }
                    .orgLogo {
                        width: 100%;
                        height: 56px;
                        background-size: contain;
                        background-position: center;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/img/ns-logo-reg@2x.png);
                        margin-top: 10px;
                    }
                `}
            </style>
        </Card>
    );
};
export default LoginInfoBox;
