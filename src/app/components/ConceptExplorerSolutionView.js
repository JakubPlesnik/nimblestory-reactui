import { useState, useContext, useEffect, useReducer } from 'react';
import { useRouter } from 'next/router';
import cn from 'classnames';
import Fullscreen from 'react-full-screen';
import dynamic from 'next/dynamic';
import UserContext from '../context/UserContext';
import LoaderContext from '../context/LoaderContext';
import ConceptExplorerContext from '../context/ConceptExplorerContext';
import TopPanel from './ConceptExplorerTopPanel';
import TopPanelFullScreen from './ConceptExplorerTopPanelFullScreen';
import ConceptExplorerNavWaypoints from './ConceptExplorerNavWaypoints';
import ConceptExplorerNavWaypointsWindow from './ConceptExplorerNavWaypointsWindow';
import ConceptExplorerMapSidebar from './ConceptExplorerMapSidebar';
import SidebarResizeControl from './ConceptExplorerSidebarResizeControl';
import useStateRef from './../hooks/UseStateRef';

import {
    ConceptExplorerPreload,
    ConceptExplorerPreloadCancel,
} from '../services/ConceptExplorerPreloader';
import ConceptExplorerNavDropdown from './ConceptExplorerNavDropdown';

const ConceptExplorerMap = dynamic(
    () => import('../components/ConceptExplorerMap'),
    { ssr: false }
);

const minSidebarWidth = 340;

const defaultLayoutState = {
    isFullScreen: false,
    sidebar: false,
    topPanel: true,
    tab: 0,
    sidebarWidth: minSidebarWidth,
};
const tabs = {
    VISUALS: 0,
    JOURNEYS: 1,
};
function onLayoutStateChange(state, action) {
    const itemkey = Object.keys(action)[0];
    const itemvalue = action[Object.keys(action)[0]];
    return { ...state, ...{ [itemkey]: itemvalue } };
}

const ConceptExplorerSolutionView = ({ isEmbedded = false }) => {
    const { pathinfo } = useContext(UserContext);

    const router = useRouter();

    const { addToHead, updateLoaders } = useContext(LoaderContext);
    const { solution } = useContext(ConceptExplorerContext);
    const { themeColors } = useContext(UserContext);
    const { color1Darkest, color2Medium } = themeColors;

    const [visual, setVisual] = useState(false);
    const [journey, setJourney] = useState(false);
    const [waypoints, setWaypoints] = useState([]);
    const [waypoint, setWaypoint, waypointRef] = useStateRef(false);
    const [dropList, setDropList] = useState([]);
    const [sidebarWidth, setSidebarWidth] = useState(minSidebarWidth);
    const [dropListSelectedItem, setDropListSelectedItem] = useState(0);

    const [layoutState, setLayoutState] = useReducer(
        onLayoutStateChange,
        defaultLayoutState
    );
    const [mouseDown, setMouseDown] = useState(false);
    const [startpos, setStartpos] = useState(0);

    const onSidebarMove = ({ pageX }) => {
        if (mouseDown && startpos) {
            const dx = startpos - pageX;
            if (dx) {
                setStartpos(pageX);
                const newSideBarWidth = sidebarWidth + dx;
                setSidebarWidth(
                    newSideBarWidth >= minSidebarWidth
                        ? newSideBarWidth
                        : minSidebarWidth
                );
            }
        }
    };

    useEffect(() => {
        if (solution && solution.visuals) {
            let index;
            const { visuals, journeys } = solution;
            const newVisuals = visuals.map(visual => ({
                ...visual,
                showAllWaypoints: false,
            }));
            const noVisuals = visuals.map(visual => ({
                ...visual,
                showAllWaypoints: true,
            }));

            const mixVisuals = [];
            for (index = 0; index < visuals.length; index++) {
                mixVisuals[index * 2] = newVisuals[index];
                mixVisuals[index * 2 + 1] = noVisuals[index];
            }

            solution.visuals = [...mixVisuals];

            const mixArray = [...mixVisuals, ...journeys];
            setDropList(mixArray);
        }
    }, [solution]);

    const onMapLoaded = () => {
        if (solution) {
            ConceptExplorerPreload(solution);
        }
    };

    const setupWaypointsSelfGuided = () => {
        const waypoints = solution.journeys.map(j => j.waypoints).flat(1);
        setWaypoints(waypoints);
    };

    const removeWaypointsSelfGuided = () => {
        setWaypoints([]);
    };

    const setupWaypoitsforJourney = () => {
        const { waypoints } = journey;
        const { visualId } = waypoints[0];

        if (visualId !== visual.id) {
            const newVisual = solution.visuals.find(v => v.id === visualId);
            setVisual(newVisual);
        }
        setWaypoints(waypoints);
    };

    const ComponentDropdownNav = visual ? (
        <ConceptExplorerNavDropdown
            solution={solution}
            dropList={dropList}
            waypoint={waypoint}
            waypoints={waypoints}
            dropListSelectedItem={dropListSelectedItem}
            setWaypoint={setWaypoint}
            setVisual={setVisual}
            setJourney={setJourney}
            setLayoutState={setLayoutState}
            setDropListSelectedItem={setDropListSelectedItem}
        />
    ) : null;

    const ComponentExplorerNavWaypointsWindow = visual ? (
        <ConceptExplorerNavWaypointsWindow
            waypoint={waypoint}
            waypoints={waypoints}
            setWaypoint={setWaypoint}
        />
    ) : null;

    const ComponentExplorerNavWaypoints = visual ? (
        <ConceptExplorerNavWaypoints
            waypoint={waypoint}
            waypoints={waypoints}
            setWaypoint={setWaypoint}
        />
    ) : null;

    /*
    Events
    - view component loaded
        - add css
    - solution
        - set first visual
        - set first journey
        - set selfguided waypoints
    - layout.tab
        - visuals tab: reset waypoints for self-guided
        - journeys tab: reset visual, waypoints for current journey
    - journey
        - reset visual, waypoints for current journey
        - update router
    - visual
        - update router
    - waypoint
        - toggle sidebar
        - change visual if waypoint visual for waypoint is different
    - sidebar width
        - update layout state

  */

    useEffect(() => {
        updateLoaders({ title: 'solution', progress: 1 });

        addToHead({
            type: 'stylesheet',
            href: 'https://unpkg.com/leaflet@1.6.0/dist/leaflet.css',
        });

        const width =
            window.innerWidth ||
            document.documentElement.clientWidth ||
            document.body.clientWidth;
        const _startSidebarWidth = width * 0.33;
        setSidebarWidth(_startSidebarWidth);

        return () => {
            ConceptExplorerPreloadCancel();
        };
    }, []);

    useEffect(() => {
        addToHead({
            type: 'css',
            data: `
        .ce-popup .leaflet-popup-tip,
        .ce-popup .leaflet-popup-content-wrapper {
            background: ${color2Medium};
            color: ${color1Darkest};
            border: 1px solid ${color1Darkest};
        }
        .ce-popup-hover .leaflet-popup-tip,
        .ce-popup-hover  .leaflet-popup-content-wrapper{
            background: ${color2Medium};
            color: ${color1Darkest};
        }
    `,
        });

        if (solution && solution.visuals.length && !visual) {
            const { visuals, journeys } = solution;

            if (visuals && router.query.visual) {
                const routeVisualIdx = visuals.findIndex(
                    s => s.slug === router.query.visual
                );
                if (routeVisualIdx >= 0) {
                    setVisual(visuals[routeVisualIdx]);
                }
            } else if (visuals) {
                setVisual(visuals[0]);
            }

            if (journeys && router.query.journey) {
                const routeJourneyIdx = journeys.findIndex(
                    j => j.slug === router.query.journey
                );

                if (routeJourneyIdx >= 0) {
                    setJourney(journeys[routeJourneyIdx]);
                    setLayoutState({ tab: tabs.JOURNEYS });
                }
            } else if (journeys) {
                setJourney(journeys[0]);
            }

            setWaypoint(false);
        }

        updateLoaders({ title: 'solution', progress: 100 });
    }, [solution]);

    useEffect(() => {
        if (solution) {
            const { tab } = layoutState;
            setWaypoint(false);

            if (tab == tabs.JOURNEYS) {
                setupWaypoitsforJourney();
                window.addEventListener('keyup', onkeyUp);
                return () => {
                    window.removeEventListener('keyup', onkeyUp);
                };
            }
        }
    }, [layoutState.tab]);

    useEffect(() => {
        if (visual) {
            if (journey && layoutState.tab === tabs.JOURNEYS) {
                setWaypoint(false);
                setupWaypoitsforJourney();
            }
        }
    }, [journey, dropListSelectedItem]);

    useEffect(() => {
        if (visual && layoutState.tab === tabs.VISUALS) {
            if (visual.showAllWaypoints) {
                setupWaypointsSelfGuided();
            } else {
                removeWaypointsSelfGuided();
            }
        }
    }, [visual, dropListSelectedItem]);

    useEffect(() => {
        if (waypoint) {
            if (visual.id !== waypoint.visualId) {
                const newVisual = solution.visualId.find(
                    v => v.id === waypoint.visualId
                );
                setVisual(newVisual);
            }
            setLayoutState({ sidebar: true });
        } else {
            setLayoutState({ sidebar: false });
        }
    }, [waypoint]);

    useEffect(() => {
        setLayoutState({ minSidebarWidth: sidebarWidth });
    }, [sidebarWidth]);

    const onkeyUp = event => {
        switch (event.keyCode) {
            case 37:
                // left key
                try {
                    window.callPrevWaypoint();
                } catch (e) {
                    // eslint-disable-next-line no-console
                    console.error(`Exception: ${e.message}`);
                }
                break;
            case 39:
                // right key
                try {
                    window.callNextWaypoint();
                } catch (e) {
                    // eslint-disable-next-line no-console
                    console.error(`Exception: ${e.message}`);
                }
                break;
        }
    };

    return (
        <Fullscreen
            enabled={layoutState.isFullScreen}
            onChange={state => setLayoutState({ isFullScreen: state })}
        >
            <div
                className="fullheightcontainer"
                onMouseMove={onSidebarMove}
                onMouseUp={() => {
                    setMouseDown(false);
                }}
            >
                {layoutState.isFullScreen === false && (
                    <TopPanel
                        pathinfo={pathinfo}
                        project={solution}
                        components={{
                            ComponentDropdownNav,
                            ComponentExplorerNavWaypointsWindow,
                        }}
                        layoutState={layoutState}
                        setLayoutState={setLayoutState}
                        waypoint={waypoint}
                        waypoints={waypoints}
                        setWaypoint={setWaypoint}
                    />
                )}

                {layoutState.isFullScreen === true && (
                    <TopPanelFullScreen
                        project={solution}
                        components={{ ComponentExplorerNavWaypoints }}
                        layoutState={layoutState}
                        setLayoutState={setLayoutState}
                        waypoint={waypoint}
                        waypoints={waypoints}
                        setWaypoint={setWaypoint}
                    />
                )}

                <div
                    className={cn('mapview', {
                        showsidebar: layoutState.sidebar,
                    })}
                >
                    <div className="mainmapcontainer">
                        {visual && (
                            <ConceptExplorerMap
                                key={visual.id}
                                visual={visual}
                                waypoints={waypoints}
                                waypoint={waypoint}
                                waypointRef={waypointRef}
                                setWaypoint={setWaypoint}
                                layoutState={layoutState}
                                onMapLoaded={() => onMapLoaded()}
                            />
                        )}
                    </div>

                    <div className="cdsidebar" style={{ width: sidebarWidth }}>
                        <SidebarResizeControl
                            onMouseDown={({ pageX }) => {
                                setStartpos(pageX);
                                setMouseDown(true);
                            }}
                        />

                        {solution && (
                            <ConceptExplorerMapSidebar
                                waypoint={waypoint}
                                isEmbedded={isEmbedded}
                                slug={solution.slug}
                            />
                        )}
                    </div>
                </div>

                {router.query.embed && (
                    <a
                        href="https://nimblestory.com"
                        alt="NimbleSotry"
                        className="nsLogo"
                        target="_blank"
                        rel="noreferrer"
                    />
                )}

                <style jsx>
                    {`
                        .nsLogo {
                            position: absolute;
                            bottom: 10px;
                            left: 56px;
                            width: 152px;
                            height: 26px;
                            background-image: url('/appfiles/img/NimbleStory_logo-poweredby_0220.png');
                            z-index: 1000;
                            background-size: contain;
                            box-sizing: content-box;
                            border: 1px solid #ccc;
                            box-shadow: 0 1px 5px rgba(0, 0, 0, 0.65);
                            border-radius: 4px;
                        }

                        .fullheightcontainer {
                            min-height: 100%;
                            height: 100%;
                            display: flex;
                            flex-direction: column;
                        }
                        .mapview {
                            position: relative;
                            flex: 1 1 auto;
                            overflow: hidden;
                            width: 100%;
                            position: relative;
                            display: flex;
                        }
                        .mainmapcontainer {
                            flex: 1 0 0;
                            flex-grow: 1;
                            flex-shrink: 0;
                            flex-basis: 100%;
                        }
                        .cdsidebar {
                            position: relative;
                            background-color: ${color2Medium};
                            min-width: ${minSidebarWidth}px;
                            flex-grow: 0;
                            flex-shrink: 0;
                            flex-basis: auto;
                        }
                        .showsidebar > .mainmapcontainer {
                            min-width: 0;
                            flex-basis: 0;
                        }
                        /* Animation */
                        .mainmapcontainer {
                            transition: flex-basis 0.1s
                                cubic-bezier(0.465, 0.183, 0.153, 0.946);
                            will-change: flex-basis;
                        }

                        .cdsidebar {
                            transition: flex-basis 0.3s
                                cubic-bezier(0.465, 0.183, 0.153, 0.946);
                        }
                    `}
                </style>
            </div>
        </Fullscreen>
    );
};
export default ConceptExplorerSolutionView;
