import { useState } from 'react';
import Router from 'next/router';
import { sendAuthRequest } from '../services/api/apiService';

const Form = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [message, setMessage] = useState('');

    const authenticate = e => {
        e.preventDefault();

        if (username != '' && password != '') {
            sendAuthRequest(username, password, onLoginSuccess, onLoginError);
            return;
        }

        setMessage('Please enter your username and password');
    };

    const onLoginSuccess = data => {
        if (data.token === undefined) {
            return;
        }
        localStorage.setItem('token', data.token);
        Router.push('/organization');
    };

    const onLoginError = error => {
        if (!error) {
            setMessage('Server is not responding');
        } else if (error.status === 401) {
            setMessage('Invalid username or password.');
        } else setMessage('Something went wrong.');
    };

    return (
        <form className="sign-in">
            <div className="form-group">
                <label htmlFor="email">Email address</label>
                <input
                    type="text"
                    name="username"
                    placeholder="username"
                    className="form-control"
                    onChange={e => setUsername(e.target.value)}
                    data-cy="login-email"
                />
            </div>
            <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                    type="password"
                    name="password"
                    placeholder="password"
                    className="form-control"
                    onChange={e => setPassword(e.target.value)}
                    data-cy="login-password"
                />
            </div>
            <a href="/resetpassword">Forgotten password?</a>

            {message != '' && (
                <div
                    style={{ marginBottom: '10px' }}
                    className="message"
                    data-cy="login-message"
                >
                    {message}
                </div>
            )}

            <button
                className="btn btn-primary"
                onClick={e => authenticate(e)}
                data-cy="login-button"
            >
                Sign In
            </button>
            <style jsx>
                {`
                    .sign-in a {
                        display: block;
                        margin-top: -10px;
                        margin-bottom: 10px;
                    }
                `}
            </style>
        </form>
    );
};

export default Form;
