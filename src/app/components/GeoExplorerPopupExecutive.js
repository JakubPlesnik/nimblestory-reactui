import GeoExplorerPopupColumn from './GeoExplorerPopupColumn';
import {
    kPropLocationName,
    kPropValuation,
    kPropAccuracyRange,
    kPropValuationExistence,
    kPropValuationCompletion,
    kPropDateOfLastInventory,
    kPropReadyForIssueCondition,
    kPropTurnoverRate,
    filterIndicatorColorVal1,
    filterIndicatorColorVal2,
    filterIndicatorColorDate,
} from '../services/geoexplorerconsts';

const GeoExplorerPopupExecutive = props => {
    const columnIds = [
        {
            title: kPropAccuracyRange,
            source: kPropValuationExistence,
            indicatorcolor: filterIndicatorColorVal1,
            formatter: value => `${value}%`,
        },
        {
            title: kPropValuation,
            source: kPropValuationCompletion,
            indicatorcolor: filterIndicatorColorVal1,
            formatter: () => '$99,999',
        },
        {
            title: kPropDateOfLastInventory,
            indicatorcolor: filterIndicatorColorDate,
        },
        {
            title: kPropReadyForIssueCondition,
            indicatorcolor: filterIndicatorColorVal1,
            formatter: value => `${value}%`,
        },
        {
            title: kPropTurnoverRate,
            indicatorcolor: filterIndicatorColorVal2,
            formatter: value => `${value}%`,
        },
    ];

    return (
        <div className="popup">
            <div className="title">{props[kPropLocationName]}</div>
            <div className="columns">
                {columnIds.map((columnId, index, { length }) => {
                    const label = columnId.title;
                    const value = columnId.source
                        ? props[columnId.source]
                        : props[columnId.title]; // props[columnId]
                    const formattedVal = columnId.formatter
                        ? columnId.formatter(value)
                        : value;
                    const color = columnId.indicatorcolor(value);

                    return (
                        <GeoExplorerPopupColumn
                            key={index}
                            color={color}
                            label={label}
                            value={formattedVal}
                            index={index}
                            length={length}
                        />
                    );
                })}
            </div>

            <style jsx>
                {`
                    .title {
                        background: #293e57;
                        color: #f8fbff;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 40px;
                        text-align: center;
                        letter-spacing: -0.1px;
                        text-transform: capitalize;
                        color: #f8fbff;
                        border-radius: 6px 6px 0px 0px;
                        height: 40px;
                    }
                    .columns {
                        height: 95px;
                        background: #f3f5f8;
                        display: flex;
                    }
                    .popup {
                        font-family: 'Roboto', sans-serif;
                        width: 400px;
                        background: #f3f5f8;
                        border-radius: 6px;
                        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                    }
                `}
            </style>
        </div>
    );
};
export default GeoExplorerPopupExecutive;
