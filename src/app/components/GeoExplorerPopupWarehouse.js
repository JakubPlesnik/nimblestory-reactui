import GeoExplorerPopupColumn from './GeoExplorerPopupColumn';

import {
    kPropLocationName,
    kLastMovement,
    kPutawayTime,
    kPropProgramUtilizationRate,
    filterIndicatorColorVal1,
    filterIndicatorColorVal2,
} from '../services/geoexplorerconsts';

const GeoExplorerPopupWarehouse = props => {
    const columnIds = [
        { title: kLastMovement, indicatorcolor: filterIndicatorColorVal2 },
        { title: kPutawayTime, indicatorcolor: filterIndicatorColorVal2 },
        {
            title: kPropProgramUtilizationRate,
            indicatorcolor: filterIndicatorColorVal1,
        },
    ];

    return (
        <div className="popup">
            <div className="title">{props[kPropLocationName]}</div>
            <div className="columns">
                {columnIds.map((columnId, index, { length }) => {
                    const label = columnId.title;
                    const value = props[columnId.title]; // props[columnId]
                    const color = columnId.indicatorcolor(value);

                    return (
                        <GeoExplorerPopupColumn
                            key={index}
                            color={color}
                            label={label}
                            value={value}
                            index={index}
                            length={length}
                        />
                    );
                })}
            </div>

            <style jsx>
                {`
                    .title {
                        background: #293e57;
                        color: #f8fbff;
                        font-size: 14px;
                        font-weight: bold;
                        line-height: 40px;
                        text-align: center;
                        letter-spacing: -0.1px;
                        text-transform: capitalize;
                        color: #f8fbff;
                        border-radius: 6px 6px 0px 0px;
                        height: 40px;
                    }
                    .columns {
                        height: 95px;
                        background: #f3f5f8;
                        display: flex;
                    }
                    .popup {
                        font-family: 'Roboto', sans-serif;
                        background: #f3f5f8;
                        border-radius: 6px;
                        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                    }
                `}
            </style>
        </div>
    );
};
export default GeoExplorerPopupWarehouse;
