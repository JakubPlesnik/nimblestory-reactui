import { useRouter } from 'next/router';
import { useContext, useState, useEffect } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import UserContext from '../context/UserContext';
import PortfolioContext from '../context/PortfolioContext';
import PortfolioProjectAssetsContext from '../context/PortfolioProjectAssetsContext';
import LoaderContext from '../context/LoaderContext';
import { AppDropdownBox } from './AppDropdownBox';
import { TopPanelProject, TopPanelProjectAssets } from './TopPanelProject';
import AppCardAsset from '../components/cards/AppCardAsset';
import AppCardFolder from '../components/cards/AppCardFolder';
import AppInputSearch from './AppInputSearch';
import FilterEmptyState from './FilterEmptyState';
import AppButtonGoUp from '../components/cards/AppButtonGoUp';
import AppFolderBreadcrumb from './AppFolderBreadcrumb';
import AppCardConteptExplorer from '../components/cards/AppCardConteptExplorer';
import AddContent from '../components/cards/AppCardAddContent';
import ModalUpload from '../components/uploadModal/modalUpload';
import { Space } from 'antd';

import { assetSortOptions } from '../services/filteringAssets';

const PortfolioProjectView = () => {
    const router = useRouter();
    const { pathinfo } = useContext(UserContext);
    const [isUploadModalOpen, setUploadModalOpen] = useState(false);
    const {
        project,
        assets,
        assetfilters,
        setAssetSortOption,
        searchFilterKeyword,
        setSearchFilterKeyword,
        selectedAssetSortOption,
        setAssetFilters,
        hasMore,
        totalAssets,
        projectSubfolders,
        projectRootFolderId,
        projectCurentSubfolder,
        setProjectCurentSubfolder,
        filteredSortedAssestsWithSubfolders,
        sortAndFilterPortfolios,
        loadData,
    } = useContext(PortfolioProjectAssetsContext);

    const { assetTypesDynamic, assetStatusDynamic } = useContext(
        PortfolioContext
    );
    const { loadersState } = useContext(LoaderContext);

    const substractHidden = router.query.projectfilter != null ? 1 : 0;

    const displayAllAssetsPanel = router.query.project === 'assets';

    const isRootFolder = projectRootFolderId == projectCurentSubfolder;

    useEffect(() => {
        var params = new URLSearchParams(window.location.search);

        if (projectSubfolders.length && params.has('folderId')) {
            const selectedFolder = projectSubfolders.find(
                item => item.id == params.get('folderId')
            );
            if (selectedFolder) {
                setProjectCurentSubfolder(selectedFolder.id);
            }
        }
    }, [projectSubfolders]);

    const refreshContent = () => {
        loadData(1, 'df', []);
        sortAndFilterPortfolios();
    };

    const getFolder = id => {
        for (const folder of projectSubfolders) {
            if (folder.id == id) {
                return folder;
            }
        }
        return null;
    };
    const getCurentFolder = () => {
        return getFolder(projectCurentSubfolder);
    };

    const getParentFolder = () => {
        const curentFolder = getCurentFolder();
        if (curentFolder === null) {
            return null;
        }

        const parentFodler = getFolder(curentFolder.parentId);
        return parentFodler;
    };

    const getPrevFolderName = () => {
        const parentFolder = getParentFolder();

        if (parentFolder === null) {
            return project.title;
        }

        return parentFolder.title;
    };

    const setPrevFolderOrRoot = () => {
        const parentFolder = getParentFolder();

        if (parentFolder === null) {
            setProjectCurentSubfolder(projectRootFolderId);
            return;
        }

        setProjectCurentSubfolder(parentFolder.id);
    };

    const breadcrumbs = () => {
        let folderId = projectCurentSubfolder;
        let breadcrumb = [];
        while (folderId != project.id) {
            const folder = getFolder(folderId);
            if (folder === null) {
                break;
            }
            breadcrumb = [[folder.title, folder.id], ...breadcrumb];
            folderId = folder.parentId;
        }
        return [[project.title, project.id], ...breadcrumb];
    };

    const getObjectCard = item => {
        if (item.entryType === 'CE') {
            return (
                <AppCardConteptExplorer
                    key={`${item.id}-projects`}
                    organization={pathinfo.organization}
                    project={item}
                />
            );
        }

        if (item.numberOfChildrens > 0) {
            return (
                <AppCardFolder
                    key={`${item.id}-folder`}
                    organization={pathinfo.organization}
                    folder={item}
                    setAsCurentFolder={setProjectCurentSubfolder}
                />
            );
        }

        return (
            <AppCardAsset
                key={`${item.id}-assets`}
                organization={pathinfo.organization}
                project={pathinfo.project}
                asset={item}
                permissions={project.permissions}
            />
        );
    };

    return (
        <div className="page-wrapper">
            {!displayAllAssetsPanel && (
                <TopPanelProject
                    project={project || {}}
                    assetsnum={totalAssets || 0}
                />
            )}

            {displayAllAssetsPanel && (
                <TopPanelProjectAssets assetsnum={totalAssets || 0} />
            )}
            <div className="pagearea">
                <div className="breadCrumb">
                    <div className="pl-0 col-12 pl-4">
                        {project && projectSubfolders && (
                            <AppFolderBreadcrumb
                                type="project"
                                pathInfo={pathinfo}
                                items={breadcrumbs()}
                                setAsCurentFolder={setProjectCurentSubfolder}
                            />
                        )}
                    </div>
                </div>

                <div className="controls">
                    {/* <div className="controlsOne">
                        <AppDropdownBox
                            label="FILTER"
                            menuOptions={assetSortOptions.map(
                                ({ title }) => title
                            )}
                            selectedOptionKey={selectedAssetSortOption}
                            onOptionSelected={item => {
                                setAssetSortOption(item);
                            }}
                        />
                    </div> */}

                    <div className="controlsOne">
                        <AppDropdownBox
                            label="SORT"
                            menuOptions={assetSortOptions.map(
                                ({ title }) => title
                            )}
                            selectedOptionKey={selectedAssetSortOption}
                            onOptionSelected={item => {
                                setAssetSortOption(item);
                            }}
                        />
                    </div>
                    <div className="controlsTwo">
                        <AppInputSearch
                            label="SEARCH"
                            searchFilterKeyword={searchFilterKeyword}
                            setSearchFilterKeyword={setSearchFilterKeyword}
                        />
                    </div>
                </div>
                <div className="divider17"></div>
                <div className="assetsContainer">
                    <Space size={[16, 35]} wrap style={{ marginBottom: '0px' }}>
                        {!isRootFolder && projectCurentSubfolder && (
                            <AppButtonGoUp
                                goUpFunction={() => {
                                    setPrevFolderOrRoot();
                                }}
                                parentFolderName={getPrevFolderName()}
                            />
                        )}

                        {filteredSortedAssestsWithSubfolders.map(item =>
                            getObjectCard(item)
                        )}

                        {project && project.uploadAllowed && (
                            <AddContent
                                setOpen={setUploadModalOpen}
                            ></AddContent>
                        )}
                    </Space>
                </div>

                {// set loading spinner in the bottom when switching new page by scrolling
                hasMore && (
                    <div className="d-flex justify-content-center">
                        <Spinner
                            animation="border"
                            role="status"
                            variant="primary"
                        >
                            <span className="sr-only">Loading...</span>
                        </Spinner>
                    </div>
                )}

                {loadersState.length == 0 &&
                    !substractHidden &&
                    !assets.length &&
                    assetfilters.length > 0 && (
                        <FilterEmptyState
                            onClick={() => {
                                setAssetFilters([]);
                            }}
                        />
                    )}

                {loadersState.length == 0 &&
                    substractHidden !== 0 &&
                    !assets.length &&
                    assetfilters.length > 0 && <FilterEmptyState />}
                <div className="divider15" />
                <div className="divider56" />
            </div>
            <style jsx>
                {`
                    .page-wrapper {
                        background-color: #f0f0f0;
                    }

                    .breadCrumb {
                        padding-top: 16px;
                        margin-left: 18px;
                        margin-bottom: 10px;
                    }
                    .pl-0 {
                        padding-left: 0px !important;
                    }
                    .controls {
                        display: flex;
                        flex-direction: row;
                        flex-wrap: nowrap;
                        align-content: center;
                        width: 100%;
                        padding-left: 17px;
                        padding-right: 28px;
                    }
                    .controlsOne {
                        min-width: 134px;
                        max-width: 350px;
                        margin-right: 10px;
                    }
                    .controlsTwo {
                        flex-grow: 1;
                        margin-right: 10px;
                    }
                    .controlsThree {
                        min-width: 134px;
                    }
                    .divider3 {
                        height: 3px;
                        clear: both;
                    }
                    .divider10 {
                        height: 10px;
                        clear: both;
                    }
                    .divider15 {
                        height: 14px;
                        clear: both;
                    }
                    .divider17 {
                        height: 17px;
                        clear: both;
                    }
                    .assetsContainer {
                        margin-left: 16px;
                        display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                    }
                `}
            </style>
            {project && (
                <ModalUpload
                    isOpen={isUploadModalOpen}
                    projectSlug={project.slug}
                    orgSlug={pathinfo.organization}
                    refreshContent={refreshContent}
                    setOpen={setUploadModalOpen}
                    contentStatuses={assetStatusDynamic}
                    contentTypes={assetTypesDynamic}
                ></ModalUpload>
            )}
        </div>
    );
};

export default PortfolioProjectView;
