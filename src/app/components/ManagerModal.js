import React, { Fragment, useState, useContext, useEffect } from 'react';
import cn from 'classnames';

import Modal, { ModalFooter, ModalTransition } from '@atlaskit/modal-dialog';
import Button from '@atlaskit/button';
import ManagerDraggableList from './ManagerDraggableList';
import { savePins, authorizeToken } from '../services/cmsapi-manage';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';
import Icon from './Icon';
import {
    ICONS
  } from '../enum/projectIcons';


const CardItem = ({ item, isActive, isHovering, isDragging, onRemoveItem }) => {
    const { themeColors } = useContext(UserContext);
    const { color1Darkest } = themeColors;

    return (
        <div
            className={cn(
                'cardwrapper',
                { dragging: !!isDragging || isActive },
                { hovering: isHovering }
            )}
        >
            {item.section === 'magaAssets' && (
               <Icon
               name={ICONS.pctAsset}
               size="24"
               type="outline"
               color={color1Darkest}
           />
            )}
            {item.section === 'conceptexplorer' && (
              <Icon
              name={ICONS.conceptexplorer}
              size="24"
              type="outline"
              color={color1Darkest}
          />
            )}
            {item.section === 'geoexplorer' && (
               <Icon
               name={ICONS.geoexplorer}
               size="24"
               type="outline"
               color={color1Darkest}
           />
            )}
            {item.section === 'projects' && (
              <Icon
              name={ICONS.project}
              size="24"
              type="outline"
              color={color1Darkest}
          />
            )}
            {item.title}
            <Button onClick={() => onRemoveItem(item)} appearance="subtle">
                Remove
            </Button>
            <style jsx>
                {`
        .dragging {
            cursor:  grabbing;
            text-decoration: none;
            background-color: #cecece;
        }
        .hovering {
            background-color: #cecece;
            text-decoration: none;
        }
        .cardwrapper {
            display: flex;
            justify-content: space-between;
            align-items: baseline;

            border-radius: 3px;
            display: flex;
            position: relative;
            padding: 10px 10px;
            border: 1px solid #cecece;
            margin-bottom: 6px;
        `}
            </style>
        </div>
    );
};

const ManagerModalFooter = ({ onSave }) => (
    <ModalFooter className={cn('modal-footer')} showKeyline={false}>
        <div>Drag/Drop to set Home Page display order</div>
        <Button
            style={{ backgroundColor: '#002445', borderRadius: '0px' }}
            variant="primary"
            onClick={onSave}
        >
            <p style={{ color: '#fff', paddingTop: '15px' }}>Save</p>
        </Button>
    </ModalFooter>
);

const ManagerModal = () => {
    const { organization, pinned, setPinned, setIsModalOpen } = useContext(
        LoginContext
    );
    const [localPins, setLocalPins] = useState([]);

    useEffect(() => {
        setLocalPins([...pinned]);
    }, []);

    const ManagerModalFooterWrapper = () => (
        <ManagerModalFooter
            onSave={() => {
                authorizeToken().then(success => {
                    if (success) {
                        setPinned(localPins);
                        savePins({
                            organization: organization.slug,
                            pins: localPins.map(i => i.id),
                        });
                    }
                });
                setIsModalOpen(false);
            }}
        />
    );

    return (
        <Fragment>
            <ModalTransition>
                <Modal
                    onClose={() => setIsModalOpen(false)}
                    heading="Organize Home Page"
                    height="100%"
                    components={{ Footer: ManagerModalFooterWrapper }}
                >
                    {localPins.length > 0 && (
                        <ManagerDraggableList
                            pinned={localPins}
                            onOrderChange={updated => {
                                setLocalPins([...updated]);
                            }}
                            components={{ Card: CardItem }}
                        />
                    )}
                    {localPins.length === 0 && (
                        <p>No items have been pinned yet</p>
                    )}
                </Modal>
            </ModalTransition>
        </Fragment>
    );
};

export default ManagerModal;
