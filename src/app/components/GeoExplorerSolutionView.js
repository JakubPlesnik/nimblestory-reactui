import dynamic from 'next/dynamic';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../context/UserContext';
import GeoExplorerContext from '../context/GeoExplorerContext';
import LoaderContext from '../context/LoaderContext';
import TopPanel from './GeoExplorerTopPanel';

import GeoExplorerMapBottomPanel from './GeoExplorerMapBottomPanel';
import {
    kPropPlantId,
    kMapPerspectiveKey,
    kMapFocusMetricKey,
    kPropProgramUtilizationRate,
    kMapPerspectiveExecutive,
} from '../services/geoexplorerconsts';

const GeoExplorerMap = dynamic(() => import('../components/GeoexplorerMap'), {
    ssr: false,
});

const GeoExplorerSolutionView = () => {
    const { pathinfo } = useContext(UserContext);
    const [topPanelState, setTopPanelState] = useState({
        collapsed: false,
        tab: 0,
    });
    const [selectedPlant, setSelectedPlant] = useState(false);
    const [mapReady, setMapReady] = useState(false);
    const [lastupdated, setLastupdated] = useState(0);

    const { addToHead, updateLoaders } = useContext(LoaderContext);
    const {
        solution,
        geosolutionsfilters,
        geosolutionalldatafilters,
        setGeosolutionsfilters,
        dataPlantTotals,
        dataPlantInventory,
        dataPlantLocationsFiltered,
        dataPlantInventoryFiltered,
    } = useContext(GeoExplorerContext);

    const onLocationSelected = locationProps => {
        if (!locationProps) {
            setSelectedPlant(false);
            return;
        }

        function getRandomInt(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min; // The maximum is exclusive and the minimum is inclusive
        }

        const availablesFilters = geosolutionalldatafilters.map(e => e.source);
        const activeFilters = Object.keys(geosolutionsfilters).filter(
            e => availablesFilters.includes(e) && geosolutionsfilters[e].length
        );
        const inventoryDataForPanel = activeFilters.length
            ? dataPlantInventoryFiltered
            : dataPlantInventory;

        let inventory =
            inventoryDataForPanel[`plant_${locationProps[kPropPlantId]}`];
        if (!inventory) {
            const copyFrom = ['111141', '98302', '116381', '121601'];
            const copyFromId = copyFrom[getRandomInt(0, 3)];
            inventory = inventoryDataForPanel[`plant_${copyFromId}`];
        }
        if (!inventory) {
            inventory = [];
        }

        const dataForPanel = { ...locationProps, inventory };
        setSelectedPlant(dataForPanel);
    };

    useEffect(() => {
        updateLoaders({ title: 'solution', progress: 1 });

        addToHead({
            type: 'stylesheet',
            href:
                'https://api.tiles.mapbox.com/mapbox-gl-js/v1.6.0/mapbox-gl.css',
        });

        const defaultFilters = {
            [kMapPerspectiveKey]: kMapPerspectiveExecutive,
            [kMapFocusMetricKey]: kPropProgramUtilizationRate,
        };

        setGeosolutionsfilters(defaultFilters);
    }, []);

    /*
  useEffect(() => {
    if (typeof window !== undefined) {
      if (solution) {
        setMapContent(
          <GeoExplorerMap
            geosolutionsfilters={geosolutionsfilters}
            settings={solution.settings}
            dataPlantLocations={dataPlantLocationsFiltered}
            dataPlantTotals={dataPlantTotals}
            onMapDisplayed={() =>{ }}
            onLocationSelected={onLocationSelected}
          />
        )
      }
    }
  }, [topPanelState, selectedPlant, solution, dataPlantLocations, dataPlantLocationsFiltered, dataPlantTotals, geosolutionsfilters])
*/

    useEffect(() => {
        setLastupdated(lastupdated + 1);
    }, [mapReady, dataPlantLocationsFiltered, dataPlantTotals]);

    const { tab } = topPanelState;
    return (
        <div className="fullheightcontainer">
            {solution && (
                <TopPanel
                    pathinfo={pathinfo}
                    project={solution}
                    asset={solution}
                    setPanelState={setTopPanelState}
                />
            )}

            <div className={`mapview viewtab-${tab}`}>
                {solution && (
                    <GeoExplorerMap
                        lastupdated={lastupdated}
                        geosolutionsfilters={geosolutionsfilters}
                        settings={solution.settings}
                        dataPlantLocations={dataPlantLocationsFiltered}
                        dataPlantTotals={dataPlantTotals}
                        onMapDisplayed={() => {
                            setMapReady(true);
                        }}
                        onLocationsDisplayed={() => {
                            updateLoaders({ title: 'solution', progress: 100 });
                        }}
                        onLocationSelected={onLocationSelected}
                    />
                )}
            </div>
            <div>
                {selectedPlant != false && (
                    <GeoExplorerMapBottomPanel
                        plant={selectedPlant}
                        setPanelState={setTopPanelState}
                    />
                )}
            </div>

            <style jsx>
                {`
                    .fullheightcontainer {
                        min-height: 100%;
                        height: 100%;
                        display: flex;
                        flex-direction: column;
                    }
                    .mapview {
                        position: relative;
                        flex: 1 1 auto;
                    }
                `}
            </style>
        </div>
    );
};

export default GeoExplorerSolutionView;
