import { useContext } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import cn from 'classnames';
import GeoExplorerContext from '../context/GeoExplorerContext';

const GeoExplorerFilterControlsMultiple = ({ filterId, options }) => {
    const { geosolutionsfilters, setGeosolutionsfilters } = useContext(
        GeoExplorerContext
    );

    return (
        <div>
            <Form
                className={cn('item-controls-container', {
                    multicolumn: options.length >= 10,
                })}
            >
                {options.map((option, key) => {
                    const optionValueArr = Array.isArray(option.value)
                        ? option.value
                        : [option.value];
                    const isSelected =
                        geosolutionsfilters[filterId] &&
                        geosolutionsfilters[filterId].some(r =>
                            optionValueArr.includes(r)
                        );
                    const elementId = `customRadio_${filterId}_${key}`.replace(
                        /[\W_]+/g,
                        '_'
                    );
                    return (
                        <Form.Check
                            key={elementId}
                            id={elementId}
                            name={elementId}
                            label={optionValueArr[0]}
                            checked={isSelected}
                            className={cn('custom-control-input', {
                                'btn-maptype-selected': isSelected,
                            })}
                            onChange={e => {
                                let selectedValues = geosolutionsfilters[
                                    filterId
                                ]
                                    ? [...geosolutionsfilters[filterId]]
                                    : [];
                                const optionValueArr = Array.isArray(
                                    option.value
                                )
                                    ? option.value
                                    : [option.value];

                                if (e.target.checked) {
                                    selectedValues = [
                                        ...selectedValues,
                                        ...optionValueArr,
                                    ];
                                } else {
                                    selectedValues = selectedValues.filter(
                                        selectedItem =>
                                            !optionValueArr.includes(
                                                selectedItem
                                            )
                                    );
                                }

                                setGeosolutionsfilters({
                                    ...geosolutionsfilters,
                                    [filterId]: selectedValues,
                                });
                            }}
                        />
                    );
                })}
            </Form>
            <div
                className={cn({
                    invisible:
                        !geosolutionsfilters[filterId] ||
                        !geosolutionsfilters[filterId].length,
                })}
            >
                <Button
                    variant="secondary"
                    onClick={() => {
                        setGeosolutionsfilters({
                            ...geosolutionsfilters,
                            [filterId]: [],
                        });
                    }}
                >
                    Clear filter
                </Button>
            </div>
            <style jsx>
                {`
                    div :global(.multicolumn) {
                        column-count: 2;
                    }
                    div :global(.custom-control-input > label) {
                        cursor: pointer;
                    }
                    div :global(.custom-control-input > input) {
                        cursor: pointer;
                    }
                    div :global(.custom-control-input) {
                        line-height: 28px;
                    }
                    div :global(.btn-maptype-selected) {
                        font-weight: bold;
                    }
                `}
            </style>
        </div>
    );
};
export default GeoExplorerFilterControlsMultiple;
