import PropTypes from 'prop-types';
import { useState, useEffect, useContext } from 'react';
import { fetchAsset } from '../services/api/apiService';
import LoaderContext from '../context/LoaderContext';
const AssetLargeImage = ({ assetId }) => {
    const { updateLoaders } = useContext(LoaderContext);
    const [imageUrl, setImageUrl] = useState('');
    useEffect(() => {
        if (Number.isInteger(assetId)) {
            updateLoaders({ title: 'largeImg', progress: 1 });
            fetchAsset(assetId, setImageUrl, null);
            return;
        }
    }, []);
    useEffect(() => {
        if (imageUrl) {
            updateLoaders({ title: 'largeImg', progress: 100 });
        }
    }, [imageUrl]);

    return (
        <div
            className="assetLarge"
            style={{ backgroundImage: `url(${imageUrl})` }}
            data-cy="asset-display-image"
        >
            <style jsx>
                {`
                    .assetLarge {
                        width: 100%;
                        height: 100%;
                        background-color: #cecece;
                        background-size: contain;
                        background-position: center;
                        background-repeat: no-repeat;
                    }
                `}
            </style>
        </div>
    );
};
AssetLargeImage.propTypes = {
    assetId: PropTypes.string.isRequired,
};
export default AssetLargeImage;
