import * as React from 'react';
import { Input } from 'antd';

const AppInputSearch = ({
    label,
    searchFilterKeyword,
    setSearchFilterKeyword,
}) => {
    const handleInputChange = e => {
        const { value } = e.target;
        setSearchFilterKeyword(value.length ? value : false);
    };

    return (
        <React.Fragment>
            <span className="label theme-color1dark--color">{label}</span>
            <Input
                allowClear
                size={'large'}
                type="text"
                placeholder="Enter search terms here"
                value={searchFilterKeyword || ''}
                className="searchbarinput theme-color1dark--color"
                onChange={handleInputChange}
            />

            <style jsx>
                {`
                    .label {
                        font-family: Roboto;
                        font-size: 16px;
                        font-weight: 500;
                        font-stretch: normal;
                        font-style: normal;
                        line-height: 1.5;
                        letter-spacing: normal;
                        text-align: left;
                        color: #262626;
                        margin-bottom: 8px;
                        display: block;
                    }

                    .searchbarwrapper {
                        position: relative;
                        height: 55px;
                        background-color: #f6f6f6;
                        border-radius: 0;
                        padding: 4px 10px;
                        padding-left: 36px;
                    }

                    .searchbarinput {
                        border: none;
                        outline: none;
                        width: 100%;
                        height: 47px;
                        background-color: #f6f6f6;
                        color: #0d3b5f;
                    }
                    .searchbarinput::-ms-clear {
                        display: none;
                    }
                `}
            </style>
        </React.Fragment>
    );
};
export default AppInputSearch;
