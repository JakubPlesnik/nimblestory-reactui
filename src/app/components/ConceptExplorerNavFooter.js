import Button from '@atlaskit/button';
import { ModalFooter } from '@atlaskit/modal-dialog';
import { N0, N900 } from '@atlaskit/theme/colors';

const ConceptExplorerNavFooter = props => (
    <div>
        <ModalFooter {...props}>
            <span />
            <Button
                appearance={props.appearance}
                onClick={props.onClose}
                style={{
                    borderRadius: 0,
                    paddingTop: 20,
                    paddingBottom: 20,
                    paddingLeft: 50,
                    paddingRight: 50,
                    margin: 'auto',
                    backgroundColor: N900,
                    color: '#fff',
                }}
            >
                <span style={{ color: N0 }}>Close</span>
            </Button>
        </ModalFooter>
    </div>
);

export default ConceptExplorerNavFooter;
