import { Fragment, useContext } from 'react';
import LoginContext from '../context/LoginContext';

const SidebarHeaderTitle = () => {
    const { organization } = useContext(LoginContext);
    const { OrgLogo } = organization;

    return (
        <Fragment>
            {OrgLogo && (
                <div className="orgLogoWrapper">
                    <div
                        className="orgLogo"
                        style={{
                            backgroundImage: `url(${organization.OrgLogo})`,
                        }}
                    />
                    <style jsx>
                        {`
                            .orgLogo {
                                width: 100%;
                                height: 86px;
                                background-size: 85%;
                                background-position: center;
                                background-repeat: no-repeat;
                                margin: 0px;
                            }
                            .orgLogoWrapper {
                                width: 248px;
                                padding-bottom: 10px;
                            }
                        `}
                    </style>
                </div>
            )}

            {!OrgLogo && (
                <div className="sidebartitle">
                    {organization.title}
                    <style jsx>
                        {`
                            .sidebartitle {
                                font-weight: 500;
                                font-size: 22px;
                                line-height: 26px;
                                text-align: center;
                            }
                        `}
                    </style>
                </div>
            )}
        </Fragment>
    );
};
export default SidebarHeaderTitle;
