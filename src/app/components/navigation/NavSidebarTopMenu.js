import React from 'react';
import NavTopElement from './NavTopElement';

const NavSidebarTopMenu = ({ topNavData }) => {
    return (
        <div>
            {topNavData.map((navElementData, i) => (
                <NavTopElement navElementData={navElementData} key={i} />
            ))}
        </div>
    );
};

export default NavSidebarTopMenu;
