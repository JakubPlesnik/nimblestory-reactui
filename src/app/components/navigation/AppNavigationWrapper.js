import React, { useEffect, useState, useContext } from 'react';
import { fetchNavigation } from '../../services/api/apiService';
import indexStyle from '../../styles/index.scss';
// remove
import Head from '../head';
import CollapseToggle from './CollapseToggle';
import MainMenu from './MainMenu';
import NavSidebarMenuWrapper from './NavSidebarMenuWrapper';
import UserContext from '../../context/UserContext';
import LoginContext from '../../context/LoginContext';

const AppNavigationWrapper = () => {
    const [openKeys, setOpenKeys] = useState([]);
    const {
        pathinfo,
        setNavigationData,
        navigationData,
        selectedNavigationItem,
        setSelectedNavigationItem,
        setNavigationCollapsed,
        navigationCollapsed,
        themeColors,
    } = useContext(UserContext);

    const { organization } = useContext(LoginContext);

    const toogleNavMenu = () => {
        setNavigationCollapsed(!navigationCollapsed);
    };

    const openMenuItem = key => {
        setOpenKeys([key]);
        setNavigationCollapsed(false);
    };

    useEffect(() => {
        if (pathinfo.organization) {
            fetchNavigation(pathinfo.organization).then(data => {
                setNavigationData(data.response);
            });
        }
    }, [pathinfo]);

    return (
        <div className="NSMainNavigation">
            <Head
                description="An Interactive Visualization Platform"
                mainstyle={{ __html: indexStyle }}
            />
            <div className="NSNavSidebar">
                {navigationData && (
                    <NavSidebarMenuWrapper
                        navData={navigationData}
                        collapsed={navigationCollapsed}
                        setSelectedMenuItem={setSelectedNavigationItem}
                    />
                )}
            </div>
            <div
                className={`NSNavMenu ${
                    navigationCollapsed ? 'collapsed' : ''
                }`}
            >
                <div className="NSNavCustomerLogo">
                    {organization && organization.OrgLogo && (
                        <img
                            style={{
                                objectFit: 'scale-down',
                                width: '100%',
                                height: '100%',
                            }}
                            src={
                                process.env.NEXT_PUBLIC_BACKEND_URL +
                                organization.OrgLogo
                            }
                        ></img>
                    )}
                </div>

                <MainMenu
                    openMenuItem={openMenuItem}
                    openKeys={openKeys}
                    setOpenKeys={setOpenKeys}
                    navData={navigationData}
                    selectedNavigationItem={selectedNavigationItem}
                    setSelectedNavigationItem={setSelectedNavigationItem}
                />
            </div>
            <CollapseToggle
                collapsed={navigationCollapsed}
                toogleNavMenu={toogleNavMenu}
            />
            <style jsx>
                {`
                    .NSMainNavigation {
                        display: flex;
                    }

                    .NSNavSidebar {
                        width: 72px;
                        height: 100vh;
                        background: ${themeColors.color1Darkest};
                        display: flex;
                        flex-direction: column;
                    }

                    .NSNavCustomerLogo {
                        padding: 20px;
                        width: 256px;
                        height: 128px;
                        background-color: #c4c4c4;
                        transition: 0.3s;
                        background-repeat: repeat-y;
                        background-size: contain;
                    }

                    .NSNavMenu {
                        background: #f5f5f5;
                        width: 256px;
                        transition: 0.3s;
                    }
                    .NSNavMenu.collapsed {
                        width: 0px;
                        opacity: 0;
                    }
                    .NSNavMenu.collapsed .NSNavCustomerLogo {
                        width: 0px;
                    }
                    .collapsed {
                        visibility: hidden;
                    }
                `}
            </style>
        </div>
    );
};

export default AppNavigationWrapper;
