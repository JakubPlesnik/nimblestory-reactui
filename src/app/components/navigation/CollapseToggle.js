import React from 'react';
import { LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons';

const CollapseToggle = ({ collapsed, toogleNavMenu }) => {
    const collapseButtonStyles = {
        fontSize: '24px',
        color: '#262626',
        background: '#FFF',
        borderRadius: '25px',
        boxShadow: '0 4px 4px 0 rgba(0, 0, 0, 0.25)',
        position: 'relative',
        top: '24px',
    };

    return (
        <div className={`NSNavCollapseButton ${collapsed ? 'collapsed' : ''}`}>
            {collapsed ? (
                <RightCircleOutlined
                    style={{ ...collapseButtonStyles, left: '0px' }}
                    onClick={toogleNavMenu}
                />
            ) : (
                <LeftCircleOutlined
                    style={{ ...collapseButtonStyles, right: '12px' }}
                    onClick={toogleNavMenu}
                />
            )}

            <style jsx>
                {`
                    .NSNavCollapseButton {
                        height: 100vh;
                        width: 0px;
                        background: red;
                    }

                    .NSNavCollapseButton.collapsed {
                        height: 100vh;
                        width: 10px;
                        background: #f4f5f7;
                    }
                `}
            </style>
        </div>
    );
};

export default CollapseToggle;
