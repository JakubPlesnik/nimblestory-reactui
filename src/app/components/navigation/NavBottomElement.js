import React, { useEffect, useState } from 'react';
import { Popover } from 'antd';
import Icon from './../Icon';
import SystemAction from './systemActions/SystemAction';
import NavigationTargetTypes from '../../enum/NavigationTargetTypes';

const getConent = items => {
    return (
        <div>
            {items.map((item, i) => {
                if (item.targetType == NavigationTargetTypes.Route) {
                    return getRouteLink(item, i);
                }

                if (item.targetType == NavigationTargetTypes.SystemAction) {
                    return <SystemAction item={item} key={i}></SystemAction>;
                }
            })}
        </div>
    );
};

const getRouteLink = (item, i) => {
    return (
        <p key={i} style={{ marginBottom: '0px' }}>
            <a
                href={item.target}
                target={item.openNewTab ? '_blank' : '_self'}
                rel="noreferrer"
            >
                {item.title}
            </a>
        </p>
    );
};

function NavBottomElement({ navElementData, childElemets }) {
    const [hovered, setHovered] = useState(false);
    const toggleHover = () => setHovered(!hovered);

    const [childContent, setChildContent] = useState([]);

    useEffect(() => {
        const content = getConent(childElemets);
        setChildContent(content);
    }, [childElemets]);

    const createElement = navElementData => {
        if (navElementData.targetType == NavigationTargetTypes.SystemAction) {
            return <SystemAction item={navElementData} />;
        } else {
            return (
                <Popover
                    placement="right"
                    title={
                        <span className="popover-title">
                            {navElementData.title}
                        </span>
                    }
                    content={childContent}
                    trigger="hover"
                >
                    <div onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
                        <Icon
                            name={navElementData.icon}
                            size="22"
                            type={hovered ? 'solid' : 'outline'}
                        />
                    </div>
                </Popover>
            );
        }
    };

    return (
        <div className="NSNavBottomElement">
            {navElementData && childElemets && createElement(navElementData)}

            <style jsx>
                {`
                    .NSNavBottomElement {
                        margin-bottom: 24px;
                        text-align: center;
                    }
                    .popover-title {
                        font-weight: bold;
                    }
                `}
            </style>
        </div>
    );
}

export default NavBottomElement;
