import Link from 'next/link';
import React, { useEffect, useState, useContext } from 'react';
import Icon from '../Icon';
import NavBottomElement from './NavBottomElement';
import NSNavSidebarTopMenu from './NavSidebarTopMenu';
import { Tooltip } from 'antd';
import UserContext from '../../context/UserContext';
import SystemAction from './systemActions/SystemAction';
import NavigationTargetTypes from '../../enum/NavigationTargetTypes';

function NavSidebarMenuWrapper({ navData, collapsed, setSelectedMenuItem }) {
    const [topNavData, setTopNavData] = useState([]);
    const [botNavData, setBotNavData] = useState([]);
    const [hovered, setHovered] = useState(false);
    const toggleHover = () => setHovered(!hovered);

    const { pathinfo } = useContext(UserContext);

    useEffect(() => {
        if (navData && navData.length > 0) {
            const bottomNav = navData
                .filter(
                    x =>
                        x.displayArea == 'bottom' &&
                        x.parentNavElementId == null
                )
                .sort((a, b) => a.sortKey - b.sortKey);
            setBotNavData(bottomNav);

            const topNav = navData
                .filter(
                    x => x.displayArea == 'top' && x.parentNavElementId == null
                )
                .sort((a, b) => a.sortKey - b.sortKey);
            setTopNavData(topNav);
        }
    }, [navData]);

    const getChildElemets = parentId => {
        return navData
            .filter(x => x.parentNavElementId == parentId)
            .sort((a, b) => a.sortKey - b.sortKey);
    };

    return (
        <div className="NSNavSidebarMenu">
            {collapsed ? (
                <Link
                    href={`/home?application=home&organization=${pathinfo.organization}`}
                    as={`/portfolio/${pathinfo.organization}`}
                >
                    <Tooltip
                        placement="right"
                        onClick={() => setSelectedMenuItem([-1])}
                        title="Home"
                    >
                        <div
                            className="NSHomeIcon"
                            onMouseEnter={toggleHover}
                            onMouseLeave={toggleHover}
                        >
                            <Icon
                                name="home"
                                size="36"
                                type={hovered ? 'solid' : 'outline'}
                            />
                        </div>
                    </Tooltip>
                </Link>
            ) : (
                <div className="NSNavLogo"></div>
            )}
            <div className="NSNavTop">
                {collapsed && <NSNavSidebarTopMenu topNavData={topNavData} />}
            </div>
            <div className="NSNavBottom">
                <hr className="Devider" />
                {botNavData &&
                    botNavData.map((navElementData, i) => {
                        if (
                            navElementData.targetType ==
                            NavigationTargetTypes.SystemAction
                        ) {
                            return (
                                <div className="NSNavBottomElement" key={i}>
                                    <SystemAction item={navElementData} />
                                </div>
                            );
                        } else {
                            return (
                                <NavBottomElement
                                    navElementData={navElementData}
                                    childElemets={getChildElemets(
                                        navElementData.id
                                    )}
                                    key={i}
                                />
                            );
                        }
                    })}
            </div>

            <style jsx>
                {`
                    .NSNavBottomElement {
                        margin-bottom: 24px;
                        text-align: center;
                    }
                    .NSNavSidebarMenu {
                        width: 100%;
                        height: 100%;
                        display: flex;
                        flex-direction: column;
                    }
                    .NSHomeIcon {
                        cursor: pointer;
                        margin: 52px 0px 15px;
                    }
                    .NSNavLogo {
                        margin: 52px 0px 15px;
                        height: 36px;
                        width: 36px;
                        background-image: url(/appfiles/img/ns-logo-notext.png);
                        background-size: cover;
                        align-self: center;
                    }
                    .NSNavTop {
                        display: flex;
                        align-self: self-start;
                        flex: 1;
                        width: 100%;
                        justify-content: flex-start;
                        flex-direction: column;
                        align-content: start;
                    }
                    .NSNavBottom {
                        display: flex;
                        align-self: flex-end;
                        width: 100%;
                        justify-content: flex-start;
                        flex-direction: column;
                        align-content: start;
                        padding-bottom: 48px;
                    }
                    .Devider {
                        width: 56px;
                        margin-bottom: 13px;
                    }
                `}
            </style>
        </div>
    );
}

export default NavSidebarMenuWrapper;
