import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Tooltip } from 'antd';
import Icon from './../Icon';
import Link from 'next/link';
import UserContext from '../../context/UserContext';
import { getContentLink, processUrl } from '../../services/navigationService';
import NavigationTargetTypes from '../../enum/NavigationTargetTypes';

const NavTopElement = ({ navElementData }) => {
    const { pathinfo } = useContext(UserContext);
    const [hovered, setHovered] = useState(false);
    const [linkdata, setLinkdata] = useState(null);

    const toggleHover = () => setHovered(!hovered);

    useEffect(() => {
        if (
            !navElementData ||
            !navElementData.targetType ||
            navElementData.targetType == NavigationTargetTypes.SystemAction
        ) {
            return;
        }

        if (navElementData.targetType == NavigationTargetTypes.Route) {
            setLinkdata({
                href: processUrl(navElementData.target, pathinfo.organization),
                as: processUrl(navElementData.alias, pathinfo.organization),
            });
            return;
        }

        if (navElementData.targetType == NavigationTargetTypes.Content) {
            const navContentLink = getContentLink(
                navElementData,
                pathinfo.organization
            );
            setLinkdata(navContentLink);
            return;
        }
    }, [navElementData]);

    return (
        <div className="NavTopElement">
            {navElementData && linkdata && (
                <Link href={linkdata.href} as={linkdata.as}>
                    <Tooltip placement="right" title={navElementData.title}>
                        <div
                            onMouseEnter={toggleHover}
                            onMouseLeave={toggleHover}
                            className="NavIconWrapper"
                        >
                            <Icon
                                name={navElementData.icon}
                                size="25"
                                type={hovered ? 'solid' : 'outline'}
                            />
                        </div>
                    </Tooltip>
                </Link>
            )}
            <style jsx>
                {`
                    .NavTopElement {
                        width: 100%;
                        justify-content: center;
                        display: flex;
                        margin: 15px 0 18px 0;
                    }
                    .NavIconWrapper {
                        cursor: pointer;
                    }
                `}
            </style>
        </div>
    );
};

export default NavTopElement;
