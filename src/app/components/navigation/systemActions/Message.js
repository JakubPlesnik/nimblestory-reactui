import React, { useEffect, useState } from 'react';
import { Badge } from 'antd';
import { fetchSystemMessage } from '../../../services/api/apiService';
import useLocalStorage from '../../../hooks/UseLocalStorage.js';
import { NotificationOutlined, NotificationFilled } from '@ant-design/icons';
import MessageDrawer from '../../../components/messages/MessageSidebar';
const Message = ({ hovered, toggleHover }) => {
    const [lastSeenMessage] = useLocalStorage('message', null);
    const [isDrawerOpen, setOpen] = useState(false);

    const [showBadge, setShowBadge] = useState(0);
    const [messages, setMessages] = useState(null);

    const showModal = () => {
        setOpen(true);
    };

    useEffect(() => {
        fetchSystemMessage().then(data => {
            if (data && data.messages) {
                setMessages(data.messages);
            }
        });
    }, []);

    useEffect(() => {
        if (messages) {
            let readedMessages = JSON.parse(lastSeenMessage);

            if (!Array.isArray(readedMessages)) {
                readedMessages = [];
                readedMessages.push(readedMessages);
            }
            if (messages.length > readedMessages.length) {
                setShowBadge(1);
                toggleHover();
            }
        }
    }, [messages]);

    return (
        <div>
            <Badge count={showBadge} dot onClick={() => showModal()}>
                {' '}
                <div onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
                    {hovered && (
                        <NotificationFilled
                            style={{ fontSize: '22px', color: 'white' }}
                        />
                    )}
                    {!hovered && (
                        <NotificationOutlined
                            style={{ fontSize: '22px', color: 'white' }}
                        />
                    )}
                </div>
            </Badge>
            {messages && (
                <MessageDrawer
                    isOpen={isDrawerOpen}
                    messagesList={{ messages }}
                    setOpen={setOpen}
                    showBadge={showBadge}
                    setShowBadge={setShowBadge}
                    toggleHover={toggleHover}
                ></MessageDrawer>
            )}
        </div>
    );
};

export default Message;
