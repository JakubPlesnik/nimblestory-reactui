import React, { useState } from 'react';
import Message from './Message';

const MessageMenuItem = ({ navElementData }) => {
    const [hovered, setHovered] = useState(false);
    const toggleHover = () => setHovered(!hovered);

    return (
        <Message
            item={navElementData}
            hovered={hovered}
            toggleHover={toggleHover}
        ></Message>
    );
};

export default MessageMenuItem;
