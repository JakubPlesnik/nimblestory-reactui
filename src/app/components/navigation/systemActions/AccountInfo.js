import React, { useState, useEffect, useContext } from 'react';
import { Popover } from 'antd';
import Icon from '../../Icon';
import Logout from './Logout';
import UserContext from '../../../context/UserContext';

const getContent = (user, org, env, ver) => {
    return (
        <div>
            <div>
                <a href="/changepassword">Change Password</a>
            </div>
            <div>
                <Logout text={'Logout'} />
            </div>
            <div className="appInfo">
                <div data-cy="user-name">User: {user}</div>
                <div data-cy="user-organization">Org: {org}</div>
                <div data-cy="user-environment">Env: {env}</div>
                <div data-cy="user-version">Ver: {ver}</div>
            </div>
            <style jsx>
                {`
                    .appInfo {
                        margin-top: 15px;
                        color: gray;
                        font-style: italic;
                    }
                `}
            </style>
        </div>
    );
};

const AccountInfo = ({ navElementData }) => {
    const [hovered, setHovered] = useState(false);
    const toggleHover = () => setHovered(!hovered);
    const { appState } = useContext(UserContext);
    const [childContent, setChildContent] = useState([]);

    useEffect(() => {
        if (appState.loginstate.user) {
            const content = getContent(
                appState.loginstate.user.fullname,
                appState.pathinfo.organization,
                appState.appenv,
                process.env.NEXT_PUBLIC_APP_VERSION
            );
            setChildContent(content);
        }
    }, [appState]);

    return (
        <Popover
            placement="right"
            title={
                <span className="popover-title">{navElementData.title}</span>
            }
            content={childContent}
            trigger="hover"
            overlayStyle={{
                width: '260px',
            }}
        >
            <div onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
                <Icon
                    name={navElementData.icon}
                    size="22"
                    type={hovered ? 'solid' : 'outline'}
                />
            </div>
        </Popover>
    );
};

export default AccountInfo;
