import React from 'react';

const ReportError = ({ text }) => {
    return (
        <a
            href="#"
            onClick={() => {
                // eslint-disable-next-line no-undef
                Usersnap.open();
            }}
        >
            {text}
        </a>
    );
};

export default ReportError;
