import React from 'react';
import Logout from './Logout';
import AccountInfo from './AccountInfo';
import MessageMenuItem from './MessageMenuItem';
import ChangeOrganization from './ChangeOrganization';
import ReportError from './ReportError';
import NavigationSystemActions from '../../../enum/NavigationSystemActions';

const renderAction = item => {
    switch (item.target) {
        case NavigationSystemActions.Logout:
            return <Logout text={item.title} />;
        case NavigationSystemActions.Messages:
            return <MessageMenuItem />;
        case NavigationSystemActions.Changeorg:
            return <ChangeOrganization navElementData={item} />;
        case NavigationSystemActions.Reporterror:
            return <ReportError text={item.title} />;
        case NavigationSystemActions.AccountInfo:
            return <AccountInfo navElementData={item} />;
        default:
            return 'undefined action';
    }
};

function SystemAction({ item }) {
    return <div>{renderAction(item)}</div>;
}

export default SystemAction;
