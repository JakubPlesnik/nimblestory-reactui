import React from 'react';
import { logout } from '../../../services/auth/authService';
import LoginContext from '../../../context/LoginContext';
import { useContext } from 'react';

const Logout = ({ text }) => {
    const { resetDataOnLogout } = useContext(LoginContext);
    return (
        <a
            href="#"
            onClick={() => logout(resetDataOnLogout)}
            data-cy="user-logout"
        >
            {text}
        </a>
    );
};

export default Logout;
