import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import { Popover } from 'antd';
import Icon from '../../Icon';
import LoginContext from '../../../context/LoginContext';
import { CheckCircleOutlined } from '@ant-design/icons';
import UserContext from '../../../context/UserContext';
import ButtonSetDefaultOrg from '../setDefaultOrg/ButtonSetDefaultOrg';
import { textTruncate } from '../../../services/textService';

const changeorg = (e, slug) => {
    e.preventDefault();
    Router.push(
        `/home?application=home&organization=${slug}`,
        `/portfolio/${slug}/`
    );
};

const getContent = (
    organizations,
    org,
    defaultOrganization,
    setDefaultOrganization
) => {
    return (
        <div>
            {organizations.map((organization, i) => {
                return (
                    <div key={i}>
                        <a
                            className={organization.slug == org ? '' : 'black'}
                            href="#"
                            onClick={e => changeorg(e, organization.slug)}
                            data-cy="change-organization"
                        >
                            {textTruncate(organization.title, 30)}
                        </a>
                        {organization.slug === defaultOrganization && (
                            <CheckCircleOutlined
                                style={{
                                    color: 'green',
                                    position: 'relative',
                                    marginLeft: '5px',
                                    top: '-2px',
                                }}
                            />
                        )}

                        {defaultOrganization != org &&
                            organization.slug == org && (
                                <ButtonSetDefaultOrg
                                    organizationSlug={org}
                                    setDefaultOrg={setDefaultOrganization}
                                />
                            )}
                    </div>
                );
            })}

            <style jsx>
                {`
                    .black {
                        color: black;
                    }
                    .btn-set-default {
                        color: #40a9ff;
                        background: #fff;
                        border-color: #40a9ff;
                    }
                `}
            </style>
        </div>
    );
};

const ChangeOrganization = ({ navElementData }) => {
    const [hovered, setHovered] = useState(false);
    const toggleHover = () => setHovered(!hovered);
    const {
        organizations,
        defaultOrganization,
        setDefaultOrganization,
    } = useContext(LoginContext);
    const { pathinfo } = useContext(UserContext);
    const [childContent, setChildContent] = useState([]);

    useEffect(() => {
        if (organizations) {
            const content = getContent(
                organizations,
                pathinfo.organization,
                defaultOrganization,
                setDefaultOrganization
            );
            setChildContent(content);
        }
    }, [organizations, pathinfo]);

    useEffect(() => {
        if (defaultOrganization) {
            const content = getContent(
                organizations,
                pathinfo.organization,
                defaultOrganization,
                setDefaultOrganization
            );
            setChildContent(content);
        }
    }, [defaultOrganization]);

    return (
        <Popover
            placement="right"
            title={
                <span className="popover-title">{navElementData.title}</span>
            }
            content={childContent}
            trigger="hover"
            overlayStyle={{
                width: '366px',
            }}
        >
            <div onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
                <Icon
                    name={navElementData.icon}
                    size="22"
                    type={hovered ? 'solid' : 'outline'}
                />
            </div>
        </Popover>
    );
};

export default ChangeOrganization;
