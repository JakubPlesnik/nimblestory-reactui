import React, { useState } from 'react';
import { Button } from 'antd';
import { saveDefault } from '../../../services/api/apiService';
import { CheckCircleOutlined } from '@ant-design/icons';

const ButtonSetDefaultOrg = ({ organizationSlug, setDefaultOrg }) => {
    const [loading, setLoading] = useState(false);
    const [buttonText, setButtonText] = useState('Set as default');
    const [showSuccessBtn, setShowSuccessBtn] = useState(false);

    const defaultSuccess = org => {
        setDefaultOrg(org);
        setButtonText('Set as default');
        setShowSuccessBtn(false);
    };

    const saveDefaultOrg = orgSlug => {
        setLoading(true);
        saveDefault(orgSlug).then(response => {
            if (response.success) {
                setButtonText('Default set');
                setShowSuccessBtn(true);
                setLoading(false);

                const timer = setTimeout(() => {
                    defaultSuccess(response.org);
                }, 2000);
                return () => clearTimeout(timer);
            } else {
                setLoading(false);
            }
        });
    };
    return (
        <Button
            type="primary"
            ghost
            size="small"
            onClick={() => saveDefaultOrg(organizationSlug)}
            loading={loading}
            className={showSuccessBtn ? 'success-btn' : ''}
            icon={showSuccessBtn ? <CheckCircleOutlined /> : ''}
            style={{
                position: 'absolute',
                right: '10px',
            }}
            data-cy="set-organization"
        >
            {buttonText}
        </Button>
    );
};

export default ButtonSetDefaultOrg;
