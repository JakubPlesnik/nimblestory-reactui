import { Menu } from 'antd';
import Link from 'next/link';
import React, { useEffect, useState, useContext } from 'react';
import Icon from './../Icon';
import UserContext from '../../context/UserContext';
import {
    getSystemActionLink,
    getContentLink,
    processUrl,
} from '../../services/navigationService';
import NavigationTargetTypes from '../../enum/NavigationTargetTypes';
import NavigationSystemActions from '../../enum/NavigationSystemActions';

const { SubMenu } = Menu;

function MainMenu({
    openKeys,
    setOpenKeys,
    navData,
    selectedNavigationItem,
    setSelectedNavigationItem,
}) {
    const rootSubmenuKeys = ['collections', 'projects'];
    const [menuData, setMenuData] = useState(false);
    const { pathinfo } = useContext(UserContext);

    useEffect(() => {
        if (navData && navData.length > 0 && Array.isArray(navData)) {
            const menuNav = navData
                .filter(x => x.displayArea == 'menu')
                .sort((a, b) => a.sortKey - b.sortKey);
            setMenuData(menuNav);
        }
    }, [navData]);

    const onOpenChange = keys => {
        const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
        if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            setOpenKeys(keys);
        } else {
            setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    };

    const onClick = e => {
        setOpenKeys([e.key]);
    };

    const onSelect = e => {
        setSelectedNavigationItem([e.key]);
    };

    const renderRoute = item => {
        return (
            <Menu.Item
                key={item.id}
                icon={
                    <span className="anticon ant-menu-item-icon">
                        <Icon
                            name={item.icon}
                            size="20"
                            color="#000"
                            type="outline"
                        />
                    </span>
                }
            >
                <Link
                    href={processUrl(item.target, pathinfo.organization)}
                    as={processUrl(item.alias, pathinfo.organization)}
                >
                    {item.title}
                </Link>
            </Menu.Item>
        );
    };

    const renderSubmenu = item => {
        return (
            <SubMenu
                key={item.id}
                icon={
                    <span className="anticon ant-menu-item-icon">
                        <Icon
                            name={item.icon}
                            size="20"
                            color="#000"
                            type={
                                openKeys.indexOf(item.id) !== -1
                                    ? 'solid'
                                    : 'outline'
                            }
                        />
                    </span>
                }
                title={item.title}
            >
                {menuData
                    .filter(x => x.parentNavElementId == item.id)
                    .map(subitem => {
                        return renderItem(subitem);
                    })}
            </SubMenu>
        );
    };

    const renderContent = item => {
        const linkData = getContentLink(item, pathinfo.organization);

        return (
            <Menu.Item
                key={item.id}
                icon={
                    <span className="anticon ant-menu-item-icon">
                        <Icon
                            name={item.icon}
                            size="20"
                            color="#000"
                            type="outline"
                        />
                    </span>
                }
            >
                {linkData && linkData.href && linkData.as && (
                    <Link href={linkData.href} as={linkData.as}>
                        {item.title}
                    </Link>
                )}
            </Menu.Item>
        );
    };

    const renderSystemAction = item => {
        return (
            <SubMenu
                key={item.id}
                icon={
                    <span className="anticon ant-menu-item-icon">
                        <Icon
                            name={item.icon}
                            size="20"
                            color="#000"
                            type={
                                openKeys.indexOf(item.id) !== -1
                                    ? 'solid'
                                    : 'outline'
                            }
                        />
                    </span>
                }
                title={item.title}
            >
                {item.target == NavigationSystemActions.ListProjects && (
                    <Menu.Item key={`projHome`}>
                        <Link
                            href={`/projects?application=portfolio&organization=${pathinfo.organization}`}
                            as={`/portfolio/${pathinfo.organization}/all`}
                        >
                            {'Projects Home'}
                        </Link>
                    </Menu.Item>
                )}

                {item.target == NavigationSystemActions.ListCollections && (
                    <Menu.Item key={`collHome`}>
                        <Link
                            href={`/collections?application=collection&organization=${pathinfo.organization}`}
                            as={`/portfolio/${pathinfo.organization}/collections`}
                        >
                            {'Collections Home'}
                        </Link>
                    </Menu.Item>
                )}

                {item.subitems.map(subitem => {
                    const linkData = getSystemActionLink(
                        subitem.slug,
                        item.target,
                        pathinfo.organization
                    );

                    return (
                        <Menu.Item key={`proj${subitem.id}`}>
                            <Link href={linkData.href} as={linkData.as}>
                                {subitem.title}
                            </Link>
                        </Menu.Item>
                    );
                })}
            </SubMenu>
        );
    };

    const renderItem = item => {
        switch (item.targetType) {
            case NavigationTargetTypes.Route:
                return renderRoute(item);
            case NavigationTargetTypes.Content:
                return renderContent(item);
            case NavigationTargetTypes.SystemAction:
                return renderSystemAction(item);
            default:
                return renderSubmenu(item);
        }
    };

    return (
        <div>
            <Menu
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['5']}
                mode="inline"
                theme="light"
                openKeys={openKeys}
                onOpenChange={onOpenChange}
                onClick={onClick}
                onSelect={onSelect}
                selectedKeys={selectedNavigationItem}
            >
                <Menu.Item
                    key={-1}
                    icon={
                        <span className="anticon ant-menu-item-icon">
                            <Icon
                                name="home"
                                size="20"
                                color="#000"
                                type="outline"
                            />
                        </span>
                    }
                >
                    <Link
                        href={`/home?application=home&organization=${pathinfo.organization}`}
                        as={`/portfolio/${pathinfo.organization}`}
                    >
                        Home
                    </Link>
                </Menu.Item>
                {menuData &&
                    menuData
                        .filter(x => x.parentNavElementId === null)
                        .map(item => {
                            return renderItem(item);
                        })}
            </Menu>
            <style jsx>
                {`
                    .anticon::hover {
                        color: #40a9ff;
                    }
                `}
            </style>
        </div>
    );
}

export default MainMenu;
