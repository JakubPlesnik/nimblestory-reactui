const SidebarResizeControl = ({ onMouseDown }) => (
    <div className="resizecontrol" onMouseDown={onMouseDown}>
        <style jsx>
            {`
                .resizecontrol {
                    position: absolute;
                    left: -6px;
                    width: 12px;
                    height: 100%;
                    right: 0;
                    z-index: 100000;
                    cursor: ew-resize;
                }
            `}
        </style>
    </div>
);
export default SidebarResizeControl;
