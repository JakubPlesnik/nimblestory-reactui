import Router from 'next/router';
import { Component, Fragment, useState, useEffect, useContext } from 'react';
import {
    DropdownMenuStateless,
    DropdownItemGroup,
    DropdownItem,
} from '@atlaskit/dropdown-menu';
import {
    GlobalNav,
    GlobalNavigationSkeleton,
    SkeletonContainerView,
    GlobalItem,
    LayoutManager,
    NavigationProvider,
    ThemeProvider,
    modeGenerator,
} from '@atlaskit/navigation-next';
import UserContext from '../context/UserContext';
import LoginContext from '../context/LoginContext';
import { GlobalIcons } from './SidebarIcons';
import ContainerNavigation from './SidebarContainerNavigation';
import ModalOrganizations from './SidebarModalOrganizations';

const defaultThemeMode = modeGenerator({
    product: {
        text: '#cecece',
        background: '#cecece',
    },
});

const HelpDropdown = () => (
    <Fragment>
        <DropdownItemGroup title="HELP OPTIONS">
            <DropdownItem
                target="_blank"
                href="https://www.nimblestory.com/support-faqs"
            >
                FAQs
            </DropdownItem>
            <DropdownItem
                target="_blank"
                href="https://www.nimblestory.com/support"
            >
                Request Help
            </DropdownItem>
            <DropdownItem
                target="_blank"
                href="https://www.nimblestory.com/support-accounts"
            >
                Invite Additional Users
            </DropdownItem>
            <DropdownItem
                onClick={() => {
                    // eslint-disable-next-line no-undef
                    Usersnap.open();
                }}
            >
                Report Error
            </DropdownItem>
        </DropdownItemGroup>
    </Fragment>
);

const UserDropdown = () => {
    const { user, setIsModalOpen } = useContext(LoginContext);
    const { pathinfo, appbuildid } = useContext(UserContext);

    const diplayManagerMenu =
        user &&
        (user.groups.includes('manager') || user.groups.includes('admin'));

    return (
        <Fragment>
            <DropdownItemGroup title={user.fullname || user.username}>
                {diplayManagerMenu && (
                    <DropdownItem onClick={() => setIsModalOpen(true)}>
                        Organize Home Page
                    </DropdownItem>
                )}

                {diplayManagerMenu && (
                    <DropdownItem
                        onClick={() => {
                            Router.push(
                                `/manager?application=portfolio&organization=${pathinfo.organization}`,
                                `/portfolio/${pathinfo.organization}/manage`
                            );
                        }}
                    >
                        Manage Content
                    </DropdownItem>
                )}

                <DropdownItem>
                    <a href="/passwordchange">Change Password</a>
                </DropdownItem>

                {/* <DropdownItem href="/logout">Log out</DropdownItem> */}

                <DropdownItem
                    onClick={() => {
                        localStorage.removeItem('token');
                        localStorage.removeItem('selectedItem');
                        Router.replace('/logout');
                    }}
                >
                    Log out
                </DropdownItem>
            </DropdownItemGroup>
            <DropdownItemGroup title={`NimbleStory ${appbuildid}`} />
        </Fragment>
    );
};

function getGNavPrimaryItems({ isCollapsed, pathinfo, organization }) {
    const iconOrgLogo = {
        ...GlobalIcons.home,
        onClick: () => {
            Router.push(
                `/home?application=home&organization=${pathinfo.organization}`,
                `/portfolio/${pathinfo.organization}/`
            );
        },
    };
    const iconPortfolio = {
        ...GlobalIcons.portfolio,
        onClick: () => {
            Router.push(
                `/projects?application=portfolio&organization=${pathinfo.organization}`,
                `/portfolio/${pathinfo.organization}/all`
            );
        },
    };

    let icons = [];

    if (!isCollapsed) {
        icons = [iconOrgLogo];
        return icons;
    }
    icons = [iconOrgLogo, iconPortfolio];

    /* to-do org check */
    const { geoexplorerAccess, conceptexplorerAccess } = organization;
    if (geoexplorerAccess) {
        const iconItem = {
            ...GlobalIcons.geoexplorer,
            onClick: () => {
                Router.push(
                    `/geoexplorersolutions?application=geoexplorer&organization=${pathinfo.organization}`,
                    `/geoexplorer/${pathinfo.organization}`
                );
            },
        };
        icons = [...icons, iconItem];
    }

    if (conceptexplorerAccess) {
        const iconItem = {
            ...GlobalIcons.conceptexplorer,
            onClick: () => {
                Router.push(
                    `/conceptsolutions?application=conceptexplorer&organization=${pathinfo.organization}`,
                    `/conceptexplorer/${pathinfo.organization}`
                );
            },
        };
        icons = [...icons, iconItem];
    }

    return icons;
}

const globalNavSecondaryItems = ({ organizations, setOrgModalOpen }) => {
    let menu = [];
    if (organizations.length > 0) {
        menu = [
            ...menu,
            {
                ...GlobalIcons.help,
                dropdownItems: HelpDropdown,
                // onClick: () => Usersnap.open(),
            },
            {
                ...GlobalIcons.organizations,
                onClick: () => setOrgModalOpen(true),
            },
        ];
    }
    menu = [
        ...menu,
        {
            ...GlobalIcons.profile,
            dropdownItems: UserDropdown,
        },
    ];
    return menu;
};

class GlobalItemWithDropdown extends Component {
    state = {
        isOpen: false,
    };

    handleOpenChange = ({ isOpen }) => this.setState({ isOpen });

    render() {
        const { items, trigger: Trigger } = this.props;
        const { isOpen } = this.state;
        return (
            <DropdownMenuStateless
                boundariesElement="window"
                isOpen={isOpen}
                onOpenChange={this.handleOpenChange}
                position="top left"
                autoPosition
                trigger={<Trigger isOpen={isOpen} />}
            >
                {items}
            </DropdownMenuStateless>
        );
    }
}
const GlobalItemComponent = ({
    dropdownItems: DropdownItems,
    ...itemProps
}) => {
    if (DropdownItems) {
        return (
            <GlobalItemWithDropdown
                trigger={({ isOpen }) => (
                    <GlobalItem isSelected={isOpen} {...itemProps} />
                )}
                items={<DropdownItems />}
            />
        );
    }
    return <GlobalItem {...itemProps} />;
};

const GlobalNavigation = ({ collapsed }) => {
    const { organization, organizations } = useContext(LoginContext);
    const { pathinfo } = useContext(UserContext);
    const [isOrgModalOpen, setOrgModalOpen] = useState(false);

    return (
        <div data-webdriver-test-key="global-navigation">
            <GlobalNav
                itemComponent={GlobalItemComponent}
                primaryItems={
                    collapsed
                        ? getGNavPrimaryItems({
                              isCollapsed: true,
                              pathinfo,
                              organization,
                          })
                        : getGNavPrimaryItems({ isCollapsed: false, pathinfo })
                }
                secondaryItems={globalNavSecondaryItems({
                    organizations,
                    setOrgModalOpen,
                })}
            />
            <ModalOrganizations
                isOpen={isOrgModalOpen}
                setOpen={setOrgModalOpen}
                organizations={organizations}
                onChangeOrg={idx => {
                    Router.push(
                        `/home?application=home&organization=${organizations[idx].slug}`,
                        `/portfolio/${organizations[idx].slug}/`
                    );
                }}
            />
        </div>
    );
};

const GlobalNavigationCollapsed = () => <GlobalNavigation collapsed />;

const skeletonContainerView = () => <SkeletonContainerView type="container" />;

const SidebarLayout = ({ children }) => {
    const [themeMode, setThemeMode] = useState(defaultThemeMode);
    const [shouldRenderSkeleton, setShouldRenderSkeleton] = useState(true);
    const [isCollapsed, setCollapsed] = useState(true);
    const { isLoggedIn, organization } = useContext(LoginContext);

    /*
    useEffect(()=> {
      if (isLoggedIn) {
        setShouldRenderSkeleton(false)
      }
    }, [isLoggedIn]) */

    useEffect(() => {
        if (organization) {
            setShouldRenderSkeleton(false);
            if (organization.themeSettings) {
                const {
                    color1Darkest,
                    color3Light,
                } = organization.themeSettings;
                const customThemeMode = modeGenerator({
                    product: {
                        text: color3Light || '#a34c00',
                        background: color1Darkest || '#c8ff00',
                    },
                });
                setThemeMode(customThemeMode);
            }
        }
    }, [isLoggedIn, organization]);

    return (
        <NavigationProvider
            initialUIController={{ isCollapsed: true, productNavWidth: 248 }}
        >
            <ThemeProvider
                theme={theme => ({
                    ...theme,
                    mode: themeMode,
                })}
            >
                <LayoutManager
                    collapseToggleTooltipContent={false}
                    shouldHideGlobalNavShadow
                    globalNavigation={
                        shouldRenderSkeleton
                            ? GlobalNavigationSkeleton
                            : isCollapsed
                            ? GlobalNavigationCollapsed
                            : GlobalNavigation
                    }
                    productNavigation={() => null}
                    containerNavigation={
                        shouldRenderSkeleton
                            ? skeletonContainerView
                            : ContainerNavigation
                    }
                    onCollapseEnd={() => setCollapsed(true)}
                    onExpandStart={() => setCollapsed(false)}
                >
                    {children}
                </LayoutManager>
            </ThemeProvider>
        </NavigationProvider>
    );
};

export default SidebarLayout;
