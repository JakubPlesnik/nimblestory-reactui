import React from 'react';
import mapboxgl from 'mapbox-gl';
import GeoExplorerPopupRouter from './GeoExplorerPopupRouter';
import {
    kPropPlantId,
    kMapFocusMetricKey,
} from '../services/geoexplorerconsts';

const kMapLayerMain = 'kMapLayerMain';
const kMapLayerCluster = 'kMapLayerCluster';

class GeoExplorerMap extends React.Component {
    popupContainer;

    locationsLoaded = false;

    static async getInitialProps({ query }) {
        return query;
    }

    constructor(props) {
        super(props);
        this.state = {
            lng: props.settings['Map Position Lng'],
            lat: props.settings['Map Position Lat'],
            zoom: props.settings.Zoom,
            locationProps: {},
        };
        this.onResize = this.onResize.bind(this); // bind function once
        this.resizeTimer = null;
        this.popupref = React.createRef();
        this.mapSize = false;
        this.mapready = false;
        this.locationsDisplayed = false;
    }

    loadMapbox() {
        const _this = this;

        this.popupContainer = document.createElement('div');

        mapboxgl.accessToken = this.props.settings['Access Token Override'];

        const map = new mapboxgl.Map({
            container: this.mapContainer,
            style: this.props.settings['Mapbox style'],
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom,
            trackResize: true,
            attributionControl: false,
        });
        map.addControl(
            new mapboxgl.AttributionControl({
                customAttribution: [this.props.settings['Attribution Text']],
                compact: true,
            })
        );

        this.map = map;

        // save map size for resize tracking
        const { width, height } = this.mapContainer.getBoundingClientRect();
        this.mapsize = { width, height };

        map.on('move', () => {
            this.setState({
                lng: map.getCenter().lng.toFixed(4),
                lat: map.getCenter().lat.toFixed(4),
                zoom: map.getZoom().toFixed(2),
            });
        });

        map.addControl(new mapboxgl.NavigationControl({ showCompass: false }));
        map.addControl(new mapboxgl.FullscreenControl());

        map.on('style.load', () => {
            if (_this.props.onMapDisplayed) {
                _this.props.onMapDisplayed();
            }
            _this.mapready = true;
        });

        // Resize Event Handler
        // //window.addEventListener('resize', this.onResize, false)

        map.on('click', function(e) {
            const features = map.queryRenderedFeatures(e.point, {
                layers: [kMapLayerMain],
            });
            if (!features.length) {
                return;
            }
            const feature = features[0];
            if (feature.properties.cluster) {
                return;
            }

            const plantTotals =
                _this.props.dataPlantTotals[
                    `plant_${feature.properties[kPropPlantId]}`
                ];

            if (!plantTotals) {
                return;
            }
            const allproperties = { ...feature.properties, ...plantTotals };
            _this.setState(() => ({
                locationProps: allproperties,
            }));

            if (!_this.popupref.current) {
                return;
            }
            const popup = new mapboxgl.Popup()
                .setLngLat(map.unproject(e.point))
                .setDOMContent(_this.popupref.current)
                .setMaxWidth(400)
                .addTo(map);

            this.selectedPopup = popup;

            popup.on('close', function() {
                if (popup === this.selectedPopup) {
                    _this.props.onLocationSelected(false);
                }
            });

            if (_this.props.onLocationSelected) {
                _this.props.onLocationSelected(feature.properties);
            }
        });
    }

    initData() {
        if (!this.props.dataPlantLocations) {
            return;
        }
        this.locationsLoaded = true;
        this.updateDataSource();
    }

    updateDataSource() {
        if (!this.locationsLoaded || !this.mapready) {
            return;
        }

        if (this.map.getLayer(kMapLayerMain))
            this.map.removeLayer(kMapLayerMain);
        if (this.map.getLayer(kMapLayerCluster))
            this.map.removeLayer(kMapLayerCluster);
        if (this.map.getSource('source1')) this.map.removeSource('source1');

        this.map.addSource('source1', {
            type: 'geojson',
            data: this.props.dataPlantLocations, // '/plant_locations.geojson', //this.props.datasources[0].geojsonUrl,
            buffer: 0,
            maxzoom: 14,
            cluster: true,
            clusterMaxZoom: 14, // Max zoom to cluster points on
            clusterRadius: 35, // Radius of each cluster when clustering points (defaults to 50)
        });

        this.updateMapPaint();

        if (!this.locationsDisplayed) {
            this.locationsDisplayed = true;
            if (this.props.onLocationsDisplayed) {
                this.props.onLocationsDisplayed();
            }
        }
    }

    updateMapPaint() {
        if (this.map.getLayer(kMapLayerMain))
            this.map.removeLayer(kMapLayerMain);
        if (this.map.getLayer(kMapLayerCluster))
            this.map.removeLayer(kMapLayerCluster);

        if (!this.locationsLoaded || !this.mapready) {
            return;
        }

        this.map.addLayer({
            id: kMapLayerMain,
            type: 'circle',
            source: 'source1',
            paint: {
                'circle-color': [
                    'case',
                    [
                        '>=',
                        [
                            'get',
                            this.props.geosolutionsfilters[kMapFocusMetricKey],
                        ],
                        90,
                    ],
                    '#1CBE02', // 43a047
                    [
                        '>=',
                        [
                            'get',
                            this.props.geosolutionsfilters[kMapFocusMetricKey],
                        ],
                        80,
                    ],
                    '#fb8c00',
                    [
                        '>=',
                        [
                            'get',
                            this.props.geosolutionsfilters[kMapFocusMetricKey],
                        ],
                        70,
                    ],
                    '#F9A502', // fdd835
                    '#DC0808', // d32f2f
                ],
                // ,//'#293E57', //'Valuation (Existence)',
                'circle-radius': 16,
                'circle-opacity': 0.7,
                'circle-blur': 0,
            },
        });

        this.map.addLayer({
            id: kMapLayerCluster,
            type: 'symbol',
            source: 'source1',
            filter: ['has', 'point_count'],
            layout: {
                'text-field': '{point_count_abbreviated}',
                'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                'text-size': 12,
            },
            paint: {
                'text-color': '#F8FBFF',
            },
        });
    }

    onResize() {
        clearTimeout(this.resizeTimer);
        this.resizeTimer = setTimeout(() => {
            this.map.resize();
        }, 100);
    }

    componentDidMount() {
        this.loadMapbox();
    }

    componentDidUpdate(prevProps) {
        if (this.mapready) {
            if (
                !this.locationsDisplayed ||
                this.props.dataPlantLocations !== prevProps.dataPlantLocations
            ) {
                this.updateDataSource();
            }

            if (typeof window !== undefined) {
                const _this = this;
                window.setTimeout(
                    () => _this.checkBoundsSizeAfterAnimation(),
                    100
                );
            }

            if (
                this.props.geosolutionsfilters[kMapFocusMetricKey] !==
                prevProps.geosolutionsfilters[kMapFocusMetricKey]
            ) {
                this.updateMapPaint();
            }
        }
        if (!this.locationsLoaded) {
            this.initData();
        }
    }

    checkBoundsSizeAfterAnimation() {
        if (this.mapContainer) {
            const { width, height } = this.mapContainer.getBoundingClientRect();
            if (this.mapsize.width != width || this.mapsize.height != height) {
                this.mapsize = { width, height };
                this.onResize();
            }
        }
    }

    render() {
        return (
            <React.Fragment>
                <div
                    ref={el => (this.mapContainer = el)}
                    className="mapContainer"
                    id="map"
                />
                <div style={{ visibility: 'hidden' }}>
                    <GeoExplorerPopupRouter
                        {...this.state.locationProps}
                        key="GeoExplorerPopupWrapper"
                        ref={this.popupref}
                    />
                </div>
                <style jsx>
                    {`
                        .sidebarStyle {
                            height: 100px;
                            display: block;
                            margin: 12px;
                            background-color: #404040;
                            color: #ffffff;
                            font-weight: bold;
                            z-index: 10;
                            position: absolute;
                        }
                        .mapContainer {
                            position: absolute;
                            top: 0;
                            right: 0;
                            left: 0;
                            bottom: 0;
                        }
                    `}
                </style>
            </React.Fragment>
        );
    }
}

GeoExplorerMap.defaultProps = {
    settings: {
        'Map Position Lng': -100.0744,
        'Map Position Lat': 37.8,
        Zoom: 4,
    },
};

export default GeoExplorerMap;
