import { memo, useState, useEffect } from 'react';
import { Draggable, DragDropContext, Droppable } from 'react-beautiful-dnd';

function Card({
    card,
    index,
    components,
    hoveringIdx,
    setHoveringIdx,
    onRemoveItem,
}) {
    const [isHovering, setHovering] = useState(false);
    const [isActive, setActive] = useState(false);

    useEffect(() => {
        setHovering(hoveringIdx.index === index);
    }, [hoveringIdx]);

    const eventHandlers = {
        onMouseEnter: e => setHoveringIdx({ index, update: e.timeStamp }),
        onMouseLeave: () => {
            setHovering(false);
            setActive(false);
        },
        onMouseUp: () => setActive(false),
    };
    const CardDisplayHelper = props => <components.Card {...props} />;
    return (
        <Draggable draggableId={card.id} index={index} shouldRespectForcePress>
            {(provided, snapshot) => (
                <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    {...eventHandlers}
                >
                    <CardDisplayHelper
                        isDragging={snapshot.isDragging}
                        isHovering={isHovering}
                        isActive={isActive}
                        item={card}
                        onRemoveItem={onRemoveItem}
                    />
                    <style jsx>
                        {`
                            div {
                                user-select: none;
                                outline: none;
                            }
                        `}
                    </style>
                </div>
            )}
        </Draggable>
    );
}

// Ensuring the whole list does not re-render when the droppable re-renders
const CardList = memo(({ cards, components, onRemoveItem }) => {
    const [hoveringIdx, setHoveringIdx] = useState({ updated: -1, index: -1 });

    return cards.map((card, index) => (
        <Card
            card={card}
            index={index}
            key={card.id}
            components={components}
            hoveringIdx={hoveringIdx}
            setHoveringIdx={setHoveringIdx}
            onRemoveItem={onRemoveItem}
        />
    ));
});

function CardApp({ pinned, onOrderChange, components }) {
    function onDragEnd(result) {
        const { source } = result;
        const { destination } = result;

        if (!destination || source.droppableId !== destination.droppableId) {
            return;
        }

        const newitems = [...pinned];
        const target = newitems.find(item => item.id === result.draggableId);

        if (!target) {
            return;
        }

        // Move the dropped item into the correct spot
        newitems.splice(source.index, 1);
        newitems.splice(destination.index, 0, target);

        onOrderChange(newitems);
    }

    function onRemoveItem(item) {
        const newitems = pinned.filter(i => i.id !== item.id);
        onOrderChange(newitems);
    }

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="list">
                {provided => (
                    <div ref={provided.innerRef} {...provided.droppableProps}>
                        <CardList
                            key={1}
                            cards={pinned}
                            components={components}
                            onRemoveItem={onRemoveItem}
                        />
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </DragDropContext>
    );
}
export default CardApp;
