import { useState } from 'react';
import { changePasswordRequest } from '../../services/api/apiService';

const ChangePasswordForm = () => {
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [message, setMessage] = useState('');
    const [redirectButtonVisible, setRedirectButtonVisible] = useState(false);

    const changePassword = e => {
        e.preventDefault();
        if (oldPassword != '' && newPassword != '') {
            changePasswordRequest(
                oldPassword,
                newPassword,
                onChangeSuccess,
                onChangeError
            );
            return;
        }

        setMessage('Please enter your current and new password');
    };

    const onChangeSuccess = () => {
        setMessage('Password changed successfully!');
        setRedirectButtonVisible(true);
        setNewPassword('');
        setOldPassword('');
        localStorage.removeItem('token');
    };

    const onChangeError = error => {
        if (!error) {
            setMessage('Server is not responding');
        } else if (error.status === 401) {
            if (error.data.message) setMessage(error.data.message);
            else setMessage(error.data.error.message);
        } else setMessage('Something went wrong.');
    };

    return (
        <form className="sign-in">
            <div className="form-group">
                <label htmlFor="oldPassword">Current Password</label>
                <input
                    type="password"
                    name="oldPassword"
                    placeholder="Current Password"
                    className="form-control"
                    value={oldPassword}
                    onChange={e => setOldPassword(e.target.value)}
                />
            </div>
            <div className="form-group">
                <label htmlFor="newPassword">New Password</label>
                <input
                    type="password"
                    name="newPassword"
                    placeholder="New Password"
                    className="form-control"
                    value={newPassword}
                    onChange={e => setNewPassword(e.target.value)}
                />
            </div>

            {message != '' && (
                <div style={{ marginBottom: '10px' }} className="message">
                    {message}
                </div>
            )}

            {!redirectButtonVisible && (
                <button
                    className="btn btn-primary"
                    onClick={e => changePassword(e)}
                >
                    Change password
                </button>
            )}

            {redirectButtonVisible && (
                <a className="btn btn-primary" href="/login">
                    Back to the login
                </a>
            )}
        </form>
    );
};

export default ChangePasswordForm;
