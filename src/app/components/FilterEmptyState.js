import Button from 'react-bootstrap/Button';

const EmptyState = ({ onClick }) => (
    <div className="emptystate">
        <h3>No results</h3>
        <p>Clear the filter options to see all items.</p>
        {onClick && (
            <Button variant="primary" onClick={onClick}>
                Clear Filter
            </Button>
        )}
        <style jsx>
            {`
                .emptystate {
                    text-align: center;
                }
            `}
        </style>
    </div>
);
export default EmptyState;
