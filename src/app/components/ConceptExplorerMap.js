import { Fragment, useState, useEffect } from 'react';
import L from 'leaflet';
import RasterCoords from '../services/vendor/leaflet-rastercoors';

let rc = null;
let markersLayer = null;

const tabs = {
    JOURNEYS: 1,
};

const customOptions = {
    maxWidth: '300',
    className: 'ce-popup',
    autoClose: false,
};

const hoverPopupOptions = {
    maxWidth: '300',
    className: 'ce-popup-hover',
    autoClose: false,
};

const ConceptExplorerMap = ({
    visual,
    waypoints,
    waypoint,
    setWaypoint,
    layoutState,
    onMapLoaded,
    waypointRef,
}) => {
    const [mapContainer, setMapContainer] = useState(null);
    const [map, setMap] = useState(null);
    const [markers, setMarkers] = useState([]);
    const [mapVersion, setMapVersion] = useState(1);
    const [popupClosed, setPopupClosed] = useState(0);

    const openCurrentMarker = () => {
        if (waypoint) {
            if (!markers.length) {
                return;
            }
            const prevOpen = markers.find(
                m => m.id !== waypoint.id && m.marker.getPopup().isOpen()
            );
            if (prevOpen) {
                prevOpen.marker.closePopup();
            }

            const wpMarked = markers.find(w => w.id === waypoint.id);
            if (wpMarked) {
                const { marker } = wpMarked;
                marker.bindPopup(waypoint.label, customOptions);
                marker.openPopup();
                if (layoutState.tab === tabs.JOURNEYS && waypoint.zoom) {
                    map.flyTo(marker.getLatLng(), waypoint.zoom);
                } else {
                    map.panTo(marker.getLatLng());
                }
            }
        }
    };

    function placeMarkers() {
        const mapIcon = L.icon({
            iconUrl: '/appfiles/icons/mappin.svg',

            iconSize: [24, 36], // size of the icon
            // shadowSize:   [50, 64], // size of the shadow
            iconAnchor: [12, 36], // point of the icon which will correspond to marker's location
            // shadowAnchor: [4, 62],  // the same for the shadow
            popupAnchor: [0, -37], // point from which the popup should open relative to the iconAnchor
        });

        if (!rc || !map) {
            return;
        }

        if (markersLayer) {
            map.removeLayer(markersLayer);
        }

        const markersArr = [];

        markersLayer = new L.FeatureGroup();
        const waypointssave = [...waypoints];
        waypointssave
            .filter(w => w.visualId === visual.id)
            .forEach(function(_waypoint) {
                const { positionX, positionY } = _waypoint;
                const { width, height } = visual.dimensions;
                const lng = (positionX / 100.0) * width;
                const lat = (positionY / 100.0) * height;
                const coords = rc.unproject([lng, lat]);

                let wpIcon;

                if (_waypoint.icon) {
                    wpIcon = L.divIcon({
                        html: _waypoint.icon,
                        iconSize: [32, 32],
                    });
                } else {
                    wpIcon = mapIcon;
                }

                const marker = L.marker({ ...coords }, { icon: wpIcon })
                    .bindPopup(_waypoint.label, customOptions)
                    .addTo(markersLayer)
                    .on('click', function() {
                        marker.bindPopup(_waypoint.label, customOptions);
                        setWaypoint(_waypoint);
                    })
                    .on(
                        'popupclose',
                        () => {
                            // track closed popup to hide sidebar
                            setPopupClosed(_waypoint.id);
                        },
                        this
                    );

                marker
                    .on('mouseover', function() {
                        if (waypointRef.current != _waypoint) {
                            marker.bindPopup(
                                _waypoint.label,
                                hoverPopupOptions
                            );
                            this.openPopup();
                        }
                    })
                    .on('mouseout', function() {
                        if (waypointRef.current != _waypoint) {
                            this.closePopup();
                        }
                    });

                markersArr.push({ ..._waypoint, marker });
            });

        markersLayer.addTo(map);
        setMarkers(markersArr);
    }

    function onMapLoad() {
        placeMarkers();
        setMapVersion(mapVersion + 1);
        if (onMapLoaded) onMapLoaded();
    }

    const baseurl = process.env.NEXT_PUBLIC_CE_VISUALS_URL;

    useEffect(() => {
        if (mapContainer) {
            L.RasterCoords = RasterCoords(L);
            const m = L.map(mapContainer, {
                attributionControl: false,
                zoomControl: false,
            });
            L.control.zoom({ position: 'bottomleft' }).addTo(m);
            setMap(m);
        }
    }, [mapContainer]);

    useEffect(() => {
        if (map) {
            const { width, height } = visual.dimensions;
            const img = [width, height];

            rc = new L.RasterCoords(map, img);

            map.setMinZoom(visual.options['minZoom']);
            map.setMaxZoom(visual.options['maxZoom']);
            map.on('load', onMapLoad);
            map.setView(
                rc.unproject([img[0], img[1]]),
                visual.options['initZoom']
            );

            const bounds = new L.LatLngBounds(
                map.unproject([1, img[1] - 1], visual.options['maxZoom']),
                map.unproject([img[0] - 1, 1], visual.options['maxZoom'])
            );

            L.tileLayer(`${baseurl}/${visual.slug}/{z}/{x}/{y}.png`, {
                noWrap: true,
                detectRetina: true,
                bounds: bounds,
            }).addTo(map);
            map.setMaxBounds(null);
        }
    }, [map]);

    useEffect(() => {
        if (map) {
            window.setTimeout(() => map.invalidateSize(true), 100);
        }
    }, [layoutState]);

    useEffect(() => {
        placeMarkers();
    }, [waypoints]);

    useEffect(() => {
        if (waypoint) {
            openCurrentMarker();
        }
    }, [waypoint]);

    useEffect(() => {
        if (waypoint) {
            openCurrentMarker();
        }
    }, [markers]);

    useEffect(() => {
        if (map && mapVersion > 1) {
            window.setTimeout(() => map.invalidateSize(true), 1000);
            openCurrentMarker();
        }
    }, [mapVersion]);

    useEffect(() => {
        if (popupClosed) {
            if (waypoint.id === popupClosed) {
                setWaypoint(false);
                setPopupClosed(0);
            }
        }
    }, [popupClosed]);

    return (
        <Fragment>
            <div
                ref={el => setMapContainer(el)}
                className="mapContainer"
                id="map"
                data-cy="concept-explorer-map"
            />

            <style jsx>
                {`
                    #map {
                        width: 100%;
                        height: 100%;
                        z-index: 10;
                        position: absolute;
                    }
                    .leaflet-container {
                        background: ${visual.backgroundColor || 'yellow'};
                    }
                `}
            </style>
        </Fragment>
    );
};
export default ConceptExplorerMap;
