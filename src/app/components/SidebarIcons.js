import OfficeBuildingFilledIcon from '@atlaskit/icon/glyph/office-building-filled';
import HomeCircleIcon from '@atlaskit/icon/glyph/home-circle';
import FolderFilledIcon from '@atlaskit/icon/glyph/folder-filled';
import WorldIcon from '@atlaskit/icon/glyph/world';
import Avatar from '@atlaskit/avatar';
import RoomMenuIcon from '@atlaskit/icon/glyph/room-menu';
import QuestionsIcon from '@atlaskit/icon/glyph/questions';

export const GlobalIcons = {
    home: {
        id: 'organization',
        icon: HomeCircleIcon,
        label: 'Organization',
    },
    portfolio: {
        id: 'portfolio',
        icon: FolderFilledIcon,
        tooltip: 'Portfolio',
    },
    help: {
        id: 'help',
        icon: QuestionsIcon,
        tooltip: 'Help',
    },
    geoexplorer: {
        id: 'geoexplorer',
        icon: WorldIcon,
        tooltip: 'Geographic Explorer',
    },
    conceptexplorer: {
        id: 'conceptexplorer',
        icon: RoomMenuIcon,
        tooltip: 'Concept Explorer',
    },
    organizations: {
        id: '10-composed-navigation-1',
        icon: OfficeBuildingFilledIcon,
        label: 'Organization',
        size: 'small',
        tooltip: 'Change Organization',
    },
    profile: {
        id: '10-composed-navigation-2',
        icon: null,
        label: 'Profile',
        size: 'small',
        tooltip: 'Account',
        component: AvatarIcon,
    },
};

export default function AvatarIcon({ className }) {
    return (
        <span
            className={className}
            style={{ marginBottom: '20px', paddingLeft: '0px' }}
        >
            <Avatar
                borderColor="transparent"
                isActive={false}
                isHover={false}
                size="small"
                isMenuFixed
            />
        </span>
    );
}
AvatarIcon.displayName = 'AvatarIcon';
