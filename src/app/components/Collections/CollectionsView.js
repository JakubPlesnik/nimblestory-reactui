import { useContext } from 'react';
import UserContext from '../../context/UserContext';
import PortfolioContext from '../../context/PortfolioContext';
import Collection from '../../components/cards/AppCardCollection';
import HomepageHeader from '../HomepageHeader';
import { Space } from 'antd';

const PortfolioProjectsView = () => {
    //const [isFilterPaneOn, setFilterPaneOn] = useState(false);
    const { pathinfo } = useContext(UserContext);
    const { collections } = useContext(PortfolioContext);

    return (
        <div className="pagearea">
            <HomepageHeader titleText={'Collections Home'} />

            <div className="divider15" />

            <div className="assetsContainer">
                <Space size={[16, 35]} wrap style={{ marginBottom: '0px' }}>
                    {collections.map(item => (
                        <Collection
                            key={`${item.id}-projects`}
                            organization={pathinfo.organization}
                            collection={item}
                        />
                    ))}
                </Space>
            </div>

            <style jsx>
                {`
                    .controls {
                        display: flex;
                        flex-direction: row;
                        flex-wrap: nowrap;
                        align-content: center;
                        width: 100%;
                        padding-left: 17px;
                        padding-right: 28px;
                    }
                    .controlsOne {
                        min-width: 134px;
                        max-width: 350px;
                        margin-right: 10px;
                    }
                    .controlsTwo {
                        flex-grow: 1;
                        margin-right: 10px;
                    }
                    .controlsThree {
                        min-width: 134px;
                    }
                    .titleSeparator {
                        margin-bottom: 22px;
                    }
                    .assetsContainer {
                        margin-left: 16px;
                        display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                    }
                    .portfolioHeader {
                        padding: 16px 36px;
                    }
                    .secondaryTitle {
                        font-weight: 500;
                        font-size: 12px;
                    }
                    .primaryTitle {
                        font-weight: 500;
                        font-size: 24px;
                        line-height: 28px;
                        padding: 9px 0px;
                    }

                    .pageTitle {
                        margin-left: 31px;
                    }
                    .divider15 {
                        height: 14px;
                        clear: both;
                    }

                    .portfolioLink {
                        cursor: pointer;
                    }
                `}
            </style>
        </div>
    );
};
export default PortfolioProjectsView;
