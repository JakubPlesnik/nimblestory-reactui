import { useContext } from 'react';

import UserContext from '../../context/UserContext';
import PortfolioProjectAssetsContext from '../../context/PortfolioProjectAssetsContext';

import { AppDropdownBox } from '../AppDropdownBox';
import AppCardAsset from '../../components/cards/AppCardAsset';

import AppInputSearch from '../AppInputSearch';

import AppFolderBreadcrumb from '../AppFolderBreadcrumb';
import TopPanelCollection from '../../components/TopPanelCollection';
import AppCardConteptExplorer from '../cards/AppCardConteptExplorer';

import { Space } from 'antd';

import { assetSortOptions } from '../../services/filteringAssets';

const CollectionDetailView = () => {
    const { pathinfo } = useContext(UserContext);

    const {
        collection,
        assets,
        setAssetSortOption,
        searchFilterKeyword,
        setSearchFilterKeyword,
        selectedAssetSortOption,
    } = useContext(PortfolioProjectAssetsContext);

    const getObjectCard = item => {
        if (item.entryType === 'CE') {
            return (
                <AppCardConteptExplorer
                    key={`${item.id}-projects`}
                    organization={pathinfo.organization}
                    project={item}
                />
            );
        }

        return (
            <AppCardAsset
                key={`${item.id}-assets`}
                organization={pathinfo.organization}
                project={pathinfo.project}
                asset={item}
            />
        );
    };

    return (
        <div className="page-wrapper">
            <TopPanelCollection
                pathinfo={pathinfo}
                collection={collection || {}}
            />
            <div className="pagearea">
                <div className="breadCrumb">
                    <div className="pl-0 col-12 pl-4">
                        {<AppFolderBreadcrumb items={[]} />}
                    </div>
                </div>

                <div className="controls">
                    {/* <div className="controlsOne">
                        <AppDropdownBox
                            label="FILTER"
                            menuOptions={assetSortOptions.map(
                                ({ title }) => title
                            )}
                            selectedOptionKey={selectedAssetSortOption}
                            onOptionSelected={item => {
                                setAssetSortOption(item);
                            }}
                        />
                    </div> */}

                    <div className="controlsOne">
                        <AppDropdownBox
                            label="SORT"
                            menuOptions={assetSortOptions.map(
                                ({ title }) => title
                            )}
                            selectedOptionKey={selectedAssetSortOption}
                            onOptionSelected={item => {
                                setAssetSortOption(item);
                            }}
                        />
                    </div>
                    <div className="controlsTwo">
                        <AppInputSearch
                            label="SEARCH"
                            searchFilterKeyword={searchFilterKeyword}
                            setSearchFilterKeyword={setSearchFilterKeyword}
                        />
                    </div>
                </div>
                <div className="divider17"></div>
                <div className="assetsContainer">
                    <Space size={[16, 35]} wrap style={{ marginBottom: '0px' }}>
                        {assets.map(item => getObjectCard(item))}
                    </Space>
                </div>

                <div className="divider15" />
                <div className="divider56" />
            </div>
            <style jsx>
                {`
                    .page-wrapper {
                        background-color: #f0f0f0;
                    }
                    .breadCrumb {
                        padding-top: 16px;
                        margin-left: 18px;
                        margin-bottom: 10px;
                    }
                    .pl-0 {
                        padding-left: 0px !important;
                    }
                    .controls {
                        display: flex;
                        flex-direction: row;
                        flex-wrap: nowrap;
                        align-content: center;
                        width: 100%;
                        padding-left: 17px;
                        padding-right: 28px;
                    }
                    .controlsOne {
                        min-width: 134px;
                        max-width: 350px;
                        margin-right: 10px;
                    }
                    .controlsTwo {
                        flex-grow: 1;
                        margin-right: 10px;
                    }
                    .controlsThree {
                        min-width: 134px;
                    }
                    .divider3 {
                        height: 3px;
                        clear: both;
                    }
                    .divider10 {
                        height: 10px;
                        clear: both;
                    }
                    .divider15 {
                        height: 14px;
                        clear: both;
                    }
                    .divider17 {
                        height: 17px;
                        clear: both;
                    }
                    .assetsContainer {
                        margin-left: 16px;
                        display: flex;
                        flex-direction: row;
                        flex-wrap: wrap;
                    }
                `}
            </style>
        </div>
    );
};

export default CollectionDetailView;
