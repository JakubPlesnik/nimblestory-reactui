/* eslint-disable no-console */
// import fetch from "isomorphic-unfetch"
import Papa from 'papaparse';

const API_PREFIX = '';

function handleErrors(response) {
    if (!response.ok) {
        if (
            /Login/.test(response.statusText) ||
            /access/.test(response.statusText)
        ) {
            // Router.push('/logout')
            window.location = '/logout';
        } else {
            throw Error(response.statusText);
        }
    }
    return response;
}

export const authorizeToken = async () => {
    const token = localStorage.getItem('token');

    let fetchResult = false;

    await fetch(
        `${API_PREFIX}/actions/maga-image-proxy/get-image/token-cookie?t=${token}`
    )
        .then(handleErrors)
        .then(response => response.json())
        .then(result => {
            fetchResult = result;
        })
        .catch(error => {
            console.dir(error);
        });

    return fetchResult;
};

export const saveProject = async projectData => {
    let fetchResult = false;

    await fetch(
        `${API_PREFIX}/actions/maga-image-proxy/get-image/save-project`,
        {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(projectData),
        }
    )
        .then(handleErrors)
        .then(response => response.json())
        .then(result => {
            fetchResult = result;
        })
        .catch(error => {
            console.dir(error);
        });

    return fetchResult;
};

export const saveAsset = async projectData => {
    let fetchResult = false;

    await fetch(`${API_PREFIX}/actions/maga-image-proxy/get-image/save-asset`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(projectData),
    })
        .then(handleErrors)
        .then(response => response.json())
        .then(result => {
            fetchResult = result;
        })
        .catch(error => {
            console.dir(error);
        });

    return fetchResult;
};

export const savePins = async data => {
    let fetchResult = false;

    await fetch(`${API_PREFIX}/actions/maga-image-proxy/get-image/save-pins`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then(handleErrors)
        .then(response => response.json())
        .then(result => {
            fetchResult = result;
        })
        .catch(error => {
            console.dir(error);
        });

    return fetchResult;
};

export const downloadTextFile = async (fileUrl, params, token) => {
    let fetchResult = false;

    const query = !params
        ? ''
        : Object.keys(params)
              .map(
                  k =>
                      `${encodeURIComponent(k)}=${encodeURIComponent(
                          params[k]
                      )}`
              )
              .join('&');

    await fetch(`${fileUrl}?${query}`, {
        method: 'GET',
        headers: {
            Accept: '*/*',
            mode: 'cors',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
    })
        .then(response => response.text())
        .then(text =>
            Papa.parse(text, { skipEmptyLines: 'greedy', header: true })
        )
        .then(result => {
            fetchResult = result.data;
        })
        .catch(error => {
            console.dir(error);
        });

    return fetchResult;
};

export const saveForm = async fields => {
    let fetchResult = false;

    await fetch(`${API_PREFIX}/actions/maga-image-proxy/get-image/save-file`, {
        method: 'POST',
        body: fields,
    })
        .then(handleErrors)
        .then(response => response.json())
        .then(result => {
            fetchResult = result;
        })
        .catch(error => {
            console.dir(error);
        });

    return fetchResult;
};
