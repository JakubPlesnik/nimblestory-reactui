const assetFromRaw = ({
    rawItem,
    itemkey,
    assetTypes,
    assetStatus,
    projects,
}) => {
    const newAssetBlank = {
        id: `new_${itemkey}`,
        hash: `new_${itemkey}`,
        entryType: 'magaAsset',
        dateEntryCreatedUnix: new Date().getTime() / 1000,
        title: '',
        ProjSlug: '',
        AssetStatus: [],
        AssetType: [],
        AssetVersion: null,
    };

    for (const key in rawItem) {
        const value = rawItem[key];
        if (!value.length) continue;

        if (key.startsWith('title')) {
            newAssetBlank.title = value;
        }

        if (key.startsWith('AssetName')) {
            newAssetBlank.title = value;
        }

        if (key.startsWith('ProjName')) {
            const project = projects.find(
                p => p.title.toLowerCase().indexOf(value.toLowerCase()) >= 0
            );
            if (project) {
                newAssetBlank.ProjSlug = project.slug;
            }
        }

        if (key.startsWith('AssetVersion')) {
            newAssetBlank.AssetVersion = value;
        }

        if (key.startsWith('AssetType')) {
            const assetTypeRawNames = value.split(',').map(item => item.trim());
            newAssetBlank.AssetType = assetTypeRawNames
                .map(rn => {
                    const matchedAssetType = assetTypes.find(
                        ({ label, value }) =>
                            label.toLowerCase().includes(rn.toLowerCase()) ||
                            value.toLowerCase().includes(rn.toLowerCase())
                    );
                    return matchedAssetType ? matchedAssetType.value : null;
                })
                .filter(e => e);

            if (newAssetBlank.AssetType.length < assetTypeRawNames.length) {
                newAssetBlank.AssetTypeIncorrect = value;
            }
        }

        if (key.startsWith('AssetStatus')) {
            newAssetBlank.AssetStatus = [];
            const assetStatusRawNames = value
                .split(',')
                .map(item => item.trim());
            newAssetBlank.AssetStatus = assetStatusRawNames
                .map(rn => {
                    const matchedStatus = assetStatus.find(
                        ({ label, value }) =>
                            label.toLowerCase().includes(rn.toLowerCase()) ||
                            value.toLowerCase().includes(rn.toLowerCase())
                    );
                    return matchedStatus ? matchedStatus.value : null;
                })
                .filter(e => e);

            if (newAssetBlank.AssetStatus.length < assetStatusRawNames.length) {
                newAssetBlank.AssetStatusIncorrect = value;
            }
        }

        if (key.startsWith('AssetUpdated')) {
            newAssetBlank.AssetUpdatedUnix = Date.parse(value) / 1000;
            newAssetBlank.AssetUpdated = value;
        }

        if (key.startsWith('Video')) {
            newAssetBlank.Video = value;
            newAssetBlank.entryType = 'magaAssetVideo';
        }
    }
    return newAssetBlank;
};

export default assetFromRaw;
