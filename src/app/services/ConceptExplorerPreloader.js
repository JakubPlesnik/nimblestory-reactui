export function ConceptExplorerPreload() {
    if (window.batch) {
        return;
    }

    /*
    const q = new Queue(
        (input, cb) => {
            const image = new Image();
            image.onload = () => cb();
            image.onerror = () => cb();
            image.src = input;
        },
        {
            id: 'PreloadTiles',
            cancelIfRunning: true,
            concurrent: 10,
            afterProcessDelay: 1,
            store: new MemoryStore(),
        }
    );

    const tiles = solution.visuals
        .map(visual =>
            visual.preload.map(path => `${visual.tilesbase}/${path}`)
        )
        .flat(1);

    tiles.forEach(input => q.push(input));

    window.batch = q;
    */
}

export function ConceptExplorerPreloadCancel() {
    if (window.batch) {
        window.batch.destroy(() => (window.batch = false));
    }
}
