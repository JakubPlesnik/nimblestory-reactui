export default function links(path) {
    if (!path.application) {
        path.application = 'portfolio';
    }
    let link = '';
    let linkAs = `/${path.application}`;
    let linkPage = 'main';

    if (path.homeLink) {
        linkAs = `/portfolio/${path.homeLink.organization}`;
        /* if (!path.homeLink.application) {
      path.homeLink.application = "portfolio"
    }
    if (path.homeLink.application == "portfolio") {
      linkAs = `/${path.homeLink.application}/${path.homeLink.organization}/all`
    }
    else {
      linkAs = `/${path.homeLink.application}/${path.homeLink.organization}`
    } */
    }

    if (path.portfolioAllLink) {
        if (!path.portfolioAllLink.application) {
            path.portfolioAllLink.application = 'portfolio';
        }
        linkAs = `/${path.portfolioAllLink.application}/${path.portfolioAllLink.organization}/all`;
    }
    if (path.ConceptExplorerLink) {
        linkAs = `/${path.ConceptExplorerLink.application}/${path.ConceptExplorerLink.organization}`;
    }
    if (path.GeoExplorerLink) {
        linkAs = `/${path.GeoExplorerLink.application}/${path.GeoExplorerLink.organization}`;
    }

    if (path.organization) {
        link = `${link}?organization=${path.organization}`;
        linkPage = 'projects';

        linkAs = `${linkAs}/${path.organization}`;

        if (path.project) {
            link = `${link}&project=${path.project}`;

            if (path.application == 'geoexplorer') {
                linkPage = 'geoexplorersolution';
            } else if (path.application == 'conceptexplorer') {
                linkPage = 'conceptsolution';
            } else {
                linkPage = 'project';
            }

            linkAs = `${linkAs}/${path.project}`;

            if (path.asset) {
                link = `${link}&asset=${path.asset}`;
                linkPage = 'asset';

                linkAs = `${linkAs}/${path.asset}`;
            }

            if (path.folderId) {
                link = `${link}`;
                linkPage = 'project';

                linkAs = `${linkAs}?folderId=${path.folderId}`;
            }
        }

        link = `/${linkPage}${link}&application=${path.application}`;
    }

    if (path.application == 'collection') {
        link = `${path.application}?organization=${path.organization}&collection=${path.collection}&application=${path.application}`;
        linkAs = `/portfolio/${path.organization}/collection/${path.collection}`;
    }

    return {
        href: link,
        as: linkAs,
    };
}
