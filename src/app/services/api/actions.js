/* eslint-disable no-console */
import axios from 'axios';
import Router from 'next/router';
import { API_URL } from './apiContstants';

/**
 * Wraps a post reqeust to specified API endpoin
 * @param endpoint Endpoint string URI
 * @param data Request data object
 * @param onSuccess Optional Setter method for the returned data
 * @param onError Optional Setter method for errors
 */
export const apiPost = async (
    endpoint,
    data = {},
    onSuccess = responseData => {
        return responseData;
    },
    onError = handleErrors
) => {
    let fetchResult = false;

    try {
        await axios
            .post(endpoint, data, getConfig())
            .then(response => response.data)
            .then(result => {
                fetchResult = result;
            })
            .catch(e => onError(e.response));

        if (fetchResult) {
            onSuccess(fetchResult);
        }
    } catch (error) {
        onError();
    }

    return fetchResult;
};

/**
 * Wraps a get reqeust to specified API endpoint
 * @param endpointRoute Endpoint string URI
 */
export const apiGet = async endpointRoute => {
    let fetchResult = false;

    await axios
        .get(endpointRoute, getConfig())
        .then(response => response.data)
        .then(result => {
            fetchResult = result;
        })
        .catch(e => handleErrors(e.response));

    return fetchResult;
};

export const apiGetImage = async (
    endpointRoute,
    onSuccess = responseData => {
        return responseData;
    },
    onError = handleErrors
) => {
    let fetchResult = false;
    var url = API_URL + endpointRoute;
    var config = getConfig();
    config.method = 'GET';
    config.cache = 'force-cache';
    fetch(url, config)
        .then(response => response.blob())
        .then(url => {
            fetchResult = true;
            let blobUrl = URL.createObjectURL(url);
            onSuccess(blobUrl);
        })
        .catch(e => onError(e));
    return fetchResult;
};

/**
 * Default error handler
 * @param response error response
 */
const handleErrors = response => {
    if (!response) {
        console.error('Network Error');
        return;
    }

    if (response.status != 200 && response.statusText.toUpperCase() != 'OK') {
        if (
            /Login/.test(response.statusText) ||
            /Unauthorized/.test(response.statusText)
        ) {
            localStorage.removeItem('token');
            Router.push('/login');
        } else {
            console.error(response.data.error.message);
            throw Error(response.data.error.message);
        }
    }
    return response;
};

/**
 * Get an axios config
 */
const getConfig = () => ({
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${getToken()}`,
    },
});

/**
 * Get JWT token from localstorage
 */
export const getToken = () => {
    try {
        if (window.localStorage) {
            return localStorage.getItem('token');
        }
    } catch (err) {
        return document.localStorage.token;
    }
};
