/* eslint-disable no-unused-vars */
import { apiGet, apiPost, apiGetImage } from './actions';
import {
    ORGANIZATIONS,
    LOGIN,
    CONTENT_TYPE,
    CONTENT_STATUS,
    PROJECT,
    CONTENT,
    PORTFOLIO,
    PORTFOLIO_CONTENT,
    SET_DEFAULT_ORGANIZATION,
    THUMBNAIL,
    ASSET,
    CUSTOMER,
    CHANGE_PASSWORD,
    PASSWORD,
    NAVIGATION,
    MESSAGE,
    HEALTHCHECK,
    COLLECTION,
} from './apiContstants';

/**
 * Fetch organizations for current user
 */
export const fetchOrgs = async () => {
    let fetchResult = false;
    fetchResult = await apiGet(ORGANIZATIONS);
    return fetchResult;
};

/**
 * Save default organisation for current user
 */
export const saveDefault = async orgslug => {
    let fetchResult = false;
    fetchResult = await apiPost(SET_DEFAULT_ORGANIZATION, { orgslug });
    return fetchResult;
};

/**
 * Fetch pinned items for organization
 */
export const fetchPinned = async organization => {
    let fetchResult = false;
    fetchResult = await apiGet(`content/${organization}/pinned`);
    return fetchResult;
};

/**
 * Fetch all content types
 */
export const fetchAssetTypes = async () => {
    let fetchResult = false;
    fetchResult = await apiGet(`${CONTENT}${CONTENT_TYPE}`);
    return fetchResult;
};

/**
 * Fetch all content statuses
 */
export const fetchAssetStatus = async () => {
    let fetchResult = false;
    fetchResult = await apiGet(`${CONTENT}${CONTENT_STATUS}`);
    return fetchResult;
};

/**
 * Fetch content detail
 */
export const fetchContentDetail = async (projectSlug, contentSlug) => {
    let fetchResult = false;
    fetchResult = await apiGet(
        `${CONTENT}${PROJECT}/${projectSlug}/${contentSlug}`
    );
    return fetchResult;
};

/**
 * Fetch project by slug
 */
export const fetchprojectBySlug = async projectSlug => {
    let fetchResult = false;
    fetchResult = await apiGet(`${PROJECT}/find/${projectSlug}`);
    return fetchResult;
};

/**
 * Fetch all projects
 */
export const fetchProjects = async (
    organization,
    page = 1,
    orderBy,
    filterBy
) => {
    let fetchResult = false;
    fetchResult = await apiGet(`${PROJECT}/${organization}`);
    return fetchResult;
};

/**
 * Fetch project from given organization with given slug
 */
export const fetchContent = async (organization, project) => {
    let fetchResult = false;
    if (project === 'assets')
        fetchResult = await apiGet(`${CONTENT}/${organization}/all`);
    else {
        fetchResult = await apiGet(`${CONTENT}${PROJECT}/${project}`);
    }

    return fetchResult;
};

export const fetchConceptExplorer = async (organization, slug) => {
    let fetchResult = false;
    fetchResult = await apiGet(`conceptexplorer/${organization}/${slug}`);

    return fetchResult;
};

export const fetchEmbedConceptExplorer = async (
    organization,
    slug,
    embedToken
) => {
    let fetchResult = false;
    fetchResult = await apiGet(
        `conceptexplorer/embed/${organization}/${slug}?embedToken=${embedToken}`
    );
    return fetchResult;
};

/**
 * Fetch project from given organization with given slug
 */
export const fetchAsset = async (assetId, onSuccess, onError) => {
    let fetchResult = false;
    fetchResult = await apiGetImage(`${ASSET}/${assetId}`, onSuccess, onError);
    return fetchResult;
};

export const fetchEmbeddedAsset = async (
    type,
    slug,
    assetId,
    embedToken,
    onSuccess,
    onError
) => {
    let fetchResult = false;
    fetchResult = await apiGetImage(
        `${ASSET}/embed/${type}/${slug}/${assetId}?embedToken=${embedToken}`,
        onSuccess,
        onError
    );
    return fetchResult;
};

/**
 * Fetch thumbnail binary from given organization with given slug
 */
export const fetchThumbnail = async (assetId, onSuccess, onError) => {
    let fetchResult = false;
    fetchResult = await apiGetImage(
        `${THUMBNAIL}/${assetId}`,
        onSuccess,
        onError
    );
    return fetchResult;
};

/**
 * Fetch project from given organization with given slug
 */
export const fetchPortfolios = async (organization, project) => {
    let fetchResult = false;
    fetchResult = await apiGet(`${CONTENT}${PROJECT}/${project}`);
    return fetchResult;
};

//COLLECTIONS
/**
 * Fetch all collections
 */
export const fetchCollections = async (
    organization,
    page = 1,
    orderBy,
    filterBy
) => {
    let fetchResult = false;
    fetchResult = await apiGet(`${COLLECTION}/${organization}`);
    return fetchResult;
};

export const fetchCollectionDetails = async (organization, project) => {
    let fetchResult = false;
    fetchResult = await apiGet(`${COLLECTION}/${organization}/${project}`);
    return fetchResult;
};

//MESSAGES AND NOTIFICATIONS

/**
 * Fetch latest system message from server
 */

export const fetchSystemMessage = async () => {
    let fetchResult = false;
    fetchResult = await apiGet(`${MESSAGE}`);
    return fetchResult;
};

/**
 * Fetch project from given organization with given slug
 */
export const fetchHealthCheck = async (onSuccess, onError) => {
    let fetchResult = false;
    fetchResult = await apiGet(`${HEALTHCHECK}`, onSuccess, onError);
    return fetchResult;
};

/**
 * Create new content
 * @param title - title string
 * @param description - description string
 * @param status - status string
 * @param projectSlug - projectSlug string
 * @param assetId - assetId array
 * @param onCreateSuccess - callback on success
 * @param onCreateError - callback on error
 */
export const createNewContentRequestAsset = async (
    title,
    description,
    status,
    projectSlug,
    type,
    assetsId,
    onCreateSuccess,
    onCreateError
) =>
    apiPost(
        `${PORTFOLIO_CONTENT}/pctAsset`,
        {
            title,
            description,
            projectSlug,
            status,
            type,
            assetsId,
        },
        onCreateSuccess,
        onCreateError
    );
/**
 * Create new content
 * @param title - title string
 * @param description - description string
 * @param status - status string
 * @param projectSlug - projectSlug string
 * @param moduleFile - module file
 * @param onCreateSuccess - callback on success
 * @param onCreateError - callback on error
 */
export const createNewContentRequestModuleFile = async (
    title,
    description,
    status,
    projectSlug,
    type,
    moduleFileId,
    onCreateSuccess,
    onCreateError
) =>
    apiPost(
        `${PORTFOLIO_CONTENT}/pctModuleFile`,
        {
            title,
            description,
            projectSlug,
            status,
            type,
            moduleFileId,
        },
        onCreateSuccess,
        onCreateError
    );
/**
 * Create new content
 * @param title - title string
 * @param description - description string
 * @param status - status string
 * @param projectSlug - projectSlug string
 * @param video - video link
 * @param onCreateSuccess - callback on success
 * @param onCreateError - callback on error
 */
export const createNewContentRequestVideo = async (
    title,
    description,
    status,
    projectSlug,
    type,
    video,
    onCreateSuccess,
    onCreateError
) =>
    apiPost(
        `${PORTFOLIO_CONTENT}/pctVideo`,
        {
            title,
            description,
            projectSlug,
            status,
            type,
            video,
        },
        onCreateSuccess,
        onCreateError
    );
/**
 * Create new content
 * @param title - title string
 * @param description - description string
 * @param status - status string
 * @param projectSlug - projectSlug string
 * @param link - link
 * @param onCreateSuccess - callback on success
 * @param onCreateError - callback on error
 */
export const createNewContentRequestLink = async (
    title,
    description,
    status,
    projectSlug,
    type,
    link,
    onCreateSuccess,
    onCreateError
) =>
    apiPost(
        `${PORTFOLIO_CONTENT}/pctLink`,
        {
            title,
            description,
            projectSlug,
            status,
            type,
            link,
        },
        onCreateSuccess,
        onCreateError
    );
/**
 * Send change password request with old and new password
 * @param oldPassword - username string
 * @param newPassword - password string
 * @param onChangeSuccess - callback on success
 * @param onChangeError - callback on error
 */
export const changePasswordRequest = async (
    oldPassword,
    newPassword,
    onChangeSuccess,
    onChangeError
) =>
    apiPost(
        `${CUSTOMER}${CHANGE_PASSWORD}`,
        {
            oldPassword,
            newPassword,
        },
        onChangeSuccess,
        onChangeError
    );

/**
 * Send email with recovery link
 * @param email - email string
 * @param onEmailSuccess - callback on success
 * @param onEmailError - callback on error
 */
export const sendRecoveryLink = async (email, onEmailSuccess, onEmailError) =>
    apiPost(
        `${CUSTOMER}${PASSWORD}/reset`,
        { email },
        onEmailSuccess,
        onEmailError
    );

/**
 * Save new passowrd
 * @param token - recovery token string
 * @param newPassword - new password string
 * @param onEmailSuccess - callback on success
 * @param onEmailError - callback on error
 */
export const saveNewPassword = async (
    token,
    newPassword,
    onSetPasswordSuccess,
    onSetPasswordError
) =>
    apiPost(
        `${CUSTOMER}${PASSWORD}/new`,
        { token, newPassword },
        onSetPasswordSuccess,
        onSetPasswordError
    );

/**
 * Send authentication request with usename and password
 * @param username - username string
 * @param password - password string
 * @param onLoginSuccess - callback on success
 * @param onLoginError - callback on error
 */
export const sendAuthRequest = async (
    username,
    password,
    onLoginSuccess,
    onLoginError
) =>
    apiPost(
        LOGIN,
        {
            username,
            password,
        },
        onLoginSuccess,
        onLoginError
    );

/**
 * Seng request for navigation elements of given organization
 * @param organizationSlug - org slug string
 * @param onSuccess - callback on success
 * @param onError - callback on error
 */
export const fetchNavigation = async (organizationSlug, onSuccess, onError) =>
    apiGet(`${NAVIGATION}/${organizationSlug}`, onSuccess, onError);
