// Routes API
export const API_URL = process.env.NEXT_PUBLIC_API_URL;

// Auth
export const LOGIN = '/login_check';
export const CUSTOMER = '/customer';
export const CHANGE_PASSWORD = '/change_password';

export const PASSWORD = '/password';

// Organization
export const ORGANIZATIONS = '/organization';
export const SET_DEFAULT_ORGANIZATION = '/organization/default';

// Assets
export const CONTENT_TYPE = '/contentTypes';
export const CONTENT_STATUS = '/contentStatus';
export const PROJECT = '/project';
export const ASSET = '/asset';
export const THUMBNAIL = '/asset/thumbnail';

export const PORTFOLIO = '/portfolio';
export const PORTFOLIO_CONTENT = '/PortfolioContent';

export const CONTENT = '/content';

export const NAVIGATION = '/navigation';

export const HEALTHCHECK = '/healthcheck';

export const COLLECTION = '/collection';

//Messages and notifications
export const MESSAGE = '/messages';
