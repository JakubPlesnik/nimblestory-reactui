export const filteredItems = (items, filters, projectFilterOptions) => {
    let filtered = items.slice();
    let filterNumber = 0;
    while (filterNumber < projectFilterOptions.length) {
        const filterSet = projectFilterOptions[filterNumber].options
            .filter(item => filters.findIndex(f => f.title === item.title) > -1)
            .map(({ filter }) => filter);
        if (filterSet.length) {
            filtered = filtered.filter(v => filterSet.some(f => f(v)));
        }
        filterNumber++;
    }
    return filtered;
};

export const projectSortOptions = [
    {
        title: 'System',
        orderBy: 'df',
        sorter: (a, b) => {
            const projA = a.dateEntryCreatedUnix || 0;
            const projB = b.dateEntryCreatedUnix || 0;
            let comparison = 0;
            if (projA > projB) {
                comparison = -1;
            } else if (projA < projB) {
                comparison = 1;
            }
            return comparison;
        },
    },
    {
        title: 'Year',
        orderBy: 'py',
        sorter: (a, b) => {
            const projA = a.ProjYear || 0;
            const projB = b.ProjYear || 0;
            let comparison = 0;
            if (projA > projB) {
                comparison = -1;
            } else if (projA < projB) {
                comparison = 1;
            }
            return comparison;
        },
    },
    {
        title: 'A-Z',
        orderBy: 'az',
        sorter: (a, b) => {
            const projA = a.title.toUpperCase();
            const projB = b.title.toUpperCase();
            let comparison = 0;
            if (projA > projB) {
                comparison = 1;
            } else if (projA < projB) {
                comparison = -1;
            }
            return comparison;
        },
    },
];

const filterDate = new Date();

export const projectFilterOptionActive = {
    title: 'Active',
    filter(item) {
        return item.ProjStatus != 'inactive';
    },
};

export const projectFilterOptionInactive = {
    title: 'Inactive',
    filter(item) {
        return item.ProjStatus == 'inactive';
    },
};
export const fetchProjectFilterOptions = assetTypes => {
    let contentTypes = {};
    if (assetTypes) {
        // build array for multi-select fields
        contentTypes = {
            title: 'Contains asset types',
            type: 'multiplechoice',
            options: assetTypes.map(type => ({
                title: type.label,
                filter(item) {
                    return item.AssetType.includes(type.value);
                },
            })),
        };
    }
    const projectFilterOptions = [
        {
            title: 'Project type',
            type: 'multiplechoice',
            options: [
                {
                    title: 'Strategy',
                    filter(item) {
                        return item.ProjType.includes('Strategy');
                    },
                },
                {
                    title: 'Comms',
                    filter(item) {
                        return item.ProjType.includes('Communications');
                    },
                },
                {
                    title: 'Branding',
                    filter(item) {
                        return item.ProjType.includes('Branding');
                    },
                },
                {
                    title: 'Other',
                    filter(item) {
                        return item.ProjType.includes('Other');
                    },
                },
            ],
        },
        {
            title: 'Completion year',
            type: 'singlechoice',
            options: [
                {
                    title: 'Current Year',
                    filter(item) {
                        return item.ProjYear == filterDate.getFullYear();
                    },
                },
                {
                    title: 'Prior Year',
                    filter(item) {
                        return item.ProjYear == filterDate.getFullYear() - 1;
                    },
                },
                {
                    title: '2 years ago',
                    filter(item) {
                        return item.ProjYear == filterDate.getFullYear() - 2;
                    },
                },
                {
                    title: '>2 years',
                    filter(item) {
                        return item.ProjYear < filterDate.getFullYear() - 2;
                    },
                },
            ],
        },
        contentTypes,
        {
            title: 'Project status',
            type: 'singlechoice',
            options: [projectFilterOptionActive, projectFilterOptionInactive],
        },
    ];

    return projectFilterOptions;
};
