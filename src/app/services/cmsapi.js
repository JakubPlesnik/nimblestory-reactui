/* eslint-disable no-console */
import axios from 'axios';

const API_PREFIX = '';

function handleErrors(response) {
    if (!response) {
        console.log('Network Error');
        return;
    }
    if (response.statusText.toUpperCase() != 'OK') {
        if (
            /Login/.test(response.statusText) ||
            /access/.test(response.statusText)
        ) {
            // Router.push('/logout')
            window.location = '/logout';
        } else {
            throw Error(response.statusText);
        }
    }
    return response;
}

async function post(endpoint, data = {}) {
    let token;

    try {
        if (window.localStorage) {
            token = localStorage.getItem('token');
        }
    } catch (err) {
        token = document.localStorage.token;
    }

    let fetchResult = false;

    await axios
        .post(endpoint, data, {
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                token,
            },
        })
        .then(response => response.data)
        .then(result => {
            fetchResult = result;
        })
        .catch(e => handleErrors(e.response));

    return fetchResult;
}

async function get(endpoint) {
    let token;

    try {
        if (window.localStorage) {
            token = localStorage.getItem('token');
        }
    } catch (err) {
        token = document.localStorage.token;
    }

    let fetchResult = false;

    await axios
        .get(endpoint, {
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                token,
            },
        })
        .then(response => response.data)
        .then(result => {
            fetchResult = result;
        })
        .catch(e => handleErrors(e.response));

    return fetchResult;
}

export const saveDefault = async orgslug => {
    let fetchResult = false;
    fetchResult = await post(
        `${API_PREFIX}/actions/maga-image-proxy/get-image/save-default`,
        { orgslug }
    );
    return fetchResult;
};

export const fetchOrgs = async () => {
    let fetchResult = false;
    fetchResult = await get(`${API_PREFIX}/organizations.json`);
    return fetchResult;
};

export const fetchEmbed = async ({ organization, application, project }) => {
    let fetchResult = false;
    fetchResult = await get(
        `${API_PREFIX}/organizations/${organization}/${application}/${project}/embed.json`
    );
    return fetchResult;
};

export const fetchProjects = async (org, url, orderBy) => {
    let fetchResult = false;
    let api_url = '';
    if (!org) {
        console.log('No org');
        return;
    }

    if (url) {
        // eslint-disable-next-line no-useless-escape
        api_url = url.replace(/^.*\/\/[^\/]+/, '');
    } else {
        api_url = `${API_PREFIX}/organizations/${org}/projects.json?page=1`;

        if (orderBy) {
            api_url += `&sort=${orderBy}`;
        }
    }

    fetchResult = await get(api_url).then(result => ({
        results: result.data,
        meta: result.meta,
    }));

    return fetchResult;
};

// POST: fetch assets filtered and sorted from back-end
export const fetchFilteredProjects = async (
    org,
    page = 1,
    orderBy,
    filterBy
) => {
    let fetchResult = false;
    const api_url = `${API_PREFIX}/organizations/${org}/projects.json?page=${page}`;

    fetchResult = await post(api_url, {
        contentType: filterBy,
        orderBy,
    }).then(result => ({ results: result.data, meta: result.meta }));

    return fetchResult;
};

// POST: fetch assets filtered and sorted from back-end
export const fetchAssets = async (
    organization,
    project,
    page = 0,
    orderBy,
    filterBy
) => {
    let fetchResult = false;

    if (!organization) {
        console.log('No org');
        return;
    }

    // load first page by default
    const api_url = `${API_PREFIX}/organizations/${organization}/${project}/assets.json?page=${page}`;

    fetchResult = await post(api_url, {
        contentType: filterBy,
        orderBy,
    }).then(result => ({
        organization,
        project,
        results: result.data,
        meta: result.meta,
    }));

    return fetchResult;
};

export const fetchPinned = async organization => {
    let fetchResult;

    const api_url = `${API_PREFIX}/organizations/${organization}/_pinned/assets.json`;
    fetchResult = await get(api_url).then(result => result.data);

    return fetchResult;
};

export const fetchAssetTypes = async () => {
    let fetchResult;

    const api_url = `${API_PREFIX}/assetTypes.json`;
    fetchResult = await get(api_url).then(result => result.data);

    return fetchResult;
};

export const fetchAssetStatus = async () => {
    let fetchResult;
    const api_url = `${API_PREFIX}/assetStatus.json`;

    fetchResult = await get(api_url).then(result => result.data);

    return fetchResult;
};

export const fetchFilteredConceptExplorerSolutions = async (
    org,
    orderBy,
    filterBy
) => {
    let fetchResult = false;
    const api_url = `${API_PREFIX}/organizations/${org}/conceptexplorer/solutions.json`;
    if (!org) {
        console.log('No org');
        return;
    }

    fetchResult = await post(api_url, {
        contentType: filterBy,
        orderBy,
    }).then(response => ({ results: response.data, meta: response.meta }));

    return fetchResult;
};

export const fetchFilteredGeoSolutions = async (
    org,
    url,
    orderBy,
    filterBy
) => {
    let fetchResult = false;
    let api_url = '';
    if (!org) {
        console.log('No org');
        return;
    }

    if (url) {
        // load next page with url
        api_url = url;
    } else {
        // load first page by default
        api_url = `${API_PREFIX}/organizations/${org}/geoexplorer/solutions.json`;
    }

    fetchResult = await post(api_url, {
        contentType: filterBy,
        orderBy,
    }).then(response => ({ results: response.data, meta: response.meta }));

    return fetchResult;
};
