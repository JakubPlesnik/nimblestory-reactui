import AppLinkData from './applink';
// import ContentTyper from

export const getSystemActionLink = (slug, target, organization) => {
    if (target == 'listprojects') {
        return AppLinkData({
            organization: organization,
            project: slug,
        });
    }

    if (target == 'listcollections') {
        return AppLinkData({
            organization: organization,
            collection: slug,
            application: 'collection',
        });
    }
};

/**
 *
 * @param {object} item
 * @param {string} organization
 * @returns
 */
export const getContentLink = (item, organization) => {
    if (item.contentType == 'CE') {
        return AppLinkData({
            organization: organization,
            project: item.projectSlug,
            application: 'conceptexplorer',
        });
    }

    if (item.contentType == 'PortfolioContent') {
        return AppLinkData({
            organization: organization,
            project: item.projectSlug,
            asset: item.contentSlug,
        });
    }
};

/**
 * Substitute parameters from url
 */
export const processUrl = (url, orgSlug) => {
    if (url.includes('{orgslug}')) {
        return url.replace('{orgslug}', orgSlug);
    }
};
