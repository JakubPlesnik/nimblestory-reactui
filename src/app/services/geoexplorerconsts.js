export const kPropPlantId = 'Plant ID';
export const kPropLocationLng = 'Latitude';
export const kPropLocationLat = 'Longitude';
export const kPropLocationName = 'Location Name';
export const kPropAddress = 'Address';
export const kPropPOC = 'MockPOCName';
export const kPropPhoneNumber = 'MockPOCPhone';
export const kPropEmail = 'MockPOCEmail';

export const kPropAccuracyRange = 'Accuracy (Range)';
export const kPropValuation = 'Valuation';
export const kPropValuationExistence = 'Valuation (Existence)';
export const kPropValuationCompletion = 'Valuation (Completion)';
export const kPropDateOfLastInventory = 'Date Of Last Inventory';
export const kPropReadyForIssueCondition = 'Ready For Issue (Condition)';
export const kPropTurnoverRate = 'Turnover Rate';
export const kPropLastMovementMo = 'Last Movement Mo';
export const kPropPutawayTimeHrs = 'Putaway Time Hrs';
export const kPropProgramUtilizationRate = 'Program Utilization Rate';

export const kPropCategory = 'Category';
export const kInvTitle = 'Title/Description';
export const kPartNumber = 'Part Number';
export const kTypeOfMaterial = 'Type of Material';
export const kOwningBSO = 'Owning BSO';
export const kOwningOrg = 'Owning Organization';
export const kResponsible = 'Responsible/Custodial Organization';
export const kManagedby = 'Managed by';
export const kPrimeContractor = 'Prime Contractor';
export const kStatus = 'Status';
export const kCondition = 'Condition';
export const kiNFADS = 'iNFADS RP Unique ID';
export const kWarehouse = 'Warehouse Number';
export const kCity = 'City';
export const kState = 'State';
export const kName = 'Name of Installation, Base, or Contractor Facility';
export const kQuantity = 'O/H Quantity';
export const kUnitPrice = 'Unit Price';
export const kExtendedPrice = 'Extended Price';
export const kManagementSystem = 'Property Management System Used';
export const kManagerName = 'Warehouse Manager Name';
export const kManagerEmail = 'Warehouse Manager Email';
export const kManagerPhone = 'Warehouse Manager Phone Number';

export const kLastMovement = 'Last Movement Mo';
export const kPutawayTime = 'Putaway Time Hrs';

export const kMapPerspectiveKey = 'kMapPerspectiveKey';
export const kMapPerspectiveWarehouse = 'kMapPerspectiveWarehouse';
export const kMapPerspectiveExecutive = 'kMapPerspectiveExecutive';

export const kMapFocusMetricKey = 'kMapFocusMetricKey';

export const kMapViews = [
    {
        label: 'Perspective',
        source: kMapPerspectiveKey,
        type: 'radio',
        options: [
            {
                label: 'Executive',
                value: kMapPerspectiveExecutive,
            },
            {
                label: 'Warehouse Mgr',
                value: kMapPerspectiveWarehouse,
            },
        ],
    },
    {
        label: 'Focus Metric',
        source: kMapFocusMetricKey,
        type: 'radio',
        options: [
            {
                label: kPropValuationExistence,
                value: kPropValuationExistence,
            },
            {
                label: kPropReadyForIssueCondition,
                value: kPropReadyForIssueCondition,
            },
            {
                label: kPropProgramUtilizationRate,
                value: kPropProgramUtilizationRate,
            },
        ],
    },
];

export const filterIndicatorColorVal1 = val => {
    if (val >= 90) return 'green';
    if (val >= 80) return 'orange';
    if (val >= 70) return 'yellow';
    return 'red';
};
export const filterIndicatorColorVal2 = val => {
    if (val <= 10) return 'green';
    if (val <= 20) return 'orange';
    if (val <= 40) return 'blue';
    return 'red';
};
export const filterIndicatorColorDate = () => 'neutral';
