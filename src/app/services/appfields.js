const assetfields = {
    id: 'id',
    hash: 'hash',
    title: 'title',
    dateEntryCreated: 'dateEntryCreated',
    dateEntryCreatedUnix: 'dateEntryCreatedUnix',
    AssetStatus: 'AssetStatus',
    AssetType: 'AssetType',
    AssetVersion: 'AssetVersion',
    filePreview: 'File2(LgPrev)',
    fileViewable: 'File3(Viewable)',
    fileSource: 'File4(Source)',
    entryType: 'entryType',
    ProjSlug: 'ProjSlug',
    ProjStatus: 'ProjStatus',
    Video: 'Video',
    Link: 'Link',
};

export function getFieldKeyByValue(value) {
    return Object.keys(assetfields).find(key => assetfields[key] === value);
}
