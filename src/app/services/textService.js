export const textTruncate = (str, length) => {
    if (!str) return '';
    const ending = '...';

    if (str.length > length) {
        return str.substring(0, length - ending.length).trim() + ending;
    }
    return str;
};

export const getFileExtension = filename => {
    var ext = /^.+\.([^.]+)$/.exec(filename);

    return ext == null ? '' : ext[1];
};
