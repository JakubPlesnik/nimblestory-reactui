export const filteredItems = (items, filters, assetFilterOptions) => {
    let filtered = items.slice();
    let filterNumber = 0;
    while (filterNumber < assetFilterOptions.length) {
        const filterSet = assetFilterOptions[filterNumber].options
            .filter(item => filters.findIndex(f => f.title === item.title) > -1)
            .map(({ filter }) => filter);
        if (filterSet.length) {
            filtered = filtered.filter(v => filterSet.some(f => f(v)));
        }
        filterNumber++;
    }
    return filtered;
};

export const assetSortOptions = [
    {
        title: 'Default',
        orderBy: 'sk',
        sorter: (a, b) => {
            const projA = a.sortKey || 0;
            const projB = b.sortKey || 0;
            let comparison = 0;
            if (projA < projB) {
                comparison = -1;
            } else if (projA > projB) {
                comparison = 1;
            }
            return comparison;
        },
    },
    {
        title: 'Newest First (Updated)',
        orderBy: 'df',
        sorter: (a, b) => {
            const projA = a.dateEntryUpdated || 0;
            const projB = b.dateEntryUpdated || 0;
            let comparison = 0;
            if (projA > projB) {
                comparison = -1;
            } else if (projA < projB) {
                comparison = 1;
            }
            return comparison;
        },
    },
    {
        title: 'Newest First (Created)',
        orderBy: 'nf',
        sorter: (a, b) => {
            const projA = a.dateEntryCreatedUnix || 0;
            const projB = b.dateEntryCreatedUnix || 0;
            let comparison = 0;
            if (projA > projB) {
                comparison = -1;
            } else if (projA < projB) {
                comparison = 1;
            }
            return comparison;
        },
    },
    {
        title: 'A-Z',
        orderBy: 'az',
        sorter: (a, b) => {
            const projA = a.title.toUpperCase();
            const projB = b.title.toUpperCase();
            let comparison = 0;
            if (projA > projB) {
                comparison = 1;
            } else if (projA < projB) {
                comparison = -1;
            }
            return comparison;
        },
    },
    {
        title: 'Oldest First (Updated)',
        orderBy: 'of',
        sorter: (a, b) => {
            const projA = a.dateEntryUpdated || 0;
            const projB = b.dateEntryUpdated || 0;
            let comparison = 0;
            if (projA < projB) {
                comparison = -1;
            } else if (projA > projB) {
                comparison = 1;
            }
            return comparison;
        },
    },
    {
        title: 'Oldest First (Created)',
        orderBy: 'oc',
        sorter: (a, b) => {
            const projA = a.dateEntryCreatedUnix || 0;
            const projB = b.dateEntryCreatedUnix || 0;
            let comparison = 0;
            if (projA < projB) {
                comparison = -1;
            } else if (projA > projB) {
                comparison = 1;
            }
            return comparison;
        },
    },
    {
        title: 'Z-A',
        orderBy: 'za',
        sorter: (a, b) => {
            const projA = a.title.toUpperCase();
            const projB = b.title.toUpperCase();
            let comparison = 0;
            if (projA < projB) {
                comparison = 1;
            } else if (projA > projB) {
                comparison = -1;
            }
            return comparison;
        },
    },
];

const filterDate = new Date();

export const assetFilterOptionActive = {
    title: 'Active',
    filter(item) {
        return item.ProjStatus != 'inactive';
    },
};

export const assetFilterOptionInactive = {
    title: 'Inactive',
    filter(item) {
        return item.ProjStatus == 'inactive';
    },
};

export const fetchAssetFilterOptions = (assetTypes, assetStatus) => {
    // fetch assetTypes from API
    let contentTypes = {};
    let status = {};

    if (assetTypes) {
        // build array for multi-select fields
        contentTypes = {
            title: 'Content Type',
            type: 'multiplechoice',
            options: assetTypes.map(type => ({
                title: type.label,
                filter(item) {
                    return item.AssetType.includes(type.value);
                },
            })),
        };
    }

    // fetch assetStatuses from API
    if (assetStatus) {
        // build array for multi-select fields
        status = {
            title: 'Status',
            type: 'multiplechoice',
            options: assetStatus.map(type => ({
                title: type.label,
                filter(item) {
                    return item.AssetStatus.includes(type.value);
                },
            })),
        };
    }

    const assetFilterOptions = [
        status,
        contentTypes,
        {
            title: 'Last updated',
            type: 'singlechoice',
            options: [
                {
                    title: 'Current Year',
                    filter(item) {
                        return (
                            item.AssetUpdatedUnix != null &&
                            new Date(
                                item.AssetUpdatedUnix * 1000
                            ).getFullYear() == filterDate.getFullYear()
                        );
                    },
                },
                {
                    title: 'Prior Year',
                    filter(item) {
                        return (
                            item.AssetUpdatedUnix != null &&
                            new Date(
                                item.AssetUpdatedUnix * 1000
                            ).getFullYear() ==
                                filterDate.getFullYear() - 1
                        );
                    },
                },
                {
                    title: '2 years ago',
                    filter(item) {
                        return (
                            item.AssetUpdatedUnix != null &&
                            new Date(
                                item.AssetUpdatedUnix * 1000
                            ).getFullYear() ==
                                filterDate.getFullYear() - 2
                        );
                    },
                },
                {
                    title: '>2 years',
                    filter(item) {
                        return (
                            item.AssetUpdatedUnix != null &&
                            new Date(
                                item.AssetUpdatedUnix * 1000
                            ).getFullYear() <
                                filterDate.getFullYear() - 2
                        );
                    },
                },
            ],
        },
        {
            title: 'Project status',
            type: 'hidden',
            options: [assetFilterOptionActive, assetFilterOptionInactive],
        },
    ];
    return assetFilterOptions;
};
