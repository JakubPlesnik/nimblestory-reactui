import Router from 'next/router';

export const logout = resetDataOnLogout => {
    resetDataOnLogout();
    localStorage.removeItem('token');
    Router.push('/login');
};
