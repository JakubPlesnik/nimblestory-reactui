import AuthenticatedUserLayout from '../components/layout/AuthenticatedUserLayout';
import NSDataPortfolio from '../components/NSDataPortfolio';
import NSDataPortfolioProjectAssets from '../components/NSDataPortfolioProjectAssets';
import PortfolioProjectView from '../components/PortfolioProjectView';
import AppPagination from '../components/AppPagination';

const PortfolioProject = () => (
    <AuthenticatedUserLayout>
        <NSDataPortfolio>
            <AppPagination>
                <NSDataPortfolioProjectAssets>
                    <PortfolioProjectView />
                </NSDataPortfolioProjectAssets>
            </AppPagination>
        </NSDataPortfolio>
    </AuthenticatedUserLayout>
);

export default PortfolioProject;
