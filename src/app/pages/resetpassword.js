import * as React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import ForgotPasswordForm from '../components/ForgotPasswordForm';
import NewPasswordForm from '../components/NewPasswordForm';
import indexStyle from '../styles/index.scss';
import Head from '../components/head';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import LoginInfoBox from '../components/login/LoginInfoBox';

const Loging = () => {
    const router = useRouter();
    const [action, setAction] = useState('');
    const [token, setToken] = useState('');

    useEffect(() => {
        if (router.query.token) {
            setAction('new');
            setToken(router.query.token);
        } else {
            setAction('forgot');
        }
    }, [router]);

    return (
        <React.Fragment>
            <Head
                description="An Interactive Visualization Platform"
                mainstyle={{ __html: indexStyle }}
            />
            <div className="fullScreenContainer">
                <div className="container">
                    <Row>
                        <Col xs="6" lg="4" xl="4" className="login">
                            <Card>
                                <Card.Header>
                                    <div className="orgLogo" />
                                </Card.Header>
                                <Card.Body>
                                    {action === 'forgot' && (
                                        <div>
                                            <h3>Forgotten Password</h3>
                                            <ForgotPasswordForm />
                                        </div>
                                    )}
                                    {action === 'new' && (
                                        <div>
                                            <h3>New Password </h3>
                                            <NewPasswordForm token={token} />
                                        </div>
                                    )}
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs="6" lg="4" className="optionsArea" />
                        <Col xs="6" lg="4" className="help">
                            <LoginInfoBox></LoginInfoBox>
                        </Col>
                    </Row>
                </div>
            </div>

            <style jsx>
                {`
                    .nslogosmall {
                        display: block;
                        width: 157px;
                        height: 30px;
                        background-size: 157px 30px;
                        background-position: center;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/img/ns-logo-small@2x.png);
                        padding-bottom: 12px;
                    }
                    .buildinfo {
                        display: flex;
                    }
                    .orgLogo {
                        width: 100%;
                        height: 56px;
                        background-size: contain;
                        background-position: center;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/img/ns-logo-reg@2x.png);
                        margin-top: 10px;
                    }
                    .orgSecondary {
                        display: block;
                        margin-left: 30px;
                        width: 120px;
                        height: 45px;
                        background-size: 120px;
                        background-position: center;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/img/Throughline_Wordmark.png);
                    }
                    .fullScreenContainer {
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        width: 100%;
                        height: 100%;
                        background-color: #002445;
                        background-position: center;
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-image: url(/appfiles/img/Climber.jpg);
                    }
                `}
            </style>
        </React.Fragment>
    );
};

export default Loging;
