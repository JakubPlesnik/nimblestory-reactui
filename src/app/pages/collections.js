import AuthenticatedUserLayout from '../components/layout/AuthenticatedUserLayout';
import NSDataPortfolio from '../components/NSDataPortfolio';
import CollectionsView from '../components/Collections/CollectionsView';

const PortfolioCollections = () => (
    <AuthenticatedUserLayout>
        <NSDataPortfolio>
            <CollectionsView />
        </NSDataPortfolio>
    </AuthenticatedUserLayout>
);

export default PortfolioCollections;
