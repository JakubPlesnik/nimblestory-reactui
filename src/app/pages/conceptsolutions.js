import SidebarLayout from '../components/SidebarLayout';
import AppRequireLogin, {
    kAppReqOrganization,
} from '../components/AppRequireLogin';
import Theme from '../components/Theme';
import NSDataConceptExplorer from '../components/NSDataConceptExplorer';
import ConceptExplorerSolutionsView from '../components/ConceptExplorerSolutionsView';

const Concepts = () => (
    <AppRequireLogin require={[kAppReqOrganization]}>
        <Theme>
            <SidebarLayout>
                <NSDataConceptExplorer>
                    <ConceptExplorerSolutionsView />
                </NSDataConceptExplorer>
            </SidebarLayout>
        </Theme>
    </AppRequireLogin>
);

export default Concepts;
