import React from 'react';
import App from 'next/app';
import getConfig from 'next/config';
import Router from 'next/router';
import axios from 'axios';
import UserContext from '../context/UserContext';

axios.defaults.baseURL = process.env.NEXT_PUBLIC_API_URL;

const { publicRuntimeConfig } = getConfig();

const defaultPath = {
    user: null,
    organization: null,
    application: null,
    project: null,
    asset: null,
    collection: null,
};

const defaultThemeColors = {
    color1Darkest: '#151f29',
    color2Medium: 'green',
    color3Light: '#cecece',
    colorHomeHeaderText: '#000000',
};

const defaultState = {
    appbuildid: publicRuntimeConfig.APPBUILDIDX || 'LocalDev',
    appenv: publicRuntimeConfig.ENV || 'LocalDev',
    pathinfo: defaultPath,
    themeColors: defaultThemeColors,
    loginstate: {},
    orgcache: {},
    navigationData: [],
    selectedNavigationItem: null,
    navigationCollapsed: true,
};

export default class MainApp extends App {
    constructor(props) {
        super(props);
        this.state = defaultState;
    }

    setThemeColors = themeColors => {
        this.setState({ themeColors });
    };

    setCurrentProject = currentProject => {
        this.setState({ currentProject });
    };

    setNavigationCollapsed = navigationCollapsed => {
        this.setState({ navigationCollapsed });
    };

    setNavigationData = navigationData => {
        this.setState({ navigationData });
    };

    setSelectedNavigationItem = selectedNavigationItem => {
        this.setState({ selectedNavigationItem });
    };

    setAppState = newstate => this.setState(newstate);

    static async getInitialProps({ Component, ctx }) {
        let pageProps = {};
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }
        return { pageProps };
    }

    componentDidMount = () => {
        const {
            application,
            organization,
            project,
            asset,
            journey,
            collection,
        } = Router.query;

        this.setState({
            pathinfo: {
                application,
                organization,
                project,
                asset,
                journey,
                collection,
            },
            pageReady: true,
        });

        if (application === 'login') {
            Router.push('/login');
        } else if (application === 'changepassword') {
            Router.push('/changepassword');
        } else if (application === 'healthcheck') {
            Router.push('/healthcheck');
        } else if (application === 'resetpassword') {
            if (journey) {
                Router.push({
                    pathname: '/resetpassword',
                    query: { token: journey },
                });
            } else Router.push('/resetpassword');
        } else if (application == 'home' && !organization && !project) {
            // Router.push('/loggedin')
            window.location = '/loggedin';
        } else if (!organization) {
            // Router.push('/organizations')
            if (!Router.router.pathname.includes('organization')) {
                window.location = '/organization';
            }
        }

        Router.events.on('routeChangeStart', () => {
            this.setState({
                pageReady: false,
            });
        });

        // const _this = this
        Router.events.on('routeChangeComplete', () => {
            const {
                application,
                organization,
                project,
                asset,
                journey,
                collection,
            } = Router.query;

            let addstateoptions = { pageReady: true };
            if (
                this.state.loginstate.organization &&
                organization != this.state.loginstate.organization.slug
            ) {
                // Handle switch org, clean old org data
                addstateoptions = {
                    ...addstateoptions,
                    orgcache: {},
                    loginstate: {
                        ...this.state.loginstate,
                        organization: false,
                    },
                };
            }
            this.setState({
                pathinfo: {
                    application,
                    organization,
                    project,
                    asset,
                    journey,
                    collection,
                },
                ...addstateoptions,
            });
        });
    };

    render() {
        const { Component, pageProps } = this.props;

        return (
            <UserContext.Provider
                value={{
                    appbuildid: this.state.appbuildid,
                    appenv: this.state.appenv,

                    pathinfo: this.state.pathinfo,
                    setPathInfo: this.setPathInfo,

                    pageReady: this.state.pageReady,
                    themeColors: this.state.themeColors,
                    setThemeColors: this.setThemeColors,

                    appState: this.state,
                    setAppState: this.setAppState,

                    setCurrentProjectGlobal: this.setCurrentProject,
                    currentProject: this.state.currentlProject,

                    setNavigationData: this.setNavigationData,
                    navigationData: this.state.navigationData,

                    selectedNavigationItem: this.state.selectedNavigationItem,
                    setSelectedNavigationItem: this.setSelectedNavigationItem,

                    navigationCollapsed: this.state.navigationCollapsed,
                    setNavigationCollapsed: this.setNavigationCollapsed,
                }}
            >
                <Component {...pageProps} />
            </UserContext.Provider>
        );
    }
}
