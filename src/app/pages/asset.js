import AuthenticatedUserLayout from '../components/layout/AuthenticatedUserLayout';
import NSDataPortfolioDetail from '../components/NSDataPortfolioDetail';
import PortfolioAssetView from '../components/PortfolioAssetView';

const Asset = () => (
    <AuthenticatedUserLayout>
        <NSDataPortfolioDetail>
            <PortfolioAssetView />
        </NSDataPortfolioDetail>
    </AuthenticatedUserLayout>
);
export default Asset;
