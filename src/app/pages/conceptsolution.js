import NSDataConceptExplorer from '../components/NSDataConceptExplorer';
import ConceptExplorerSolutionView from '../components/ConceptExplorerSolutionView';
import AuthenticatedUserLayout from '../components/layout/AuthenticatedUserLayout';

const Concepts = () => (
    <AuthenticatedUserLayout>
        <NSDataConceptExplorer>
            <ConceptExplorerSolutionView />
        </NSDataConceptExplorer>
    </AuthenticatedUserLayout>
);

export default Concepts;
