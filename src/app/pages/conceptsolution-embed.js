import { useState } from 'react';
import AppRequireLogin, { kAppReqTheme } from '../components/AppRequireLogin';
import Theme from '../components/Theme';
import NSDataConceptExplorer from '../components/NSDataConceptExplorer';
import ConceptExplorerSolutionView from '../components/ConceptExplorerSolutionView';

const Concepts = () => {
    const [data, setData] = useState(null);

    return (
        <AppRequireLogin require={[kAppReqTheme]} setData={setData}>
            <Theme>
                <NSDataConceptExplorer data={data}>
                    <ConceptExplorerSolutionView isEmbedded={true} />
                </NSDataConceptExplorer>
            </Theme>
        </AppRequireLogin>
    );
};

export default Concepts;
