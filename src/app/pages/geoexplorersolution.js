import SidebarLayout from '../components/SidebarLayout';
import AppRequireLogin, {
    kAppReqOrganization,
} from '../components/AppRequireLogin';
import NSDataGeoExplorer from '../components/NSDataGeoExplorer';
import GeoExplorerSolutionView from '../components/GeoExplorerSolutionView';
import Theme from '../components/Theme';

const GeoExplorerSolutions = () => (
    <AppRequireLogin require={[kAppReqOrganization]}>
        <Theme>
            <SidebarLayout>
                <NSDataGeoExplorer>
                    <GeoExplorerSolutionView />
                </NSDataGeoExplorer>
            </SidebarLayout>
        </Theme>
    </AppRequireLogin>
);

export default GeoExplorerSolutions;
