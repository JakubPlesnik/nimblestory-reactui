import SidebarLayout from '../components/SidebarLayout';
import AppRequireLogin, {
    kAppReqOrganization,
} from '../components/AppRequireLogin';
import NSDataGeoExplorer from '../components/NSDataGeoExplorer';
import GeoExplorerSolutionsView from '../components/GeoExplorerSolutionsView';
import Theme from '../components/Theme';

const GeoExplorerSolutions = () => (
    <AppRequireLogin require={[kAppReqOrganization]}>
        <Theme>
            <SidebarLayout>
                <NSDataGeoExplorer>
                    <GeoExplorerSolutionsView />
                </NSDataGeoExplorer>
            </SidebarLayout>
        </Theme>
    </AppRequireLogin>
);

export default GeoExplorerSolutions;
