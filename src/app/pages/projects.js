import AuthenticatedUserLayout from '../components/layout/AuthenticatedUserLayout';
import NSDataPortfolio from '../components/NSDataPortfolio';
import PortfolioProjectsView from '../components/PortfolioProjectsView';
import AppPagination from '../components/AppPagination';

const PortfolioProjects = () => (
    <AuthenticatedUserLayout>
        <AppPagination>
            <NSDataPortfolio>
                <PortfolioProjectsView />
            </NSDataPortfolio>
        </AppPagination>
    </AuthenticatedUserLayout>
);

export default PortfolioProjects;
