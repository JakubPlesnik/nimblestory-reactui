import NSDataPortfolio from '../components/NSDataPortfolio';
import NSDataGeoExplorer from '../components/NSDataGeoExplorer';
import NSDataConceptExplorer from '../components/NSDataConceptExplorer';
import HomepageView from '../components/HomepageView';
import AuthenticatedUserLayout from '../components/layout/AuthenticatedUserLayout';

const Asset = () => (
    <AuthenticatedUserLayout>
        <NSDataPortfolio>
            <NSDataGeoExplorer>
                <NSDataConceptExplorer>
                    <HomepageView />
                </NSDataConceptExplorer>
            </NSDataGeoExplorer>
        </NSDataPortfolio>
    </AuthenticatedUserLayout>
);
export default Asset;
