import AuthenticatedUserLayout from '../components/layout/AuthenticatedUserLayout';
import NSDataPortfolio from '../components/NSDataPortfolio';
import NSDataPortfolioProjectAssets from '../components/NSDataPortfolioProjectAssets';
import CollectionDetailView from '../components/Collections/CollectionDetailView';
import AppPagination from '../components/AppPagination';

const PortfolioCollection = () => (
    <AuthenticatedUserLayout>
        <NSDataPortfolio>
            <AppPagination>
                <NSDataPortfolioProjectAssets>
                    <CollectionDetailView />
                </NSDataPortfolioProjectAssets>
            </AppPagination>
        </NSDataPortfolio>
    </AuthenticatedUserLayout>
);

export default PortfolioCollection;
