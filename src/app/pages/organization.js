import Router from 'next/router';
import { useState, useContext, useEffect } from 'react';
import SidebarLayout from '../components/SidebarLayout';
import AppRequireLogin, {
    kAppReqOrganization,
} from '../components/AppRequireLogin';
import Theme from '../components/Theme';
import ModalOrganizations from '../components/SidebarModalOrganizations';
import LoginContext from '../context/LoginContext';
import { saveDefault } from '../services/api/apiService';

const ORGModal = () => {
    const { organizations } = useContext(LoginContext);
    const [isOrgModalOpen, setOrgModalOpen] = useState(false);

    useEffect(() => {
        if (organizations) {
            let slug = '';
            organizations.forEach(element => {
                if (element['default']) {
                    slug = element['slug'];
                }
            });

            if (slug !== '') {
                Router.push(
                    `/home?application=home&organization=${slug}`,
                    `/portfolio/${slug}/`
                );
            } else {
                if (organizations.length == 1) {
                    saveDefault(organizations[0].slug);
                    Router.push(
                        `/home?application=home&organization=${organizations[0].slug}`,
                        `/portfolio/${organizations[0].slug}/`
                    );
                } else {
                    setOrgModalOpen(true);
                }
            }
        }
    }, [organizations]);

    return (
        <div>
            {organizations.length > 1 && (
                <ModalOrganizations
                    isOutSideNotClosable
                    isOpen={isOrgModalOpen}
                    setOpen={setOrgModalOpen}
                    organizations={organizations}
                    onChangeOrg={idx => {
                        Router.push(
                            `/home?application=home&organization=${organizations[idx].slug}`,
                            `/portfolio/${organizations[idx].slug}/`
                        );
                    }}
                />
            )}
        </div>
    );
};

const OrgsPage = () => (
    <AppRequireLogin require={[kAppReqOrganization]}>
        <Theme>
            <SidebarLayout>
                <ORGModal />
            </SidebarLayout>
        </Theme>
    </AppRequireLogin>
);
export default OrgsPage;
