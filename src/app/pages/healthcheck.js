import React, { useEffect, useState } from 'react';
import { fetchHealthCheck } from '../services/api/apiService';

const Healthcheck = () => {
    const [apiStatus, setApiStatus] = useState('pending');
    const [apiHealthCheck, setApiHealthCheck] = useState(null);

    useEffect(async () => {
        const result = await fetchHealthCheck();

        if (result) {
            setApiStatus('OK');
            setApiHealthCheck(result);
        } else {
            setApiStatus('ERROR');
        }
    }, []);

    return (
        <div>
            <h2>UI Healthcheck</h2>
            <ul>
                <li>
                    <span>NODE_ENV:</span> {process.env.NODE_ENV}
                </li>
                <li>
                    <span>API_URL:</span> {process.env.NEXT_PUBLIC_BACKEND_URL}
                </li>
                <li>
                    <span>APP_VERSION:</span>{' '}
                    {process.env.NEXT_PUBLIC_APP_VERSION
                        ? process.env.NEXT_PUBLIC_APP_VERSION
                        : 'NaN'}
                </li>
            </ul>
            <h2>API Healthcheck</h2>
            <ul>
                <li>
                    <span>API_STATUS:</span> {apiStatus}
                </li>
                {apiHealthCheck && (
                    <div>
                        <li>
                            <span>APP_DEBUG:</span>{' '}
                            {apiHealthCheck['APP_DEBUG']}
                        </li>
                        <li>
                            <span>APP_ENV:</span> {apiHealthCheck['APP_ENV']}
                        </li>
                        <li>
                            <span>DB-status:</span>{' '}
                            {apiHealthCheck['DB-status']}
                        </li>
                        <li>
                            <span>PHP-status:</span>{' '}
                            {apiHealthCheck['PHP-status']}
                        </li>
                        <li>
                            <span>REDIS-status:</span>{' '}
                            {apiHealthCheck['REDIS-status']}
                        </li>
                    </div>
                )}
            </ul>
        </div>
    );
};

export default Healthcheck;
