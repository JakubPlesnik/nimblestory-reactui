import SidebarLayout from '../components/SidebarLayout';
import AppRequireLogin, {
    kAppReqOrganization,
    kAppReqAdmin,
} from '../components/AppRequireLogin';
import NSDataPortfolio from '../components/NSDataPortfolio';
import NSDataGeoExplorer from '../components/NSDataGeoExplorer';
import NSDataPortfolioProjectAssets from '../components/NSDataPortfolioProjectAssets';
import NSManagerData from '../components/NSManagerData';
import ManagerViewPortfolio from '../components/ManagerViewPortfolio';
import AppPagination from '../components/AppPagination';
import Theme from '../components/Theme';
import manageStyle from '../styles/manage.scss';

const Asset = () => (
    <AppRequireLogin require={[kAppReqOrganization, kAppReqAdmin]}>
        <Theme insertedStyles={[manageStyle]}>
            <SidebarLayout>
                <NSDataPortfolio>
                    <NSDataGeoExplorer>
                        <AppPagination>
                            <NSDataPortfolioProjectAssets>
                                <NSManagerData>
                                    <ManagerViewPortfolio />
                                </NSManagerData>
                            </NSDataPortfolioProjectAssets>
                        </AppPagination>
                    </NSDataGeoExplorer>
                </NSDataPortfolio>
            </SidebarLayout>
        </Theme>
    </AppRequireLogin>
);
export default Asset;
