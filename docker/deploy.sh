echo "deploy.sh: Begin..."
echo $HOSTNAME

en_dockername=${1:-docker_node_1}
en_apiurl=${2:-https://bad.deploy.run/}

echo "deploy.sh: Destroy Old Container"
sudo docker-compose down --remove-orphans

echo "deploy.sh: - Setting local file rights"
sudo chown -R 1000 ../

echo "deploy.sh: - Setting docker container file rights"
sudo docker exec pimcore-php chown -R 1000 .
sudo docker exec pimcore-php chown -R www-data:www-data /var/www/*

echo "deploy.sh: Fix environment file."
echo 'sed -i "s|zzzPLACEHOLDERzzz|${en_apiurl}|" ../src/app/.env.production'
sed -i "s|zzzPLACEHOLDERzzz|${en_apiurl}|" ../src/app/.env.production
cat ../src/app/.env.production

echo "deploy.sh: Build New Containers"
sudo docker-compose up -d

echo "deploy.sh: NPM Install"
sudo docker exec frontend-react npm install

echo "deploy.sh: NPM Rebuild node-sass"
sudo docker exec frontend-react npm rebuild node-sass

echo "deploy.sh: NPM Rebuild node-sass"
sudo docker exec frontend-react npm run build

echo "deploy.sh: NPM run start-forever"
sudo docker exec frontend-react npm run start-forever

echo "deploy.sh: ...End"